{% set version =  salt['pillar.get']('version', "1.0.0") %}
{% set package_name = 'LogRecordServer' %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/desktop/logserver/{0}".format(package_name) %}

upgrade_LogRecordServer:
  salt.state:
    - tgt: 'LogRecordServer.test.desktop.yewu.js'
    - sls:
      - LogRecordServer.deploy
    - pillar:
        LogRecordServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
