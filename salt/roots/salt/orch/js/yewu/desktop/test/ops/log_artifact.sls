log_artifact:
  salt.state:
    - tgt: 'PolicyDataServer.test.desktop.yewu.js'
    - sls:
      - redis.log_artifact
      - nginx.log_artifact
      - PolicyDataServer.web.log_artifact
      - PolicyDataServer.celerybeat.log_artifact
      - PolicyDataServer.celeryd.log_artifact
