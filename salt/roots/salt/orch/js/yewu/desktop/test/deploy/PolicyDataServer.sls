{% set version =  salt['pillar.get']('version', "1.0.0") %}
{% set package_name = 'PolicyDataServer' %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/desktop/policy/{0}".format(package_name) %}

deploy_celerybeat:
  salt.state:
    - tgt: 'PolicyDataServer.test.desktop.yewu.js'
    - sls: PolicyDataServer.celerybeat.deploy
    - pillar:
        PolicyDataServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_celery:
  salt.state:
    - tgt: 'PolicyDataServer.test.desktop.yewu.js'
    - sls: PolicyDataServer.celeryd.deploy
    - pillar:
        PolicyDataServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: deploy_celerybeat

deploy_web:
  salt.state:
    - tgt: 'PolicyDataServer.test.desktop.yewu.js'
    - sls:
      - PolicyDataServer.web.deploy
    - pillar:
        PolicyDataServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: deploy_celery

deploy_nginx_server:
  salt.state:
    - tgt: 'PolicyDataServer.test.desktop.yewu.js'
    - sls: PolicyDataServer.nginx.enable
    - require:
      - salt: deploy_web
