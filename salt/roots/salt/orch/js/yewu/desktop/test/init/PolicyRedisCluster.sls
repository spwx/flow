deploy_redis_master:
  salt.state:
    - tgt: 'PolicyDataServer.test.desktop.yewu.js'
    - sls:
      - base
      - redis
      - redis.sys_config

deploy_redis_slave:
  salt.state:
    - tgt: 'openresty.test.desktop.yewu.js'
    - sls:
      - base
      - redis
      - redis.sys_config
    - require:
      - salt: deploy_redis_master