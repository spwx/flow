{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/PolicyDataServer/VERSION') %}
{% set package_name = 'PolicyDataServer' %}

# True/False
{% set test = salt['pillar.get']('test', 'True') %}

deploy_celerybeat:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls: PolicyDataServer.celerybeat.deploy
    - test: {{ test }}
    - pillar:
        PolicyDataServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'

deploy_celery:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls: PolicyDataServer.celeryd.deploy
    - test: {{ test }}
    - pillar:
        PolicyDataServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
    - require:
      - salt: deploy_celerybeat

deploy_web:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls:
      - PolicyDataServer.web.deploy
    - test: {{ test }}
    - pillar:
        PolicyDataServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
    - require:
      - salt: deploy_celery

deploy_nginx_server:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls: PolicyDataServer.nginx.enable
    - test: {{ test }}
    - require:
      - salt: deploy_web
