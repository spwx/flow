deploy_python_env:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls:
      - python3

deploy_celerybeat:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls:
      - PolicyDataServer.celerybeat
    - require:
      - salt: deploy_python_env

deploy_celery:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls:
      - PolicyDataServer.celeryd
    - require:
      - salt: deploy_python_env

deploy_web:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls:
      - PolicyDataServer.web
      - sys_config.sysctl_conf
    - require:
      - salt: deploy_python_env

deploy_nginx_server:
  salt.state:
    - tgt: 'PolicyDataServer.prod.desktop.yewu.js'
    - sls:
      - nginx
      - sys_config.sysctl_conf