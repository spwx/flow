{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/deskplatform/VERSION') %}
{% set package_name = 'deskplatform' %}
{% set test = salt['pillar.get']('test', 'True') %}


deploy_web:
  salt.state:
    - tgt: 'operation.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopOperation.web.deploy
    - pillar:
        WxDesktopOperation_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
          test: 'online'
