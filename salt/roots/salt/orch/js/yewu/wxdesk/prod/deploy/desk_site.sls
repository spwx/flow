{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/desk-site/VERSION') %}
{% set package_name = 'desk-site' %}
{% set test = salt['pillar.get']('test', 'true') %}

deploy_desk_site:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - sls: WxDeskSite.frontend.deploy
    - test: {{ test }}
    - pillar:
        WxDeskSite_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'

deploy_nginx_server:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - sls: WxDeskSite.nginx.web.frontend
    - test: {{ test }}
    - require:
      - salt: deploy_desk_site
