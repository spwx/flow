{% set test = salt['pillar.get']('test', 'True') %}

deploy_WxDeskManagement_web:
  salt.state:
    - tgt: 'WxVoiceMaster_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - python3
      - mongo
      - mongos.enable_auth
      - webagent.deploy
      - WxDeskManagement.web
