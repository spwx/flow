{% set test = salt['pillar.get']('test', 'True') %}

init_wximage_server:
  salt.state:
    - tgt: 'WxImageServer.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - nginx
      - WxImageServer.server
      - WxImageServer.nginx.server
      - WxImageServer.web

deploy_wximage_client:
  salt.state:
    - tgt: 'WxVoiceMaster_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxImageServer.client

deploy_wximage_backend_nginx:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.nginx.backend

deploy_wximage_frontend_nginx:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.nginx.frontend
