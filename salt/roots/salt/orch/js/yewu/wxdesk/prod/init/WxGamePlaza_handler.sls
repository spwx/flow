{% set node_pcre = salt['pillar.get']('node_pcre', 'gameplaza_(handler(06|07|08|09)|web(13|14))') %}

# True/False
{% set test = salt['pillar.get']('test', 'True') %}

init_wxgameplaza_handlers:
  salt.state:
    - tgt: '{{ node_pcre }}.prod.gameplaza.yewu.js'
    - tgt_type: pcre
    - sls:
      - WxGamePlaza.handler
