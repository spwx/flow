{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/accountsync/VERSION') %}
{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'accountsync' %}
{% set test = salt['pillar.get']('test', 'True') %}


deploy_WxPolicyServer_accountsync:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.accountsync.deploy
    - pillar:
        accountsync_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
