{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/GamePlazaHandler/VERSION') %}
{% set node_pcre = salt['pillar.get']('node_pcre', '___') %}

# True/False
{% set test = salt['pillar.get']('test', 'True') %}

# --// constant variable
{% set PACKAGE_NAME = 'GamePlazaHandler' %}

upgrade_gameplaza_handler:
  salt.state:
    - tgt: '{{ node_pcre }}.prod.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_handler_pkg:
          package_name: {{ PACKAGE_NAME }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz.MD5'
