{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.web

init_celerybeat:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.celerybeat

init_celeryd:
  salt.state:
    - tgt: 'policy_worker0(1|2).{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxPolicyServer.celeryd

init_policyinit:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.policyinit

init_scheduler:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.scheduler

init_logmgr:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.logmgr