{% set test = salt['pillar.get']('test', 'True') %}

init_LwMarket_web:
  salt.state:
    - tgt: 'WxDesktopAccount_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxDesktopAccount.web
