{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/policymanager/VERSION') %}
{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'policymanager' %}
{% set test = salt['pillar.get']('test', 'True') %}

deploy_WxPolicyServer_web:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.web.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_celerybeat:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.celerybeat.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_celeryd:
  salt.state:
    - tgt: 'policy_worker0(1|2).{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxPolicyServer.celeryd.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_policyinit:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.policyinit.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_scheduler:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.scheduler.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_logmgr:
  salt.state:
    - tgt: 'policy_redis.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.logmgr.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
