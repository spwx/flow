{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/GamePlaza/VERSION') %}
{% set PACKAGE_NAME = 'GamePlaza' %}

# True/False
{% set test = salt['pillar.get']('test', 'True') %}

deploy_celerybeat:
  salt.state:
    - tgt: 'gameplaza_web01.prod.gameplaza.yewu.js'
    - test: {{ test  }}
    - sls:
      - WxGamePlaza.celerybeat.deploy
    - pillar:
        wxgameplaza_web_pkg:
          package_name: {{ PACKAGE_NAME }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz.MD5'

deploy_celery:
  salt.state:
    - tgt: 'gameplaza_web(01|02).prod.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test  }}
    - sls:
      - WxGamePlaza.celeryd.deploy
    - pillar:
        wxgameplaza_web_pkg:
          package_name: {{ PACKAGE_NAME }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz.MD5'
