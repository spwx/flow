{% set version = salt['pillar.get']('version', '4625.36') %}
{% set package_name = 'HttpAgent' %}
{% set test = salt['pillar.get']('test', 'True') %}

init_httpagent_blue_server:
  salt.state:
    - tgt: 'handler.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue

upgrade_httpagent_blue_version:
  salt.state:
    - tgt: 'handler.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue
        wxgameplaza_httpagent_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: init_httpagent_blue_server

init_httpagent_green_server:
  salt.state:
    - tgt: 'handler.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_green

upgrade_httpagent_green_version:
  salt.state:
    - tgt: 'handler.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_green
        wxgameplaza_httpagent_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: init_httpagent_green_server

install_nginx:
  salt.state:
    - tgt: 'handler.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - nginx

config_nginx:
  salt.state:
    - tgt: 'handler.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.nginx.handler.enable
    - pillar:
        wxgameplaza_nginx_httpagent_active_upstream: all
