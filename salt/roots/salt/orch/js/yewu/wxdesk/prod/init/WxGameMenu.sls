# True/False
{% set test = salt['pillar.get']('test', 'True') %}

init_celerybeat:
  salt.state:
    - tgt: 'WxVoiceMaster_web01.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celerybeat

init_celeryd:
  salt.state:
    - tgt: 'WxVoiceMaster_web01.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celeryd
