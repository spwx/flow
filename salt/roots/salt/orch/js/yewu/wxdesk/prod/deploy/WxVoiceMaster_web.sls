{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/VoiceMaster/VERSION') %}

{% set package_name = 'VoiceMaster' %}
{% set test = salt['pillar.get']('test', 'True') %}

deploy_WxVoiceMaster_web:
  salt.state:
    - tgt: 'WxVoiceMaster_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.web.deploy
    - pillar:
        WxVoiceMaster_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxVoiceMaster_celerybeat:
  salt.state:
    - tgt: 'WxVoiceMaster_web01.prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.celerybeat.deploy
    - pillar:
        WxVoiceMaster_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxVoiceMaster_celeryd:
  salt.state:
    - tgt: 'WxVoiceMaster_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.celeryd.deploy
    - pillar:
        WxVoiceMaster_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxVoiceMaster_nginx:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.nginx.backend

deploy_WxVoiceMaster_frontend:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.nginx.frontend
