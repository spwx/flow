
{% set blue_node_pcre = 'gameplaza_web0.+' %}
{% set green_node_pcre = 'gameplaza_web1(1|2|5|6)' %}

{% set test = salt['pillar.get']('test', 'True') %}
{% set node_pcre = salt['pillar.get']('node_pcre', '___') %}

init_WxGamePlaza_web:
  salt.state:
    - tgt: '{{ node_pcre }}.prod.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxGamePlaza.web

init_WxGamePlaza_celerybeat:
  salt.state:
    - tgt: 'gameplaza_web01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celerybeat

init_WxGamePlaza_celeryd:
  salt.state:
    - tgt: 'gameplaza_web(01|02).prod.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celeryd
