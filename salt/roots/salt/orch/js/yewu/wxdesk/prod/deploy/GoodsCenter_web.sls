{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'goodscenter' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/goodscenter/VERSION') %}

deploy_GoodsCenter_web:
  salt.state:
    - tgt: 'LwMarket_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - GoodsCenter.web.deploy
    - pillar:
        goodscenter_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_backend:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - GoodsCenter.nginx.backend
    - pillar:
        goodscenter_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'


deploy_frontend:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - GoodsCenter.frontend.deploy
      - GoodsCenter.nginx.frontend
    - pillar:
        goodscenter_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
