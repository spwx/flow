{% set test = salt['pillar.get']('test', 'True') %}
# redis cluster02
stage_activity_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: stage_activity_redis

reward_activity_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: reward_activity_redis

head_activity_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: head_activity_redis

damage_activity_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: damage_activity_redis

continuous_victory_activity_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: continuous_victory_activity_redis

score_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: score_redis

statistic_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: statistic_redis

bar_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: bar_redis

ranker_player_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: ranker_player_redis

reduce_stress_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: reduce_stress_redis

ad_announcement_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: ad_announcement_redis

lock_key_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: lock_key_redis

pay_notice_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: pay_notice_redis

bar_player_uploadtime_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: bar_player_uploadtime_redis

daily_task_redis:
  salt.state:
    - tgt: 'redis_cluster02.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: daily_task_redis