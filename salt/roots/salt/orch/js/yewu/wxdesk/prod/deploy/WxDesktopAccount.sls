{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'desktop_account' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/desktop_account/VERSION') %}

deploy_WxDesktopAccount_web:
  salt.state:
    - tgt: 'WxDesktopAccount_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxDesktopAccount.web.deploy
    - pillar:
        wxdesktop_account_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_backend:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopAccount.nginx.backend
    - pillar:
        wxdesktop_account_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'


deploy_frontend:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopAccount.frontend.deploy
      - WxDesktopAccount.nginx.frontend
    - pillar:
        wxdesktop_account_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
