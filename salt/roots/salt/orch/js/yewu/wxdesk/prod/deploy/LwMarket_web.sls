{% set env_id = salt['pillar.get']('env_id', '') %}

{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/lightsupermarket/VERSION') %}

{% set package_name = 'lightsupermarket' %}

{% set test = salt['pillar.get']('test', 'True') %}

deploy_LwMarket_web:
  salt.state:
    - tgt: 'LwMarket_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - LwMarket.web.deploy
    - pillar:
        LwMarket_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_LwMarket_celeryd:
  salt.state:
    - tgt: 'LwMarket_web01.prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - LwMarket.celeryd.deploy
    - pillar:
        LwMarket_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_LwMarket_backend:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - LwMarket.frontend.deploy
      - LwMarket.nginx.backend
    - pillar:
        LwMarket_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_LwMarket_frontend:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - LwMarket.frontend.deploy
      - LwMarket.nginx.frontend
    - pillar:
        LwMarket_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'