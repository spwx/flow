# salt-run state.orch arguments.
# salt-run state.orch js.yewu.gameplaza.prod.deploy.handler pillar='{"version":"xxx","env_id":"green","test":"True"}' test=True

{% set version = salt['pillar.get']('version', '4625.36') %}
# blue/green
{% set env_id = salt['pillar.get']('env_id', '___') %}
# True/False
{% set test = salt['pillar.get']('test', 'True') %}

# --// constant variable
{% set PACKAGE_NAME = 'HttpAgent' %}

upgrade_httpagent_{{ env_id }}:
  salt.state:
    - tgt: 'handler.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_{{ env_id }}
        wxgameplaza_httpagent_pkg:
          package_name: {{ PACKAGE_NAME }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz.MD5'

