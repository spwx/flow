{% set env_id = salt['pillar.get']('env_id', '') %}
{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/oss/VERSION') %}
{% set package_name = 'oss' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/oss/{0}".format(package_name) %}

deploy_WxImageServer_web:
  salt.state:
    - tgt: 'WxImageServer.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.web.deploy
    - pillar:
        WxImageServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
