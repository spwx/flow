{% set test = salt['pillar.get']('test', 'True') %}
# redis cluster01
accountcenter_celery_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: accountcenter_celery_redis

ac_account_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: ac_account_redis

voice_celery_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: voice_celery_redis

voice_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: voice_redis

gameplaza_celery_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: gameplaza_celery_redis

cache_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: cache_redis


lwsm_order_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: lwsm_order_redis

wx_policy_celery_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: wx_policy_celery_redis

policy_redis:
  salt.state:
    - tgt: 'policy_redis.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: policy_redis


wx_token_redis:
  salt.state:
    - tgt: 'redis_cluster01.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - redis
    - pillar:
        redis_env_id: wx_token_redis
