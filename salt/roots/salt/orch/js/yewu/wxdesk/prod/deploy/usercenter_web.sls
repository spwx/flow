{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'usercenter' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/usercenter/VERSION') %}

deploy_usercenter_web:
  salt.state:
    - tgt: 'user_center(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - usercenter.web.deploy
    - pillar:
        usercenter_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

