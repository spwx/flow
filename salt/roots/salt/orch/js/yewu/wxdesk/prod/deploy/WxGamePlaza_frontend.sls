{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/gameplaza-frontend/VERSION') %}
{% set package_name = 'gameplaza-frontend' %}
{% set test = salt['pillar.get']('test', 'True') %}

deploy_WxGamePlaza_frontend_frontendNginx:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.frontend.deploy
      - WxGamePlaza.nginx.web.frontend
    - pillar:
        wxgameplaza_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxGamePlaza_frontend_backendNginx:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.frontend.deploy
    - pillar:
        wxgameplaza_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
