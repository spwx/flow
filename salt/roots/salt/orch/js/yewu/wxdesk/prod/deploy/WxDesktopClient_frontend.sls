{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/desktopclient-frontend/VERSION') %}
{% set package_name = 'desktopclient-frontend' %}

{% set test = salt['pillar.get']('test', True) %}

deploy_WxDesktopClient_frontend_frontendNginx:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopClient.frontend.deploy
      - WxDesktopClient.nginx.web.frontend
    - pillar:
        WxDesktopClient_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxDesktopClient_frontend_backendNginx:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopClient.frontend.deploy
    - pillar:
        WxDesktopClient_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

