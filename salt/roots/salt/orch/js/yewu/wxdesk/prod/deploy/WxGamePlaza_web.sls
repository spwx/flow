{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/GamePlaza/VERSION') %}
{% set PACKAGE_NAME = 'GamePlaza' %}

{% set node_pcre = salt['pillar.get']('node_pcre', '___') %}

# True/False
{% set test = salt['pillar.get']('test', 'True') %}

upgrade_wxgameplaza_web:
  salt.state:
    - tgt: '{{ node_pcre }}.prod.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxGamePlaza.web.deploy
    - pillar:
        wxgameplaza_web_pkg:
          package_name: {{ PACKAGE_NAME }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz.MD5'

