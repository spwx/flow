{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'statscenter' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/statscenter/VERSION') %}

deploy_statscenter_web:
  salt.state:
    - tgt: 'WxDesktopAccount_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - statscenter.web.deploy
    - pillar:
        statscenter_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'


deploy_statscenter_celerybeat:
  salt.state:
    - tgt: 'WxDesktopAccount_web01.prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - statscenter.celerybeat.deploy
    - pillar:
        statscenter_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
