{% set test = salt['pillar.get']('test', 'True') %}

init_LwMarket_web:
  salt.state:
    - tgt: 'LwMarket_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - LwMarket.web

init_LwMarket_celeryd:
  salt.state:
    - tgt: 'LwMarket_web01.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - LwMarket.celeryd

Deploy_LwMarket_Wximage_Client:
  salt.state:
    - tgt: 'LwMarket_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - LwMarket.client
