{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/publicity/VERSION') %}
{% set package_name = 'publicity' %}

{% set test = salt['pillar.get']('test', True) %}

deploy_frontend_frontendNginx:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPublicity.frontend.deploy
      - WxPublicity.nginx.web.frontend
    - pillar:
        WxPublicity_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_frontend_backendNginx:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPublicity.frontend.deploy
    - pillar:
        WxPublicity_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
