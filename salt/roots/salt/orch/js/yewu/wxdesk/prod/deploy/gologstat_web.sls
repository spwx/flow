{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'gologstat' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/gologstat/VERSION') %}

deploy_gologstat_web:
  salt.state:
    - tgt: 'operation.prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - gologstat.web.deploy
    - pillar:
        gologstat_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'


deploy_gologstat_celerybeat:
  salt.state:
    - tgt: 'operation.prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - gologstat.deploy_celery
    - pillar:
        gologstat_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
