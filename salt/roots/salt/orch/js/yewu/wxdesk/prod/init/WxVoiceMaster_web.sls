{% set test = salt['pillar.get']('test', 'True') %}

init_WxVoiceMaster_web:
  salt.state:
    - tgt: 'WxVoiceMaster_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - python3
      - mongo
      - mongos.enable_auth
      - WxVoiceMaster.web
      - WxVoiceMaster.celeryd

init_WxVoiceMaster_celerybeat:
  salt.state:
    - tgt: 'WxVoiceMaster_web01.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.celerybeat
