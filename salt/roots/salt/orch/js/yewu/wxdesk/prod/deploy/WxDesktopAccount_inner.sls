{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'desktop_account' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/desktop_account/VERSION') %}

deploy_WxDesktopAccount_web_inner:
  salt.state:
    - tgt: 'WxDesktopAccount_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxDesktopAccount.web.deploy_inner
    - pillar:
        wxdesktop_account_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_backend_inner:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopAccount.nginx.backend_inner
    - pillar:
        wxdesktop_account_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

