{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'user_center(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - usercenter.web
