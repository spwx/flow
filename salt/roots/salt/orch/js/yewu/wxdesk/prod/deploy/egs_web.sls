{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set package_name = 'wxgameplaza_egs' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/wxgameplaza_egs/VERSION') %}

deploy_egs_web:
  salt.state:
    - tgt: 'egs(01|02|03|04).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - egs.web.deploy
    - pillar:
        egs_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'


deploy_backend:
  salt.state:
    - tgt: 'nginx_backend_164.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - egs.nginx.backend_inner
