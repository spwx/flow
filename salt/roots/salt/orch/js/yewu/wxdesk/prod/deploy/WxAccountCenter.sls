{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/accountcenter/VERSION') %}
{% set package_name = 'accountcenter' %}
{% set test = salt['pillar.get']('test', 'True') %}

deploy_WxAccountCenter_celerybeat:
  salt.state:
    - tgt: 'WxAccountCenter.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxAccountCenter.celerybeat.deploy
    - pillar:
        WxAccountCenter_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxAccountCenter_celeryd:
  salt.state:
    - tgt: 'WxAccountCenter.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxAccountCenter.celeryd.deploy
    - pillar:
        WxAccountCenter_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

