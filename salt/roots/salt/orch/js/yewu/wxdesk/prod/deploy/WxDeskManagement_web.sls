{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/qdd_management/VERSION') %}

{% set package_name = 'qdd_management' %}
{% set test = salt['pillar.get']('test', 'True') %}

deploy_WxDeskManagement_web:
  salt.state:
    - tgt: 'WxVoiceMaster_web(01|02).prod.desktop.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxDeskManagement.web.deploy
    - pillar:
        WxDeskManagement_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxDeskManagement_backend:
  salt.state:
    - tgt: 'nginx_backend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDeskManagement.frontend.deploy
      - WxDeskManagement.nginx.backend
    - pillar:
        WxDeskManagement_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxDeskManagement_frontend:
  salt.state:
    - tgt: 'nginx_frontend.prod.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDeskManagement.frontend.deploy
      - WxDeskManagement.nginx.frontend
    - pillar:
        WxDeskManagement_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ package_name }}-{{ version }}.tar.gz.MD5'
