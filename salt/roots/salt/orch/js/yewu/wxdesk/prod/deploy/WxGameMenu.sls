{% set version = salt['cmd.shell']('cat /data/SicentTools/artifacts/files/gamemenu/VERSION') %}
{% set PACKAGE_NAME = 'gamemenu' %}

# True/False
{% set test = salt['pillar.get']('test', 'True') %}

deploy_WxGameMenu_celerybeat:
  salt.state:
    - tgt: 'WxVoiceMaster_web01.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celerybeat.deploy
    - pillar:
        WxGameMenu_pkg:
          package_name: {{ PACKAGE_NAME }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz.MD5'

deploy_WxGameMenu_celeryd:
  salt.state:
    - tgt: 'WxVoiceMaster_web01.prod.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celeryd.deploy
    - pillar:
        WxGameMenu_pkg:
          package_name: {{ PACKAGE_NAME }}
          version: '{{ version }}'
          pkg_url: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz'
          pkg_checksum: 'salt://files/{{ PACKAGE_NAME }}-{{ version }}.tar.gz.MD5'
