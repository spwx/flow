deploy_wx_redis:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - sls:
      - base
      - redis
    - pillar:
        redis_env_id: wx_redis

deploy_voice_celery_redis:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - sls:
      - base
      - redis
    - pillar:
        redis_env_id: voice_celery_redis

deploy_gameplaza_celery_redis:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - sls:
      - base
      - redis
    - pillar:
        redis_env_id: gameplaza_celery_redis

deploy_ac_redis:
  salt.state:
    - tgt: 'WxAccountCenter.test.desktop.yewu.js'
    - sls:
      - base
      - redis
