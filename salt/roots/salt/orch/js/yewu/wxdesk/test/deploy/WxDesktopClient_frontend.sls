{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'desktopclient-frontend' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/desktop/client/frontend/{0}".format(package_name) %}

deploy_WxDesktopClient_frontend_frontendNginx:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopClient.frontend.deploy
      - WxDesktopClient.nginx.web.frontend
    - pillar:
        WxDesktopClient_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxDesktopClient_frontend_backendNginx:
  salt.state:
    - tgt: 'nginx.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopClient.frontend.deploy
    - pillar:
        WxDesktopClient_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
