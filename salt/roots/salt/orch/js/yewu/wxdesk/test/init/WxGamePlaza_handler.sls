{% set version = salt['pillar.get']('version', '3850.2') %}
{% set package_name = 'GamePlazaHandler' %}
{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/wxgameplaza/handler/{0}".format(package_name) %}

init_handler_env:
  salt.state:
    - tgt: '(mongo_shardsvr02_p|gameplaza_handler02).test.gameplaza.yewu.js'
    - tgt_type: pcre
    - sls:
      - base
      - webagent
      - python3
      - WxGamePlaza.handler

deploy_handlers_server:
  salt.state:
    - tgt: '(mongo_shardsvr02_p|gameplaza_handler02).test.gameplaza.yewu.js'
    - tgt_type: pcre
    - sls:
      - webagent.deploy
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_handler_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: init_handler_env