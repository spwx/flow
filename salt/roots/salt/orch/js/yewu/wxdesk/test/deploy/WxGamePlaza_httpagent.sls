{% set version = salt['pillar.get']('version', '1.0') %}
{% set package_name = 'HttpAgent' %}
{% set test = salt['pillar.get']('test', 'True') %}
{% set _ftp_artifact_prefix = "ftp://files.dev.js/jenkins/js/yewu/gameplaza/agent/{0}".format(package_name) %}

upgrade_httpagent_blue_version:
  salt.state:
    - tgt: 'mongo_configsvr02.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue
        wxgameplaza_httpagent_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

{% if test|lower is equalto 'false' %}
restart_gameplaza_handler_blue_servers:
  salt.function:
    - tgt: 'mongo_shardsvr02_p.test.gameplaza.yewu.js'
    - name: cmd.run
    - arg:
      - initctl restart wxgameplaza_handler
    - require:
      - salt: upgrade_httpagent_blue_version
{% endif %}

upgrade_httpagent_green_version:
  salt.state:
    - tgt: 'mongo_configsvr02.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_green
        wxgameplaza_httpagent_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

{% if test|lower is equalto 'false' %}
restart_gameplaza_handler_green_servers:
  salt.function:
    - tgt: 'gameplaza_handler02.test.gameplaza.yewu.js'
    - name: cmd.run
    - arg:
      - service gameplaza_handler_green restart
    - require:
      - salt: upgrade_httpagent_green_version
{%endif%}

