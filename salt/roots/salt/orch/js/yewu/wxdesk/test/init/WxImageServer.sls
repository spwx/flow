{% set test = salt['pillar.get']('test', 'True') %}

init_WxImageServer_server:
  salt.state:
    - tgt: 'WxVoiceMaster.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - nginx
      - WxImageServer.server
      - WxImageServer.nginx.server

init_WxImageServer_web:
  salt.state:
    - tgt: 'WxImageServer.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - python3
      - mongo
      - mongos.enable_auth
      - WxImageServer.web

deploy_WxImageServer_client:
  salt.state:
    - tgt: 'WxDeskManagement.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.client

deploy_WxGamePlaza_backend_nginx:
  salt.state:
    - tgt: 'nginx.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.nginx.backend

deploy_WxGamePlaza_frontend_nginx:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.nginx.frontend
