deploy_mongos01:
  salt.state:
    - tgt: '(mongo_(shardsvr0(1|2)_p|configsvr02)|gameplaza_handler02).test.gameplaza.yewu.js'
    - sls:
      - mongo
      - mongos.enable_auth

deploy_mongos02:
  salt.state:
    - tgt: 'Wx(DeskManagement|VoiceMaster).test.desktop.yewu.js'
    - sls:
      - mongo
      - mongos.enable_auth




