{% set test = salt['pillar.get']('test', 'True') %}

init_WxVoiceMaster_web:
  salt.state:
    - tgt: 'WxVoiceMaster.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - python3
      - mongo
      - mongos.enable_auth
      - WxVoiceMaster.web
      - WxVoiceMaster.celerybeat
      - WxVoiceMaster.celeryd
