deploy_WxDeskManagement_web:
  salt.state:
    - tgt: 'WxDeskManagement.test.desktop.yewu.js'
    - sls:
      - base
      - python3
      - mongo
      - mongos.enable_auth
      - webagent
      - WxDeskManagement.web
