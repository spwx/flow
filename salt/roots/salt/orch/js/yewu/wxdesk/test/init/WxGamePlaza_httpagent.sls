{% set version = salt['pillar.get']('version', '1.0') %}
{% set package_name = 'HttpAgent' %}
{% set test = salt['pillar.get']('test', 'True') %}
{% set _ftp_artifact_prefix = "ftp://files.dev.js/jenkins/js/yewu/gameplaza/agent/{0}".format(package_name) %}

deploy_httpagent_blue_server:
  salt.state:
    - tgt: 'mongo_configsvr02.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue

deploy_httpagent_green_server:
  salt.state:
    - tgt: 'mongo_configsvr02.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_green

upgrade_httpagent_blue_version:
  salt.state:
    - tgt: 'mongo_configsvr02.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue
        wxgameplaza_httpagent_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: deploy_httpagent_blue_server

upgrade_httpagent_green_version:
  salt.state:
    - tgt: 'mongo_configsvr02.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_green
        wxgameplaza_httpagent_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: deploy_httpagent_green_server
