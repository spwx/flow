# deploy mongos cluster
deploy_base_env:
  salt.state:
    - tgt: 'mongo(s|_configsvr.+).test.gameplaza.yewu.js'
    - tgt_type: pcre
    - sls:
      - base
      - mongo

deploy_shardsvr_base_env:
  salt.state:
    - tgt: 'mongo_shardsvr*.test.gameplaza.yewu.js'
    - sls:
      - base
      - mongo
      - mongo_shardsvr

config_shardsvr:
  salt.state:
    - tgt: 'mongo_shardsvr\d+_p.test.gameplaza.yewu.js'
    - tgt_type: pcre
    - sls:
      - mongo_shardsvr.config
    - require:
      - salt: deploy_shardsvr_base_env

deploy_configsvr01:
  salt.state:
    - tgt: 'mongo_configsvr*.test.gameplaza.yewu.js'
    - sls:
      - mongo_configsvr
    - require:
      - salt: deploy_base_env

config_configsvr01:
  salt.state:
    - tgt: 'mongo_configsvr01.test.gameplaza.yewu.js'
    - sls:
      - mongo_configsvr.config
    - require:
      - salt: deploy_configsvr01

deploy_mongos:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - sls:
      - mongos
      - mongos.create_user
      - mongos.add_shard
      - mongos.init_gameplaza_db
    - require:
      - salt: config_shardsvr
      - salt: config_configsvr01

# enable auth
enable_shardsvr01_auth:
  salt.state:
    - tgt: 'mongo_shardsvr*.test.gameplaza.yewu.js'
    - sls:
      - mongo_shardsvr.enable_auth
    - require:
      - salt: deploy_mongos

enable_configsvr01_auth:
  salt.state:
    - tgt: 'mongo_configsvr*.test.gameplaza.yewu.js'
    - sls:
      - mongo_configsvr.enable_auth
    - require:
      - salt: enable_shardsvr01_auth

enable_mongos_auth:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - sls:
      - mongos.enable_auth
    - require:
      - salt: enable_configsvr01_auth
