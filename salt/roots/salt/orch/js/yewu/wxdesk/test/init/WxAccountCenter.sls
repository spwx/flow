{% set test = salt['pillar.get']('test', 'True') %}

init_WxAccountCenter:
  salt.state:
    - tgt: 'WxAccountCenter.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - python3

init_celerybeat:
  salt.state:
    - tgt: 'WxAccountCenter.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxAccountCenter.celerybeat

init_celeryd:
  salt.state:
    - tgt: 'WxAccountCenter.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxAccountCenter.celeryd
