{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'publicity' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/desktop/{0}".format(package_name) %}

deploy_frontend_frontendNginx:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPublicity.frontend.deploy
      - WxPublicity.nginx.web.frontend
    - pillar:
        WxPublicity_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_frontend_backendNginx:
  salt.state:
    - tgt: 'nginx.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPublicity.frontend.deploy
    - pillar:
        WxPublicity_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
