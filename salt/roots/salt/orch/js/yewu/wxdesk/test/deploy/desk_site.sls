{% set version =  salt['pillar.get']('version', "1.0.0") %}
{% set test = salt['pillar.get']('test', 'True') %}
{% set package_name = 'desk-site' %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/desktop/site/{0}".format(package_name) %}

deploy_desk_site:
  salt.state:
    - tgt: 'nginx.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDeskSite.frontend.deploy
    - pillar:
        WxDeskSite_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
deploy_nginx_server:
  salt.state:
    - tgt: 'nginx.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDeskSite.nginx.web.frontend
    - require:
      - salt: deploy_desk_site
