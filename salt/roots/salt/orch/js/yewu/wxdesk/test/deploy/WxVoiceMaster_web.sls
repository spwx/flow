{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'VoiceMaster' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/voicemaster/web/{0}".format(package_name) %}

deploy_WxVoiceMaster_web:
  salt.state:
    - tgt: 'WxVoiceMaster.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.web.deploy
    - pillar:
        WxVoiceMaster_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxVoiceMaster_celerybeat:
  salt.state:
    - tgt: 'WxVoiceMaster.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.celerybeat.deploy
    - pillar:
        WxVoiceMaster_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxVoiceMaster_celeryd:
  salt.state:
    - tgt: 'WxVoiceMaster.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.celeryd.deploy
    - pillar:
        WxVoiceMaster_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxVoiceMaster_nginx:
  salt.state:
    - tgt: 'nginx.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.nginx.backend

deploy_WxVoiceMaster_frontend:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.nginx.frontend
