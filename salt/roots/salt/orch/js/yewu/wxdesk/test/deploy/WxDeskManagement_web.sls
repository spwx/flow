{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'qdd_management' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/qdd/web/{0}".format(package_name) %}

deploy_WxDeskManagement_web:
  salt.state:
    - tgt: 'WxDeskManagement.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDeskManagement.web.deploy
    - pillar:
        WxDeskManagement_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxDeskManagement_nginx:
  salt.state:
    - tgt: 'nginx.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDeskManagement.frontend.deploy
      - WxDeskManagement.nginx.backend
    - pillar:
        WxDeskManagement_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxDeskManagement_frontend:
  salt.state:
    - tgt: 'mongos.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDeskManagement.frontend.deploy
      - WxDeskManagement.nginx.frontend
    - pillar:
        WxDeskManagement_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
