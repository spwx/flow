{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'oss' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/oss/{0}".format(package_name) %}

deploy_WxImageServer_web:
  salt.state:
    - tgt: 'WxVoiceMaster.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.web.deploy
    - pillar:
        WxVoiceMaster_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'