{% set test = salt['pillar.get']('test', 'True') %}

init_WxGameMenu:
  salt.state:
    - tgt: 'WxDeskManagement.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - python3

init_celerybeat:
  salt.state:
    - tgt: 'WxDeskManagement.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celerybeat

init_celeryd:
  salt.state:
    - tgt: 'WxDeskManagement.test.desktop.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celeryd
