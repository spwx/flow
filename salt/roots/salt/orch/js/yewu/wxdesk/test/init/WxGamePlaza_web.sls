{% set test = salt['pillar.get']('test', 'True') %}

init_WxGamePlaza_web:
  salt.state:
    - tgt: 'mongo_(configsvr02|shardsvr01_p).test.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - python3
      - mongo
      - mongos.enable_auth
      - webagent
      - WxGamePlaza.web
      - WxGamePlaza.celeryd

deploy_WxGamePlaza_webagent:
  salt.state:
    - tgt: 'mongo_(configsvr02|shardsvr01_p).test.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - webagent.deploy

deploy_WxGamePlaza_celerybeat:
  salt.state:
    - tgt: 'mongo_configsvr02.test.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celerybeat
