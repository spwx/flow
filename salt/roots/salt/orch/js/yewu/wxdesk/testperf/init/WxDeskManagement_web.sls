{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_WxDeskManagement_web:
  salt.state:
    - tgt: 'app03.{{ env_id }}.gameplaza.yewu.js'
    - sls:
      - WxDeskManagement.web
