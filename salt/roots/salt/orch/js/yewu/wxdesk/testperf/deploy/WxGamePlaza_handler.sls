{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version = salt['pillar.get']('version', '3850.2') %}
{% set test = salt['pillar.get']('test', 'True') %}
{% set package_name = 'GamePlazaHandler' %}
{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/wxgameplaza/handler/{0}".format(package_name) %}

deploy_handlers_server_blue:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_handler_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
