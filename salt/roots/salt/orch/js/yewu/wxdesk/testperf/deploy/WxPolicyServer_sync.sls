{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version =  salt['pillar.get']('version', "4.0.0+793520.4") %}
{% set package_name = 'accountsync' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/policy/{0}".format(package_name) %}


deploy_WxPolicyServer_accountsync:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.accountsync.deploy
    - pillar:
        accountsync_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'