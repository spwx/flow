{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'barshop_py' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/barshop_py/{0}".format(package_name) %}


deploy_web_report:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxBarshop.web_report.deploy
    - pillar:
        WxBarshop_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_backend_report:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxBarshop.nginx_report.backend
    - pillar:
        WxBarshop_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'


deploy_frontend_report:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxBarshop.frontend_report.deploy
      - WxBarshop.nginx_report.frontend
    - pillar:
        WxBarshop_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
