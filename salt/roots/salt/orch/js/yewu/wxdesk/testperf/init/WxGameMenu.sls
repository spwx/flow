{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_celerybeat:
  salt.state:
    - tgt: 'app03.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celerybeat

init_celeryd:
  salt.state:
    - tgt: 'app03.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celeryd
