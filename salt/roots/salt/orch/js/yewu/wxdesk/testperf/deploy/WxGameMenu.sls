{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version =  salt['pillar.get']('version', "1.0+0f31a1.1") %}
{% set package_name = 'gamemenu' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/qdd/gamemenu/{0}".format(package_name) %}

deploy_WxGameMenu_celerybeat:
  salt.state:
    - tgt: 'app03.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celerybeat.deploy
    - pillar:
        WxGameMenu_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxGameMenu_celeryd:
  salt.state:
    - tgt: 'app03.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGameMenu.celeryd.deploy
    - pillar:
        WxGameMenu_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
