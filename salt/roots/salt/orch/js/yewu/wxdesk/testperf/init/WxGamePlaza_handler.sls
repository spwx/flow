{% set version = salt['pillar.get']('version', '1.5+e5d245.105') %}
{% set package_name = 'GamePlazaHandler' %}
{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn:21/jenkins/js/yewu/wxgameplaza/handler/{0}".format(package_name) %}

{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_handler_env:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler

deploy_handlers_server:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_handler_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: init_handler_env
