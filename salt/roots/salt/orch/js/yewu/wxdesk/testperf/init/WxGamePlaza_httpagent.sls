{% set version = salt['pillar.get']('version', '4625.39') %}
{% set package_name = 'HttpAgent' %}
{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/gameplaza/agent/{0}".format(package_name) %}
{% set test = salt['pillar.get']('test', 'True') %}
{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}

init_httpagent_blue_server:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue

upgrade_httpagent_blue_version:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue
        wxgameplaza_httpagent_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
    - require:
      - salt: init_httpagent_blue_server

