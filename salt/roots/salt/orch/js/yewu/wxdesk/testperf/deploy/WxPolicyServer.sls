{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version =  salt['pillar.get']('version', "4.0.0+793520.4") %}
{% set package_name = 'policymanager' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/policy/{0}".format(package_name) %}

deploy_WxPolicyServer_web:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.web.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_celerybeat:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.celerybeat.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_celeryd:
  salt.state:
    - tgt: 'app0(3|4).{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxPolicyServer.celeryd.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_policyinit:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.policyinit.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_scheduler:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.scheduler.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxPolicyServer_logmgr:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.logmgr.deploy
    - pillar:
        wxpolicy_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'