{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.web

init_celerybeat:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.celerybeat

init_celeryd:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxVoiceMaster.celeryd
