{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_WxGamePlaza_web:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxGamePlaza.web

deploy_WxGamePlaza_celerybeat:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celerybeat

deploy_WxGamePlaza_default-celery-worker:
  salt.state:
    - tgt: 'app0(1|3).{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celeryd

