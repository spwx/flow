
{% set test = salt['pillar.get']('test', 'True') %}
{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}

deploy_cache_redis:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: cache_redis

deploy_ac_account_redis:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: ac_account_redis

deploy_accountcenter_celery_redis:
  salt.state:
    - tgt: 'app03.{{ env_id }}.gameplaza.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: accountcenter_celery_redis

deploy_voice_celery_redis:
  salt.state:
    - tgt: 'app03.{{ env_id }}.gameplaza.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: voice_celery_redis

deploy_gameplaza_celery_redis:
  salt.state:
    - tgt: 'app03.{{ env_id }}.gameplaza.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: gameplaza_celery_redis

deploy_policy_redis:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: policy_redis

deploy_policymgr_redis:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: policymgr_redis