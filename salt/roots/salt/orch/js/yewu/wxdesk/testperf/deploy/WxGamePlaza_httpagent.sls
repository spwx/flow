{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version = salt['pillar.get']('version', '1.0') %}
{% set package_name = 'HttpAgent' %}
{% set test = salt['pillar.get']('test', 'True') %}
{% set _ftp_artifact_prefix = "ftp://files.dev.js/jenkins/js/yewu/gameplaza/agent/{0}".format(package_name) %}

upgrade_httpagent_blue_version:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue
        wxgameplaza_httpagent_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

{% if test|lower is equalto 'false' %}
restart_gameplaza_handler_blue_servers:
  salt.function:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - name: cmd.run
    - arg:
      - initctl restart wxgameplaza_handler
    - require:
      - salt: upgrade_httpagent_blue_version
{% endif %}

