{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version =  salt['pillar.get']('version', "4.0.0+793520.4") %}
{% set package_name = 'egs' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/wxgameplaza/egs"%}

deploy_egs_web:
  salt.state:
    - tgt: 'app04.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - egs.web.deploy
    - pillar:
        egs_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_egs_nginx:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - egs.nginx.backend_inner
