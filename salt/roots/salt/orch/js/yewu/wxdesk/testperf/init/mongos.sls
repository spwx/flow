{% set test = salt['pillar.get']('test', 'True') %}
{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}

init-mongo-environment:
  salt.state:
    - tgt: 'mongo_cluster.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - mongo

install-mongo-configsvr:
  salt.state:
    - tgt: 'mongo_cluster.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - mongo_shardsvr
    - pillar:
        mongo_shardsvr_env_id: mongo_configsvr

config-mongo-configsvr:
  salt.state:
    - tgt: 'mongo_cluster.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - mongo_shardsvr.replication
    - pillar:
        mongo_shardsvr_env_id: mongo_configsvr

install-mongo-shardsvr01:
  salt.state:
    - tgt: 'mongo_cluster.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - mongo_shardsvr

config-mongo-shardsvr:
  salt.state:
    - tgt: 'mongo_cluster.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - mongo_shardsvr.replication

add-shardsvr-2-cluster:
  salt.state:
    - tgt: 'mongo_cluster.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - mongos
      - mongos.addShard

# add db and create shardkey, index
