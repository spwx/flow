{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'GamePlaza' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/wxgameplaza/web/{0}".format(package_name) %}

deploy_WxGamePlaza_web:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.web.deploy
    - pillar:
        wxgameplaza_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxGamePlaza_celerybeat:
  salt.state:
    - tgt: 'app01.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celerybeat.deploy
    - pillar:
        wxgameplaza_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxGamePlaza_celeryd:
  salt.state:
    - tgt: 'app0(1|3).{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celeryd.deploy
    - pillar:
        wxgameplaza_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxGamePlaza_nginx:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.nginx.web.backend
