{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - LwMarket.web

deploy_worker:
  salt.state:
    - tgt: 'app02.{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - LwMarket.celeryd

