{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'deskplatform' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/qdd/{0}".format(package_name) %}

deploy_web:
  salt.state:
    - tgt: 'desktop_operation.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxDesktopOperation.web.deploy
    - pillar:
        WxDesktopOperation_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
          test: 'test'
