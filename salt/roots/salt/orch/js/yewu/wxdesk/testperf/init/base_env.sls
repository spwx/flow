{% set test = salt['pillar.get']('test', 'True') %}
{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}

install-python3:
  salt.state:
    - tgt: 'app0(1|2|3).{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - python3

install-mongos:
  salt.state:
    - tgt: 'app0(1|2|3).{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - mongo
      - mongos

# install nginx(frontend, backend, wximageserver-nginx)
install-nginx:
  salt.state:
    - tgt: 'app0(1|2|3).{{ env_id }}.gameplaza.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - nginx

