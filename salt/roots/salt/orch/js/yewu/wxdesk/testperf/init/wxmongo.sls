{% set test = salt['pillar.get']('test', 'True') %}
{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}

{% set dbs = ['gameplaza', 'gamedesk', 'voice', 'misc'] %}

{% for dbName in dbs %}
# add db and create shardkey, index
init-db-{{ dbName }}:
  salt.state:
    - tgt: 'mongo_cluster.{{ env_id }}.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - WxMongo.initDB
    - pillar:
        DBNAME: {{ dbName }}
{% endfor %}
