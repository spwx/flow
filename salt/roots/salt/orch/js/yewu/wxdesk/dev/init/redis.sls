{% set test = salt['pillar.get']('test', 'True') %}

wx_token_redis:
  salt.state:
    - tgt: 'redis.dev.wxdesk.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: wx_token_redis

voice_redis:
  salt.state:
    - tgt: 'redis.dev.wxdesk.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: voice_redis

gameplaza_celery_redis:
  salt.state:
    - tgt: 'redis.dev.wxdesk.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: gameplaza_celery_redis

gampelza_redis:
  salt.state:
    - tgt: 'redis.dev.wxdesk.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: gampelza_redis

lswm_order_redis:
  salt.state:
    - tgt: 'redis.dev.wxdesk.yewu.js'
    - sls: redis
    - test: {{ test }}
    - pillar:
        redis_env_id: lswm_order_redis
