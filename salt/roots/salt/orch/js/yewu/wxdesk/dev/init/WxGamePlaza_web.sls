{% set test = salt['pillar.get']('test', 'True') %}

init_WxGamePlaza_web:
  salt.state:
    - tgt: 'web01.dev.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.web

deploy_WxGamePlaza_celerybeat:
  salt.state:
    - tgt: 'web01.dev.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celerybeat

deploy_WxGamePlaza_default-celery-worker:
  salt.state:
    - tgt: 'web01.dev.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.celeryd
