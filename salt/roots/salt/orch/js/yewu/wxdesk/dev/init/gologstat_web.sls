{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'desktop_operation.testperf01.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - gologstat.web
