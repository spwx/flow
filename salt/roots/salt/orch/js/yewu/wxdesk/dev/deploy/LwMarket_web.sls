{% set env_id = salt['pillar.get']('env_id', 'dev') %}
{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'lightsupermarket' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/lightsupermarket/{0}".format(package_name) %}

deploy_web:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - LwMarket.web.deploy
    - pillar:
        LwMarket_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_worker:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - LwMarket.celeryd.deploy
    - pillar:
        LwMarket_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_backend:
  salt.state:
    - tgt: 'backendnginx.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - LwMarket.frontend.deploy
      - LwMarket.nginx.backend
    - pillar:
        LwMarket_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_frontend:
  salt.state:
    - tgt: 'frontnginx.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - LwMarket.frontend.deploy
      - LwMarket.nginx.frontend
    - pillar:
        LwMarket_web_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
