{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxBarshop.web

init_webb:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxBarshop.webb

init_webc:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxBarshop.webc

