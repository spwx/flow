{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.web

init_celerybeat:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.celerybeat

init_celeryd:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxPolicyServer.celeryd

init_policyinit:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.policyinit

init_scheduler:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.scheduler

init_logmgr:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxPolicyServer.logmgr