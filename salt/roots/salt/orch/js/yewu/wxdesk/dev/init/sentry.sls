{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'elk01.dev.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - sentry
