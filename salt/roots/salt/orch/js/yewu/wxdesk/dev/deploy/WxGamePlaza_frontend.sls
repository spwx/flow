{% set env_id = salt['pillar.get']('env_id', 'dev') %}
{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'gameplaza-frontend' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/wxgameplaza/frontend/{0}".format(package_name) %}

deploy_WxGamePlaza_frontend_frontendNginx:
  salt.state:
    - tgt: 'frontnginx.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.frontend.deploy
      - WxGamePlaza.nginx.web.frontend
    - pillar:
        wxgameplaza_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_WxGamePlaza_frontend_backendNginx:
  salt.state:
    - tgt: 'backendnginx.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxGamePlaza.frontend.deploy
    - pillar:
        wxgameplaza_frontend_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
