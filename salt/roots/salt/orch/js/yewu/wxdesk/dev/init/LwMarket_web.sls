{% set env_id = salt['pillar.get']('env_id', 'dev') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_web:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - LwMarket.web

deploy_worker:
  salt.state:
    - tgt: 'web01.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - LwMarket.celeryd
