{% set env_id = salt['pillar.get']('env_id', 'dev') %}
{% set test = salt['pillar.get']('test', 'True') %}

init_WxImageServer_server:
  salt.state:
    - tgt: 'oss.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.web

