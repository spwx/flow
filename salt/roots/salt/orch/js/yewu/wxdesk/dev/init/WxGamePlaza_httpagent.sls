{% set version = salt['pillar.get']('version', '4625.39') %}
{% set package_name = 'HttpAgent' %}
{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/gameplaza/agent/{0}".format(package_name) %}
{% set test = salt['pillar.get']('test', 'True') %}
{% set env_id = salt['pillar.get']('env_id', 'dev') %}

init_httpagent_blue_server:
  salt.state:
    - tgt: 'web(01|02).{{ env_id }}.wxdesk.yewu.js'
    - tgt_type: pcre
    - test: {{ test }}
    - sls:
      - WxGamePlaza.handler
      - WxGamePlaza.handler.deploy
    - pillar:
        wxgameplaza_env_id: wxgameplaza_httpagent_blue
