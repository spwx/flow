{% set env_id = salt['pillar.get']('env_id', 'dev') %}
{% set version =  salt['pillar.get']('version', "1.0") %}
{% set package_name = 'oss' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/oss/{0}".format(package_name) %}

deploy_WxImageServer_web:
  salt.state:
    - tgt: 'oss.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.web.deploy
    - pillar:
        WxImageServer_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'


deploy_backend_nginx:
  salt.state:
    - tgt: 'backendnginx.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.nginx.backend

deploy_frontend_nginx:
  salt.state:
    - tgt: 'frontnginx.{{ env_id }}.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - WxImageServer.nginx.frontend
