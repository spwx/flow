{% set env_id = salt['pillar.get']('env_id', 'testperf01') %}
{% set version =  salt['pillar.get']('version', "4.0.0+793520.4") %}
{% set package_name = 'gologstat' %}
{% set test = salt['pillar.get']('test', 'True') %}

{% set _ftp_artifact_prefix = "ftp://artifact.nextbu.cn/jenkins/js/yewu/gologstat/{0}".format(package_name) %}

deploy_gologstat_web:
  salt.state:
    - tgt: 'desktop_operation.testperf01.gameplaza.yewu.js'
    - test: {{ test }}
    - sls:
      - gologstat.web.deploy
    - pillar:
        gologstat_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'

deploy_gologstat_celery:
  salt.state:
    - tgt: 'web02.dev.wxdesk.yewu.js'
    - test: {{ test }}
    - sls:
      - gologstat.deploy_celery
    - pillar:
        gologstat_pkg:
          package_name: {{ package_name }}
          version: '{{ version }}'
          pkg_url: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz'
          pkg_checksum: '{{ _ftp_artifact_prefix }}/{{ version }}/{{ package_name }}-{{ version }}.tar.gz.MD5'
