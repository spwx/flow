wxgameplaza:
  frontend:
    install_path: /data/SicentWebsite/wxgameplaza/frontend
    res_url_maps:
      - url: /assets
        path: template/assets/
      - url: /bgame
        path: template/bgame/
      - url: /sgame
        path: template/sgame/
      - url: /box
        path: template/box/
      - url: /_build
        path: template/_build/
      - url: /heroes
        path: template/heroes/
      - url: /favicon.ico
        path: template/favicon.ico
      - url: /wx/pages/
        path: template/wx/pages/
      - url: /wx/MP_verify_YkGS4I519BFVIhr4.txt
        path: template/wx/MP_verify_YkGS4I519BFVIhr4.txt
      - url: /wx/MP_verify_s3jlEdtFJ7EAN7Gu.txt
        path: template/wx/MP_verify_s3jlEdtFJ7EAN7Gu.txt