LogRecordServer:
  install_path: /data/SicentApp/LogRecordServer
  log_dir: /data/SicentApp/LogRecordServer/logs
  cfg:
    output: /data/SicentApp/LogRecordServer/outputpath
    copypath: /data/SicentApp/LogRecordServer/ftpdir

# close selinux and reboot system.
vsftpd:
  users:
    PolicyFTP:
      password: 'js*%##))!!WX'
      conf:
        anon_world_readable_only: 'NO'
        anon_upload_enable: 'YES'
        anon_mkdir_write_enable: 'YES'
        anon_other_write_enable: 'YES'
        local_root: '/data/SicentApp/LogRecordServer/ftpdir'