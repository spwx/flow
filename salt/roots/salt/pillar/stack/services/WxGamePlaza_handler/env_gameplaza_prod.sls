{% set ip_addrs = stack.get('ip_addrs', {}) %}

wxgameplaza:
  handler:
    install_path: /data/SicentWebserver/swgameplaza/handler
    log_dir: /data/SicentWebserver/swgameplaza/handler/logs
    svc_name: swgameplaza_handler
    settings:
      LOL_UPLOAD_TIME_LIMIT: 900
      PUBG_UPLOAD_TIME_LIMIT: 180
      HOST: 0.0.0.0
      PORT: 9000
      WORKER: 4
      DEBUG: False
      RESPONSE_TIMEOUT: 15
      REQUEST_TIMEOUT: 5