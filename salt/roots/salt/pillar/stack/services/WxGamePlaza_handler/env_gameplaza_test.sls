{% set ip_addrs = stack.get('ip_addrs', {}) %}
wxgameplaza:
  handler:
    settings:
      LOG_DEBUG: True
      LOL_UPLOAD_TIME_LIMIT: 3
      PLAZA_HOST: {{ ip_addrs.nginx_backend }}
      RESPONSE_TIMEOUT: 20
      REQUEST_TIMEOUT: 20
