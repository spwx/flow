{% set ip_addrs = stack.get('ip_addrs', {}) %}
wxgameplaza:
  handler:
    settings:
      LOL_UPLOAD_TIME_LIMIT: 3
      PUBG_UPLOAD_TIME_LIMIT: 3
      HOST: 0.0.0.0
      PORT: 19999
      WORKER: 2
      DEBUG: True
      RESPONSE_TIMEOUT: 2
      REQUEST_TIMEOUT: 2
