{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set usercenter_configs = stack.get('usercenter_configs', {}) %}
{% set wx_token_redis = ip_addrs.get('wx_token_redis', {}) %}


usercenter:
  web:
    install_path: /data/SicentWebserver/desktop/usercenter

  settings:

    debug: False
    #数据库配置
    motor_url: mongodb://usercenter:user123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/usercenter

    #服务ip端口
    app:
      host: 0.0.0.0
      port: 22125
      workers: 8
    #是否开启keep_alive
    KEEP_ALIVE: false
    #计费主机地址
    wei_xin_web_app_id: wxb9356136dc4f65cf
    wei_xin_web_app_secret: 22d6fb2cdb7ba0c4b9ee44faed2b7c27

    #wx_share_app_id: wxd3592354f0dde0c5
    #wx_share_app_secret: 773ab309e1fc2948411945083b6fc12b

    wx_auth_back_url: http://plaza.wxdesk.com/users/wx/auth/back
    # QQ参数

    qq_app_id: 101441770
    qq_app_key: b8a00faa0f518cc461e8fb14a9d07ecc
    wx_redirect_url: {{ usercenter_configs.wx_redirect_url }}
    qq_redirect_url: {{ usercenter_configs.qq_redirect_url }}
    bind_url: {{ usercenter_configs.bind_url }}
    WX_TOKEN_REDIS: redis://:{{ wx_token_redis.password }}@{{ wx_token_redis.ip }}:{{ wx_token_redis.port }}/0


