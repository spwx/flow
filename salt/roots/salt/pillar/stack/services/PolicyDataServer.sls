PolicyDataServer:
  celerybeat:
    install_path: /data/SicentWebserver/desktop/PolicyDataServer/celerybeat
    log_dir: /data/SicentWebserver/desktop/PolicyDataServer/celerybeat/logs
  celeryd:
    install_path: /data/SicentWebserver/desktop/PolicyDataServer/celeryd
    log_dir: /data/SicentWebserver/desktop/PolicyDataServer/celeryd/logs
  web:
    install_path: /data/SicentWebserver/desktop/PolicyDataServer/web
    log_dir: /data/SicentWebserver/desktop/PolicyDataServer/web/logs