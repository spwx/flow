WxDeskManagement:
  web:
    disable_logging: false
    settings:
      LOG_DEBUG: True # # test,dev env advice set True ,but, online env muse to set False
      TOKEN: False  # enable token
      DEBUG: True
      LWSM_MONGODB_URL: mongodb://10.34.60.20:27017/lwsm  # light weight super market mongo url
      # 长连系统发送数据接口
      LCS_PUBLISH_CONN:
        host: 10.34.60.19
        port: 5672
        username: sicent
        password: sicent
        vhost: sicent_vhost
        exchange_name: amq.direct
        routingkey: longsystem

      MEDIA_URL: "/upload/"
      LWSM_ORDER_REDIS: redis://:redis123@10.34.60.20:6384/13  # 超市订单分布式锁redis
      CELERY_BROKER_URL: redis://:redis123@10.34.60.20:6384/14
      CELERY_RESULT_URL: redis://:redis123@10.34.60.20:6384/15

      ZZOT_HOST: 10.34.57.48 # 0013.96ni.net # ZZOT is zero zero one three brief
      ZZOT_BAR_PAY_HOST: pay.cbarpay.com # test 10.34.57.48

      ORDER_EXPIRE_TIME: 900 # 订单有效期15分钟
      IMAGE_SERVER_URL: http://10.34.52.180
