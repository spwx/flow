{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set lwsm_order_redis = ip_addrs.get('lwsm_order_redis', {}) %}
LwMarket:
  web:
    install_path: /data/SicentWebserver/LwMarket/web
    log_dir: /data/SicentWebserver/LwMarket/web/logs
    settings:
        LOG_DEBUG: False # # test,dev env advice set True ,but, online env muse to set False
        TOKEN: True  # enable token
        DEBUG: False
        ALLOWED_HOSTS: ["lwsm.wxdesk.com", "221.228.80.71", "172.30.236.102"]  # default enable dev env host ip
        ENABLE_UWSGI: True  # setting ENABLE_UWSGI is True if running uwsgi start django else False
        LWSM_MONGODB_URL: mongodb://lwsm:lwsm123@127.0.0.1:{{ ip_addrs.mongos.port }}/lwsm # mongodb url
        MEDIA_ROOT: "/data/wximage/client/"
        MEDIA_URL: "/upload/"
        LWSM_ORDER_REDIS: redis://:{{ lwsm_order_redis.password }}@{{ lwsm_order_redis.ip }}:{{ lwsm_order_redis.port }}/{{ lwsm_order_redis.db }}
        CELERY_BROKER_URL: redis://:{{ lwsm_order_redis.password }}@{{ lwsm_order_redis.ip }}:{{ lwsm_order_redis.port }}/1
        CELERY_RESULT_URL: redis://:{{ lwsm_order_redis.password }}@{{ lwsm_order_redis.ip }}:{{ lwsm_order_redis.port }}/2
        CACHE_REDIS: redis://:{{ lwsm_order_redis.password }}@{{ lwsm_order_redis.ip }}:{{ lwsm_order_redis.port }}/3

        ZZOT_HOST: 0013.96ni.net # 0013.96ni.net
        ZZOT_BAR_PAY_HOST: pay.cbarpay.com

        ORDER_EXPIRE_TIME: 900 # 订单有效期15分钟
        LOG_ADDRESS:
        - __: overwrite
        - 221.228.80.111
        - 22002
        GAMEPLAZA_HOST: 172.30.236.102:8083
        OSS_HOST: 172.30.236.102:8085
        VOICE_API_HOST: 172.30.236.102:8084
