{% set ip_addrs = stack.get('ip_addrs', {}) %}

LwMarket:
  web:
    settings:
        LOG_DEBUG: True # # test,dev env advice set True ,but, online env muse to set False
        TOKEN: False  # enable token
        DEBUG: True
        LWSM_MONGODB_URL: mongodb://lwsm:lwsm123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/lwsm  # light weight super market mongo url
        MEDIA_URL: "/upload/"

        ZZOT_HOST: 10.34.57.48 # 0013.96ni.net # ZZOT is zero zero one three brief
        ZZOT_BAR_PAY_HOST: 10.34.57.48 # testpay.cbarpay.com

        ORDER_EXPIRE_TIME: 900 # 订单有效期15分钟
        #IMAGE_SERVER_URL: http://10.34.60.126
        LOG_ADDRESS:
        - 10.34.52.174
        - 22002
        GAMEPLAZA_HOST: {{ip_addrs.nginx_backend}} :8083
        OSS_HOST: 10.34.56.107:8085