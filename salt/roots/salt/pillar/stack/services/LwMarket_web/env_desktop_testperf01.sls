{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set LWSM_CACHE_REDIS = ip_addrs.get('LWSM_CACHE_REDIS', {}) %}

LwMarket:
  user: 'root'
  group: 'root'
  web:
    settings:
        LOG_DEBUG: True # # test,dev env advice set True ,but, online env muse to set False
        TOKEN: False  # enable token
        LWSM_MONGODB_URL: mongodb://lwsm:lwsm123@10.34.60.130:27017/lwsm  # light weight super market mongo url
        DEBUG: True
        MEDIA_URL: "/upload/"
        LWSM_ORDER_REDIS: redis://:redis123@10.34.60.122:22212/3  # 超市订单分布式锁redis
        CELERY_BROKER_URL: redis://:redis123@10.34.60.122:22212/4
        CELERY_RESULT_URL: redis://:redis123@10.34.60.122:22212/5

        ZZOT_HOST: 10.34.57.48 # 0013.96ni.net # ZZOT is zero zero one three brief
        ZZOT_BAR_PAY_HOST: 10.34.57.48 # testpay.cbarpay.com

        ORDER_EXPIRE_TIME: 900 # 订单有效期15分钟
        LOG_ADDRESS:
        - 10.34.52.174
        - 22002
        GAMEPLAZA_HOST:  10.34.60.122:8083
        VOICE_API_HOST: 10.34.60.122:8084
        OSS_HOST: 10.34.60.122:8085
        CACHE_REDIS: redis://:{{ LWSM_CACHE_REDIS.password }}@{{ LWSM_CACHE_REDIS.ip }}:{{ LWSM_CACHE_REDIS.port }}/{{ LWSM_CACHE_REDIS.db }}