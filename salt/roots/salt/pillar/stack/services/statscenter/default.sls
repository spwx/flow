{% set statscenter_configs = stack.get('statscenter_configs', {}) %}

{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set statscenter_redis = ip_addrs.statscenter_redis %}

statscenter:
  web:
    install_path: /data/SicentWebserver/desktop/statscenter

  settings:
    debug: False
    #缓存配置
    redis_url: redis://:{{ statscenter_redis.password }}@{{ statscenter_redis.ip }}:{{ statscenter_redis.port }}/{{ statscenter_redis.db }}

    #数据库配置
    motor_url: mongodb://statscenter:stats123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/statscenter

    #服务ip端口
    app:
      host: 0.0.0.0
      port: 22125
      workers: 8
    #是否开启keep_alive
    KEEP_ALIVE: false
    #计费主机地址
    nfe_url: {{ statscenter_configs.nfe_url }}
    #0013收款码主机地址
    payment_code_0013_url: {{ statscenter_configs.payment_code_0013_url }}
    #0钱多多管理后台主机地址
    qdd_manager_url: {{ statscenter_configs.qdd_manager_url }}
