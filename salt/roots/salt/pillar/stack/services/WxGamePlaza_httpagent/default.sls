wxgameplaza_httpagent_blue:
  httpagent:
    install_path: /data/SicentWebserver/gameplaza/httpagent_blue
    log_dir: /data/SicentWebserver/gameplaza/httpagent_blue/logs
    svc_name: httpagent_blue
    app_conf:
      appname: httpagent_blue
      httpport: 9000
      runmode: pro
      AdminHttpPort: '8087'
    handler_conf:
      handler:
        addr: ':3315'
        cpuNum: 4
    user_infos:
      - username: desktop
        password: gameplaza

wxgameplaza_httpagent_green:
  httpagent:
    install_path: /data/SicentWebserver/gameplaza/httpagent_green
    log_dir: /data/SicentWebserver/gameplaza/httpagent_green/logs
    svc_name: httpagent_green
    app_conf:
      appname: httpagent_green
      httpport: 9001
      runmode: pro
      AdminHttpPort: '8089'
    handler_conf:
      handler:
        addr: ':3316'
        cpuNum: 4
    user_infos:
      - username: desktop
        password: gameplaza
