{% set desktop_account_configs = stack.get('desktop_account_configs', {}) %}

{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxBarshop:
  web:
    install_path: /data/SicentWebserver/desktop/WxBarshop/web
    log_dir: /data/SicentWebserver/desktop/WxBarshop/web/logs
    settings:
        CALLBACK_VERIFICATION_URL: {{ desktop_account_configs.callback_verification_url }}
        CALLBACK_VERIFICATION_URL_PUBWIN: {{ desktop_account_configs.callback_verification_url_pubwin }}
        CHAIN_BAR_URL: http://10.34.50.62:8081/cxf/http/service/data/childAcc
        CHAIN_BAR_URL_PUBWIN: http://10.34.45.26:8081/cxf/http/service/data/childAcc
        LOG_DEBUG: True
        CACHE_REDIS: redis://:redis123@10.34.56.103:6388/1  # web cache and sesssoion
        # SECURITY WARNING: don't run with debug turned on in production!
        DEBUG: True
        #DEBUG: False
        LOG_BASE_DIR: /data/SicentWebserver/desktop/WxBarshop/web/logs
        ALLOWED_HOSTS: ["qdd.wxdesk.com", "localhost", "127.0.0.1", "testserver"]
        ENABLE_UWSGI: True  # setting ENABLE_UWSGI is True if running uwsgi start django else False
        #REDIS_URLS: ["redis://localhost:6379/0","redis://localhost:6379/0"]

        MEDIA_ROOT: "../image_upload/"

        RABBITMQ_URL: "amqp://guest:guest@localhost:5672"
        VIP_URL: "http://10.34.53.178:8088/interface/vip_api.php?snbid=%s"

        DB_BARSHOP_BASE:
          NAME: 'barshopBase'
          USER: '020'
          PASSWORD: 'love123'
          HOST: '10.34.52.160'
          PORT: '3306'

        DB_BARSHOP_00:
          NAME: 'barshop_00'
          USER: '020'
          PASSWORD: 'love123'
          HOST: '10.34.52.160'
          PORT: '3306'

        DB_BARSHOP_01:
          NAME: 'barshop_01'
          USER: '020'
          PASSWORD: 'love123'
          HOST: '10.34.52.160'
          PORT: '3306'

        LCS_PUBLISH_CONN:
            host: 10.34.60.19
            port: 5672
            username: sicent
            password: sicent
            vhost: sicent_vhost
            exchange_name: amq.direct
            routingkey: longsystem

        WX_URL: http://webapi.sicent.com  # <!-- 计费网吧信息接口 -->
        OLTERRACE: http://10.34.44.22:8181  #

        OLD_BARSHOP: http://10.34.52.162:8083
        JF_SWITCH_URL: http://10.34.57.48
        JF_ORDER_URL: http://pay.cbarpay.com
        JF_ORDERQUERY_URL: http://10.34.57.48
        JF_MD5KEY: gameplaza#0013
        VALID_URL: "http://10.34.52.184,http://10.34.41.59:8000,http://10.34.60.127:8088,http://barshopb.wxdesk.com,chrome-devtools,hm.baidu.com,10.34.41.18"

        USER_CENTER_URL: http://{{ip_addrs.nginx_backend}}:8087

        USER_CENTER_CACHE_REDIS: redis://:redis123@{{ip_addrs.user_center_redis.ip}}:{{ip_addrs.user_center_redis.port}}/{{ip_addrs.user_center_redis.db}}  # 用户中心redis
        SERVICE_HOST: http://barshopb.wxdesk.com
