{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set voice_celery_redis = ip_addrs.voice_celery_redis %}
WxVoiceMaster:
  web:
    settings:
      js_auth_url: http://fop.sicent.com/cxf/http/service/data/idcservice
      pb_auth_url: http://fop.sicent.com/cxf/http/service/data/idcservice
      policy_data_server_url: http://172.30.236.78:8080/policytasks
      yunying_platform_notice_url: http://172.30.236.96:8088/interface/get_voicenotice.php
      voice_api_url: /server-setting.html
      voice_api_urgency: True
