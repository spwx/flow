{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set voice_celery_redis = ip_addrs.voice_celery_redis %}
WxVoiceMaster:
  web:
    install_path: /data/SicentWebserver/desktop/WxVoiceMaster/web
    log_dir: /data/SicentWebserver/desktop/WxVoiceMaster/web/logs
    settings:
      web_host: 0.0.0.0
      web_port: 22121
      web_works: 4
      web_log_dir: /data/SicentWebserver/desktop/WxVoiceMaster/web/logs/voice_web
      interface_host: 0.0.0.0
      interface_port: 22122
      interface_works: 4
      interface_log_dir: /data/SicentWebserver/desktop/WxVoiceMaster/web/logs/voice_interface
      js_auth_url: http://10.34.51.51:8181/cxf/http/service/data/idcservice
      pb_auth_url: http://10.34.45.12:8181/cxf/http/service/data/idcservice
      policy_data_server_url: http://10.34.53.172:8081/wxdesktop/loldesktopui/1.0
      yunying_platform_notice_url: http://10.34.53.178:8088/interface/get_voicenotice.php
      voice_api_url: /server-setting.html
      voice_api_urgency: True
      voice_home_url: http://publicity.wxdesk.com/voice-old/index.htm
      voice_home_v1_url: http://publicity.wxdesk.com/voice/index.htm

      auth_center_url: http://auth.wxdesk.com