{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set wx_token_redis = ip_addrs.get('wx_token_redis', {}) %}

wxgameplaza:
  web:
    daemonize: /data/SicentWebserver/wxgameplaza/web/logs/wxgameplaza_web.log
    settings:
      DEBUG: True
      LOG_DEBUG: True
      VOICE_ADDRESS:
        ip: 10.34.60.19
        port: 5672
        username: sicent
        password: sicent
        vhost: sicent_vhost
        routingkey: longsystem
      OPERATION_PLATFORM_HOST: 10.34.53.178:8088
      LOG_ADDRESS: 
        - __: overwrite
        - '10.34.60.123'
        - 22002
      ALLOWED_HOSTS: 
        - __: overwrite
        - "*"

      ZZOT_HOST: 10.34.57.48 # 0013.96ni.net # ZZOT is zero zero one three brief
      ZZOT_BAR_PAY_HOST: 10.34.57.48 # testpay.cbarpay.com
      DESK_PROMOTION_ACTIVITY_BEGIN_time: 1498665600 # 6.29
      DESK_PROMOTION_ACTIVITY_END_time: 1499443199 # 17.7.7

      WX_TOKEN_REDIS: redis://:{{ wx_token_redis.password }}@{{ wx_token_redis.ip }}:{{ wx_token_redis.port }}/0

      APP_ID: wxd3592354f0dde0c5
      APP_SECRET: 773ab309e1fc2948411945083b6fc12b

  celeryd:
    queue: schedule
