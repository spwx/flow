{% set ip_addrs = stack.get('ip_addrs', {}) %}

wxgameplaza:
  web:
    daemonize: /data/SicentWebserver/wxgameplaza/web/logs/wxgameplaza_web.log
    settings:
      DEBUG: True
      LOG_DEBUG: True
      VOICE_ADDRESS:
        ip: 10.34.60.19
        port: 5672
        username: sicent
        password: sicent
        vhost: sicent_vhost
        routingkey: longsystem
      OPERATION_PLATFORM_HOST: 10.34.53.178:8088
      LOG_ADDRESS: 
        - __: overwrite
        - '10.34.52.174'
        - 22002
      ALLOWED_HOSTS: 
        - __: overwrite
        - "*"
