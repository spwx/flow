{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set gobeat = stack.get('gobeat', {}) %}
{% set wx_token_redis = ip_addrs.get('wx_token_redis', {}) %}
{% set gameplaza_celery_redis = ip_addrs.get('gameplaza_celery_redis', {}) %}

wxgameplaza:
  web:
    install_path: /data/SicentWebserver/wxgameplaza/web
    log_dir: /data/SicentWebserver/wxgameplaza/web/logs
    settings:
      BROKER_URL: redis://:{{ gameplaza_celery_redis.password }}@{{ gameplaza_celery_redis.ip }}:{{ gameplaza_celery_redis.port }}/0
      CELERY_RESULT_BACKEND: redis://:{{ gameplaza_celery_redis.password }}@{{ gameplaza_celery_redis.ip }}:{{ gameplaza_celery_redis.port }}/0
      CELERY_REDIS_SCHEDULER_URL: redis://:{{ gameplaza_celery_redis.password }}@{{ gameplaza_celery_redis.ip }}:{{ gameplaza_celery_redis.port }}/1
      LOG_ADDRESS: ['221.228.80.111', 22002] # environment conf
      VOICE_ADDRESS: # environment conf
        ip: 221.228.80.107
        port: 5672
        username: sicent
        password: sicent
        vhost: sicent_vhost
        routingkey: longsystem
      LOG_DEBUG: False # environment conf
      TOKEN: True
      DEBUG: False # environment conf
      ALLOWED_HOSTS: ["plaza.wxdesk.com", "221.228.80.71", "172.30.236.102"]  # default enable dev env host ip
      ENABLE_UWSGI: False
      # environment conf
      OPERATION_PLATFORM_HOST: 172.30.236.96:8088  #  plaza get ad and announcement info from operation platform
      ANNOUNCE_AD_CACHE_TIME: 300
      ZABBIX_SETTINGS:
          worker_status: "/var/run/celery/worker_status"
          task_status: "/var/run/celery/task_status"
      ZZOT_HOST: 0013.96ni.net # ZZOT is zero zero one three brief
      ZZOT_PAY_HOST: barpay.back.sw0013.com  # test env is 10.34.57.51:8080
      DESK_PROMOTION_ACTIVITY_BEGIN_time: 1498665600 # 6.29
      DESK_PROMOTION_ACTIVITY_END_time: 1499443199 # 17.7.7
      WX_TOKEN_REDIS: redis://:{{ wx_token_redis.password }}@{{ wx_token_redis.ip }}:{{ wx_token_redis.port }}/0
      APP_ID: wxb9356136dc4f65cf
      APP_SECRET: 22d6fb2cdb7ba0c4b9ee44faed2b7c27
      LEAGUE_BEGIN_TIME: 1512522000
      LEAGUE_END_TIME: 1515081540
      GOBEAT_HOST: {{gobeat.IP}}:{{gobeat.PORT}}
