{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set gologstat_redis = ip_addrs.gologstat_redis %}

gologstat:
  settings:
    redis_url: redis://:{{ gologstat_redis.password }}@{{ gologstat_redis.ip }}:{{ gologstat_redis.port }}/{{ gologstat_redis.db }}
    GOLOG_MONGODB_URL: mongodb://log:log123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/log