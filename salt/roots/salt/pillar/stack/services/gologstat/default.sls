{% set gologstat_configs = stack.get('gologstat_configs', {}) %}

{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set gologstat_redis = ip_addrs.gologstat_redis %}

gologstat:
  web:
    install_path: /data/SicentWebserver/desktop/gologstat

  settings:
    debug: False
    #缓存配置
    redis_url: redis://:{{ gologstat_redis.password }}@{{ gologstat_redis.ip }}:{{ gologstat_redis.port }}/{{ gologstat_redis.db }}

    #服务ip端口
    app:
      host: 0.0.0.0
      port: 8071
      workers: 1

    WXDESK_MONGODB_URL: mongodb://gamedesk:desk123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/gamedesk
    VOICE_MONGODB_URL: mongodb://voicemaster:voice123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/voice
    LWSM_MONGODB_URL: mongodb://lwsm:lwsm123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/lwsm
    MISC_MONGODB_URL: mongodb://misc:misc123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/misc
    GAMEPLAZA_MONGODB_URL: mongodb://gameplaza:plaza123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/gameplaza
    GOLOG_MONGODB_URL: mongodb://{{ ip_addrs.mongo_configsvr03 }}:3306/log


