{% set ip_addrs = stack.get('ip_addrs', {}) %}

{% set gm_celery_broker_redis = ip_addrs.gamemenu_celery_broker_redis %}
{% set gm_celery_result_redis = ip_addrs.gamemenu_celery_result_redis %}

WxGameMenu:
  celeryd:
    install_path: /data/SicentWebserver/desktop/WxGameMenu/celeryd
    log_dir: /data/SicentWebserver/desktop/WxGameMenu/celeryd/logs
  celerybeat:
    install_path: /data/SicentWebserver/desktop/WxGameMenu/celerybeat
    log_dir: /data/SicentWebserver/desktop/WxGameMenu/celerybeat/logs
  settings:
    GAMEMENU_CELERY_BROKER_URL: redis://:{{ gm_celery_broker_redis.password }}@{{ gm_celery_broker_redis.ip }}:{{ gm_celery_broker_redis.port }}/{{ gm_celery_broker_redis.db }}
    GAMEMENU_CELERY_RESULT_URL: redis://:{{ gm_celery_result_redis.password }}@{{ gm_celery_result_redis.ip }}:{{ gm_celery_result_redis.port }}/{{ gm_celery_result_redis.db }}
    SHUNWANG_MENU_HOST: "autopm.icafe28.com" 
    USER_NAME: "10001|"
    PASSWORD: "OWY0M2E4YNWYwLTllZjktMDYwOTQzZjQzNDE2LTEwMDAxFLYSHUNWANG"
    MEDIA_ROOT: "/data/wximage/client/media"
    WXDESK_MONGODB_URL: mongodb://gamedesk:desk123@127.0.0.1:{{ ip_addrs.mongos.port }}/gamedesk
