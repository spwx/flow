{% set ip_addrs = stack.get('ip_addrs', {}) %}
sentry:
  sentry_redis:
    ip: {{ ip_addrs.sentry_redis.ip }}
    port: {{ ip_addrs.sentry_redis.port }}
  pg_host: {{ ip_addrs.pg_host }}
