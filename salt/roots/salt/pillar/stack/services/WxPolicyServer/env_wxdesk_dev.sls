{% set ip_addrs = stack.get('ip_addrs', {}) %}

{% set pm_celery_broker_redis = ip_addrs.policymgr_celery_broker_redis %}
{% set pm_celery_result_redis = ip_addrs.policymgr_celery_result_redis %}
{% set policy_redis = ip_addrs.policy_redis %}
{% set policymgr_redis = ip_addrs.policymgr_redis %}

wxpolicy:
  web:
    install_path: /data/SicentWebserver/desktop/wxpolicy/web
  celerybeat:
    install_path: /data/SicentWebserver/desktop/wxpolicy/celerybeat
  celeryd: 
    install_path: /data/SicentWebserver/desktop/wxpolicy/celeryd
  policyinit: 
    install_path: /data/SicentWebserver/desktop/wxpolicy/policyinit
  scheduler:
    install_path: /data/SicentWebserver/desktop/wxpolicy/shcduler
  logmgr: 
    install_path: /data/SicentWebserver/desktop/wxpolicy/logmgr
    log_dir: /data/SicentWebserver/desktop/wxpolicy/logs
  settings:
    web_host: 0.0.0.0
    web_port: 8080
    web_works: 2
    logmgr_dir: /data/SicentWebserver/desktop/wxpolicy/logs
    logmgr_backup_dir: /data/SicentWebserver/desktop/wxpolicy/logs/backup
    log_endpoint: tcp://{{ ip_addrs.WxPolicyServer }}:5000

    policy_mongodb_url: mongodb://policys:policy123@127.0.0.1:{{ ip_addrs.mongos.port }}/policys
    policy_redis_url: redis://:{{ policy_redis.password }}@{{ policy_redis.ip }}:{{ policy_redis.port }}/{{ policy_redis.db }}
    policymgr_redis_url: redis://:{{ policymgr_redis.password }}@{{ policymgr_redis.ip }}:{{ policymgr_redis.port }}/{{ policymgr_redis.db }}
    celery_broker_url: redis://:{{ pm_celery_broker_redis.password }}@{{ pm_celery_broker_redis.ip }}:{{ pm_celery_broker_redis.port }}/{{ pm_celery_broker_redis.db }}
    celery_result_url: redis://:{{ pm_celery_result_redis.password }}@{{ pm_celery_result_redis.ip }}:{{ pm_celery_result_redis.port }}/{{ pm_celery_result_redis.db }}


  accountsync:
    install_path: /data/SicentWebserver/desktop/accountsync
    settings:
      policy_host: 127.0.0.1
      policy_port: 8080
      log_dir: /data/SicentWebserver/desktop/accountsync/logs
      log_backup_dir: /data/SicentWebserver/desktop/wxpolicy/logs/backup
