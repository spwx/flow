{% set desktop_account_configs = stack.get('desktop_account_configs', {}) %}

{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set user_center_redis = ip_addrs.user_center_redis %}

wxdesktop_account:
  web:
    install_path: /data/SicentWebserver/desktop/wxdesktop_account

  settings:
    #缓存配置
    redis_url: redis://:{{ user_center_redis.password }}@{{ user_center_redis.ip }}:{{ user_center_redis.port }}/{{ user_center_redis.db }}

    #数据库配置
    motor_url: mongodb://account:account123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/account
    mongo_db: account

    #启动sanic应用的参数
    app:
      host: 0.0.0.0
      port: 22115
      workers: 8

    #是否开启keep_alive
    KEEP_ALIVE: false

    #数据库迁移配置信息(仅在首次数据库迁移时使用)
    barshop_mysql:
        host: {{ desktop_account_configs.barshop_mysql_host }}
        user: {{ desktop_account_configs.barshop_mysql_user }}
        password: {{ desktop_account_configs.barshop_mysql_password }}
        port: {{ desktop_account_configs.barshop_mysql_port }}
        database: {{ desktop_account_configs.barshop_mysql_database }}
        charset: utf8

    #兼容老版本凡商超市的业务
    old_barshop_manager_url: {{ desktop_account_configs.old_barshop_manager_url }}
    old_barshop_web_url: {{ desktop_account_configs.old_barshop_web_url }}
    lwsm_web_url: http://{{ ip_addrs.nginx_backend}}:8086

    #计费那边校验接口
    #陈本辉
    sicent_idcservice_host: {{ desktop_account_configs.sicent_idcservice_host }}
    #测试环境
    sicent_querynbidinfo_host: {{ desktop_account_configs.sicent_querynbidinfo_host }}

    #陈本辉
    pubwin_idcservice_host: {{ desktop_account_configs.pubwin_idcservice_host }}
    #测试环境
    pubwin_querynbidinfo_host: {{ desktop_account_configs.pubwin_querynbidinfo_host }}

    callback_verification_url: {{ desktop_account_configs.callback_verification_url }}

    callback_verification_url_pubwin: {{ desktop_account_configs.callback_verification_url_pubwin }}

    #token_expire: 3600
    #账户中心发出请求超时时间
    http_timeout: 3
    token_expire:  604800 # 7 * 24 * 3600
    debug: false

    lwsm_mongo_url: mongodb://lwsm:lwsm123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/lwsm
    lwsm_db: lwsm