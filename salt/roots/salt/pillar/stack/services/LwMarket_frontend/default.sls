LwMarket:
  frontend:
    install_path: /data/SicentWebsite/desktop/LwMarket/frontend
    res_url_maps:
      - url: /client/
        path: frontend/client/
      - url: /control/
        path: frontend/control/
      - url: /control_0/
        path: frontend/control_0/
      - url: /lib/
        path: frontend/lib/