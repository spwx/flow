{% set goodscenter_configs = stack.get('goodscenter_configs', {}) %}
{% set desktop_account_configs = stack.get('desktop_account_configs', {}) %}

{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set goodscenter_redis = ip_addrs.goodscenter_redis %}

goodscenter:
  web:
    install_path: /data/SicentWebserver/desktop/goodscenter

  settings:
    debug: false

    #缓存配置
    redis_url: redis://:{{ goodscenter_redis.password }}@{{ goodscenter_redis.ip }}:{{ goodscenter_redis.port }}/{{ goodscenter_redis.db }}

    #数据库配置
    motor_url: mongodb://goodscenter:goods123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/goodscenter

    app:
      host: 0.0.0.0
      port: 22120
      workers: 8

    #是否开启keep_alive
    KEEP_ALIVE: false
    # 图片资源服务器地址
    OSS_HOST: {{ goodscenter_configs.oss_host }}
    # 老凡商加载图片资源地址
    IMAGE_URl_HOST: {{ goodscenter_configs.old_barshop_image_url }}
    # 老凡商数据库地址
    barshop_mysql:
        host: {{ desktop_account_configs.barshop_mysql_host }}
        user: {{ desktop_account_configs.barshop_mysql_user }}
        password: {{ desktop_account_configs.barshop_mysql_password }}
        port: {{ desktop_account_configs.barshop_mysql_port }}
        database: {{ desktop_account_configs.barshop_mysql_database }}
        charset: utf8
