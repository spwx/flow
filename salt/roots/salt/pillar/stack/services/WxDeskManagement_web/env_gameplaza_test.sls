WxDeskManagement:
  web:
    disable_logging: false
    settings:
      CALLBACK_VERIFICATION_URL: http://10.34.44.33/QddCheck.do
      CALLBACK_VERIFICATION_URL_PUBWIN: http://10.34.45.22/QddCheck.do
      CHAIN_BAR_URL_PUBWIN: http://10.34.45.26:8081/cxf/http/service/data/childAcc
      CHAIN_BAR_URL: http://10.34.50.62:8081/cxf/http/service/data/childAcc
      LOG_DEBUG: True
      DEBUG: True
      ALLOWED_HOSTS:
        - __: overwrite
        - '*'
      WINDOWS_ICON_URL: http://ico.wxdesk.com:8080/images/?snbid=%(snbid)s
      APP_ROOT_MENU_URL: http://ico.wxdesk.com:8080/game_menu_code/?snbid=%(snbid)s
      APP_MENU_CATEGORY_URL: http://ico.wxdesk.com:22501/adm/%(menu)s
      APP_MENU_LIST_URL: http://ico.wxdesk.com:22501/adm/%(menu)s/%(category)s
      APP_MENU_URL: http://ico.wxdesk.com:22501/adm/%(menu)s/%(category)s/%(app)s/%(version)s
      policedataserver:
          host: 10.34.53.172
          port: 8081
          loginui_url: /wxdesktop/loginui/1.0
          loldesktopui_url: /wxdesktop/loldesktopui/1.0
      MAIN_URL: "/"
