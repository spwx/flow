WxDeskManagement:
  web:
    disable_logging: false
    settings:
      CALLBACK_VERIFICATION_URL: http://10.34.57.67/QddCheck.do
      CALLBACK_VERIFICATION_URL_PUBWIN: http://10.34.45.22/QddCheck.do
      CHAIN_BAR_URL_PUBWIN: http://10.34.45.24/cxf/http/service/data/childAcc
      CHAIN_BAR_URL: http://10.34.57.68:8081/cxf/http/service/data/childAcc
      LOG_DEBUG: True
      DEBUG: True
      ALLOWED_HOSTS:
        - __: overwrite
        - '*'
      #测试环境需要配置hosts(10.34.53.177 ico.wxdesk.com  10.34.53.177 ico.wxdesk.com)
      WINDOWS_ICON_URL: http://ico.wxdesk.com:8080/images/?snbid=%(snbid)s
      APP_ROOT_MENU_URL: http://ico.wxdesk.com:8080/game_menu_code/?snbid=%(snbid)s
      APP_MENU_CATEGORY_URL: http://ico.wxdesk.com:22501/adm/%(menu)s
      APP_MENU_LIST_URL: http://ico.wxdesk.com:22501/adm/%(menu)s/%(category)s
      APP_MENU_URL: http://ico.wxdesk.com:22501/adm/%(menu)s/%(category)s/%(app)s/%(version)s
      policedataserver:
          __: overwrite
          host: 10.34.60.127
          port: 8080
          policy_url: /policytasks
          loginui_uri: /wxdesktop/loginui/1.0
          loldesktopui_uri: /wxdesktop/loldesktopui/1.0
      DOMAIN: http://10.34.60.122:8081
      MAIN_URL: "/"
      BAR_SHOP_HOST: 10.34.49.199:8083
      LWSM_HOST: 10.34.60.122:8086 # online host lwsm.wxdesk.com
      LWSM_ORDER_REDIS: redis://:redis123@10.34.60.122:22212/3  # 超市订单分布式锁redis
      GOODS_CENTER_HOST: 10.34.60.125:8090