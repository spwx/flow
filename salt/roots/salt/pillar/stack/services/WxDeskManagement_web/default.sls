{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set lwsm_order_redis = ip_addrs.get('lwsm_order_redis', {}) %}
{% set cache_redis = ip_addrs.cache_redis %}
WxDeskManagement:
  web:
    install_path: /data/SicentWebserver/desktop/WxDeskManagement/web
    log_dir: /data/SicentWebserver/desktop/WxDeskManagement/web/logs
    daemonize: /data/SicentWebserver/desktop/WxDeskManagement/web/logs/management.log
    port: 22131
    settings:
      CALLBACK_VERIFICATION_URL: http://qian.sicent.com/QddCheck.do
      CALLBACK_VERIFICATION_URL_PUBWIN: http://qian.pubwinol.com/QddCheck.do
      CHAIN_BAR_URL_PUBWIN: http://122.224.184.21/qddsmx/cxf/http/service/data/childAcc
      CHAIN_BAR_URL: http://webapi.sicent.com/qddsmx/cxf/http/service/data/childAcc
      LOG_DEBUG: False
      CACHE_REDIS: redis://:{{ cache_redis.password }}@{{ cache_redis.ip }}:{{ cache_redis.port }}/{{ cache_redis.db }}
      DEBUG: False
      ENABLE_UWSGI: False
      ALLOWED_HOSTS: ["qdd.wxdesk.com", "221.228.80.71", "221.228.80.83", "172.30.236.102", "172.30.236.83"]  # default enable dev env host ip
      MEDIA_ROOT: /data/wximage/client
      WINDOWS_ICON_URL: http://ico.wxdesk.com:8080/images/?snbid=%(snbid)s
      APP_ROOT_MENU_URL: http://ico.wxdesk.com:8080/game_menu_code/?snbid=%(snbid)s
      APP_MENU_CATEGORY_URL: http://ico.wxdesk.com:22501/adm/%(menu)s
      APP_MENU_LIST_URL: http://ico.wxdesk.com:22501/adm/%(menu)s/%(category)s
      APP_MENU_URL: http://ico.wxdesk.com:22501/adm/%(menu)s/%(category)s/%(app)s/%(version)s
      policedataserver:
          host: 172.30.236.78
          port: 8080
          policy_url: /policytasks
          loginui_uri: /wxdesktop/loginui/1.0
          loldesktopui_uri: /wxdesktop/loldesktopui/1.0
      MAIN_URL: "/"
      BAR_SHOP_HOST: barshop.16288.cn
      LWSM_HOST: lwsm.wxdesk.com
      LWSM_ORDER_REDIS: redis://:{{ lwsm_order_redis.password }}@{{ lwsm_order_redis.ip }}:{{ lwsm_order_redis.port }}/{{ lwsm_order_redis.db }}
      DOMAIN: http://qdd.wxdesk.com
      js_auth_url: http://fop.sicent.com/cxf/http/service/data/idcservice
      pb_auth_url: http://fop.pubwinol.com/cxf/http/service/data/idcservice
      GOODS_CENTER_HOST: {{ ip_addrs.nginx_backend }}:8090