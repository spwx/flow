{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set egs_configs = stack.get('egs_configs', {}) %}
{% set egs_redis = ip_addrs.get('egs_redis', {}) %}
{% set egs_collect_redis = ip_addrs.get('egs_collect_redis', {}) %}

egs:
  settings:
    debug: True
    ACCESSKEYID: "LTAIxSn2wjy6TJHV"
    ACCESSKEYSECRET: "gR03zhGnr8frgN233udYdOov8NoQNl"
    OSS_END_POINT: "oss-cn-shenzhen.aliyuncs.com"
    EGS_REDIS_URL: redis://:{{ egs_redis.password }}@{{ egs_redis.ip }}:{{ egs_redis.port }}/{{ egs_redis.db }}
    EGS_COLLECT_APP_URL: redis://:{{ egs_collect_redis.password }}@{{ egs_collect_redis.ip }}:{{ egs_collect_redis.port }}/{{ egs_collect_redis.db }}
    ENCRYPT: False
