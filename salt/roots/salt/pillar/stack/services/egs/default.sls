{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set egs_configs = stack.get('egs_configs', {}) %}
{% set egs_redis = ip_addrs.get('egs_redis', {}) %}
{% set egs_collect_redis = ip_addrs.get('egs_collect_redis', {}) %}


egs:
  web:
    install_path: /data/SicentWebserver/desktop/egs

  settings:

    HOST: 0.0.0.0
    PORT: 22227
    WORKER: 8
    DEBUG: FALSE
    RESPONSE_TIMEOUT: 15
    REQUEST_TIMEOUT: 5
    EGSDB_URL: mongodb://egs:egs123@{{ ip_addrs.mongos.ip }}:{{ ip_addrs.mongos.port }}/egs

    EGS_REDIS_URL: redis://:{{ egs_redis.password }}@{{ egs_redis.ip }}:{{ egs_redis.port }}/0
    EGS_COLLECT_APP_URL: redis://:{{ egs_collect_redis.password }}@{{ egs_collect_redis.ip }}:{{ egs_collect_redis.port }}/0
    ENCRYPT: True
    EGS_EXPIRE: 300

    ACCESSKEYID: "LTAIxSn2wjy6TJHV"
    ACCESSKEYSECRET: "gR03zhGnr8frgN233udYdOov8NoQNl"
    OSS_END_POINT: "oss-cn-shenzhen.aliyuncs.com"