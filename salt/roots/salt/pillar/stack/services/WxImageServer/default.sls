{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxImageServer:
  web:
    install_path: /data/SicentWebserver/desktop/WxImageServer/web
    log_dir: /data/SicentWebserver/desktop/WxImageServer/web/oss/logs
    settings:
      HOST: 127.0.0.1
      PORT: 8000
      WORKER: 4
      DEBUG: False
      OSS_MONGO_URL: mongodb://oss:oss123@127.0.0.1:{{ ip_addrs.mongos.port }}/oss # mongodb url
      OSS_IMAGE_PATH: oss/image/
      OSS_ROOT_PATH: /data/wximage/server
      OSS_CDN_HOST: uploadqdd.wxdesk.com # online env host uploadqdd.wxdesk.com
