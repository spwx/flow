dns:
  sicent:
    - 10.34.37.10
    - 10.34.37.11

ntp:
  servers:
    - stdtime.gov.hk

ip_addrs:
  jenkins_master: 10.34.41.191 # build.nextbu.cn
  zabbix: 10.34.52.196 # zabbix.nextbu.cn
  redmine: 10.34.52.123
  redmine_test: 10.34.52.124
  graylog01: 10.34.52.126
  elasticsearch01: 10.34.52.124
  elasticsearch02: 10.34.52.125
