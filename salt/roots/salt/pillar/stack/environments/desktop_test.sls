dns:
  sicent:
    - 10.34.37.10
    - 10.34.37.11

ntp:
  servers:
    - stdtime.gov.hk

ip_addrs:
  LogRecordServer: 10.34.41.85
  PolicyDataServer: 10.34.53.2
  openresty: 10.34.53.177

log_artifact:
  templogs:
    artifact_ftp: artifact.nextbu.cn
    ftp_user: tmp_gameplaza
    ftp_pass: gameplaza!tmp
    artifact_http: http://files.dev.js/TempLogs/desktop/
  everydaylogs:
    artifact_ftp: ftp://artifact.nextbu.cn
    ftp_user: gameplaza
    ftp_pass: gameplaza!log
    artifact_http: http://files.dev.js/EverydayLogs/desktop/
