ntp:
  servers:
    - t2.swomc.net

ip_addrs:
  mongos:
    ip: 127.0.0.1
    port: 3306
  mongo_configsvr01: 172.30.238.117
  mongo_configsvr02: 172.30.238.118
  mongo_configsvr03: 172.30.238.119
  mongo_configsvr05: 172.30.238.121
  mongo_configsvr06: 172.30.238.122
  mongo_shardsvr02_p: 172.30.238.118
  mongo_shardsvr03_p: 172.30.238.119
  mongo_shardsvr04_p: 172.30.238.119
  mongo_shardsvr05_p: 172.30.238.121
  mongo_shardsvr06_p: 172.30.238.122

  mongo02_configsvr01: 172.30.238.123 # js.yewu.gameplaza.prod.mongo_cluster01
  mongo02_configsvr02: 172.30.238.124 # js.yewu.gameplaza.prod.mongo_cluster02
  mongo02_configsvr03: 172.30.238.136

  mongo02_shardsvr01_p: 172.30.238.123
  mongo02_shardsvr01_s1: 172.30.238.124
  mongo02_shardsvr02_p: 172.30.238.124
  mongo02_shardsvr02_s1: 172.30.238.123

  mongo02_arbiter01: 172.30.238.136
  mongo02_arbiter02: 172.30.238.136


  wxgameplaza_httpagent: 221.228.80.109 # blue(8081), green(8082)

  wxgameplaza_web01: 172.30.236.131  # web_blue minion_id: gameplaza_web01
  wxgameplaza_web02: 172.30.236.132  # web_blue minion_id: gameplaza_web02
  wxgameplaza_web03: 172.30.236.133  # web_blue minion_id: gameplaza_web03
  wxgameplaza_web04: 172.30.236.134  # web_blue minion_id: gameplaza_web04
  wxgameplaza_web05: 172.30.236.135  # web_blue minion_id: gameplaza_web05
  wxgameplaza_web06: 172.30.236.136  # web_blue minion_id: gameplaza_web06
  wxgameplaza_web07: 172.30.236.170  # web_blue minion_id: gameplaza_web07
  wxgameplaza_web08: 172.30.236.156  # web_blue minion_id: gameplaza_web08
  wxgameplaza_web09: 172.30.236.157  # web_blue minion_id: gameplaza_web09

  wxgameplaza_web11: 172.30.236.145  # web_green minion_id: gameplaza_web11
  wxgameplaza_web12: 172.30.236.146  # web_green minion_id: gameplaza_web12
  wxgameplaza_web13: 172.30.236.171  # web_green minion_id: gameplaza_web13
  wxgameplaza_web14: 172.30.236.172  # web_green minion_id: gameplaza_web14
  wxgameplaza_web15: 172.30.236.151  # web_green minion_id: gameplaza_web15
  wxgameplaza_web16: 172.30.236.152  # web_green minion_id: gameplaza_web16
  wxgameplaza_web17: 172.30.236.173  # web_green minion_id: gameplaza_web17
  wxgameplaza_web18: 172.30.236.158  # web_green minion_id: gameplaza_web18
  wxgameplaza_web19: 172.30.236.159  # web_green minion_id: gameplaza_web19


  wxgameplaza_celerybeat: 172.30.236.131
  wxgameplaza_celeryd_schedule: 172.30.236.131
  wxgameplaza_celeryd_default: 172.30.236.132
  wxgameplaza_celeryd_zabbix: 172.30.236.132

  wxgameplaza_handler_blue01: 172.30.236.110 # + blue minion_id: gameplaza_handler09
  wxgameplaza_handler_blue02: 172.30.236.140 # + blue minion_id: gameplaza_handler06
  wxgameplaza_handler_green01: 172.30.236.147  # green minion_id: gameplaza_web01
  wxgameplaza_handler_green02: 172.30.236.148  # green minion_id: gameplaza_web02
  wxgameplaza_handler_green03: 172.30.236.203 # + green minion_id: gameplaza_handler08

  WxVoiceMaster_web01: 172.30.236.208 # WxVoiceMaster_web01, web, api, celerybeat, celeryd
  WxVoiceMaster_web02:  172.30.236.209 # WxVoiceMaster_web02 web,api, celeryd
  WxDeskManagement_web01: 172.30.236.208 # WxVoiceMaster_web01 web
  WxDeskManagement_web02: 172.30.236.209 # WxVoiceMaster_web02 web
  WxImageServer: 172.30.236.210
  WxAccountCenter: 172.30.236.207
  PolicyDataServer: 172.30.236.206
  WxDesktopAccount_web01: 172.30.236.57
  WxDesktopAccount_web02: 172.30.236.49  #TODO服务器还没有申请下来，申请下来这儿需要根据实际情况进行修改

  WxPolicyServer: 172.30.236.78
  WxPolicyWorker1: 172.30.236.77
  WxPolicyWorker2: 172.30.236.76
  PolicyRedis: 172.30.236.98

  LwMarket_web01: 172.30.236.61
  LwMarket_web02: 172.30.236.62

  GoodsCenter_web01: 172.30.236.61
  GoodsCenter_web02: 172.30.236.62

  nginx_frontend: 221.228.80.108
  nginx_backend_wlan: 221.228.80.71
  nginx_backend: 172.30.236.102
  nginx02_backend_wlan: 221.228.80.83
  nginx02_backend: 172.30.236.83
  nginx_backend_wlan_164: 221.228.80.15
  nginx_backend_164: 172.30.236.164
  internginx: 172.30.236.211
  internginx_wlan: 221.228.80.14
  redis_cluster01: 172.30.236.126
  redis_cluster02: 172.30.236.127
  logsrv_nginx_wlan: 221.228.80.11
  logsrv_nginx: 172.30.236.178


  desktop_platform_interface: http://172.30.236.96:8088/interface
  statscenter_upstream:  # 统计中心 upstream
    - host: 172.30.236.49
      port: 22125
      weight: 1
    - host: 172.30.236.57
      port: 22125
      weight: 1

  usercenter_upstream:  
    - host: 172.30.236.190
      port: 22126
      weight: 1
    - host: 172.30.236.191
      port: 22126
      weight: 1

  egs_upstream:  # egs upstream
    - host: 172.30.236.160
      port: 22227
      weight: 1
    - host: 172.30.236.161
      port: 22227
      weight: 1
    - host: 172.30.236.162
      port: 22227
      weight: 1
    - host: 172.30.236.163
      port: 22227
      weight: 1



  # redis service
  # account redis server: 2
  # default use 0/1, other db start index: 6
  accountcenter_celery_redis:
    ip: 172.30.236.126
    port: 22211
    password: redis123

  # gamemenu redis: 2
  gamemenu_celery_broker_redis:
    ip: 172.30.236.126
    port: 22211
    db: 6
    password: redis123

  gamemenu_celery_result_redis:
    ip: 172.30.236.126
    port: 22211
    db: 7
    password: redis123
  # end redis gamemenu

  # accountcenter redis: 2
  ac_account_redis:
    ip: 172.30.236.126
    port: 22212
    db: 0
    password: redis123

  ac_account_other_redis:
    ip: 172.30.236.126
    port: 22212
    db: 1
    password: redis123

  # voice celery redis server: 2
  voice_celery_redis:
    ip: 172.30.236.126
    port: 22213
    password: redis123

  voice_redis:
    ip: 172.30.236.126
    port: 22214
    db: 0
    password: redis123

  # gameplaza celery redis server: 2
  gameplaza_celery_redis:
    ip: 172.30.236.126
    port: 22215
    password: redis123

  # deskmgr redis: 1
  cache_redis:
    ip: 172.30.236.126
    port: 22216
    db: 0
    password: redis123

  # deskmgr redis: 1
  gologstat_redis:
    ip: 172.30.236.126
    port: 22216
    db: 1
    password: redis123


  lottery_redis:
    ip: 172.30.236.126
    port: 22217
    db: 0
    password: redis123

  lwsm_order_redis:
    ip: 172.30.236.126
    port: 22218
    db: 0
    password: redis123

  # WxPolicyServer redis: 4
  policy_redis:
    ip: 172.30.236.76  # not on 126
    port: 22219
    db: 0
    password: redis123

  policymgr_celery_broker_redis:
    ip: 172.30.236.126
    port: 22220
    db: 0
    password: redis123

  policymgr_celery_result_redis:
    ip: 172.30.236.126
    port: 22220
    db: 1
    password: redis123

  policymgr_redis:
    ip: 172.30.236.126
    port: 22220
    db: 2
    password: redis123
  # WxPolicyServer redis: end


  wx_token_redis:
    ip: 172.30.236.126
    port: 22221
    db: 0
    password: redis123

# user center redis
  user_center_redis:
    ip: 172.30.236.126
    port: 22222
    db: 0
    password: redis123


  award_share_redis:
    ip: 172.30.236.126
    port: 22222
    db: 1
    password: redis123

  first_game_redis:
    ip: 172.30.236.126
    port: 22222
    db: 2
    password: redis123

  goodscenter_redis:
    ip: 172.30.236.126
    port: 22222
    db: 3
    password: redis123

  statscenter_redis:
    ip: 172.30.236.126
    port: 22222
    db: 4
    password: redis123

  user_info_center_redis:
    ip: 172.30.236.126
    port: 22222
    db: 5
    password: redis123



  egs_redis:
    ip: 172.30.236.126
    port: 22223
    db: 0
    password: redis123

  egs_collect_redis:
    ip: 172.30.236.126
    port: 22224
    db: 0
    password: redis123


  # deskcomponent redis: 14
  stage_activity_redis:
    ip: 172.30.236.127
    port: 22211
    db: 0
    password: redis123
  reward_activity_redis:
    ip: 172.30.236.127
    port: 22212
    db: 0
    password: redis123
  head_activity_redis:
    ip: 172.30.236.127
    port: 22213
    db: 0
    password: redis123
  damage_activity_redis:
    ip: 172.30.236.127
    port: 22214
    db: 0
    password: redis123
  continuous_victory_activity_redis:
    ip: 172.30.236.127
    port: 22215
    db: 0
    password: redis123
  score_redis:
    ip: 172.30.236.127
    port: 22216
    db: 0
    password: redis123
  statistic_redis:
    ip: 172.30.236.127
    port: 22217
    db: 0
    password: redis123
  bar_redis:
    ip: 172.30.236.127
    port: 22218
    db: 0
    password: redis123
  ranker_player_redis:
    ip: 172.30.236.127
    port: 22219
    db: 0
    password: redis123
  reduce_stress_redis:
    ip: 172.30.236.127
    port: 22220
    db: 0
    password: redis123
  ad_announcement_redis:
    ip: 172.30.236.127
    port: 22221
    db: 0
    password: redis123
  lock_key_redis:
    ip: 172.30.236.127
    port: 22222
    db: 0
    password: redis123
  pay_notice_redis:
    ip: 172.30.236.127
    port: 22223
    db: 0
    password: redis123
  bar_player_uploadtime_redis:
    ip: 172.30.236.127
    port: 22224
    db: 0
    password: redis123

  daily_task_redis:
    ip: 172.30.236.127
    port: 22225
    db: 0
    password: redis123

  pubg_activity_redis:
    ip: 172.30.236.127
    port: 22226
    db: 0
    password: redis123

  league_draw_lottery_redis:
    ip: 172.30.236.127
    port: 22226
    db: 1
    password: redis123
  
  # end redis cluster

  policy_slaveof1_redis:
    ip: 172.30.236.98
    port: 22211
    db: 0
    password: redis123

  policy_slaveof2_redis:
    ip: 172.30.236.98
    port: 22212
    db: 0
    password: redis123

  # end redis cluster

# long connect system routing redis
  lcs_routing_redis:
    ip: 127.0.0.1
    port: 22200
    db: 0
    password: redis123


  DETAIL_CLIENT_IP: 221.228.80.107

  sentry_redis:
    ip: 172.30.236.127
    port: 22226
    db: 1
    password: redis123

  pg_host: 10.34.60.128

log_artifact:
  templogs:
    artifact_ftp: templogs.js-ops.com:2122
    ftp_user: tmp_gameplaza
    ftp_pass: gameplaza!tmp
    artifact_http: http://templogs.js-ops.com:2120/Ad.Logs/gameplaza/
  everydaylogs:
    artifact_ftp: everydaylogs.js-ops.com:2122
    ftp_user: gameplaza
    ftp_pass: gameplaza!log
    artifact_http: http://everydaylogs.js-ops.com:2120/Ad.logs/gameplaza/

desktop_account_configs:  #  online

    barshop_mysql_host: 172.30.238.115
    barshop_mysql_user: '"020"'
    barshop_mysql_password: sicent1216
    barshop_mysql_port: 3306
    barshop_mysql_database: barshopBase

    #兼容老版本凡商超市的业务
    old_barshop_manager_url: http://barshop.16288.cn

    old_barshop_web_url: http://barshop.16288.cn

    lwsm_web_url: http://172.30.236.102:8086

    #计费那边校验接口
    #陈本辉
    sicent_idcservice_host: http://fop.sicent.com
    #测试环境
    sicent_querynbidinfo_host: http://webapi.sicent.com

    #陈本辉
    pubwin_idcservice_host: http://fop.sicent.com
    #测试环境
    pubwin_querynbidinfo_host: http://webapi.pubwinol.com

    callback_verification_url: http://qian.sicent.com/QddCheckToken.do

    callback_verification_url_pubwin: http://qian.pubwinol.com/QddCheckToken.do


goodscenter_configs:
    oss_host: 172.30.236.102:8085 # oss 地址

    old_barshop_image_url: http://barshop.16288.cn/ # 凡商图片地址

statscenter_configs:
    #计费主机地址
    nfe_url: http://fop.sicent.com
    #0013收款码主机地址
    payment_code_0013_url: http://qdd.sw0013.com
    #0钱多多管理后台主机地址
    qdd_manager_url: http://qdd.wxdesk.com

usercenter_configs:
    wx_redirect_url: http://plaza.wxdesk.com/box/user-login-wx.html
    qq_redirect_url: http://plaza.wxdesk.com/box/user-login-qq.html
    bind_url: http://plaza.wxdesk.com/box/user-home.html

gobeat:
  IP: 172.30.236.177
  PORT: 8080