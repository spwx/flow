dns:
  sicent:
    - 10.34.37.10
    - 10.34.37.11

ntp:
  servers:
    - stdtime.gov.hk

ip_addrs:
  salt_master: 192.168.56.11
  nginx: 192.168.56.12
  redis: 192.168.56.12
  mongo: 192.168.56.12
