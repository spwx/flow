dns:
  sicent:
    - 10.34.37.10
    - 10.34.37.11

ntp:
  servers:
    - stdtime.gov.hk

ports:
  WxGamePlaza_web: 22111
  WxVoiceMaster_web: 22121
  WxDeskManagement_web: 22131
  nginx: 80 ~ 8080-8089 gameplaza-web(8083) voicemaster-web(8082) wxdeskmgr-web(8081) voicemaster-api(8084)
  httpagent: 22141
  redis: 22211
  mongo: 22317(27017/3306)
  webagent: 8001

ip_addrs:

  mongos: # minion_id: mongo.test.gameplaza.yewu.js
    ip: 10.34.56.101
    port: 27017
  mongo_configsvr01: 10.34.56.102 # id: mongo_configsvr01.test.gameplaza.yewu.js
  mongo_configsvr02: 10.34.56.103 # id: mongo_configsvr02.test.gameplaza.yewu.js
  mongo_shardsvr01_p: 10.34.56.104 # id: mongo_shardsvr01_p.test.gameplaza.yewu.js
  mongo_shardsvr02_p: 10.34.56.105 # id: mongo_shardsvr02_p.test.gameplaza.yewu.js

  wxgameplaza_web01: 10.34.56.103
  wxgameplaza_httpagent: 10.34.56.103
  wxgameplaza_handler01: 10.34.56.105
  wxgameplaza_celerybeat: 10.34.56.103
  wxgameplaza_celeryd_default: 10.34.56.104
  wxgameplaza_celeryd_schedule: 10.34.56.103

  WxVoiceMaster: 10.34.52.180 # id: WxVoiceMaster.test.desktop.yewu.js
  WxDeskManagement: 10.34.52.182 #id: WxDeskManagement.test.desktop.yewu.js
  WxImageServer: 10.34.52.180
  WxAccountCenter: 10.34.52.181 #id: WxAccountCenter.test.desktop.yewu.js
  PolicyDataServer: 10.34.53.2 #id: Policydataserver.test.desktop.yewu.js
  WxGameMenu: 10.34.52.182

  # nginx servers
  nginx_frontend: 10.34.56.101
  nginx_backend: 10.34.56.107 # id: nginx.test.gameplaza.yewu.js

  # redis service
  # wx redis server
  wx_redis:
    ip: 10.34.56.101
    port: 22211
    password: redis123
  # --//end

  # account redis server
  accountcenter_celery_redis:
    ip: 10.34.52.181
    port: 22211
    password: redis123

  # voice celery redis server: 2
  voice_celery_redis:
    ip: 10.34.56.101
    port: 22212
    password: redis123

  voice_redis:
    ip: 10.34.56.101
    port: 22212
    db: 2
    password: redis123

  # gameplaza celery redis server
  gameplaza_celery_redis:
    ip: 10.34.56.101
    port: 22213
    password: redis123

  # deskcomponent redis: 14
  stage_activity_redis:
    ip: 10.34.56.101
    port: 22211
    db: 0
    password: redis123
  reward_activity_redis:
    ip: 10.34.56.101
    port: 22211
    db: 1
    password: redis123
  head_activity_redis:
    ip: 10.34.56.101
    port: 22211
    db: 2
    password: redis123
  damage_activity_redis:
    ip: 10.34.56.101
    port: 22211
    db: 3
    password: redis123
  continuous_victory_activity_redis:
    ip: 10.34.56.101
    port: 22211
    db: 4
    password: redis123
  score_redis:
    ip: 10.34.56.101
    port: 22211
    db: 5
    password: redis123
  statistic_redis:
    ip: 10.34.56.101
    port: 22211
    db: 6
    password: redis123
  bar_redis:
    ip: 10.34.56.101
    port: 22211
    db: 7
    password: redis123
  ranker_player_redis:
    ip: 10.34.56.101
    port: 22211
    db: 8
    password: redis123
  reduce_stress_redis:
    ip: 10.34.56.101
    port: 22211
    db: 9
    password: redis123
  ad_announcement_redis:
    ip: 10.34.56.101
    port: 22211
    db: 10
    password: redis123
  lock_key_redis:
    ip: 10.34.56.101
    port: 22211
    db: 11
    password: redis123
  pay_notice_redis:
    ip: 10.34.56.101
    port: 22211
    db: 12
    password: redis123
  bar_player_uploadtime_redis:
    ip: 10.34.56.101
    port: 22211
    db: 13
    password: redis123

  # deskmgr redis: 1
  cache_redis:
    ip: 10.34.56.101
    port: 22211
    db: 14
    password: redis123

  # accountcenter redis: 2+2
  ac_account_redis:
    ip: 10.34.52.181
    port: 22211
    db: 3
    password: redis123
  ac_account_other_redis:
    ip: 10.34.52.181
    port: 22211
    db: 4
    password: redis123
  # end redis cluster

  # gamemenu redis: 2
  gamemenu_celery_broker_redis:
    ip: 10.34.56.101
    port: 22213
    db: 2
    password: redis123
  gamemenu_celery_result_redis:
    ip: 10.34.56.101
    port: 22213
    db: 3
    password: redis123
  # end redis gamemenu

  # lottery redis： 1
  lottery_redis:
    ip: 10.34.56.101
    port: 22213
    db: 4
    password: redis123
  # end redis lottery

log_artifact:
  templogs:
    artifact_ftp: artifact.nextbu.cn
    ftp_user: tmp_gameplaza
    ftp_pass: gameplaza!tmp
    artifact_http: http://files.dev.js/TempLogs/gameplaza
  everydaylogs:
    artifact_ftp: ftp://artifact.nextbu.cn
    ftp_user: gameplaza
    ftp_pass: gameplaza!log
    artifact_http: http://files.dev.js/EverydayLogs/gameplaza
