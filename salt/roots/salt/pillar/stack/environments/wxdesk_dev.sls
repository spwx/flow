ntp:
  servers:
    - t2.swomc.net

ip_addrs:
  mongos:
    ip: 10.34.56.104
    port: 3306

  mongo_shard01: 10.34.56.104
  mongo_shard02: 10.34.56.105

  mongo02_configsvr01: 10.34.56.104 # js.yewu.gameplaza.prod.mongo_cluster01
  mongo02_configsvr02: 10.34.56.105  # js.yewu.gameplaza.prod.mongo_cluster02

  mongo_configsvr03: 10.34.56.110


  mongo02_shardsvr01_p: 10.34.56.104
  mongo02_shardsvr01_s1: 10.34.56.105
  mongo02_shardsvr02_p: 10.34.56.105
  mongo02_shardsvr02_s1: 10.34.56.104

  # developer mongo test env
  mongodev_shard01: 10.34.52.177
  mongodev_shard02: 10.34.52.178

  mongodev_configsvr01: 10.34.52.177
  mongodev_configsvr02: 10.34.52.178
  mongodev_configsvr03: 10.34.52.179

  mongodev_shardsvr01_p: 10.34.52.177
  mongodev_shardsvr01_s1: 10.34.52.178
  mongodev_arbiter01: 10.34.52.179

  mongodev_shardsvr02_p: 10.34.52.178
  mongodev_shardsvr02_s1: 10.34.52.177
  mongodev_arbiter02: 10.34.52.179


  wxgameplaza_handler01: 10.34.56.101
  wxgameplaza_web01: 10.34.56.101
  nginx_front: 10.34.56.106
  nginx_backend: 10.34.56.107
  wxgameplaza_handler_blue: 10.34.56.101
  wxgameplaza_handler_green: 10.34.56.102
  WxImageServer: 10.34.52.180
  WxDeskManagement_web01: 10.34.56.101
  WxVoiceMaster_web01: 10.34.56.101
  LwMarket_web01: 10.34.56.101
  WxDesktopAccount_web01: 10.34.56.101
  WxDesktopAccount_web02: 10.34.56.101

  GoodsCenter_web01: 10.34.56.101

  desktop_platform_interface: http://10.34.53.178:8088/interface
  statscenter_upstream:  # 统计中心 upstream
    - host: 10.34.56.101
      port: 22125
      weight: 1
    - host: 10.34.56.101
      port: 22125
      weight: 1

  usercenter_upstream:  # 用户信息中心 upstream
    - host: 10.34.56.101
      port: 22126
      weight: 1
    - host: 10.34.56.101
      port: 22126
      weight: 1

  egs_upstream:  # egs upstream
    - host: 10.34.56.101
      port: 22227
      weight: 1


  # account use test env
  accountcenter_celery_redis:
    ip: 10.34.60.123
    port: 22211
    password: redis123

  ac_account_redis:
    ip: 10.34.60.122
    port: 22212
    db: 0
    password: redis123
  ac_account_other_redis:
    ip: 10.34.60.122
    port: 22212
    db: 1
    password: redis123

  wx_token_redis:
    ip: 10.34.56.103
    port: 6384
    db: 0
    password: redis123

  daily_task_redis:
    ip: 10.34.56.103
    port: 6384
    db: 0
    password: redis123


  # voice celery redis server: 2
  voice_celery_redis:
    ip: 10.34.56.103
    port: 6385
    db: 0
    password: redis123

  voice_redis:
    ip: 10.34.56.103
    port: 6385
    db: 0
    password: redis123

  # gameplaza celery redis server
  gameplaza_celery_redis:
    ip: 10.34.56.103
    port: 6386
    db: 0
    password: redis123

  # gamemenu redis: 2
  gamemenu_celery_broker_redis:
    ip: 10.34.56.103
    port: 6386
    db: 0
    password: redis123
  gamemenu_celery_result_redis:
    ip: 10.34.56.103
    port: 6386
    db: 0
    password: redis123

  # deskcomponent redis: 14
  stage_activity_redis:
    ip: 10.34.56.103
    port: 6387
    db: 0
    password: redis123

  reward_activity_redis:
    ip: 10.34.56.103
    port: 6387
    db: 1
    password: redis123
  head_activity_redis:
    ip: 10.34.56.103
    port: 6387
    db: 2
    password: redis123

  damage_activity_redis:
    ip: 10.34.56.103
    port: 6387
    db: 3
    password: redis123
  continuous_victory_activity_redis:
    ip: 10.34.56.103
    port: 6387
    db: 4
    password: redis123
  score_redis:
    ip: 10.34.56.103
    port: 6387
    db: 5
    password: redis123
  statistic_redis:
    ip: 10.34.56.103
    port: 6387
    db: 6
    password: redis123
  bar_redis:
    ip: 10.34.56.103
    port: 6387
    db: 7
    password: redis123
  ranker_player_redis:
    ip: 10.34.56.103
    port: 6387
    db: 8
    password: redis123
  reduce_stress_redis:
    ip: 10.34.56.103
    port: 6387
    db: 9
    password: redis123
  ad_announcement_redis:
    ip: 10.34.56.103
    port: 6387
    db: 10
    password: redis123
  lock_key_redis:
    ip: 10.34.56.103
    port: 6387
    db: 11
    password: redis123
  pay_notice_redis:
    ip: 10.34.56.103
    port: 6387
    db: 12
    password: redis123
  bar_player_uploadtime_redis:
    ip: 10.34.56.103
    port: 6387
    db: 13
    password: redis123

  # deskmgr redis: 1
  cache_redis:
    ip: 10.34.56.103
    port: 6387
    db: 14
    password: redis123

  # lottery redis： 1
  lottery_redis:
    ip: 10.34.56.103
    port: 6387
    db: 15
    password: redis123

  # LwMarket_redis 3
  lwsm_order_redis:
    ip: 10.34.56.103
    port: 6388
    db: 0
    password: redis123

  # user center redis
  user_center_redis:
    ip: 10.34.56.103
    port: 6388
    db: 2
    password: redis123
  # 凡商超市缓存redis
  barshop_cache_redis:
    ip: 10.34.56.103
    port: 6388
    db: 1
    password: redis123

  goodscenter_redis:
    ip: 10.34.56.103
    port: 6388
    db: 3
    password: redis123


  award_share_redis:
    ip: 10.34.56.103
    port: 6388
    db: 3
    password: redis123

  user_info_center_redis:
    ip: 10.34.56.103
    port: 6388
    db: 3
    password: redis123




  egs_redis:
    ip: 10.34.56.103
    port: 6388
    db: 0
    password: redis123

  egs_collect_redis:
    ip: 10.34.56.103
    port: 6388
    db: 0
    password: redis123


  first_game_redis:
    ip: 10.34.56.103
    port: 6388
    db: 4
    password: redis123

  statscenter_redis:
    ip: 10.34.56.103
    port: 6388
    db: 4
    password: redis123

  # WxPolicyServer redis: 4
  policy_redis:
    ip: 10.34.56.103  # not on 126
    port: 6388
    db: 2
    password: redis123

  policymgr_celery_broker_redis:
    ip: 10.34.56.103
    port: 6388
    db: 3
    password: redis123

  policymgr_celery_result_redis:
    ip: 10.34.56.103
    port: 6388
    db: 4
    password: redis123

  policymgr_redis:
    ip: 10.34.56.103
    port: 6388
    db: 5
    password: redis123
  # WxPolicyServer redis: end

  pubg_activity_redis:
    ip: 10.34.56.103
    port: 6389
    db: 0
    password: redis123

  league_draw_lottery_redis:
    ip: 10.34.56.103
    port: 6389
    db: 0
    password: redis123
  

  DETAIL_CLIENT_IP: 10.34.60.17

  gologstat_redis:
    ip: 10.34.56.103
    port: 6389
    db: 0
    password: redis123

  sentry_redis:
    ip: 10.34.56.103
    port: 6389
    db: 0
    password: redis123

  pg_host: 10.34.60.128

log_artifact:
  templogs:
    artifact_ftp: templogs.js-ops.com:2122
    ftp_user: tmp_gameplaza
    ftp_pass: gameplaza!tmp
    artifact_http: http://templogs.js-ops.com:2120/Ad.Logs/gameplaza/
  everydaylogs:
    artifact_ftp: everydaylogs.js-ops.com:2122
    ftp_user: gameplaza
    ftp_pass: gameplaza!log
    artifact_http: http://everydaylogs.js-ops.com:2120/Ad.logs/gameplaza/


#计费那边测试环境配置
desktop_account_configs:
    barshop_mysql_host: 10.34.52.160
    barshop_mysql_user: '"020"'
    barshop_mysql_password: love123
    barshop_mysql_port: 3306
    barshop_mysql_database: barshopBase

    #兼容老版本凡商超市的业务
    old_barshop_manager_url: http://10.34.52.162:8083

    old_barshop_web_url: http://10.34.52.161

    lwsm_web_url: http://10.34.56.107:8086

    #计费那边校验接口
    # 开发环境
    sicent_idcservice_host: http://10.34.58.13:8181

    sicent_querynbidinfo_host: http://10.34.59.13:8181

    pubwin_idcservice_host: http://10.34.58.13:8181

    pubwin_querynbidinfo_host: http://10.34.59.13:8181

    #token_expire: 3600
    #账户中心发出请求超时时间
    http_timeout: 3

    callback_verification_url: http://10.34.57.67/QddCheckToken.do

    callback_verification_url_pubwin: http://10.34.45.22/QddCheckToken.do

goodscenter_configs:
    oss_host: 10.34.56.107:8085  # oss 地址

    old_barshop_image_url: http://10.34.49.198 # 凡商图片地址


statscenter_configs:
    #计费主机地址
    nfe_url: http://10.34.58.13:8181
    #0013收款码主机地址
    payment_code_0013_url: http://10.34.60.119:8080
    #0钱多多管理后台主机地址
    qdd_manager_url: http://10.34.41.30:8000

usercenter_configs:
    wx_redirect_url: http://t4.sw0013.com/box/user-login-wx.html
    qq_redirect_url: http://t4.sw0013.com/box/user-login-qq.html
    bind_url: http://t4.sw0013.com/box/user-home.html

gobeat:
  IP: 10.34.60.17
  PORT: 9900