dns:
  sicent:
    - 10.34.37.10
    - 10.34.37.11

ntp:
  servers:
    - stdtime.gov.hk

ip_addrs:

  mongos:
    ip: 10.34.60.130
    port: 27017
  mongo_configsvr01: 10.34.60.130
  mongo_shardsvr01_p: 10.34.60.130

  mongo_configsvr03: 10.34.56.110

  # nginx servers
  nginx_frontend: 10.34.60.121
  nginx_backend: 10.34.60.122
  nginx_backend2: 10.34.60.127

  wxgameplaza_web01: 10.34.60.121
  wxgameplaza_celerybeat: 10.34.60.121
  wxgameplaza_celeryd_default: 10.34.60.121
  wxgameplaza_celeryd_schedule: 10.34.60.123

  wxgameplaza_httpagent: 10.34.60.121
  wxgameplaza_handler01: 10.34.60.121

  WxVoiceMaster_web01: 10.34.60.122
  WxImageServer: 10.34.60.123
  WxAccountCenter: 10.34.60.122
  WxDeskManagement_web01: 10.34.60.123
  WxGameMenu: 10.34.60.123
  WxPolicyServer: 10.34.60.127
  WxBarshop_web01: 10.34.60.127

  LwMarket_web01: 10.34.60.122

  WxDesktopOperation_web: 10.34.60.128
  GoodsCenter_web01: 10.34.60.127
  WxDesktopAccount_web01: 10.34.60.127
  WxDesktopAccount_web02: 10.34.60.127

  desktop_platform_interface: http://10.34.53.178:8088/interface

  statscenter_upstream:  # 统计中心 upstream
    - host: 10.34.60.123
      port: 22125
      weight: 1
  usercenter_upstream:
    - host: 10.34.60.127
      port: 22126
      weight: 1

  egs_upstream:  # egs upstream
    - host: 10.34.60.127
      port: 22227
      weight: 1

  # WxPolicyServer Redis Server
  policy_redis_master:
    ip: 10.34.60.127
    port: 22211
    password: redis123

  policymgr_redis_master:
    ip: 10.34.60.127
    port: 22212
    password: redis123
  # WxPolicyServer Redis Server END

  # account redis server
  accountcenter_celery_redis:
    ip: 10.34.60.123
    port: 22211
    password: redis123

  # voice celery redis server: 2
  voice_celery_redis:
    ip: 10.34.60.123
    port: 22212
    password: redis123

  voice_redis:
    ip: 10.34.60.123
    port: 22212
    db: 2
    password: redis123

  # gameplaza celery redis server
  gameplaza_celery_redis:
    ip: 10.34.60.123
    port: 22213
    password: redis123

  # gamemenu redis: 2
  gamemenu_celery_broker_redis:
    ip: 10.34.60.123
    port: 22213
    db: 2
    password: redis123
  gamemenu_celery_result_redis:
    ip: 10.34.60.123
    port: 22213
    db: 3
    password: redis123

  # deskcomponent redis: 14
  stage_activity_redis:
    ip: 10.34.60.122
    port: 22211
    db: 0
    password: redis123
  reward_activity_redis:
    ip: 10.34.60.122
    port: 22211
    db: 1
    password: redis123
  head_activity_redis:
    ip: 10.34.60.122
    port: 22211
    db: 2
    password: redis123
  damage_activity_redis:
    ip: 10.34.60.122
    port: 22211
    db: 3
    password: redis123
  continuous_victory_activity_redis:
    ip: 10.34.60.122
    port: 22211
    db: 4
    password: redis123
  score_redis:
    ip: 10.34.60.122
    port: 22211
    db: 5
    password: redis123
  statistic_redis:
    ip: 10.34.60.122
    port: 22211
    db: 6
    password: redis123
  bar_redis:
    ip: 10.34.60.122
    port: 22211
    db: 7
    password: redis123
  ranker_player_redis:
    ip: 10.34.60.122
    port: 22211
    db: 8
    password: redis123
  reduce_stress_redis:
    ip: 10.34.60.122
    port: 22211
    db: 9
    password: redis123
  ad_announcement_redis:
    ip: 10.34.60.122
    port: 22211
    db: 10
    password: redis123
  lock_key_redis:
    ip: 10.34.60.122
    port: 22211
    db: 11
    password: redis123
  pay_notice_redis:
    ip: 10.34.60.122
    port: 22211
    db: 12
    password: redis123
  bar_player_uploadtime_redis:
    ip: 10.34.60.122
    port: 22211
    db: 13
    password: redis123

  # deskmgr redis: 1
  cache_redis:
    ip: 10.34.60.122
    port: 22211
    db: 14
    password: redis123

  # accountcenter redis: 2
  ac_account_redis:
    ip: 10.34.60.122
    port: 22212
    db: 0
    password: redis123
  ac_account_other_redis:
    ip: 10.34.60.122
    port: 22212
    db: 1
    password: redis123

  # lottery redis： 1
  lottery_redis:
    ip: 10.34.60.122
    port: 22212
    db: 2
    password: redis123

  # LwMarket_redis 3
  LWSM_ORDER_REDIS:
    ip: 10.34.60.122
    port: 22212
    db: 3
    password: redis123

  CELERY_BROKER_URL:
    ip: 10.34.60.122
    port: 22212
    db: 4
    password: redis123

  CELERY_RESULT_URL:
    ip: 10.34.60.122
    port: 22212
    db: 5
    password: redis123

  # Wxtoken配置：1
  wx_token_redis:
    ip: 10.34.60.122
    port: 22212
    db: 6
    password: redis123

  # 每日任务：1
  daily_task_redis:
    ip: 10.34.60.122
    port: 22212
    db: 7
    password: redis123

  # LwMarket_cache_redis:1
  LWSM_CACHE_REDIS:
    ip: 10.34.60.122
    port: 22212
    db: 8
    password: redis123

  # WxPolicyServer redis: 4
  policymgr_celery_broker_redis:
    ip: 10.34.60.127
    port: 22212
    db: 0
    password: redis123

  policymgr_celery_result_redis:
    ip: 10.34.60.127
    port: 22212
    db: 1
    password: redis123

  policymgr_redis:
    ip: 10.34.60.127
    port: 22212
    db: 2
    password: redis123

  policy_redis:
    ip: 10.34.60.127
    port: 22211
    db: 0
    password: redis123

  # WxPolicyServer Redis END

  # WxBarshop_redis 1
  CACHE_REDIS:
    ip: 10.34.60.127
    port: 22211
    db: 4
    password: redis123

  # 用户中心redis
  user_center_redis:
    ip: 10.34.60.127
    port: 22211
    db: 5
    password: redis123

  # 奖励分享增加redis 1
  award_share_redis:
    ip: 10.34.60.127
    port: 22211
    db: 6
    password: redis123

  first_game_redis:
    ip: 10.34.60.127
    port: 22211
    db: 8
    password: redis123

  #云商品(01/02环境共用redis)
  goodscenter_redis:
    ip: 10.34.60.127
    port: 22211
    db: 7
    password: redis123

  statscenter_redis:
    ip: 10.34.60.127
    port: 22211
    db: 9
    password: redis123

  #PUBG redis:1
  pubg_activity_redis:
    ip: 10.34.60.122
    port: 22213
    db: 15
    password: redis123
  #PUBG redis end


  league_draw_lottery_redis:
    ip: 10.34.60.122
    port: 22213
    db: 14
    password: redis123
  
  #GoLogStat redis: 1
  gologstat_redis:
    ip: 10.34.60.127
    port: 22211
    db: 10
    password: redis123
  #GoLogStat redis end

  user_info_center_redis:
    ip: 10.34.60.127
    port: 22211
    db: 11
    password: redis123

  #电竞模式redis
  egs_redis:
    ip: 10.34.60.127
    port: 22211
    db: 12
    password: redis123

  egs_collect_redis:
    ip: 10.34.60.127
    port: 22211
    db: 13
    password: redis123

  DETAIL_CLIENT_IP: 10.34.60.17

goods_center_configs:
    oss_host: 10.34.60.122:8085  # oss 地址
    old_barshop_image_url: http://10.34.49.198 # 凡商图片地址

desktop_account_configs:
    barshop_mysql_host: 10.34.52.160
    barshop_mysql_user: '"020"'
    barshop_mysql_password: love123
    barshop_mysql_port: 3306
    barshop_mysql_database: barshopBase

    #兼容老版本凡商超市的业务
    old_barshop_manager_url: http://10.34.49.199:8083
    old_barshop_web_url: http://10.34.49.198
    lwsm_web_url: http://10.34.60.122:8086

    #计费那边校验接口
    #陈本辉
    sicent_idcservice_host: http://10.34.51.51
    #测试环境
    sicent_querynbidinfo_host: http://10.34.52.71:8081

    #陈本辉 http://10.34.45.12
    pubwin_idcservice_host: http://10.34.51.51
    #测试环境
    pubwin_querynbidinfo_host: http://10.34.45.22:8081

    #token_expire: 3600
    #账户中心发出请求超时时间
    http_timeout: 3

    callback_verification_url: http://10.34.57.67/QddCheckToken.do

    callback_verification_url_pubwin: http://10.34.45.22/QddCheckToken.do

statscenter_configs:
    #计费主机地址
    nfe_url: http://10.34.51.51:8181
    #0013收款码主机地址
    payment_code_0013_url: http://10.34.60.119:8080
    #0钱多多管理后台主机地址
    qdd_manager_url: http://10.34.60.122:8081

usercenter_configs:
    wx_redirect_url: http://t4.sw0013.com/box/user-login-wx.html
    qq_redirect_url: http://t4.sw0013.com/box/user-login-qq.html
    bind_url: http://t4.sw0013.com/box/user-home.html

gobeat:
  IP: 10.34.60.17
  PORT: 9900



