dns:
  sicent:
    - 10.34.37.10
    - 10.34.37.11

ntp:
  servers:
    - stdtime.gov.hk

ip_addrs:

  mongos:
    ip: 10.34.60.131
    port: 27017
  mongo_configsvr01: 10.34.60.131
  mongo_shardsvr01_p: 10.34.60.131

  mongo_configsvr03: 10.34.56.110

  # nginx servers
  nginx_frontend: 10.34.60.124
  nginx_backend: 10.34.60.125

  wxgameplaza_web01: 10.34.60.124
  wxgameplaza_celerybeat: 10.34.60.124
  wxgameplaza_celeryd_default: 10.34.60.124
  wxgameplaza_celeryd_schedule: 10.34.60.126

  wxgameplaza_httpagent: 10.34.60.124
  wxgameplaza_handler01: 10.34.60.124

  WxVoiceMaster_web01: 10.34.60.125
  WxImageServer: 10.34.60.126
  WxAccountCenter: 10.34.60.125
  WxDeskManagement_web01: 10.34.60.126
  WxGameMenu: 10.34.60.126
  WxPolicyServer:  # 未配置
  WxBarshop_web01: # 未配置
  WxDesktopOperation_web: # 未配置
  LwMarket_web01: 10.34.60.125
  GoodsCenter_web01: 10.34.60.126

  desktop_platform_interface: http://10.34.53.178:8088/interface

  statscenter_upstream:  # 统计中心 upstream
    - host: 10.34.60.126
      port: 22125
      weight: 1

  # account redis server
  accountcenter_celery_redis:
    ip: 10.34.60.126
    port: 22211
    password: redis123

  # voice celery redis server: 2
  voice_celery_redis:
    ip: 10.34.60.126
    port: 22212
    password: redis123

  voice_redis:
    ip: 10.34.60.126
    port: 22212
    db: 2
    password: redis123

  # gameplaza celery redis server
  gameplaza_celery_redis:
    ip: 10.34.60.126
    port: 22213
    password: redis123

  # gamemenu redis: 2
  gamemenu_celery_broker_redis:
    ip: 10.34.60.126
    port: 22213
    db: 2
    password: redis123
  gamemenu_celery_result_redis:
    ip: 10.34.60.126
    port: 22213
    db: 3
    password: redis123

  # deskcomponent redis: 14
  stage_activity_redis:
    ip: 10.34.60.125
    port: 22211
    db: 0
    password: redis123
  reward_activity_redis:
    ip: 10.34.60.125
    port: 22211
    db: 1
    password: redis123
  head_activity_redis:
    ip: 10.34.60.125
    port: 22211
    db: 2
    password: redis123
  damage_activity_redis:
    ip: 10.34.60.125
    port: 22211
    db: 3
    password: redis123
  continuous_victory_activity_redis:
    ip: 10.34.60.125
    port: 22211
    db: 4
    password: redis123
  score_redis:
    ip: 10.34.60.125
    port: 22211
    db: 5
    password: redis123
  statistic_redis:
    ip: 10.34.60.125
    port: 22211
    db: 6
    password: redis123
  bar_redis:
    ip: 10.34.60.125
    port: 22211
    db: 7
    password: redis123
  ranker_player_redis:
    ip: 10.34.60.125
    port: 22211
    db: 8
    password: redis123
  reduce_stress_redis:
    ip: 10.34.60.125
    port: 22211
    db: 9
    password: redis123
  ad_announcement_redis:
    ip: 10.34.60.125
    port: 22211
    db: 10
    password: redis123
  lock_key_redis:
    ip: 10.34.60.125
    port: 22211
    db: 11
    password: redis123
  pay_notice_redis:
    ip: 10.34.60.125
    port: 22211
    db: 12
    password: redis123
  bar_player_uploadtime_redis:
    ip: 10.34.60.125
    port: 22211
    db: 13
    password: redis123

  # deskmgr redis: 1
  cache_redis:
    ip: 10.34.60.125
    port: 22211
    db: 14
    password: redis123

  # accountcenter redis: 2
  ac_account_redis:
    ip: 10.34.60.125
    port: 22212
    db: 0
    password: redis123
  ac_account_other_redis:
    ip: 10.34.60.125
    port: 22212
    db: 1
    password: redis123

  # lottery redis： 1
  lottery_redis:
    ip: 10.34.60.125
    port: 22212
    db: 2
    password: redis123

 # LwMarket_redis 3
  LWSM_ORDER_REDIS:
    ip: 10.34.60.125
    port: 22212
    db: 3
    password: redis123

  CELERY_BROKER_URL:
    ip: 10.34.60.125
    port: 22212
    db: 4
    password: redis123

  CELERY_RESULT_URL:
    ip: 10.34.60.125
    port: 22212
    db: 5
    password: redis123

  # OSS增加
  wx_token_redis:
    ip: 10.34.60.125
    port: 22212
    db: 6
    password: redis123

  # 4.5期增加
  daily_task_redis:
    ip: 10.34.60.125
    port: 22212
    db: 7
    password: redis123

  # WxPolicyServer redis 4

  # LwMarket_cache_redis:1
  LWSM_CACHE_REDIS:
    ip: 10.34.60.125
    port: 22212
    db: 9
    password: redis123

  # WxBarshop redis 1
  CACHE_REDIS:
    ip: 10.34.60.125
    port: 22212
    db: 8
    password: redis123

  # 用户中心redis
  user_center_redis:
    ip: 10.34.60.127
    port: 22211
    db: 5
    password: redis123

  # WxPolicyServer Redis Server
  policy_redis_master:
    ip: 10.34.60.127
    port: 22211
    password: redis123

  policymgr_redis_master:
    ip: 10.34.60.127
    port: 22212
    password: redis123
  # WxPolicyServer Redis Server END
  
  # WxPolicyServer redis: 4
  policymgr_celery_broker_redis:
    ip: 10.34.60.127
    port: 22212
    db: 0
    password: redis123

  policymgr_celery_result_redis:
    ip: 10.34.60.127
    port: 22212
    db: 1
    password: redis123

  policymgr_redis:
    ip: 10.34.60.127
    port: 22212
    db: 2
    password: redis123

  policy_redis:
    ip: 10.34.60.127
    port: 22211
    db: 0
    password: redis123

  # WxPolicyServer Redis END

  first_game_redis:
    ip: 10.34.60.127
    port: 22211
    db: 8
    password: redis123


  #云商品
  goodscenter_redis:
    ip: 10.34.60.127
    port: 22211
    db: 7
    password: redis123

  statscenter_redis:
    ip: 10.34.60.127
    port: 22211
    db: 9
    password: redis123

  #PUBG redis:1
  pubg_activity_redis:
    ip: 10.34.60.125
    port: 22213
    db: 15
    password: redis123

  DETAIL_CLIENT_IP: 10.34.60.17


goodscenter_configs:
    oss_host: 10.34.60.125:8085  # oss 地址
    old_barshop_image_url: http://10.34.49.198 # 凡商图片地址

desktop_account_configs:
    barshop_mysql_host: 10.34.52.160
    barshop_mysql_user: '"020"'
    barshop_mysql_password: love123
    barshop_mysql_port: 3306
    barshop_mysql_database: barshopBase

    #兼容老版本凡商超市的业务
    old_barshop_manager_url: http://10.34.49.199:8083
    old_barshop_web_url: http://10.34.49.198
    lwsm_web_url: http://10.34.60.122:8086

    #计费那边校验接口
    #陈本辉
    sicent_idcservice_host: http://10.34.51.51
    #测试环境
    sicent_querynbidinfo_host: http://10.34.52.71:8081

    #陈本辉
    pubwin_idcservice_host: http://10.34.45.12
    #测试环境
    pubwin_querynbidinfo_host: http://10.34.45.22:8081

    #token_expire: 3600
    #账户中心发出请求超时时间
    http_timeout: 3

    callback_verification_url: http://10.34.57.67/QddCheckToken.do

    callback_verification_url_pubwin: http://10.34.45.22/QddCheckToken.do

statscenter_configs:
    #计费主机地址
    nfe_url: http://10.34.51.51
    #0013收款码主机地址
    payment_code_0013_url: http://10.34.60.119:8080
    #0钱多多管理后台主机地址
    qdd_manager_url: http://10.34.60.122:8081

gobeat:
  IP: 10.34.60.17
  PORT: 9900