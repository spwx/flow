{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxImageServer:
  client:
    store_path: /data/wximage/client
    server:
      ip: {{ ip_addrs.WxImageServer }}
      store_path: /data/wximage/server

LwMarket:
  web:
    bind: {{ ip_addrs.LwMarket_web02 }}

filebeat:
  config:
    filebeat.prospectors:



      - input_type: log
        document_type: lwsm
        paths:
          - {{ stack.LwMarket.web.install_path }}/lightsupermarket/logs/lwsm.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

    output.logstash:
      hosts: ["172.30.236.79:5000"]
