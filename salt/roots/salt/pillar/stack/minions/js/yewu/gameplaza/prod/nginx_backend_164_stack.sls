{% set ip_addrs = stack.get('ip_addrs', {}) %}

egs:
  nginx_inner:
     backend: # port: 8092
        server_name:  {{ ip_addrs.nginx_backend_164 }} {{ ip_addrs.nginx_backend_wlan_164 }}
        access_log_file: egs_backend.log
        blue_servers: {{ ip_addrs.egs_upstream }}

        green_servers: []

