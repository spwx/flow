{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongo_configsvr:
  svc_name: mongo_configsvr
  dataPath: {{ stack.mongo.data_path }}/configdb
  replSetName: devtest_cfg_shard
  external_net_bindip: {{ ip_addrs.mongodev_configsvr03 }}
  port: 3307
  is_master: false
  is_configsvr: true

mongodev_arbiter01:
  svc_name: mongodev_arbiter01
  external_net_bindip: {{ ip_addrs.mongodev_arbiter01 }}
  dataPath: {{ stack.mongo.data_path }}/mongodev_arbiter01
  port: 3308
  is_master: false
  replSetName: devtest_shard01
  cacheSizeGB: 1

mongodev_arbiter02:
  svc_name: mongodev_arbiter02
  external_net_bindip: {{ ip_addrs.mongodev_arbiter02 }}
  dataPath: {{ stack.mongo.data_path }}/mongodev_arbiter02
  port: 3309
  is_master: false
  replSetName: devtest_shard02
  cacheSizeGB: 1
