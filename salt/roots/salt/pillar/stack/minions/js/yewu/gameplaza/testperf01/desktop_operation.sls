environment: desktop_testperf01

hostname: desktop_platform.testperf01.gameplaza.yewu.js

roles:
- python3
- mongo
- mongos
- redis
- accountcenter
- nginx
- elasticsearch
- logstash

services:
- WxDesktopOperation_web

- gologstat
