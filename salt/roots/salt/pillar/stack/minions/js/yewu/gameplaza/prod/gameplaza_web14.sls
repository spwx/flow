environment: gameplaza_prod

hostname: gameplaza_web14.prod.gameplaza.yewu.js
minion_role: prod.gameplaza_web
roles:
  - mongo
  - mongos
  - webagent
  - python3
  - deskcomponent
  - accountcenter

services:
- WxGamePlaza_web
