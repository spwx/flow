{% set ip_addrs = stack.get('ip_addrs', {}) %}
stage_activity_redis:
  install_prefix: /data/SicentApp/stage_activity_redis
  log_dir: /data/SicentApp/stage_activity_redis/log
  data_dir: /data/SicentApp/stage_activity_redis/db
  bind: {{ ip_addrs.stage_activity_redis.ip }}
  password: {{ ip_addrs.stage_activity_redis.password }}
  port : {{ ip_addrs.stage_activity_redis.port }}
  svc_name: redis_stage_activity

reward_activity_redis:
  install_prefix: /data/SicentApp/reward_activity_redis
  log_dir: /data/SicentApp/reward_activity_redis/log
  data_dir: /data/SicentApp/reward_activity_redis/db
  bind: {{ ip_addrs.reward_activity_redis.ip }}
  password: {{ ip_addrs.reward_activity_redis.password }}
  port : {{ ip_addrs.reward_activity_redis.port }}
  svc_name: redis_reward_activity

head_activity_redis:
  install_prefix: /data/SicentApp/head_activity_redis
  log_dir: /data/SicentApp/head_activity_redis/log
  data_dir: /data/SicentApp/head_activity_redis/db
  bind: {{ ip_addrs.head_activity_redis.ip }}
  password: {{ ip_addrs.head_activity_redis.password }}
  port : {{ ip_addrs.head_activity_redis.port }}
  svc_name: redis_head_activity

damage_activity_redis:
  install_prefix: /data/SicentApp/damage_activity_redis
  log_dir: /data/SicentApp/damage_activity_redis/log
  data_dir: /data/SicentApp/damage_activity_redis/db
  bind: {{ ip_addrs.damage_activity_redis.ip }}
  password: {{ ip_addrs.damage_activity_redis.password }}
  port : {{ ip_addrs.damage_activity_redis.port }}
  svc_name: redis_damage_activity

continuous_victory_activity_redis:
  install_prefix: /data/SicentApp/continuous_victory_activity_redis
  log_dir: /data/SicentApp/continuous_victory_activity_redis/log
  data_dir: /data/SicentApp/continuous_victory_activity_redis/db
  bind: {{ ip_addrs.continuous_victory_activity_redis.ip }}
  password: {{ ip_addrs.continuous_victory_activity_redis.password }}
  port : {{ ip_addrs.continuous_victory_activity_redis.port }}
  svc_name: redis_continuous_victory_activity

score_redis:
  install_prefix: /data/SicentApp/score_redis
  log_dir: /data/SicentApp/score_redis/log
  data_dir: /data/SicentApp/score_redis/db
  bind: {{ ip_addrs.score_redis.ip }}
  password: {{ ip_addrs.score_redis.password }}
  port : {{ ip_addrs.score_redis.port }}
  svc_name: redis_score

statistic_redis:
  install_prefix: /data/SicentApp/statistic_redis
  log_dir: /data/SicentApp/statistic_redis/log
  data_dir: /data/SicentApp/statistic_redis/db
  bind: {{ ip_addrs.statistic_redis.ip }}
  password: {{ ip_addrs.statistic_redis.password }}
  port : {{ ip_addrs.statistic_redis.port }}
  svc_name: redis_statistic

bar_redis:
  install_prefix: /data/SicentApp/bar_redis
  log_dir: /data/SicentApp/bar_redis/log
  data_dir: /data/SicentApp/bar_redis/db
  bind: {{ ip_addrs.bar_redis.ip }}
  password: {{ ip_addrs.bar_redis.password }}
  port : {{ ip_addrs.bar_redis.port }}
  svc_name: redis_bar

ranker_player_redis:
  install_prefix: /data/SicentApp/ranker_player_redis
  log_dir: /data/SicentApp/ranker_player_redis/log
  data_dir: /data/SicentApp/ranker_player_redis/db
  bind: {{ ip_addrs.ranker_player_redis.ip }}
  password: {{ ip_addrs.ranker_player_redis.password }}
  port : {{ ip_addrs.ranker_player_redis.port }}
  svc_name: redis_ranker_player

reduce_stress_redis:
  install_prefix: /data/SicentApp/reduce_stress_redis
  log_dir: /data/SicentApp/reduce_stress_redis/log
  data_dir: /data/SicentApp/reduce_stress_redis/db
  bind: {{ ip_addrs.reduce_stress_redis.ip }}
  password: {{ ip_addrs.reduce_stress_redis.password }}
  port : {{ ip_addrs.reduce_stress_redis.port }}
  svc_name: redis_reduce_stress

ad_announcement_redis:
  install_prefix: /data/SicentApp/ad_announcement_redis
  log_dir: /data/SicentApp/ad_announcement_redis/log
  data_dir: /data/SicentApp/ad_announcement_redis/db
  bind: {{ ip_addrs.ad_announcement_redis.ip }}
  password: {{ ip_addrs.ad_announcement_redis.password }}
  port : {{ ip_addrs.ad_announcement_redis.port }}
  svc_name: redis_ad_announcement

lock_key_redis:
  install_prefix: /data/SicentApp/lock_key_redis
  log_dir: /data/SicentApp/lock_key_redis/log
  data_dir: /data/SicentApp/lock_key_redis/db
  bind: {{ ip_addrs.lock_key_redis.ip }}
  password: {{ ip_addrs.lock_key_redis.password }}
  port : {{ ip_addrs.lock_key_redis.port }}
  svc_name: redis_lock_key

pay_notice_redis:
  install_prefix: /data/SicentApp/pay_notice_redis
  log_dir: /data/SicentApp/pay_notice_redis/log
  data_dir: /data/SicentApp/pay_notice_redis/db
  bind: {{ ip_addrs.pay_notice_redis.ip }}
  password: {{ ip_addrs.pay_notice_redis.password }}
  port : {{ ip_addrs.pay_notice_redis.port }}
  svc_name: redis_pay_notice

bar_player_uploadtime_redis:
  install_prefix: /data/SicentApp/bar_player_uploadtime_redis
  log_dir: /data/SicentApp/bar_player_uploadtime_redis/log
  data_dir: /data/SicentApp/bar_player_uploadtime_redis/db
  bind: {{ ip_addrs.bar_player_uploadtime_redis.ip }}
  password: {{ ip_addrs.bar_player_uploadtime_redis.password }}
  port : {{ ip_addrs.bar_player_uploadtime_redis.port }}
  svc_name: redis_bar_player_uploadtime


daily_task_redis:
  install_prefix: /data/SicentApp/daily_task_redis
  log_dir: /data/SicentApp/daily_task_redis/log
  data_dir: /data/SicentApp/daily_task_redis/db
  bind: {{ ip_addrs.daily_task_redis.ip }}
  password: {{ ip_addrs.daily_task_redis.password }}
  port : {{ ip_addrs.daily_task_redis.port }}
  svc_name: redis_daily_task

pubg_activity_redis:
  install_prefix: /data/SicentApp/pubg_activity_redis
  log_dir: /data/SicentApp/pubg_activity_redis/log
  data_dir: /data/SicentApp/pubg_activity_redis/db
  bind: {{ ip_addrs.pubg_activity_redis.ip }}
  password: {{ ip_addrs.pubg_activity_redis.password }}
  port : {{ ip_addrs.pubg_activity_redis.port }}
  svc_name: redis_pubg_activity


diskPerf:
  data:
    - DEVICENAME: sda
    - DEVICENAME: sda4

zbxRedis:
  data:
    - SVCNAME: redis_stage_activity
      REDISHOST: {{ ip_addrs.stage_activity_redis.ip }}
      REDISPORT: {{ ip_addrs.stage_activity_redis.port }}
    - SVCNAME: redis_reward_activity
      REDISHOST: {{ ip_addrs.reward_activity_redis.ip }}
      REDISPORT: {{ ip_addrs.reward_activity_redis.port }}
    - SVCNAME: redis_head_activity
      REDISHOST: {{ ip_addrs.head_activity_redis.ip }}
      REDISPORT: {{ ip_addrs.head_activity_redis.port }}
    - SVCNAME: redis_damage_activity
      REDISHOST: {{ ip_addrs.damage_activity_redis.ip }}
      REDISPORT: {{ ip_addrs.damage_activity_redis.port }}
    - SVCNAME: redis_continuous_victory_activity
      REDISHOST: {{ ip_addrs.continuous_victory_activity_redis.ip }}
      REDISPORT: {{ ip_addrs.continuous_victory_activity_redis.port }}
    - SVCNAME: redis_score
      REDISHOST: {{ ip_addrs.score_redis.ip }}
      REDISPORT: {{ ip_addrs.score_redis.port }}
    - SVCNAME: redis_statistic
      REDISHOST: {{ ip_addrs.statistic_redis.ip }}
      REDISPORT: {{ ip_addrs.statistic_redis.port }}
    - SVCNAME: redis_bar
      REDISHOST: {{ ip_addrs.bar_redis.ip }}
      REDISPORT: {{ ip_addrs.bar_redis.port }}
    - SVCNAME: redis_ranker_player
      REDISHOST: {{ ip_addrs.ranker_player_redis.ip }}
      REDISPORT: {{ ip_addrs.ranker_player_redis.port }}
    - SVCNAME: redis_reduce_stress
      REDISHOST: {{ ip_addrs.reduce_stress_redis.ip }}
      REDISPORT: {{ ip_addrs.reduce_stress_redis.port }}
    - SVCNAME: redis_ad_announcement
      REDISHOST: {{ ip_addrs.ad_announcement_redis.ip }}
      REDISPORT: {{ ip_addrs.ad_announcement_redis.port }}
    - SVCNAME: redis_lock_key
      REDISHOST: {{ ip_addrs.lock_key_redis.ip }}
      REDISPORT: {{ ip_addrs.lock_key_redis.port }}
    - SVCNAME: redis_pay_notice
      REDISHOST: {{ ip_addrs.pay_notice_redis.ip }}
      REDISPORT: {{ ip_addrs.pay_notice_redis.port }}
    - SVCNAME: redis_bar_player_uploadtime
      REDISHOST: {{ ip_addrs.bar_player_uploadtime_redis.ip }}
      REDISPORT: {{ ip_addrs.bar_player_uploadtime_redis.port }}
    - SVCNAME: redis_daily_task
      REDISHOST: {{ ip_addrs.daily_task_redis.ip }}
      REDISPORT: {{ ip_addrs.daily_task_redis.port }}
    - SVCNAME: redis_pubg_activity
      REDISHOST: {{ ip_addrs.pubg_activity_redis.ip }}
      REDISPORT: {{ ip_addrs.pubg_activity_redis.port }}
