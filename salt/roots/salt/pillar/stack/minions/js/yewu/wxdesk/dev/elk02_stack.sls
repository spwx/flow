elasticsearch:
  version: '5.4.3'
  pkg_url: http://uploadqdd.wxdesk.com/upload/share_image/elasticsearch-5.4.3.rpm
  jvm:
    Xms: 2g
    Xmx: 2g
  config:
    cluster.name: sample
    node.name: node-1
    node.master: false
    node.data: true
    path.data: /data/SicentApp/elasticsearch/db
    path.logs: /data/SicentApp/elasticsearch/logs
    network.host: 0.0.0.0
    http.port: 9200
    bootstrap.system_call_filter: false
    # discovery.zen.ping.unicast.hosts: []
