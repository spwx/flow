environment: desktop_testperf01

hostname: app04.testperf01.gameplaza.yewu.js

roles:
- python3
- mongo
- mongos
- redis
- accountcenter
- nginx
- deskcomponent

services:
- WxPolicyServer
- WxBarshop_web
- WxBarshop_webb
- WxBarshop_webc
- WxBarshop_web_report
- WxBarshop_frontend
- WxDesktopAccount
- GoodsCenter
- usercenter
- egs