{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongo_shardsvr:
  bind_ip: {{ ip_addrs.get('mongo_shardsvr03_p', '0.0.0.0') }}
  primary: {{ ip_addrs.get('mongo_shardsvr03_p', '0.0.0.0') }}

# 119 2mongo_shardsvr
# port: 27018/27118

mongo_shardsvr02:
  bind_ip: {{ ip_addrs.get('mongo_shardsvr03_p', '0.0.0.0') }}
  port: 27118
  primary: {{ ip_addrs.get('mongo_shardsvr03_p', '0.0.0.0') }}
