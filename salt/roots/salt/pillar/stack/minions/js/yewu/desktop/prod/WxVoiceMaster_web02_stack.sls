{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxImageServer:
  client:
    store_path: /data/wximage/client
    server:
      ip: {{ ip_addrs.WxImageServer }}
      store_path: /data/wximage/server

WxDeskManagement:
  web:
    bind: {{ ip_addrs.WxDeskManagement_web02 }}


filebeat:
  config:
    filebeat.prospectors:
      - input_type: log
        document_type: management_log_db
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/db.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
      - input_type: log
        document_type: management_log_django_request
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/django_request.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
      - input_type: log
        document_type: management_log_django.request
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/django.request.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_activity
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/activity.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
      - input_type: log
        document_type: management_log_desktop
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/desktop.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
      - input_type: log
        document_type: management_log_django_redis
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/django_redis.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_get_reward
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/get_reward.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_game
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/game.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_guide
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/guide.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_lottery
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/lottery.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_lwsm
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/lwsm.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: management_log_middleware
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/middleware.log

        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_record
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/record.log

        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_report
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/report.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: management_log_score
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/score.log

        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_taskcenter
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/taskcenter.log

        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_timed_task
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/timed_task.log

        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

      - input_type: log
        document_type: management_log_verification
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/verification.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

    output.logstash:
      hosts: ["172.30.236.79:5000"]
