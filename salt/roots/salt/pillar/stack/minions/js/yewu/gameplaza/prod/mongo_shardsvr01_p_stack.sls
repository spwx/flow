{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set my_port = "27018" %}

mongo_configsvr:
  bind_ip: {{ ip_addrs.get('mongo_configsvr01', '0.0.0.0') }}
  primary: {{ ip_addrs.get('mongo_configsvr01', '0.0.0.0') }}
  servers:
    - host: {{ ip_addrs.get('mongo_configsvr02', '0.0.0.0') }}
      port: 27019

mongo_shardsvr:
  bind_ip: {{ ip_addrs.get('mongo_shardsvr01_p', '0.0.0.0') }}
  primary: {{ ip_addrs.get('mongo_shardsvr01_p', '0.0.0.0') }}

mongos:
  bind_ip: {{ ip_addrs.get('mongos', '0.0.0.0') }}
  port: 3306
  shardsvrs:
    - __: overwrite
    - primary: {{ ip_addrs.get('mongo_shardsvr01_p', '0.0.0.0') }}
      port: 27018
    - primary: {{ ip_addrs.get('mongo_shardsvr02_p', '0.0.0.0') }}
      port: 27018
    - primary: {{ ip_addrs.get('mongo_shardsvr03_p', '0.0.0.0') }}
      port: 27018
    - primary: {{ ip_addrs.get('mongo_shardsvr03_p', '0.0.0.0') }}
      port: 27118
  admin_users:
    - username: dba
      password: plazaDba
      roles:
        - role: userAdminAnyDatabase
          db: admin
    - username: clusterAdmin
      password: plazaAdmin
      roles:
        - role: clusterAdmin
          db: admin
  gameplaza:
    username: gameplaza
    password: plaza123
    dba_password: plazaDba
    clusterAdmin_password: plazaAdmin
    dbname: gameplaza
  voicemaster:
    username: voicemaster
    password: voice123
    dba_password: plazaDba
    clusterAdmin_password: plazaAdmin
    dbname: voice
  gamedesk:
    username: gamedesk
    password: desk123
    dba_password: plazaDba
    clusterAdmin_password: plazaAdmin
    dbname: gamedesk
  misc:
    username: misc
    password: misc123
    dba_password: plazaDba
    clusterAdmin_password: plazaAdmin
    dbname: misc
