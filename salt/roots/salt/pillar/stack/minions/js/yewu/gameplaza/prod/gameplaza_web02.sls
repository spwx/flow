environment: gameplaza_prod

hostname: gameplaza_web02.prod.gameplaza.yewu.js
minion_role: prod.gameplaza_web
roles:
  - mongo
  - mongos
  - webagent
  - python3
  - deskcomponent
  - accountcenter

services:
- WxGamePlaza_web
- WxGamePlaza_celeryd
