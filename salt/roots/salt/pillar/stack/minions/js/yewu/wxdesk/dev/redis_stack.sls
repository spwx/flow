{% set ip_addrs = stack.get('ip_addrs', {}) %}


wx_token_redis:
  install_prefix: /data/SicentApp/wx_token_redis
  log_dir: /data/SicentApp/wx_token_redis/log
  data_dir: /data/SicentApp/wx_token_redis/db
  bind: {{ ip_addrs.wx_token_redis.ip }}
  password: {{ ip_addrs.wx_token_redis.password }}
  port : {{ ip_addrs.wx_token_redis.port }}
  enable_save: False
  svc_name: wx_token_redis


voice_redis:
  install_prefix: /data/SicentApp/voice_redis
  log_dir: /data/SicentApp/voice_redis/log
  data_dir: /data/SicentApp/voice_redis/db
  bind: {{ ip_addrs.voice_redis.ip }}
  password: {{ ip_addrs.voice_redis.password }}
  port : {{ ip_addrs.voice_redis.port }}
  enable_save: False
  svc_name: voice_redis



gameplaza_celery_redis:
  install_prefix: /data/SicentApp/gameplaza_celery_redis
  log_dir: /data/SicentApp/gameplaza_celery_redis/log
  data_dir: /data/SicentApp/gameplaza_celery_redis/db
  bind: {{ ip_addrs.gameplaza_celery_redis.ip }}
  password: {{ ip_addrs.gameplaza_celery_redis.password }}
  port : {{ ip_addrs.gameplaza_celery_redis.port }}
  enable_save: False
  svc_name: gameplaza_celery_redis


gampelza_redis:
  install_prefix: /data/SicentApp/stage_activity_redis
  log_dir: /data/SicentApp/stage_activity_redis/log
  data_dir: /data/SicentApp/stage_activity_redis/db
  bind: {{ ip_addrs.stage_activity_redis.ip }}
  password: {{ ip_addrs.stage_activity_redis.password }}
  port : {{ ip_addrs.stage_activity_redis.port }}
  enable_save: False
  svc_name: gameplaza_redis

lswm_order_redis:
  install_prefix: /data/SicentApp/lwsm_order_redis
  log_dir: /data/SicentApp/lwsm_order_redis/log
  data_dir: /data/SicentApp/lwsm_order_redis/db
  bind: {{ ip_addrs.lwsm_order_redis.ip }}
  password: {{ ip_addrs.lwsm_order_redis.password }}
  port : {{ ip_addrs.lwsm_order_redis.port }}
  enable_save: False
  svc_name: lwsm_order_redis

pubg_activity_redis:
  install_prefix: /data/SicentApp/pubg_activity_redis
  log_dir: /data/SicentApp/pubg_activity_redis/log
  data_dir: /data/SicentApp/pubg_activity_redis/db
  bind: {{ ip_addrs.pubg_activity_redis.ip }}
  password: {{ ip_addrs.pubg_activity_redis.password }}
  port : {{ ip_addrs.pubg_activity_redis.port }}
  enable_save: False
  svc_name: pubg_activity_redis
