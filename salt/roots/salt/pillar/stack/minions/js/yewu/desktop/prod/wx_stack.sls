
{% set ip_addrs = stack.get('ip_addrs', {}) %}

wxgameplaza:
  nginx:
    handler:
      backend: # port 8088
        server_name: upload.wxdesk.com {{ip_addrs.nginx02_backend}} {{ip_addrs.nginx02_backend_wlan}}
        blue_servers:
          - host: {{ ip_addrs.wxgameplaza_handler_blue01 }}
            port: 19999
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_handler_blue02 }}
            port: 19999
            weight: 2
        green_servers:
          - host: {{ ip_addrs.wxgameplaza_handler_green01 }}
            port: 19999
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_handler_green02 }}
            port: 19999
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_handler_green03 }}
            port: 19999
            weight: 2