environment: gameplaza_test

hostname: mongo_configsvr01.test.gameplaza.yewu.js

minion_role: test.mongo

roles:
  - mongo
  - mongo_configsvr
  - redis
