{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongodev_configsvr:
  svc_name: mongodev_configsvr
  dataPath: {{ stack.mongo.data_path }}/configdb
  replSetName: devtest_cfg_shard
  external_net_bindip: {{ ip_addrs.mongodev_configsvr01 }}
  port: 3307
  is_master: true
  is_configsvr: true
  sources:
  - name: mongodev_configsvr01
    master: true
    arbiter: false
    ip: {{ ip_addrs.mongodev_configsvr01 }}
    port: 3307
  - name: mongodev_configsvr02
    master: false
    arbiter: false
    ip: {{ ip_addrs.mongodev_configsvr02 }}
    port: 3307
  - name: mongodev_configsvr03
    master: false
    arbiter: false
    ip: {{ ip_addrs.mongodev_configsvr03 }}
    port: 3307

  users:
  - database: admin
    name: admin
    passwd: admin2018
    user: admin
    password: admin2018
    authdb: admin

mongodev_shardsvr01:
  svc_name: mongodev_shardsvr01_p
  external_net_bindip: {{ ip_addrs.mongodev_shardsvr01_p }}
  dataPath: {{ stack.mongo.data_path }}/mongodev_shardsvr01_p
  port: 3308
  is_master: true
  replSetName: devtest_shard01
  cacheSizeGB: 1
  sources:
  - name: mongodev_shardsvr01_p
    master: true
    arbiter: false 
    ip: {{ ip_addrs.mongodev_shardsvr01_p }}
    port: 3308
  - name: mongodev_shardsvr01_s1
    master: false
    arbiter: false 
    ip: {{ ip_addrs.mongodev_shardsvr01_s1 }}
    port: 3309
  - name: mongodev_arbiter01
    master: false
    arbiter: true
    ip: {{ ip_addrs.mongodev_arbiter01 }}
    port: 3308
  users:
    - database: admin
      name: admin
      passwd: admin2018
      user: admin
      password: admin2018
      authdb: admin

mongodev_shardsvr02:
  svc_name: mongodev_shardsvr02_s1
  external_net_bindip: {{ ip_addrs.mongodev_shardsvr02_s1 }}
  dataPath: {{ stack.mongo.data_path }}/mongodev_shardsvr02_s1
  port: 3309
  is_master: false
  replSetName: devtest_shard02
  cacheSizeGB: 1

WxMongo:
  users:
    - database: gameplaza
      name: gameplaza
      passwd: plaza123
      roles: ['dbOwner']
    - database: voice
      name: voicemaster
      passwd: voice123
      roles: ['dbOwner']
    - database: gamedesk
      name: gamedesk
      passwd: desk123
      roles: ['dbOwner']
    - database: misc
      name: misc
      passwd: misc123
      roles: ['dbOwner']
    - database: lwsm
      name: lwsm
      passwd: lwsm123
      roles: ['dbOwner']
    - database: policys
      name: policys
      passwd: policy123
      roles: ['dbOwner']
    - database: oss
      name: oss
      passwd: oss123
      roles: ['dbOwner']
    - database: account
      name: account
      passwd: account123
      roles: ['dbOwner']


mongos:
  use_external_net: true
  external_net_bindip: {{ ip_addrs.mongodev_configsvr01 }}
