elasticsearch:
  config:
    node.name: demo-node-2
    discovery.zen.ping.unicast.hosts: ["10.34.52.124", "10.34.52.125"]  

filebeat:
  config:
    filebeat.prospectors:
      - input_type: log
        document_type: redmineProdLog
        paths:
          - /opt/redmine/log/production.log
      - output.logstash:
          hosts:
            - 10.34.52.125:5000
