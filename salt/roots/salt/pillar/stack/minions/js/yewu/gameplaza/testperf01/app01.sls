environment: desktop_testperf01

hostname: app01.testperf01.gameplaza.yewu.js

roles:
- python3
- mongo
- mongos
- nginx
- accountcenter
- deskcomponent

services:
- WxGamePlaza_web
- WxGamePlaza_celerybeat
- WxGamePlaza_celeryd # default celery worker
- WxGamePlaza_httpagent
- WxGamePlaza_handler
- WxDeskManagement_frontend
- WxVoiceMaster_frontend
- WxGamePlaza_frontend
- WxDesktopClient_frontend
- WxPublicity_frontend
- LwMarket_frontend
- WxBarshop_frontend
- WxBarshop_frontendb
- WxBarshop_frontendc
- WxImageServer_frontend
- WxDesktopAccount_frontend
- WxBarshop_frontend_report