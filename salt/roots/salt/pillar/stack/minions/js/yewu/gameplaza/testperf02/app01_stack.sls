{% set ip_addrs = stack.get('ip_addrs', {}) %}

# blue env
wxgameplaza:
  handler:
    settings:
      LOCAL_IP: {{ ip_addrs.wxgameplaza_handler01 }}
      CPU_COUNT: 2
      SERVER_ADDRESS: # environment conf
      - __: overwrite
      - {{ ip_addrs.wxgameplaza_httpagent }}
      - '3315'
  web:
    bind: {{ ip_addrs.wxgameplaza_web01 }}
    processes: 2
  celeryd:
    queue: schedule
    nodes_name: schedule_worker
  nginx:
    web:
      frontend:
        server_name: plaza.wxdesk.com img.wxdesk.com style.wxdesk.com {{ ip_addrs.nginx_frontend }}
        access_log_file: wxgameplaza_front.log
        proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8083/
    handler:
      frontend:
        server_name: upload.wxdesk.com
        proxy_host: {{ ip_addrs.nginx_backend }}:8088

WxImageServer:
  nginx:
    frontend:
      server_name: uploadqdd.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8085/

WxDeskManagement:
  nginx:
    frontend:
      server_name: qdd.wxdesk.com
      access_log_file: WxDeskManagement_frontend.log
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8081/
      proxy_pass_upload_url: http://{{ ip_addrs.nginx_backend }}:8085/upload/
      oss_url: http://{{ ip_addrs.nginx_backend }}:8085/oss/

WxVoiceMaster:
  nginx:
    frontend:
      server_name: voice.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8082/

LwMarket:
  nginx:
    web:
      frontend:
        server_name: lwsm.wxdesk.com
        proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8086

goodscenter:
  nginx:
    frontend:
      server_name: goods.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8090/