{% set ip_addrs = stack.get('ip_addrs', {}) %}


wx_policy_redis:
  install_prefix: /data/SicentApp/wx_policy_redis
  log_dir: /data/SicentApp/wx_policy_redis/log
  data_dir: /data/SicentApp/wx_policy_redis/db
  bind: {{ ip_addrs.policy_redis.ip }}
  password: {{ ip_addrs.policy_redis.password }}
  port: {{ ip_addrs.policy_redis.port }}
  svc_name: wx_policy_redis


filebeat:
  config:
    filebeat.prospectors:

      - input_type: log
        document_type: wxpolicy
        paths:
          - {{ stack.wxpolicy.logmgr.log_dir }}/policy.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after

    output.logstash:
      hosts: ["172.30.236.79:5000"]
