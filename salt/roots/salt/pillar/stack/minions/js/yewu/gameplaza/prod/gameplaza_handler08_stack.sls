{% set ip_addrs = stack.get('ip_addrs', {}) %}

filebeat:
  config:
    filebeat.prospectors:

      - input_type: log
        document_type: game_handler
        paths:
          - {{ stack.wxgameplaza.handler.install_path }}/GamePlazaHandler/logs/gameplaza_handler.log
        fields:
          ip: 172.30.236.203
    output.logstash:
      hosts: ["172.30.236.79:5000"]
