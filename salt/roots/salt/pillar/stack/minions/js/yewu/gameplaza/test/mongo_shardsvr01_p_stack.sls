{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set my_port = "27018" %}

mongo_shardsvr:
  bind_ip: {{ ip_addrs.get('mongo_shardsvr01_p', '') }}
  primary: {{ ip_addrs.get('mongo_shardsvr01_p', '') }}

redis:
  bind: {{ ip_addrs.get('redis_stage', '')}}
  password: 'redis123'

wxgameplaza:
  web:
    bind: {{ ip_addrs.get('wxgameplaza_web02', '')}}
