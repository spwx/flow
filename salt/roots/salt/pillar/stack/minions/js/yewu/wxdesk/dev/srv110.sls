environment: wxdesk_dev

hostname: newestnginx.dev.wxdesk.yewu.js

roles:
  - nginx
  - mongo
  - mongo_shardsvr
  - mongos
