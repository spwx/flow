environment: gameplaza_test

hostname: mongo_shardsvr02_p.test.gameplaza.yewu.js

minion_role: test.mongo

roles:
  - mongo
  - python3
  - deskcomponent
  - accountcenter
  - mongo_shardsvr
  - mongos
  - webagent

services:
- WxGamePlaza_handler