environment: gameplaza_prod

roles:
- nginx
- vsftpd
- mongo
- mongos
- python3

services:
- WxImageServer
- mongo
