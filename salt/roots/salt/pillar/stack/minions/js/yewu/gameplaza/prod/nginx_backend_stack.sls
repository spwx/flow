{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxDesktopClient:
  nginx:
    web:
      frontend:
        server_name: right.wxdesk.com
        access_log_file: WxDesktopClient_frontend.log

WxImageServer:
  nginx:
    backend: # port: 8085
      server_name: uploadqdd.wxdesk.com oss.wxdesk.com {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
      proxy_pass_url: http://{{ ip_addrs.WxImageServer }}/

WxDeskManagement:
  nginx:
    backend: # port: 8081
      server_name: qdd.wxdesk.com {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
      access_log_file: WxDeskManagement_backend.log
      statscenter_upstream: {{ ip_addrs.statscenter_upstream }}
      feedback_url: {{ ip_addrs.desktop_platform_interface }}/setfeedback.php
      oss_url: http://{{ ip_addrs.nginx_backend_wlan }}:8085/oss/

      blue_servers:
        - host: {{ ip_addrs.WxDeskManagement_web01 }}
          port: 22131
          weight: 1
      green_servers:
        - host: {{ ip_addrs.WxDeskManagement_web02 }}
          port: 22131
          weight: 1

WxVoiceMaster:
  nginx:
    backend:
      web: # port: 8082
        server_name: voice.wxdesk.com {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
        access_log_file: WxVoiceMaster_backend_web.log

        blue_servers:
          - host: {{ ip_addrs.WxVoiceMaster_web01 }}
            port: 22121
            weight: 1
        green_servers:
          - host: {{ ip_addrs.WxVoiceMaster_web02 }}
            port: 22121
            weight: 1
      api: # port: 8084
        server_name: voice.wxdesk.com {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
        access_log_file: WxVoiceMaster_backend_api.log
        blue_servers:
          - host: {{ ip_addrs.WxVoiceMaster_web01 }}
            port: 22122
            weight: 1
        green_servers:
          - host: {{ ip_addrs.WxVoiceMaster_web02 }}
            port: 22122
            weight: 1

wxgameplaza:
  nginx:
    web:
      backend: # port: 8083
        server_name: plaza.wxdesk.com img.wxdesk.com style.wxdesk.com {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
        usercenter_upstream: {{ ip_addrs.usercenter_upstream }}
        oss_url: http://{{ ip_addrs.nginx_backend_wlan }}:8085/oss/
        egs_upstream: {{ ip_addrs.egs_upstream }}

        blue_servers:
          - host: {{ ip_addrs.wxgameplaza_web01 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web02 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web03 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web04 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web05 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web06 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web07 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web08 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web09 }}
            port: 22111
            weight: 2

        green_servers:
          - host: {{ ip_addrs.wxgameplaza_web11 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web12 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web15 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web16 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web13 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web14 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web17 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web18 }}
            port: 22111
            weight: 2
          - host: {{ ip_addrs.wxgameplaza_web19 }}
            port: 22111
            weight: 2

LwMarket:
  nginx:
    web:
      backend: # port: 8086
        server_name: lwsm.wxdesk.com {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
        access_log_file: lwsm_backend.log
        blue_servers:
          - host: {{ ip_addrs.LwMarket_web01 }}
            port: 22114
            weight: 1
          - host: {{ ip_addrs.LwMarket_web02 }}
            port: 22114
            weight: 1

wxdesktop_account:
  nginx:
     backend: # port: 8087
        server_name: auth.wxdesk.com {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
        access_log_file: account_backend.log
        blue_servers:
          - host: {{ ip_addrs.WxDesktopAccount_web01 }}
            port: 22115
            weight: 1
          - host: {{ ip_addrs.WxDesktopAccount_web02 }}
            port: 22115
            weight: 1

        green_servers: []

  nginx_inner:
     backend: # port: 8100
        server_name:  {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
        access_log_file: account_backend.log
        blue_servers:
          - host: {{ ip_addrs.WxDesktopAccount_web01 }}
            port: 22123
            weight: 1
          - host: {{ ip_addrs.WxDesktopAccount_web02 }}
            port: 22123
            weight: 1

        green_servers: []

  web:
    bind: {{ ip_addrs.WxDesktopAccount_web01 }}
    processes: 2


goodscenter:
  nginx:
     backend: # port: 8090
        server_name: goods.wxdesk.com {{ ip_addrs.nginx_backend }}
        access_log_file: goods_backend.log
        blue_servers:
          - host: {{ ip_addrs.GoodsCenter_web01 }}
            port: 22120
            weight: 1
          - host: {{ ip_addrs.GoodsCenter_web02 }}
            port: 22120
            weight: 1

        green_servers: []

  web:
    bind: {{ ip_addrs.GoodsCenter_web01 }}
    processes: 2

