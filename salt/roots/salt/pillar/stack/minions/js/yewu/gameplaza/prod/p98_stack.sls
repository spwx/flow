{% set ip_addrs = stack.get('ip_addrs', {}) %}



policy_slaveof1_redis:
  install_prefix: /data/SicentWebserver/policy_slaveof1_redis
  log_dir: /data/SicentWebserver/policy_slaveof1_redis/log
  data_dir: /data/SicentWebserver/policy_slaveof1_redis/db
  bind: {{ ip_addrs.policy_slaveof1_redis.ip }}
  password: {{ ip_addrs.policy_slaveof1_redis.password }}
  port: {{ ip_addrs.policy_slaveof1_redis.port }}
  svc_name: policy_slaveof1_redis



policy_slaveof2_redis:
  install_prefix: /data/SicentWebserver/policy_slaveof2_redis
  log_dir: /data/SicentWebserver/policy_slaveof2_redis/log
  data_dir: /data/SicentWebserver/policy_slaveof2_redis/db
  bind: {{ ip_addrs.policy_slaveof2_redis.ip }}
  password: {{ ip_addrs.policy_slaveof2_redis.password }}
  port: {{ ip_addrs.policy_slaveof2_redis.port }}
  svc_name: policy_slaveof2_redis
