environment: gameplaza_test

roles:
- python3
- redis
- accountcenter

services:
- WxAccountCenter_celerybeat
- WxAccountCenter_celeryd
