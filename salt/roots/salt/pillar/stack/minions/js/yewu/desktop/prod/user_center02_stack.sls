
filebeat:
  config:
    filebeat.prospectors:

      - input_type: log
        document_type: usercenter
        paths:
          - {{ stack.usercenter.web.install_path }}/usercenter/logs/user_center.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
        fields:
          ip: 172.30.236.147


    output.logstash:
      hosts: ["172.30.236.79:5000"]
