environment: desktop_testperf02

hostname: app04.testperf02.gameplaza.yewu.js

roles:
- python3
- mongo
- mongos
- redis
- accountcenter
- nginx

services:
- WxDesktopAccount