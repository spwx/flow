environment: gameplaza_prod

hostname: gameplaza_handler09.prod.gameplaza.yewu.js
minion_role: prod.gameplaza_handler
roles:
  - mongo
  - mongos
  - redis
  - webagent
  - python3
  - deskcomponent
  - accountcenter

services:
- WxGamePlaza_handler
