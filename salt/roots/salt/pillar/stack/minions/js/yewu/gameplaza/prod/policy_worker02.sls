environment: gameplaza_prod

hostname: policy_worker02.prod.gameplaza.yewu.js
minion_role: prod.policy_redis
roles:
  - mongo
  - mongos
  - python3
  - redis


services:
- WxPolicyServer

