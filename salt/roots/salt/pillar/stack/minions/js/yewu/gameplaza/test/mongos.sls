environment: gameplaza_test


hostname: mongos.test.gameplaza.yewu.js

minion_role: test.mongo

roles:
  - mongo
  - mongos
  - redis
  - nginx
  - python3

services:
- WxDeskManagement_frontend
- WxVoiceMaster_frontend
- WxGamePlaza_frontend
- WxDesktopClient_frontend
- WxPublicity_frontend
