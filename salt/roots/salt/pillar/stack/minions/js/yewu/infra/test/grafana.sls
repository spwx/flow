environment: gameplaza_test

hostname: grafana.test.infra.yewu.js

roles:
  - python3
  - jdk
  - filebeat
  - logstash
  - kibana
  - elasticsearch
