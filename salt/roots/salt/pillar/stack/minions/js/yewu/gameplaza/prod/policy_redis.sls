environment: gameplaza_prod

hostname: policy_redis.prod.gameplaza.yewu.js
minion_role: prod.policy_redis
roles:
  - mongo
  - mongos
  - python3
  - redis
  - accountcenter


services:
- WxPolicyServer

