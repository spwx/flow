{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% from "WxLogstash/vars.j2" import WxLogstash with context %}


elasticsearch:
  version: '5.4.3'
  pkg_url: http://uploadqdd.wxdesk.com/upload/share_image/elasticsearch-5.4.3.rpm
  jvm:
    Xms: 2g
    Xmx: 2g
  config:
    cluster.name: sample
    node.name: node-1
    node.master: true
    node.data: true
    path.data: /data/SicentApp/elasticsearch/db
    path.logs: /data/SicentApp/elasticsearch/logs
    network.host: 0.0.0.0
    http.port: 9200
    bootstrap.system_call_filter: false
    # discovery.zen.ping.unicast.hosts: []



filebeat:
  config:
    filebeat.prospectors:
    {%- for filename in WxLogstash.WxGamePlaza %}
      - input_type: log
        document_type: web_log_{{ filename }}
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/{{ filename }}.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
        fields:
          ip: 10.34.56.101
    {%- endfor %}


    {%- for filename in WxLogstash.WxDeskManagement %}
      - input_type: log
        document_type: management_log_{{ filename }}
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/{{ filename }}.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
        fields:
          ip: 10.34.56.101
    {%- endfor %}
    
    output.logstash:
      hosts: ["10.34.60.128:5000"]
