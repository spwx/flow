environment: gameplaza_prod

{% set ip_addrs = stack.get('ip_addrs', {}) %}


lcs_routing_redis:
  install_prefix: /data/SicentApp/lcs_routing_redis
  log_dir: /data/SicentApp/lcs_routing_redis/log
  data_dir: /data/SicentApp/lcs_routing_redis/db
  bind: {{ ip_addrs.lcs_routing_redis.ip }}
  password: {{ ip_addrs.lcs_routing_redis.password }}
  port : {{ ip_addrs.lcs_routing_redis.port }}
  svc_name: redis_lcs_routing