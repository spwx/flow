environment: wxdesk_dev

hostname: web02.dev.wxdesk.yewu.js

roles:
- python3
- deskcomponent
- accountcenter
- nginx
- mongos
- filebeat

services:
- WxGamePlaza_handler
- gologstat


