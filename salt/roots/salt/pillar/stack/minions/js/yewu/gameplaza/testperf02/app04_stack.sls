{% set ip_addrs = stack.get('ip_addrs', {}) %}

policy_redis:
  install_prefix: /data/SicentApp/policy_redis
  log_dir: /data/SicentApp/policy_redis/log
  data_dir: /data/SicentApp/policy_redis/db
  bind: {{ ip_addrs.policy_redis.ip }}
  password: {{ ip_addrs.policy_redis.password }}
  port : {{ ip_addrs.policy_redis.port }}
  svc_name: policy_redis

policymgr_redis:
  install_prefix: /data/SicentApp/policymgr_redis
  log_dir: /data/SicentApp/policymgr_redis/log
  data_dir: /data/SicentApp/policymgr_redis/db
  bind: {{ ip_addrs.policymgr_redis.ip }}
  password: {{ ip_addrs.policymgr_redis.password }}
  port : {{ ip_addrs.policymgr_redis.port }}
  enable_save: False
  svc_name: policymgr_redis

WxBarshop:
  nginx:
     backend: # port: 8087
        server_name: barshopv.wxdesk.com {{ ip_addrs.nginx_backend2 }}
        access_log_file: barshop_backend.log
        blue_servers:
          - host: {{ ip_addrs.WxBarshop_web01 }}
            port: 22118
            weight: 1
  nginxb:
     backend: # port: 8088
        server_name: barshopb.wxdesk.com {{ ip_addrs.nginx_backend2 }}
        access_log_file: barshopb_backend.log
        blue_servers:
          - host: {{ ip_addrs.WxBarshop_web01 }}
            port: 22116
            weight: 1
  nginxc:
     backend: # port: 8089
        server_name: barshopc.wxdesk.com {{ ip_addrs.nginx_backend2 }}
        access_log_file: barshopc_backend.log
        blue_servers:
          - host: {{ ip_addrs.WxBarshop_web01 }}
            port: 22117
            weight: 1

  web:
    bind: {{ ip_addrs.WxBarshop_web01 }}
    processes: 2
  webb:
    bind: {{ ip_addrs.WxBarshop_web01 }}
    processes: 2
  webc:
    bind: {{ ip_addrs.WxBarshop_web01 }}
    processes: 2
