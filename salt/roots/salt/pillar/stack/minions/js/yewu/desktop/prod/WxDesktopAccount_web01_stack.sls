
filebeat:
  config:
    filebeat.prospectors:



      - input_type: log
        document_type: user_account
        paths:
          - {{ stack.wxdesktop_account.web.install_path }}/desktop_account/logs/user_account.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
        fields:
          ip: 172.30.236.57


    output.logstash:
      hosts: ["172.30.236.79:5000"]
