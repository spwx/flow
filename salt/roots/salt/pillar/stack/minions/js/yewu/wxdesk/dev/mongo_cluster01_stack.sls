{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongo_configsvr:
  svc_name: mongo_configsvr
  dataPath: {{ stack.mongo.data_path }}/configdb
  replSetName: dev_cfg_shard
  external_net_bindip: {{ ip_addrs.mongo02_configsvr01 }}
  port: 3307
  is_master: true
  is_configsvr: true
  sources:
  - name: mongo02_configsvr01
    master: true
    arbiter: false
    ip: {{ ip_addrs.mongo02_configsvr01 }}
    port: 3307
  - name: mongo02_configsvr02
    master: false
    arbiter: false
    ip: {{ ip_addrs.mongo02_configsvr02 }}
    port: 3307
  users:
  - database: admin
    name: admin
    passwd: admin2018
    user: admin
    password: admin2018
    authdb: admin

mongo02_shardsvr01:
  svc_name: mongo02_shardsvr01_p
  external_net_bindip: {{ ip_addrs.mongo02_shardsvr01_p }}
  dataPath: {{ stack.mongo.data_path }}/mongo02_shardsvr01_p
  port: 3308
  is_master: true
  replSetName: dev_shard01
  cacheSizeGB: 1
  sources:
  - name: mongo02_shardsvr01_p
    master: true
    arbiter: false 
    ip: {{ ip_addrs.mongo02_shardsvr01_p }}
    port: 3308
  - name: mongo02_shardsvr01_s1
    master: false
    arbiter: false 
    ip: {{ ip_addrs.mongo02_shardsvr01_s1 }}
    port: 3309
  users:
    - database: admin
      name: admin
      passwd: admin2018
      user: admin
      password: admin2018
      authdb: admin

mongo02_shardsvr02:
  svc_name: mongo02_shardsvr02_s1
  external_net_bindip: {{ ip_addrs.mongo02_shardsvr02_s1 }}
  dataPath: {{ stack.mongo.data_path }}/mongo02_shardsvr02_s1
  port: 3309
  is_master: false
  replSetName: dev_shard02
  cacheSizeGB: 1

WxMongo:
  users:
    - database: gameplaza
      name: gameplaza
      passwd: plaza123
      roles: ['dbOwner']
    - database: voice
      name: voicemaster
      passwd: voice123
      roles: ['dbOwner']
    - database: gamedesk
      name: gamedesk
      passwd: desk123
      roles: ['dbOwner']
    - database: misc
      name: misc
      passwd: misc123
      roles: ['dbOwner']
    - database: lwsm
      name: lwsm
      passwd: lwsm123
      roles: ['dbOwner']
    - database: policys
      name: policys
      passwd: policy123
      roles: ['dbOwner']
    - database: oss
      name: oss
      passwd: oss123
      roles: ['dbOwner']
    - database: account
      name: account
      passwd: account123
      roles: ['dbOwner']
    - database: goodscenter
      name: goodscenter
      passwd: goods123
      roles: ['dbOwner']
    - database: agentsell
      name: agentsell
      passwd: agent123
      roles: ['dbOwner']
    - database: statscenter
      name: statscenter
      passwd: stats123
      roles: ['dbOwner']
    - database: usercenter
      name: usercenter
      passwd: user123
      roles: ['dbOwner']
    - database: egs
      name: egs
      passwd: egs123
      roles: ['dbOwner']


mongos:
  use_external_net: true
  external_net_bindip: {{ ip_addrs.mongo02_configsvr01 }}
