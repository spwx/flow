{% set ip_addrs = stack.get('ip_addrs', {}) %}
mongo_configsvr:
  bind_ip: {{ ip_addrs.get('mongo_configsvr02', '127.0.0.1') }}

redis:
  bind: {{ ip_addrs.get('redis_bar', '127.0.0.1') }}
  password: 'redis123'

wxgameplaza:
  celeryd:
    queue: schedule
    nodes_name: schedule_worker
  web:
    bind: {{ ip_addrs.wxgameplaza_web01 }}
  nginx:
    httpagent:
      server_name: "localhost {{ ip_addrs.wxgameplaza_httpagent }}"
      servers:
        httpagent_blue:
          host: {{ ip_addrs.wxgameplaza_httpagent }}
          port: 8081
        httpagent_green:
          host: {{ ip_addrs.wxgameplaza_httpagent }}
          port: 8082
