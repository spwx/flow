environment: gameplaza_test

hostname: gameplaza_handler02.test.gameplaza.yewu.js

roles:
  - python3
  - deskcomponent
  - accountcenter
  - mongo
  - mongos
  - webagent

services:
- WxGamePlaza_handler
