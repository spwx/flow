{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxImageServer:
  server:
    store_path: /data/wximage/server
    exports: 
      - /data/wximage/server {{ ip_addrs.WxDeskManagement_web01 }}(insecure,rw,sync)
      - /data/wximage/server {{ ip_addrs.WxDeskManagement_web02 }}(insecure,rw,sync)
      - /data/wximage/server {{ ip_addrs.LwMarket_web01 }}(insecure,rw,sync)
      - /data/wximage/server {{ ip_addrs.LwMarket_web02 }}(insecure,rw,sync)
  nginx:
    server:
      server_name: {{ ip_addrs.WxImageServer }}
      store_path: /data/wximage/server

vsftpd:
  users:
    wximage:
      password: 'wximage123'