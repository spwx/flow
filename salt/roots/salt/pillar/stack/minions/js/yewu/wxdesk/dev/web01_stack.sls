{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set WxLogstash = stack.get('WxLogstash', {}) %}


wxgameplaza:
  nginx:
    web:
      frontend:
        server_name: plaza.wxdesk.com img.wxdesk.com style.wxdesk.com {{ ip_addrs.nginx_front }}
        access_log_file: wxgameplaza_front.log
        proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8083/
    httpagent:
      server_name: upload.wxdesk.com
      access_log_file: httpagent_bg_access.log
      servers:
        httpagent_blue:
          host: {{ ip_addrs.wxgameplaza_httpagent }}
          port: 8081

  web:
    bind: {{ ip_addrs.wxgameplaza_web01 }}

  handler:
    settings:
      LOCAL_IP: {{ ip_addrs.wxgameplaza_handler01 }}


WxDeskManagement:
  web:
    bind: {{ ip_addrs.wxgameplaza_web01 }}


LwMarket:
  web:
    bind: {{ ip_addrs.wxgameplaza_web01 }}


filebeat:
  config:
    filebeat.prospectors:
    {% for filename in WxLogstash.WxGamePlaza %}
      - input_type: log
        document_type: web_log_{{ filename }}
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/{{ filename }}.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
        fields:
          ip: 10.34.56.101
    {%- endfor %}


    {% for filename in WxLogstash.WxDeskManagement %}
      - input_type: log
        document_type: management_log_{{ filename }}
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/{{ filename }}.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after
        fields:
          ip: 10.34.56.101
    {%- endfor %}
    
    output.logstash:
      hosts: ["10.34.60.128:5000"]
