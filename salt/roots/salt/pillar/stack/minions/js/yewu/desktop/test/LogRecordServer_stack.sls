{% set ip_addrs = stack.get('ip_addrs', {}) %}
LogRecordServer:
  cfg:
    svrip: {{ ip_addrs.get('LogRecordServer', '127.0.0.1')}}

vsftpd:
  users:
    PolicyFTP:
      password: 'policyFtp'