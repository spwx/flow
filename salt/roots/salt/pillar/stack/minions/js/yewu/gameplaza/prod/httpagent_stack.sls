{% set ip_addrs = stack.get('ip_addrs', {}) %}
wxgameplaza:
  nginx:
    httpagent:
      server_name: upload.wxdesk.com {{ ip_addrs.wxgameplaza_httpagent }}
      access_log_file: httpagent_bg_access.log
      proxy_host: {{ ip_addrs.nginx02_backend_wlan}}:8088