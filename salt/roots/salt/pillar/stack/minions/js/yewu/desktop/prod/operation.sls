environment: gameplaza_prod

hostname: operation.prod.desktop.yewu.js

roles:
- nginx
- python3
- mongo
- mongos

services:
- WxDesktopOperation_web
- gologstat
