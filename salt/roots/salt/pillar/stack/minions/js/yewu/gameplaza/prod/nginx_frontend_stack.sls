{% set ip_addrs = stack.get('ip_addrs', {}) %}

# --// new nginx configure
WxDesktopClient:
  nginx:
    web:
      frontend:
        server_name: right.wxdesk.com
        access_log_file: WxDesktopClient_frontend.log

WxImageServer:
  nginx:
    frontend:
      server_name: uploadqdd.wxdesk.com oss.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend_wlan }}:8085/

WxDeskManagement:
  nginx:
    frontend:
      server_name: qdd.wxdesk.com
      access_log_file: WxDeskManagement_frontend.log
      proxy_pass_url: http://{{ ip_addrs.nginx_backend_wlan }}:8081/
      proxy_pass_upload_url: http://{{ ip_addrs.nginx_backend_wlan }}:8085/upload/
      oss_url: http://{{ ip_addrs.nginx_backend_wlan }}:8085/oss/

WxVoiceMaster:
  nginx:
    frontend:
      server_name: voice.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend_wlan }}:8082/

LwMarket:
  nginx:
    web:
      frontend:
        server_name: lwsm.wxdesk.com
        proxy_pass_url: http://{{ ip_addrs.nginx_backend_wlan }}:8086

wxgameplaza:
  nginx:
    web:
      frontend:
        server_name: plaza.wxdesk.com img.wxdesk.com style.wxdesk.com {{ ip_addrs.nginx_frontend }}
        access_log_file: wxgameplaza_front.log
        proxy_pass_url: http://{{ ip_addrs.nginx_backend_wlan }}:8083/
        oss_url: http://{{ ip_addrs.nginx_backend_wlan }}:8085/oss/
        egs_outer_url: http://{{ ip_addrs.nginx_backend_wlan_164 }}:8092

wxdesktop_account:
  nginx:
    frontend:
      server_name: auth.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend_wlan }}:8087/

goodscenter:
  nginx:
    frontend:
      server_name: goods.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend_wlan }}:8090/
