environment: gameplaza_prod

hostname: user_center01.prod.desktop.yewu.js

roles:
- python3
- mongo
- mongos
- accountcenter
- deskcomponent

services:
- WxDesktopAccount
- usercenter
