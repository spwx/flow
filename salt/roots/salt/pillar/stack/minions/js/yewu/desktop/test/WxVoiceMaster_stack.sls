{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxImageServer:
  server:
    store_path: /data/wximage/server
    exports: 
      - /data/wximage/server {{ ip_addrs.WxDeskManagement }}(insecure,rw,sync)
  client:
    store_path: /data/wximage/client
    server:
      ip: {{ ip_addrs.WxImageServer }}
      store_path: /data/wximage/server
  nginx:
    server:
      server_name: {{ ip_addrs.WxImageServer }}
      store_path: /data/wximage/server

python3:
  install_prefix: /data/SicentApp/python3
