{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set artifacts_path = '/data/SicentTools/artifacts' %}
# close selinux and reboot system.
vsftpd:
  users:
    jenkinsSalt:
      password: 'js*%##@!!salt'
      conf:
        anon_world_readable_only: 'NO'
        anon_upload_enable: 'YES'
        anon_mkdir_write_enable: 'YES'
        anon_other_write_enable: 'YES'
        local_root: {{ artifacts_path }}

saltops:
  install_path: /data/SicentWebserver/salt/ops
  log_dir: /data/SicentWebserver/salt/ops/logs
  uwsgi:
    bind: {{ ip_addrs.get('salt_master', '127.0.0.1')}}
    port: 8085
  nginx:
    server_name: "localhost {{ ip_addrs.get('salt_master', '127.0.0.1') }}"
    backend:
      servers:
        - host: {{ ip_addrs.get('salt_master', '127.0.0.1')}}
          port: 8085
  settings:
    DEBUG: True
    ALLOWED_HOSTS: ['*']
    STATIC_ROOT: /data/SicentWebserver/salt/ops/frontend
    SALT_API:
      URL: http://salt.nextbu.cn:8000
      USERNAME: saltpad
      PASSWORD: saltpad
      EAUTH: pam
    PKG_ARTIFACT:
      PATH: {{ artifacts_path }}
      SAVE_COUNT: 5
    DATABASES:
      default:
        ENGINE: 'django.db.backends.mysql'
        HOST: {{ ip_addrs.get('salt_master', '127.0.0.1') }}
        PORT: '3306'
        NAME: 'saltops'
        USER: 'saltops'
        PASSWORD: 'saltops'
