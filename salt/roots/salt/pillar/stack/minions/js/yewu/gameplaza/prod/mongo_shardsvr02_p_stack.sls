{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongo_configsvr:
  bind_ip: {{ ip_addrs.get('mongo_configsvr02', '0.0.0.0') }}

mongo_shardsvr:
  bind_ip: {{ ip_addrs.get('mongo_shardsvr02_p', '0.0.0.0') }}
  primary: {{ ip_addrs.get('mongo_shardsvr02_p', '0.0.0.0') }}
