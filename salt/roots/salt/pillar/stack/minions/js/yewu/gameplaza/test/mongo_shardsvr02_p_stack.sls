{% set ip_addrs = stack.get('ip_addrs', {}) %}
mongo_shardsvr:
  bind_ip: {{ ip_addrs.get('mongo_shardsvr02_p', '') }}
  primary: {{ ip_addrs.get('mongo_shardsvr02_p', '') }}

redis:
  bind: {{ ip_addrs.get('redis_reward', '') }}
  password: 'redis123'

# blue env
wxgameplaza:
  handler:
    settings:
      LOCAL_IP: {{ ip_addrs.wxgameplaza_handler01 }}
