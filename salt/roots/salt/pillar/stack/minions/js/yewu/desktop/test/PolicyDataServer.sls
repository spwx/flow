environment: gameplaza_test

hostname: PolicyDataServer.test.desktop.yewu.js

minion_role: test.PolicyDataServer

roles:
  - redis
  - nginx
  - python3
  - accountcenter
  - deskcomponent
services:
  - PolicyDataServer
