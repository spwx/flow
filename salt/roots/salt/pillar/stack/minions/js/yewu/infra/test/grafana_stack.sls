elasticsearch:
  config:
    node.name: demo-node-1
    discovery.zen.ping.unicast.hosts: ["10.34.52.124", "10.34.52.125"]  

logstash:
  pathConfig:
    - name: redmineOpstackXyz
      config: |
        input {
          beats {
            port => 5000
          }
        }
        output {
          if [type] = 'redmineProdLog' {
            elasticsearch {
              hosts=>["10.34.52.125#x3A9200"]
              password => "changeme"
              user=>"elastic"
              index => "logstash-redmineOpstackXyz-%{+YYYY.MM.dd}"
              document_type => "%{type}"
              workers => 1
              flush_size => 20
            }
          }
        }

