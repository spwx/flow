environment: salt_dev

roles:
  - mongo
  - redis
  - python3
  - ruby
