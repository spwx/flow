{% set ip_addrs = stack.get('ip_addrs', {}) %}
WxDeskManagement:
  web:
    bind: {{ ip_addrs.WxDeskManagement }}

WxImageServer:
  client:
    store_path: /data/wximage/client
    server:
      ip: {{ ip_addrs.WxImageServer }}
      store_path: /data/wximage/server

python3:
  install_prefix: /data/SicentApp/python3


filebeat:
  config:
    filebeat.prospectors:
      - input_type: log
        document_type: management_log_db
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/db.log
      - input_type: log
        document_type: management_log_django_request
        paths:
          - {{ stack.WxDeskManagement.web.install_path }}/qdd_management/logs/django_request.log
    output.logstash:
      hosts: ["10.34.52.125:5000"]
