environment: wxdesk_dev
hostname: mongo_cluster01.dev.wxdesk.yewu.js
roles:
  - python3
  - mongo
  - mongo_shardsvr
  - mongos
