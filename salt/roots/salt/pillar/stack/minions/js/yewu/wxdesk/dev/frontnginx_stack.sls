{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxDeskManagement:
  nginx:
    frontend:
      server_name: qdd.wxdesk.com {{ ip_addrs.nginx_front }}
      access_log_file: WxDeskManagement_frontend.log
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8081/
      proxy_pass_upload_url: http://{{ ip_addrs.nginx_backend }}:8085/upload/
      oss_url: http://{{ ip_addrs.nginx_backend }}:8085/oss/

WxVoiceMaster:
  nginx:
    frontend:
      server_name: voice.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8082/

wxgameplaza:
  nginx:
    web:
      frontend:
        server_name: plaza.wxdesk.com img.wxdesk.com style.wxdesk.com
        access_log_file: wxgameplaza_front.log
        proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8083/
        oss_url: http://{{ ip_addrs.nginx_backend }}:8085/oss/
        egs_outer_url: http://{{ ip_addrs.nginx_backend }}:8092

    handler:
      frontend:
        server_name: upload.wxdesk.com
        proxy_host: {{ ip_addrs.nginx_backend }}:8088

WxImageServer:
  nginx:
    frontend:
      server_name: uploadqdd.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8085/

LwMarket:
  nginx:
    web:
      frontend:
        server_name: lwsm.wxdesk.com
        proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8086

wxdesktop_account:
  nginx:
    frontend:
      server_name: auth.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8087/

goodscenter:
  nginx:
    frontend:
      server_name: goods.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8090/
