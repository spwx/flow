environment: wxdesk_dev
hostname: elk01.dev.wxdesk.yewu.js
roles:
  - elasticsearch
  - logstash
  - filebeat

services:
  - sentry
    
