{% set ip_addrs = stack.get('ip_addrs', {}) %}

WxDeskManagement:
  nginx:
    backend: # port: 8081
      server_name: qdd.wxdesk.com {{ ip_addrs.nginx_backend }}
      access_log_file: WxDeskManagement_backend.log
      feedback_url: {{ ip_addrs.desktop_platform_interface }}/setfeedback.php
      statscenter_upstream: {{ ip_addrs.statscenter_upstream }}
      oss_url: http://{{ ip_addrs.nginx_backend }}:8085/oss/

      blue_servers:
        - host: {{ ip_addrs.WxDeskManagement_web01 }}
          port: 22131
          weight: 1

WxVoiceMaster:
  nginx:
    backend:
      web: # port: 8082
        server_name: voice.wxdesk.com {{ ip_addrs.nginx_backend }}
        access_log_file: WxVoiceMaster_backend_web.log
        blue_servers:
          - host: {{ ip_addrs.WxVoiceMaster_web01 }}
            port: 22121
            weight: 1
      api: # port: 8084
        server_name: voice.wxdesk.com {{ ip_addrs.nginx_backend }}
        access_log_file: WxVoiceMaster_backend_api.log
        blue_servers:
          - host: {{ ip_addrs.WxVoiceMaster_web01 }}
            port: 22122
            weight: 1
wxgameplaza:
  nginx:
    web: # port 8083
      backend:
        server_name: plaza.wxdesk.com img.wxdesk.com style.wxdesk.com {{ ip_addrs.nginx_backend }}
        usercenter_upstream: {{ ip_addrs.usercenter_upstream }}
        egs_upstream: {{ ip_addrs.egs_upstream }}
        
        oss_url: http://{{ ip_addrs.nginx_backend }}:8085/oss/

        blue_servers:
          - host: {{ ip_addrs.wxgameplaza_web01 }}
            port: 22111
            weight: 2
    handler:
      backend: # port 8088
        server_name: upload.wxdesk.com {{ip_addrs.nginx_backend}}
        blue_servers:
          - host: {{ ip_addrs.wxgameplaza_handler_blue }}
            port: 19999
            weight: 2
        green_servers:
          - host: {{ ip_addrs.wxgameplaza_handler_green }}
            port: 19999
            weight: 2

WxImageServer:
  nginx:
    backend: # port: 8085
      server_name: uploadqdd.wxdesk.com {{ ip_addrs.nginx_backend }}
      proxy_pass_url: http://{{ ip_addrs.WxImageServer }}/


LwMarket:
  nginx:
    web:
       backend: # port: 8086
          server_name: lwsm.wxdesk.com {{ ip_addrs.nginx_backend }}
          access_log_file: lwsm_backend.log
          blue_servers:
            - host: {{ ip_addrs.LwMarket_web01 }}
              port: 22114
              weight: 1
  web:
    bind: {{ ip_addrs.LwMarket_web01 }}
    processes: 2

wxdesktop_account:
  nginx:
     backend: # port: 8087
        server_name: auth.wxdesk.com {{ ip_addrs.nginx_backend }}
        access_log_file: account_backend.log
        blue_servers:
          - host: {{ ip_addrs.WxDesktopAccount_web01 }}
            port: 22115
            weight: 1
        green_servers: []

  nginx_inner:
     backend: # port: 8100
        server_name:  {{ ip_addrs.nginx_backend }} {{ ip_addrs.nginx_backend_wlan }}
        access_log_file: account_backend.log
        blue_servers:
          - host: {{ ip_addrs.WxDesktopAccount_web01 }}
            port: 22123
            weight: 1

        green_servers: []
  web:
    bind: {{ ip_addrs.WxDesktopAccount_web01 }}
    processes: 2


goodscenter:
  nginx:
     backend: # port: 8090
        server_name: goods.wxdesk.com {{ ip_addrs.nginx_backend }}
        access_log_file: goods_backend.log
        blue_servers:
          - host: {{ ip_addrs.GoodsCenter_web01 }}
            port: 22120
            weight: 1
        green_servers: []

  web:
    bind: {{ ip_addrs.GoodsCenter_web01 }}
    processes: 2


egs:
  nginx_inner:
     backend: # port: 8092
        server_name:  {{ ip_addrs.nginx_backend }} 
        access_log_file: egs_backend.log
        blue_servers: {{ ip_addrs.egs_upstream }}

        green_servers: []
