
{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongo_configsvr:
  svc_name: mongo_configsvr
  dataPath: {{ stack.mongo.data_path }}/configdb
  replSetName: cfg_shard
  external_net_bindip: {{ ip_addrs.mongo02_configsvr02 }}
  port: 3307
  is_master: false
  is_configsvr: true

mongo02_shardsvr02:
  svc_name: mongo02_shardsvr02_p
  external_net_bindip: {{ ip_addrs.mongo02_shardsvr02_p }}
  dataPath: {{ stack.mongo.data_path }}/mongo02_shardsvr02_p
  port: 3308
  is_master: true
  replSetName: shard02
  cacheSizeGB: 40
  sources:
  - name: mongo02_shardsvr02_p
    master: true
    arbiter: false 
    ip: {{ ip_addrs.mongo02_shardsvr02_p }}
    port: 3308
  - name: mongo02_shardsvr02_s1
    master: false
    arbiter: false 
    ip: {{ ip_addrs.mongo02_shardsvr02_s1 }}
    port: 3309
  - name: mongo02_arbiter01
    master: false
    arbiter: true
    ip: {{ ip_addrs.mongo02_arbiter01 }}
    port: 3308
  users:
    - database: admin
      name: admin
      passwd: admin2018
      user: admin
      password: admin2018
      authdb: admin

mongo02_shardsvr01:
  svc_name: mongo02_shardsvr01_s1
  external_net_bindip: {{ ip_addrs.mongo02_shardsvr01_s1 }}
  dataPath: {{ stack.mongo.data_path }}/mongo02_shardsvr01_s1
  port: 3309
  is_master: false
  replSetName: shard01
  cacheSizeGB: 40

WxMongo:
  users:
    - database: gameplaza
      name: gameplaza
      passwd: plaza123
      roles: ['dbOwner']
    - database: voice
      name: voicemaster
      passwd: voice123
      roles: ['dbOwner']
    - database: gamedesk
      name: gamedesk
      passwd: desk123
      roles: ['dbOwner']
    - database: misc
      name: misc
      passwd: misc123
      roles: ['dbOwner']
    - database: lwsm
      name: lwsm
      passwd: lwsm123
      roles: ['dbOwner']
    - database: oss
      name: oss
      passwd: oss123
      roles: ['dbOwner']

diskPerf:
  data: 
    - DEVICENAME: sda
    - DEVICENAME: nvme0n1
    - DEVICENAME: nvme0n1p1
