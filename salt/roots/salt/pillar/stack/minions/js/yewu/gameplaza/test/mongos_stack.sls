{% set ip_addrs = stack.get('ip_addrs', {}) %}
mongos:
  bind_ip: 0.0.0.0
  configsvr:
    repl_set_name: cfg_shard
    servers:
    - host: {{ ip_addrs.get('mongo_configsvr01', '') }}
      port: 27019
    - host: {{ ip_addrs.get('mongo_configsvr02', '') }}
      port: 27019
  shardsvrs:
    - primary: {{ ip_addrs.get('mongo_shardsvr01_p', '') }}
      port: 27018
    - primary: {{ ip_addrs.get('mongo_shardsvr02_p', '') }}
      port: 27018
  admin_users:
    - username: dba
      password: 123456
      roles:
        - role: userAdminAnyDatabase
          db: admin
    - username: clusterAdmin
      password: 123456
      roles:
        - role: clusterAdmin
          db: admin
  gameplaza:
    username: gameplaza
    password: plaza123
    dba_password: 123456
    clusterAdmin_password: 123456
    dbname: gameplaza
  voicemaster:
    username: voicemaster
    password: voice123
    dba_password: 123456
    clusterAdmin_password: 123456
    dbname: voice
  gamedesk:
    username: gamedesk
    password: desk123
    dba_password: 123456
    clusterAdmin_password: 123456
    dbname: gamedesk
  misc:
    username: misc
    password: misc123
    dba_password: 123456
    clusterAdmin_password: 123456
    dbname: misc
  oss:
    username: oss
    password: oss123
    dba_password: 123456
    clusterAdmin_password: 123456
    dbname: oss

redis:
  bind: {{ ip_addrs.get('redis_celery', '') }}
  password: 'redis123'

wx_redis:
  install_prefix: /data/SicentApp/wx_redis
  log_dir: /data/SicentApp/wx_redis/log
  data_dir: /data/SicentApp/wx_redis/db
  bind: {{ ip_addrs.wx_redis.ip }}
  password: {{ ip_addrs.wx_redis.password }}
  port: {{ ip_addrs.wx_redis.port }}
  svc_name: wx_redis

voice_celery_redis:
  install_prefix: /data/SicentApp/voice_celery_redis
  log_dir: /data/SicentApp/voice_celery_redis/log
  data_dir: /data/SicentApp/voice_celery_redis/db
  bind: {{ ip_addrs.voice_celery_redis.ip }}
  password: {{ ip_addrs.voice_celery_redis.password }}
  port : {{ ip_addrs.voice_celery_redis.port }}
  svc_name: voice_celery_redis

gameplaza_celery_redis:
  install_prefix: /data/SicentApp/gameplaza_celery_redis
  log_dir: /data/SicentApp/gameplaza_celery_redis/log
  data_dir: /data/SicentApp/gameplaza_celery_redis/db
  bind: {{ ip_addrs.gameplaza_celery_redis.ip }}
  password: {{ ip_addrs.gameplaza_celery_redis.password }}
  port: {{ ip_addrs.gameplaza_celery_redis.port }}
  svc_name: gameplaza_celery_redis

WxDeskManagement:
  nginx:
    frontend:
      server_name: qdd.wxdesk.com
      access_log_file: WxDeskManagement_frontend.log
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8081/

WxVoiceMaster:
  nginx:
    frontend:
      server_name: voice.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8082/

wxgameplaza:
  nginx:
    web:
      frontend:
        server_name: plaza.wxdesk.com img.wxdesk.com style.wxdesk.com
        access_log_file: wxgameplaza_front.log
        proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8083/
        proxy_pass_upload_url: http://{{ ip_addrs.nginx_backend }}:8085/upload/

WxDesktopClient:
  nginx:
    web:
      frontend:
        server_name: right.wxdesk.com
        access_log_file: WxDesktopClient_frontend.log

WxImageServer:
  nginx:
    frontend:
      server_name: uploadqdd.wxdesk.com
      proxy_pass_url: http://{{ ip_addrs.nginx_backend }}:8085/


zbxRedis:
  data:
    - SVCNAME: wx_redis
      REDISHOST: {{ ip_addrs.wx_redis.ip }}
      REDISPORT: {{ ip_addrs.wx_redis.port }}
    - SVCNAME: voice_celery_redis
      REDISHOST: {{ ip_addrs.voice_celery_redis.ip }}
      REDISPORT: {{ ip_addrs.voice_celery_redis.port }}
    - SVCNAME: gameplaza_celery_redis
      REDISHOST: {{ ip_addrs.gameplaza_celery_redis.ip }}
      REDISPORT: {{ ip_addrs.gameplaza_celery_redis.port }}

zbxMongo:
  MONGOHOST: {{ ip_addrs.mongos.ip }}
