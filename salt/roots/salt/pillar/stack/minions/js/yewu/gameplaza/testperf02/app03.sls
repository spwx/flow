environment: desktop_testperf02

hostname: app03.testperf02.gameplaza.yewu.js

roles:
- python3
- nginx
- mongo
- mongos
- redis
- accountcenter
- deskcomponent

services:
- WxDeskManagement_web
- WxGameMenu
- WxImageServer
- WxGamePlaza_celeryd # scheduler celery worker
- WxDesktopAccount
- GoodsCenter
- statscenter
