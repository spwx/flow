environment: flow_sites
hostname: redmine.test.oa.yewu.js

roles:
  - ruby
  - jdk
  - elasticsearch
  - filebeat
