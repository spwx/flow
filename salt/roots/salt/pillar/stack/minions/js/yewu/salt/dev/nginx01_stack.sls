keepalived:
  conf_data:
    vrrp_instance:
      VI_1:
        interface: eth1
        priority: 110
        virtual_ipaddress:
          - 192.168.56.31
        track_script: 
          - chk_nginx_status
    vrrp_script chk_nginx_status:
      script: "\"service nginx status\""
      interval: 1
      weight: -2
