environment: gameplaza_prod

hostname: nginx_backend.prod.gameplaza.yewu.js
minion_role: prod.nginx_backend
roles:
  - nginx
services:
  - WxDesktopClient_frontend
  - WxDeskManagement_frontend
  - WxVoiceMaster_frontend
  - WxGamePlaza_frontend
  - WxPublicity_frontendd
  - LwMarket_frontend