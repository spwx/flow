{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongo_configsvr:
  svc_name: mongo_configsvr
  dataPath: {{ stack.mongo.data_path }}/configdb
  replSetName: cfg_shard
  external_net_bindip: {{ ip_addrs.mongo_configsvr01 }}
  port: 27019
  is_master: true
  is_configsvr: true
  sources:
  - name: mongo00
    master: true
    arbiter: false
    ip: {{ ip_addrs.mongo_configsvr01 }}
    port: 27019
  users:
  - database: admin
    name: admin
    passwd: admin123
    user: admin
    password: admin123
    authdb: admin

mongo_shardsvr:
  external_net_bindip: {{ ip_addrs.mongo_shardsvr01_p }}
  dataPath: {{ stack.mongo.data_path }}/mongo_shardsvr
  port: 27018
  is_master: true
  replSetName: shard01
  sources:
  - name: mongo00
    master: true
    arbiter: false 
    ip: {{ ip_addrs.mongo_shardsvr01_p }}
    port: 27018
  users:
    - database: admin
      name: admin
      passwd: admin123
      user: admin
      password: admin123
      authdb: admin

WxMongo:
  users:
    - database: gameplaza
      name: gameplaza
      passwd: plaza123
      roles: ['dbOwner']
    - database: voice
      name: voicemaster
      passwd: voice123
      roles: ['dbOwner']
    - database: gamedesk
      name: gamedesk
      passwd: desk123
      roles: ['dbOwner']
    - database: misc
      name: misc
      passwd: misc123
      roles: ['dbOwner']
    - database: policys
      name: policys
      passwd: policy123
      roles: ['dbOwner']
    - database: lwsm
      name: lwsm
      passwd: lwsm123
      roles: ['dbOwner']
    - database: oss
      name: oss
      passwd: oss123
      roles: ['dbOwner']
    - database: account
      name: account
      passwd: account123
      roles: ['dbOwner']
    - database: goodscenter
      name: goodscenter
      passwd: goods123
      roles: ['dbOwner']
    - database: statscenter
      name: statscenter
      passwd: stats123
      roles: ['dbOwner']
