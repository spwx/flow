{% set ip_addrs = stack.get('ip_addrs', {}) %}

accountcenter_celery_redis:
  install_prefix: /data/SicentApp/accountcenter_celery_redis
  log_dir: /data/SicentApp/accountcenter_celery_redis/log
  data_dir: /data/SicentApp/accountcenter_celery_redis/db
  bind: {{ ip_addrs.accountcenter_celery_redis.ip }}
  password: {{ ip_addrs.accountcenter_celery_redis.password }}
  port : {{ ip_addrs.accountcenter_celery_redis.port }}
  svc_name: redis_accountcenter_celery

ac_account_redis:
  install_prefix: /data/SicentApp/ac_account_redis
  log_dir: /data/SicentApp/ac_account_redis/log
  data_dir: /data/SicentApp/ac_account_redis/db
  bind: {{ ip_addrs.ac_account_redis.ip }}
  password: {{ ip_addrs.ac_account_redis.password }}
  port : {{ ip_addrs.ac_account_redis.port }}
  svc_name: redis_ac_account

voice_celery_redis:
  install_prefix: /data/SicentApp/voice_celery_redis
  log_dir: /data/SicentApp/voice_celery_redis/log
  data_dir: /data/SicentApp/voice_celery_redis/db
  bind: {{ ip_addrs.voice_celery_redis.ip }}
  password: {{ ip_addrs.voice_celery_redis.password }}
  port : {{ ip_addrs.voice_celery_redis.port }}
  svc_name: redis_voice_celery

voice_redis:
  install_prefix: /data/SicentApp/voice_redis
  log_dir: /data/SicentApp/voice_redis/log
  data_dir: /data/SicentApp/voice_redis/db
  bind: {{ ip_addrs.voice_redis.ip }}
  password: {{ ip_addrs.voice_redis.password }}
  port : {{ ip_addrs.voice_redis.port }}
  svc_name: redis_voice

gameplaza_celery_redis:
  install_prefix: /data/SicentApp/gameplaza_celery_redis
  log_dir: /data/SicentApp/gameplaza_celery_redis/log
  data_dir: /data/SicentApp/gameplaza_celery_redis/db
  bind: {{ ip_addrs.gameplaza_celery_redis.ip }}
  password: {{ ip_addrs.gameplaza_celery_redis.password }}
  port: {{ ip_addrs.gameplaza_celery_redis.port }}
  svc_name: redis_gameplaza_celery

cache_redis:
  install_prefix: /data/SicentApp/cache_redis
  log_dir: /data/SicentApp/cache_redis/log
  data_dir: /data/SicentApp/cache_redis/db
  bind: {{ ip_addrs.cache_redis.ip }}
  password: {{ ip_addrs.cache_redis.password }}
  port: {{ ip_addrs.cache_redis.port }}
  svc_name: redis_cache

lottery_redis:
  install_prefix: /data/SicentApp/lottery_redis
  log_dir: /data/SicentApp/lottery_redis/log
  data_dir: /data/SicentApp/lottery_redis/db
  bind: {{ ip_addrs.lottery_redis.ip }}
  password: {{ ip_addrs.lottery_redis.password }}
  port: {{ ip_addrs.lottery_redis.port }}
  svc_name: redis_lottery

lwsm_order_redis:
  install_prefix: /data/SicentApp/lwsm_order_redis
  log_dir: /data/SicentApp/lwsm_order_redis/log
  data_dir: /data/SicentApp/lwsm_order_redis/db
  bind: {{ ip_addrs.lwsm_order_redis.ip }}
  password: {{ ip_addrs.lwsm_order_redis.password }}
  port: {{ ip_addrs.lwsm_order_redis.port }}
  svc_name: redis_lwsm_order

wx_token_redis:
  install_prefix: /data/SicentApp/wx_token_redis
  log_dir: /data/SicentApp/wx_token_redis/log
  data_dir: /data/SicentApp/wx_token_redis/db
  bind: {{ ip_addrs.wx_token_redis.ip }}
  password: {{ ip_addrs.wx_token_redis.password }}
  port: {{ ip_addrs.wx_token_redis.port }}
  svc_name: redis_wx_token

user_center_redis:
  install_prefix: /data/SicentApp/user_center_redis
  log_dir: /data/SicentApp/user_center_redis/log
  data_dir: /data/SicentApp/user_center_redis/db
  bind: {{ ip_addrs.user_center_redis.ip }}
  password: {{ ip_addrs.user_center_redis.password }}
  port: {{ ip_addrs.user_center_redis.port }}
  svc_name: redis_user_center


wx_policy_celery_redis:
  install_prefix: /data/SicentApp/wx_policy_redis
  log_dir: /data/SicentApp/wx_policy_redis/log
  data_dir: /data/SicentApp/wx_policy_redis/db
  bind: {{ ip_addrs.policymgr_celery_broker_redis.ip }}
  password: {{ ip_addrs.policymgr_celery_broker_redis.password }}
  port: {{ ip_addrs.policymgr_celery_broker_redis.port }}
  svc_name: wx_policy_redis
  enable_save: False



egs_redis:
  install_prefix: /data/SicentApp/egs_redis
  log_dir: /data/SicentApp/egs_redis/log
  data_dir: /data/SicentApp/egs_redis/db
  bind: {{ ip_addrs.egs_redis.ip }}
  password: {{ ip_addrs.egs_redis.password }}
  port: {{ ip_addrs.egs_redis.port }}
  svc_name: egs


egs_collect_redis:
  install_prefix: /data/SicentApp/egs_collect_redis
  log_dir: /data/SicentApp/egs_collect_redis/log
  data_dir: /data/SicentApp/egs_collect_redis/db
  bind: {{ ip_addrs.egs_collect_redis.ip }}
  password: {{ ip_addrs.egs_collect_redis.password }}
  port: {{ ip_addrs.egs_collect_redis.port }}
  svc_name: egs_collect


diskPerf:
  data:
    - DEVICENAME: sda
    - DEVICENAME: sda4

zbxRedis:
  data:
    - SVCNAME: redis_accountcenter_celery
      REDISHOST: {{ ip_addrs.accountcenter_celery_redis.ip }}
      REDISPORT: {{ ip_addrs.accountcenter_celery_redis.port }}
    - SVCNAME: redis_ac_account
      REDISHOST: {{ ip_addrs.ac_account_redis.ip }}
      REDISPORT: {{ ip_addrs.ac_account_redis.port }}
    - SVCNAME: redis_voice_celery
      REDISHOST: {{ ip_addrs.voice_celery_redis.ip }}
      REDISPORT: {{ ip_addrs.voice_celery_redis.port }}
    - SVCNAME: redis_voice
      REDISHOST: {{ ip_addrs.voice_redis.ip }}
      REDISPORT: {{ ip_addrs.voice_redis.port }}
    - SVCNAME: redis_gameplaza_celery
      REDISHOST: {{ ip_addrs.gameplaza_celery_redis.ip }}
      REDISPORT: {{ ip_addrs.gameplaza_celery_redis.port }}
    - SVCNAME: redis_cache
      REDISHOST: {{ ip_addrs.cache_redis.ip }}
      REDISPORT: {{ ip_addrs.cache_redis.port }}
    - SVCNAME: redis_lottery
      REDISHOST: {{ ip_addrs.lottery_redis.ip }}
      REDISPORT: {{ ip_addrs.lottery_redis.port }}
    - SVCNAME: redis_lwsm
      REDISHOST: {{ ip_addrs.lwsm_order_redis.ip }}
      REDISPORT: {{ ip_addrs.lwsm_order_redis.port }}
    - SVCNAME: redis_wx_token
      REDISHOST: {{ ip_addrs.wx_token_redis.ip }}
      REDISPORT: {{ ip_addrs.wx_token_redis.port }}
    - SVCNAME: redis_user_center
      REDISHOST: {{ ip_addrs.user_center_redis.ip }}
      REDISPORT: {{ ip_addrs.user_center_redis.port }}
    - SVCNAME: egs_collect
      REDISHOST: {{ ip_addrs.egs_collect_redis.ip }}
      REDISPORT: {{ ip_addrs.egs_collect_redis.port }}
    - SVCNAME: egs
      REDISHOST: {{ ip_addrs.egs_redis.ip }}
      REDISPORT: {{ ip_addrs.egs_redis.port }}
