environment: wxdesk_dev

hostname: backendnginx.dev.wxdesk.yewu.js

roles:
- nginx

services:
- WxGamePlaza_frontend
- WxDeskManagement_frontend
- WxDesktopClient_frontend
- WxPublicity_frontend
- LwMarket_frontend
- WxImageServer