{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongo02_configsvr03:
  svc_name: mongo02_configsvr03
  dataPath: {{ stack.mongo.data_path }}/configdb
  replSetName: cfg_shard
  external_net_bindip: {{ ip_addrs.mongo02_configsvr03 }}
  port: 3307
  is_master: false
  is_configsvr: true

mongo02_arbiter01:
  svc_name: mongo02_arbiter01
  external_net_bindip: {{ ip_addrs.mongo02_arbiter01 }}
  dataPath: {{ stack.mongo.data_path }}/mongo02_arbiter01
  port: 3308
  is_master: false
  replSetName: shard01
  cacheSizeGB: 10

mongo02_arbiter02:
  svc_name: mongo02_arbiter02
  external_net_bindip: {{ ip_addrs.mongo02_arbiter02 }}
  dataPath: {{ stack.mongo.data_path }}/mongo02_arbiter02
  port: 3309
  is_master: false
  replSetName: shard02
  cacheSizeGB: 10
