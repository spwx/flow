{% set ip_addrs = stack.get('ip_addrs', {}) %}
mongo_configsvr:
  bind_ip: {{ ip_addrs.get('mongo_configsvr01', '') }}
  primary: {{ ip_addrs.get('mongo_configsvr01', '') }}
  servers:
    - host: {{ ip_addrs.get('mongo_configsvr02', '') }}
      port: 27019

redis:
  bind: {{ ip_addrs.get('redis_cache', '') }}
  password: 'redis123'

redis_reduce_stress:
  install_prefix: /data/SicentApp/redis_reduce_stress
  log_dir: /data/SicentApp/redis_reduce_stress/log
  data_dir: /data/SicentApp/redis_reduce_stress/db
  bind: {{ ip_addrs.get('redis_reduce_stress', '')}}
  password: 'redis123'
  port: 6380
  svc_name: redis_reduce_stress