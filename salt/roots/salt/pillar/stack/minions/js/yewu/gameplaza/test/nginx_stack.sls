{% set ip_addrs = stack.get('ip_addrs', {}) %}
WxDeskManagement:
  nginx:
    backend: # port: 8081
      server_name: qdd.wxdesk.com {{ ip_addrs.nginx_backend }}
      access_log_file: WxDeskManagement_backend.log
      blue_servers:
        - host: {{ ip_addrs.WxDeskManagement }}
          port: 22131
          weight: 1

WxVoiceMaster:
  nginx:
    backend:
      web: # port: 8082
        server_name: voice.wxdesk.com {{ ip_addrs.nginx_backend }}
        access_log_file: WxVoiceMaster_backend_web.log
        blue_servers:
          - host: {{ ip_addrs.WxVoiceMaster }}
            port: 22121
            weight: 1
      api: # port: 8084
        server_name: voice.wxdesk.com {{ ip_addrs.nginx_backend }}
        access_log_file: WxVoiceMaster_backend_api.log
        blue_servers:
          - host: {{ ip_addrs.WxVoiceMaster }}
            port: 22122
            weight: 1

wxgameplaza:
  nginx:
    web:
      backend: # port: 8083
        server_name: plaza.wxdesk.com img.wxdesk.com style.wxdesk.com {{ ip_addrs.nginx_backend }}
        blue_servers:
          - host: {{ ip_addrs.get('wxgameplaza_web01', '127.0.0.1') }}
            port: 22111
            weight: 1

WxDesktopClient:
  nginx:
    web:
      frontend:
        server_name: right.wxdesk.com
        access_log_file: WxDesktopClient_frontend.log

WxImageServer:
  nginx:
    backend:
      server_name: uploadqdd.wxdesk.com {{ ip_addrs.nginx_backend }}
      proxy_pass_url: http://{{ ip_addrs.WxImageServer }}/
