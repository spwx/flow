environment: gameplaza_test

roles:
- python3
- webagent
- mongo
- mongos
- deskcomponent
- accountcenter
- filebeat

services:
- WxDeskManagement_web
- WxImageServer
- WxGameMenu
