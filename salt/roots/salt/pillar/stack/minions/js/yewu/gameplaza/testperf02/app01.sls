environment: desktop_testperf02

hostname: app01.testperf02.gameplaza.yewu.js

roles:
- python3
- mongo
- mongos
- nginx
- accountcenter
- deskcomponent

services:
- WxGamePlaza_web
- WxGamePlaza_celerybeat
- WxGamePlaza_celeryd # default celery worker
- WxGamePlaza_httpagent
- WxGamePlaza_handler
- WxDeskManagement_frontend
- WxPublicity_frontend
- WxGamePlaza_frontend
- WxDesktopClient_frontend
- LwMarket_frontend
