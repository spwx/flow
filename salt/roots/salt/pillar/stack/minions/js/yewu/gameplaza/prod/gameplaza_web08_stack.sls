{% set ip_addrs = stack.get('ip_addrs', {}) %}
wxgameplaza:
  web:
    bind: {{ ip_addrs.wxgameplaza_web08 }}
    processes: 12

filebeat:
  config:
    filebeat.prospectors:


      - input_type: log
        document_type: web_log_activity
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/activity.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_announcement
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/announcement.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_db
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/db.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_desktop
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/desktop.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_django.request
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/django.request.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_django_request_logfile
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/django_request_logfile.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_game_record
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/game_record.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_game_round
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/game_round.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_gameplaza
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/gameplaza.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_get_activity
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/get_activity.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_get_reward
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/get_reward.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_lottery
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/lottery.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_modify_reward
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/modify_reward.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_mytask
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/mytask.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_olddata
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/olddata.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_ops
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/ops.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_other_activity
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/other_activity.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_record
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/record.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_redis
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/redis.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_redis_logfile
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/redis_logfile.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_report
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/report.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_reset_activity
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/reset_activity.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_scheduled_task
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/scheduled_task.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_score
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/score.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_stage_activity
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/stage_activity.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_task_progress
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/task_progress.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_taskcenter
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/taskcenter.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_upload_lol_data
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/upload_lol_data.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_voice
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/voice.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after


      - input_type: log
        document_type: web_log_wechat
        paths:
          - {{ stack.wxgameplaza.web.install_path }}/GamePlaza/logs/wechat.log
        multiline.pattern: '^pid'
        multiline.negate: true
        multiline.match: after



    output.logstash:
      hosts: ["172.30.236.79:5000"]
