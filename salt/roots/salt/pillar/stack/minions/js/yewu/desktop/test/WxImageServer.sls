
environment: gameplaza_test

hostname: WxImageServer.test.desktop.yewu.js

roles:
  - python3
  - redis
  - mongo
  - mongos
  - nginx

services:
  - WxImageServer
