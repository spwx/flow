environment: gameplaza_prod

hostname: nginx_frontend.prod.gameplaza.yewu.js
minion_role: prod.nginx_frontend

roles:
  - nginx
services:
  - WxDesktopClient_frontend
  - WxDeskManagement_frontend
  - WxVoiceMaster_frontend
  - WxGamePlaza_frontend
  - WxDeskSite
  - WxPublicity_frontend
  - LwMarket_frontend
