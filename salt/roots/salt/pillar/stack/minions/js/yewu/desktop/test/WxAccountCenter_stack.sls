{% set ip_addrs = stack.get('ip_addrs', {}) %}

redis:
  bind: {{ ip_addrs.accountcenter_celery_redis.ip }}
  password: {{ ip_addrs.accountcenter_celery_redis.password }}
  port: {{ ip_addrs.accountcenter_celery_redis.port }}

zbxRedis:
  data:
    - SVCNAME: redis
      REDISHOST: {{ ip_addrs.accountcenter_celery_redis.ip }}
      REDISPORT: {{ ip_addrs.accountcenter_celery_redis.port }}
