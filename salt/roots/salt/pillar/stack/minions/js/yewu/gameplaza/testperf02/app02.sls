environment: desktop_testperf02

hostname: app02.testperf02.gameplaza.yewu.js

roles:
- python3
- nginx
- mongo
- mongos
- redis
- accountcenter
- deskcomponent

services:
- WxVoiceMaster_web
- WxVoiceMaster_celerybeat
- WxVoiceMaster_celeryd
- WxAccountCenter_celerybeat
- WxAccountCenter_celeryd
- WxDeskManagement_frontend
- WxVoiceMaster_frontend
- WxGamePlaza_frontend
- WxDesktopClient_frontend
- LwMarket_frontend
- LwMarket_web
- LwMarket_celeryd