environment: gameplaza_test

hostname: mongo_configsvr02.test.gameplaza.yewu.js

minion_role: test.mongo

roles:
  - python3
  - deskcomponent
  - accountcenter
  - mongo
  - mongo_configsvr
  - mongos
  - webagent

services:
- WxGamePlaza_web
- WxGamePlaza_celerybeat
- WxGamePlaza_celeryd
- WxGamePlaza_httpagent
