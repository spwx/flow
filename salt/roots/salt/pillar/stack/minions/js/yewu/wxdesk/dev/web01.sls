environment: wxdesk_dev

hostname: web01.dev.wxdesk.yewu.js

roles:
- python3
- deskcomponent
- accountcenter
- nginx
- mongos
- mongo
- filebeat

services:
- WxGamePlaza_web
- WxGamePlaza_celerybeat
- WxGamePlaza_celeryd # default celery worker
- WxGamePlaza_httpagent
- WxGamePlaza_handler
- WxDeskManagement_frontend
- WxDeskManagement_web
- WxVoiceMaster_frontend
- WxGamePlaza_frontend
- WxDesktopClient_frontend
- WxPublicity_frontend
- LwMarket_frontend
- LwMarket_web
- WxImageServer_frontend
- WxVoiceMaster_web
- WxDesktopAccount
- GoodsCenter
- statscenter
- WxGamePlaza_handler
- usercenter
- egs


