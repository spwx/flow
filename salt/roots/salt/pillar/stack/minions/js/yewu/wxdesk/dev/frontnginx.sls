environment: wxdesk_dev

hostname: frontnginx.dev.wxdesk.yewu.js

roles:
- nginx

services:
- WxDeskManagement_frontend
- WxDesktopClient_frontend
- WxPublicity_frontend
- LwMarket_frontend
- WxImageServer
- WxGamePlaza_frontend
