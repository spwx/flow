environment: gameplaza_prod

roles:
- python3
- mongo
- mongos
- webagent
- accountcenter
- deskcomponent

services:
- WxVoiceMaster_web
- WxVoiceMaster_celeryd
- WxDeskManagement_web
