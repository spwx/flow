environment: gameplaza_test

hostname: nginx.test.gameplaza.yewu.js

minion_role: test.nginx

roles:
  - nginx

services:
- WxDeskSite
- WxDeskManagement_frontend
- WxVoiceMaster_frontend
- WxGamePlaza_frontend
- WxDesktopClient_frontend
- WxPublicity_frontend