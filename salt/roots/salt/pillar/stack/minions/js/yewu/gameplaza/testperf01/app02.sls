environment: desktop_testperf01

hostname: app02.testperf01.gameplaza.yewu.js

roles:
- python3
- nginx
- mongo
- mongos
- redis
- accountcenter
- deskcomponent

services:
- WxVoiceMaster_web
- WxVoiceMaster_celerybeat
- WxVoiceMaster_celeryd
- WxAccountCenter_celerybeat
- WxAccountCenter_celeryd
- WxDeskManagement_frontend
- WxVoiceMaster_frontend
- WxGamePlaza_frontend
- WxDesktopClient_frontend
- LwMarket_web
- LwMarket_celeryd
- LwMarket_frontend