{% set ip_addrs = stack.get('ip_addrs', {}) %}

redis_pay_notice:
  install_prefix: /data/SicentApp/redis_pay_notice
  log_dir: /data/SicentApp/redis_pay_notice/log
  data_dir: /data/SicentApp/redis_pay_notice/db
  bind: {{ ip_addrs.get('redis_pay_notice', '')}}
  password: 'redis123'
  port: 6380
  svc_name: redis_pay_notice

redis_statistic:
  install_prefix: /data/SicentApp/redis_statistic
  log_dir: /data/SicentApp/redis_statistic/log
  data_dir: /data/SicentApp/redis_statistic/db
  bind: {{ ip_addrs.get('redis_statistic', '')}}
  password: 'redis123'
  port: 6381
  svc_name: redis_statistic

