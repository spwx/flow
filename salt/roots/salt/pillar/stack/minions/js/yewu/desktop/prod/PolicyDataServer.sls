environment: gameplaza_prod

hostname: PolicyDataServer.prod.desktop.yewu.js

minion_role: test.PolicyDataServer

roles:
  - redis
  - nginx
  - python3
  - accountcenter
  - deskcomponenet

services:
  - PolicyDataServer
