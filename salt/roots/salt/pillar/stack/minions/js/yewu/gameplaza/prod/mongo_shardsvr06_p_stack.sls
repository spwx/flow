{% set ip_addrs = stack.get('ip_addrs', {}) %}

mongo_shardsvr:
  bind_ip: {{ ip_addrs.get('mongo_shardsvr06_p', '0.0.0.0') }}
  port: 3308
  primary: {{ ip_addrs.get('mongo_shardsvr06_p', '0.0.0.0') }}
  repl_set_name: shardsvr06

