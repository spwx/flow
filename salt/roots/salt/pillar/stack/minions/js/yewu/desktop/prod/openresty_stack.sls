{% set ip_addrs = stack.get('ip_addrs', {}) %}

redis:
  bind: {{ ip_addrs.get('openresty', '127.0.0.1') }}
  port: 6381
  svc_name: redis_6381
  password: 'policy2017'
  install_prefix: /data/SicentWebserver/desktop/PolicyDataServer/redis/slave01
  log_dir: /data/SicentWebserver/desktop/PolicyDataServer/redis/slave01/log
  data_dir: /data/SicentWebserver/desktop/PolicyDataServer/redis/slave01/db
  master:
    host: {{ ip_addrs.get('PolicyDataServer', '127.0.0.1') }}
    port: 6381
    password: 'policy2017'
