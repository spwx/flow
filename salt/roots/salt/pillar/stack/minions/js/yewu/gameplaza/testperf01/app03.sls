environment: desktop_testperf01

hostname: app03.testperf01.gameplaza.yewu.js

roles:
- python3
- nginx
- mongo
- mongos
- redis
- accountcenter
- deskcomponent

services:
- WxDeskManagement_web
- WxGameMenu
- WxImageServer
- WxGamePlaza_celeryd # schedule celery worker
- WxPolicyServer # WxPolicyServer celery worker
- WxBarshop_web
- WxImageServer
- statscenter
- gologstat