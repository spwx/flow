{% set ip_addrs = stack.get('ip_addrs', {}) %}
PolicyDataServer:
  nginx:
    server_name: {{ ip_addrs.get('PolicyDataServer', '127.0.0.1')}}
  web:
    settings:
      CELERY_BROKER_URL: "redis://:policy2017@{{ ip_addrs.get('PolicyDataServer', '127.0.0.1') }}:6381/3"
      CELERY_RESULT_BACKEND: "redis://:policy2017@{{ ip_addrs.get('PolicyDataServer', '127.0.0.1') }}:6381/3"
      BACKEND_REDIS_URL: "redis://:policy2017@{{ ip_addrs.get('PolicyDataServer', '127.0.0.1') }}:6381/6"
      IDC_REDIS_URL: "redis://:policy2017@{{ ip_addrs.get('PolicyDataServer', '127.0.0.1') }}:6381/0"
      LOG_ADDRESS: ["10.34.52.174", 22002]
      MAIL:
        HOST: "mail.shunwang.com"
        USER: "webadmin"
        PASS: "Welcome321"
      NOTIFICATION_EMAIL:
        FROM: "webadmin@shunwang.com"
        TO: ["zhuyi@shunwang.com","wanglele@shunwang.com","yanglinshan@shunwang.com"]
      WXVOICE:
        business: "wxvoice"
        policy_urn: "/wxvoice/update/1.0"
        full_sync_lock: "lock:full_sync_wxvoice"
        api_key: "3820dcd2-c689-4a5a-b9c7-c26a02caf97b"
        provision_url: "http://10.34.53.178:8088/interface/getVoiceStrategy.php?snbid=%s"
        default_provision_url: "http://10.34.53.178:8088/interface/getVoiceStrategy.php"

redis:
  bind: {{ ip_addrs.get('PolicyDataServer', '127.0.0.1') }}
  port: 6381
  svc_name: redis_6381
  install_prefix: /data/SicentWebserver/desktop/PolicyDataServer/redis/master
  log_dir: /data/SicentWebserver/desktop/PolicyDataServer/redis/master/log
  data_dir: /data/SicentWebserver/desktop/PolicyDataServer/redis/master/db
  password: 'policy2017'

python3:
  install_prefix: /data/SicentApp/python3
