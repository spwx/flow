{% set ip_addrs = stack.get('ip_addrs', {}) %}

redis:
  bind: {{ ip_addrs.get('redis', '127.0.0.1') }}
  password: 'redis123'

mongo:
  bind_ip: {{ ip_addrs.get('mongo', '127.0.0.1') }}