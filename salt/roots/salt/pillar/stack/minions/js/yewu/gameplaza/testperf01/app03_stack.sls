{% set ip_addrs = stack.get('ip_addrs', {}) %}

accountcenter_celery_redis:
  install_prefix: /data/SicentApp/accountcenter_celery_redis
  log_dir: /data/SicentApp/accountcenter_celery_redis/log
  data_dir: /data/SicentApp/accountcenter_celery_redis/db
  bind: {{ ip_addrs.accountcenter_celery_redis.ip }}
  password: {{ ip_addrs.accountcenter_celery_redis.password }}
  port : {{ ip_addrs.accountcenter_celery_redis.port }}
  svc_name: redis_accountcenter_celery

voice_celery_redis:
  install_prefix: /data/SicentApp/voice_celery_redis
  log_dir: /data/SicentApp/voice_celery_redis/log
  data_dir: /data/SicentApp/voice_celery_redis/db
  bind: {{ ip_addrs.voice_celery_redis.ip }}
  password: {{ ip_addrs.voice_celery_redis.password }}
  port : {{ ip_addrs.voice_celery_redis.port }}
  svc_name: redis_voice_celery

gameplaza_celery_redis:
  install_prefix: /data/SicentApp/gameplaza_celery_redis
  log_dir: /data/SicentApp/gameplaza_celery_redis/log
  data_dir: /data/SicentApp/gameplaza_celery_redis/db
  bind: {{ ip_addrs.gameplaza_celery_redis.ip }}
  password: {{ ip_addrs.gameplaza_celery_redis.password }}
  port: {{ ip_addrs.gameplaza_celery_redis.port }}
  svc_name: redis_gameplaza_celery

WxImageServer:
  client:
    store_path: /data/wximage/client
    server:
      ip: {{ ip_addrs.WxImageServer }}
      store_path: /data/wximage/server
  server:
    store_path: /data/wximage/server
    exports: 
      - /data/wximage/server {{ ip_addrs.WxDeskManagement_web01 }}(insecure,rw,sync)
  nginx:
    server:
      server_name: {{ ip_addrs.WxImageServer }}
      store_path: /data/wximage/server

WxDeskManagement:
  web:
    bind: {{ ip_addrs.WxDeskManagement_web01 }}

wxgameplaza:
  celeryd:
    queue: schedule
    nodes_name: schedule_worker

WxBarshop:
  nginx:
     backend: # port: 8087
        server_name: 10.34.60.127 {{ ip_addrs.nginx_backend }}
        access_log_file: barshop_backend.log
        blue_servers:
          - host: {{ ip_addrs.WxBarshop_web01 }}
            port: 22118
            weight: 1
  web:
    bind: {{ ip_addrs.WxBarshop_web01 }}
    processes: 2
