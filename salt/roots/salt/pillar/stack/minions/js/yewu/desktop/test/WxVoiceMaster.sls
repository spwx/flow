environment: gameplaza_test

hostname: WxVoiceMaster.test.desktop.yewu.js

roles:
  - python3
  - redis
  - mongo
  - mongos
  - deskcomponent
  - accountcenter
  - nginx

services:
  - WxVoiceMaster_web
  - WxVoiceMaster_celeryd
  - WxVoiceMaster_celerybeat
