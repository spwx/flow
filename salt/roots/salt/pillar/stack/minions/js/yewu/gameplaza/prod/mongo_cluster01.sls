environment: gameplaza_prod
hostname: mongo_cluster01.prod.gameplaza.yewu.js
roles:
  - python3
  - mongo
  - mongo_shardsvr
  - mongos
