{% set ip_addrs = stack.get('ip_addrs', {}) %}

{% set ac_celery_redis = ip_addrs.accountcenter_celery_redis %}
{% set ac_account_redis = ip_addrs.ac_account_redis %}
{% set ac_account_other_redis = ip_addrs.ac_account_other_redis %}
WxAccountCenter:
  settings:
    ACCOUNT_CELERY_BROKER_URL: redis://:{{ ac_celery_redis.password }}@{{ ac_celery_redis.ip }}:{{ ac_celery_redis.port }}/0
    ACCOUNT_CELERY_RESULT_URL: redis://:{{ ac_celery_redis.password }}@{{ ac_celery_redis.ip }}:{{ ac_celery_redis.port }}/1

    ACCOUNT_REDIS: redis://:{{ ac_account_redis.password }}@{{ ac_account_redis.ip }}:{{ ac_account_redis.port }}/{{ ac_account_redis.db }}  # 保存账号列表的redisdb
    AIO_ACCOUNT_REDIS:
      db: {{ ac_account_redis.db }}
      ip: {{ ac_account_redis.ip }}
      password: {{ ac_account_redis.password }}
      port: {{ ac_account_redis.port }}

    ACCOUNT_OTHER_REDIS: redis://:{{ ac_account_other_redis.password }}@{{ ac_account_other_redis.ip }}:{{ ac_account_other_redis.port }}/{{ ac_account_other_redis.db }}  # 缓存账号其他信息的redis db
    AIO_ACCOUNT_OTHER_REDIS:
      db: {{ ac_account_other_redis.db }}
      ip: {{ ac_account_other_redis.ip }}
      password: {{ ac_account_other_redis.password }}
      port: {{ ac_account_other_redis.port }}

    FEE_BAR_ACCOUNT_HOST: 10.34.44.230:8888  #dev:10.34.59.11:8000 test:10.34.44.230:8888 online:down.pubwinol.com
    DEBUG: True
