ruby:
  install_path: /data/SicentApp/ruby
  pkg_url: http://oqc4nhm21.bkt.clouddn.com/ruby/ruby-2.3.0.tar.gz
  pkg_checksum: http://oqc4nhm21.bkt.clouddn.com/ruby/ruby-2.3.0.tar.gz.md5
  mirror_urls:
    - http://mirrors.ustc.edu.cn/rubygems/
