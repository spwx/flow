redis:
  install_prefix: /data/SicentApp/redis
  log_dir: /data/SicentApp/redis/log
  data_dir: /data/SicentApp/redis/db
  port: 6379

sys_config:
  sysctl_conf:
    net_core_somaxconn: 65535
    vm_overcommit_memory: 1