redis:
  install_prefix: /data/SicentWebserver/gameplaza/apps/redis
  log_dir: /data/SicentWebserver/gameplaza/apps/redis/log
  data_dir: /data/SicentWebserver/gameplaza/apps/redis/db
  loglevel: debug