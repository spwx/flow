python3:
  mirror_url: http://mirrors.aliyun.com/pypi
  mirror_host: mirrors.aliyun.com
  install_prefix: /data/SicentApp/python27
  pkg_url: salt://python3/files/Python-2.7.13.tgz
  pkg_checksum: salt://python3/files/Python-2.7.13.tgz.md5
