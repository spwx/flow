mongo:
  secret: |
    VPocmtxc8PuQ5UPSmSK2PmIsq0hHOREGG3/+gE54goeJBh+Np1BJANWTixwsOlGa
    hbXoI9EpBbaUFABUBpxTraug5OmcC0kxM+ROXeZnmDBCezMeGA2MGxHuaEoAWsXT
    bZvXTHbZbMdylMY2ZoL2eUhgI+32kXXHHM5MElDHBwit9RCsV8Bfr7vpuXF++IlP
    0tCH82V27cxoEqHOS/YJb5QHZmDMpOrj7geo/KKhsVWJpnJ9/5ez5gahUI8+Gj74
    IwAsFuHNTGb669ntR69+A+a7T6lGEZxd2r0SAllJaDpbkFKD/BGEbpMJ9HBTJ0IQ
    ETkwxoofad1TdrrAjRY29lS4Ny+fa3vu6tX4mhqdyj5HsKDWH9UhZndfCANj2RN5
    VAfNsUkAda8f0fQ3kzdWC6RRvfFhA6WOA7Y/BmEwDru5XRnqGnbZ7zGPBa0uX9EZ
    UaYiM/iD7ebQk6P1DPEE2b9XCu/KTxa/qNX28TsPdJAp8napRwif+qVnwpDBM/IV
    W9NJz+goI/rI/M2oMP0XGBOrqQJOmP3XBNTKj0l6y/Qou0LH8nbsg4e+4oWCnHla
    XozGy5ORh4G5WJrdt1APjPDdjnKyKDz6+gCneZIsiZ9ylbZcR0yIL5Q2QsvADDOp
    pllc+ZaqHYkG01VLwAZ4ahJsZyXamhP65mIjZBJk8hyNdX+gsnkRQb2tF0DO17Kf
    +5DxKPbrZ+Hqh2JVQgOrGN3I5zX9YLqWydqGReqdYWvrzqyMnTsSl0YeCi1KmVXU
    bwLhw22+HrKRmBcdITW9ygcwxC3mgH4sxON++UzLpfWfMBRC5hD3KWF/dxVuV39Z
    5QmM21k5edrL2cS4oGAY2+Ji369nw9rLpI/K2a1btTLKs3Wjd/ViAA9ITpCdKWf4
    3lf1ROwUXAneuEwJFUDHVtnDstYW95ACIwJMa3kvz6+XOOb+AOD0gfqJhXhYSMJ+
    bGDKPeOLp9gTY/zumHDLlMKzt2HuVyl8TFqFfY1dneX1
  install_path: /data/SicentApp/mongo
  data_path: /data/SicentApp/mongo/db
  log_path: /data/SicentApp/mongo/log
  version: 3.2.11
  #pkg_checksum: http://oqc4nhm21.bkt.clouddn.com/mongo/mongodb-linux-x86_64-3.2.11.tgz.md5
  #pkg_url: http://oqc4nhm21.bkt.clouddn.com/mongo/mongodb-linux-x86_64-3.2.11.tgz
  pkg_url: salt://mongo/files/mongodb-linux-x86_64-3.2.11.tgz
  pkg_checksum: salt://mongo/files/mongodb-linux-x86_64-3.2.11.tgz.md5
