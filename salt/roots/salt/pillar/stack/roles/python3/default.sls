python3:
  mirror_url: http://mirrors.aliyun.com/pypi
  mirror_host: mirrors.aliyun.com
  install_prefix: /data/SicentApp/python3
  pkg_url: salt://python3/files/Python-3.5.2.tgz
  pkg_checksum: salt://python3/files/Python-3.5.2.tgz.md5
