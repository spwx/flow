{% set ip_addrs = stack.get('ip_addrs', {}) %}

{% set voice_celery_redis = ip_addrs.voice_celery_redis %}
{% set voice_redis = ip_addrs.get('voice_redis', {}) %}

{% set stage_activity_redis = ip_addrs.get('stage_activity_redis', {}) %}
{% set reward_activity_redis = ip_addrs.get('reward_activity_redis', {}) %}
{% set head_activity_redis = ip_addrs.get('head_activity_redis', {}) %}
{% set damage_activity_redis = ip_addrs.get('damage_activity_redis', {}) %}
{% set continuous_victory_activity_redis = ip_addrs.get('continuous_victory_activity_redis', {}) %}
{% set score_redis = ip_addrs.get('score_redis', {}) %}
{% set statistic_redis = ip_addrs.get('statistic_redis', {}) %}
{% set bar_redis = ip_addrs.get('bar_redis', {}) %}
{% set ranker_player_redis = ip_addrs.get('ranker_player_redis', {}) %}
{% set reduce_stress_redis = ip_addrs.get('reduce_stress_redis', {}) %}
{% set ad_announcement_redis = ip_addrs.get('ad_announcement_redis', {}) %}
{% set lock_key_redis = ip_addrs.get('lock_key_redis', {}) %}
{% set pay_notice_redis = ip_addrs.get('pay_notice_redis', {}) %}
{% set bar_player_uploadtime_redis = ip_addrs.get('bar_player_uploadtime_redis', {}) %}
{% set lottery_redis = ip_addrs.get('lottery_redis', {}) %}
{% set daily_task_redis = ip_addrs.get('daily_task_redis', {}) %}
{% set award_share_redis = ip_addrs.get('award_share_redis', {}) %}
{% set first_game_redis = ip_addrs.get('first_game_redis', {}) %}
{% set user_info_center_redis = ip_addrs.get('user_info_center_redis', {}) %}
{% set league_draw_lottery_redis = ip_addrs.get('league_draw_lottery_redis', {}) %}




deskcomponent:
  settings:
    STAGE_ACTIVITY_REDIS: redis://:{{ stage_activity_redis.password }}@{{ stage_activity_redis.ip }}:{{ stage_activity_redis.port }}/{{ stage_activity_redis.db }}         # 关卡任务活动redis
    AIO_STAGE_ACTIVITY_REDIS:
      db: {{ stage_activity_redis.db }}
      ip: {{ stage_activity_redis.ip }}
      password: {{ stage_activity_redis.password }}
      port: {{ stage_activity_redis.port }}

    REWARD_ACTIVITY_REDIS: redis://:{{ reward_activity_redis.password }}@{{ reward_activity_redis.ip }}:{{ reward_activity_redis.port }}/{{ reward_activity_redis.db }}       # 悬赏任务活动redis
    AIO_REWARD_ACTIVITY_REDIS:
      db: {{ reward_activity_redis.db }}
      ip: {{ reward_activity_redis.ip }}
      password: {{ reward_activity_redis.password }}
      port: {{ reward_activity_redis.port }}
    HEAD_ACTIVITY_REDIS: redis://:{{ head_activity_redis.password }}@{{ head_activity_redis.ip }}:{{ head_activity_redis.port }} / {{ head_activity_redis.db }}        # 人头王活动redis
    AIO_HEAD_ACTIVITY_REDIS:
      db: {{ head_activity_redis.db }}
      ip: {{ head_activity_redis.ip }}
      password: {{ head_activity_redis.password }}
      port: {{ head_activity_redis.port }}
    DAMAGE_ACTIVITY_REDIS: redis://:{{ damage_activity_redis.password }}@{{ damage_activity_redis.ip }}:{{ damage_activity_redis.port }}/{{ damage_activity_redis.db }}       # 战力榜活动redis
    AIO_DAMAGE_ACTIVITY_REDIS:
      db: {{ damage_activity_redis.db }}
      ip: {{ damage_activity_redis.ip }}
      password: {{ damage_activity_redis.password }}
      port: {{ damage_activity_redis.port }}
    CONTINUOUS_VICTORY_ACTIVITY_REDIS: redis://:{{ continuous_victory_activity_redis.password }}@{{ continuous_victory_activity_redis.ip }}:{{ continuous_victory_activity_redis.port }}/{{ continuous_victory_activity_redis.db }}  # 连胜王活动redis
    AIO_CONTINUOUS_VICTORY_ACTIVITY_REDIS:
      db: {{ continuous_victory_activity_redis.db }}
      ip: {{ continuous_victory_activity_redis.ip }}
      password: {{ continuous_victory_activity_redis.password }}
      port: {{ continuous_victory_activity_redis.port }}
    SCORE_REDIS: redis://:{{ score_redis.password }}@{{ score_redis.ip }}:{{ score_redis.port }}/{{ score_redis.db }}                         # 积分redis
    AIO_SCORE_REDIS:
      db: {{ score_redis.db }}
      ip: {{ score_redis.ip }}
      password: {{ score_redis.password }}
      port: {{ score_redis.port }}
    STATISTIC_REDIS_URL: redis://:{{ statistic_redis.password }}@{{ statistic_redis.ip }}:{{ statistic_redis.port }}/{{ statistic_redis.db }}         # 统计信息redis 主要是关注和参与度
    AIO_STATISTIC_REDIS_URL:
      db: {{ statistic_redis.db }}
      ip: {{ statistic_redis.ip }}
      password: {{ statistic_redis.password }}
      port: {{ statistic_redis.port }}
    BAR_REDIS_URL: redis://:{{ bar_redis.password }}@{{ bar_redis.ip }}:{{ bar_redis.port }}/{{ bar_redis.db }}               # 网吧榜单redis 以及其他杂项的redis
    AIO_BAR_REDIS_URL:
      db: {{ bar_redis.db }}
      ip: {{ bar_redis.ip }}
      password: {{ bar_redis.password }}
      port: {{ bar_redis.port }}
    RANKER_PLAYER_URL: redis://:{{ ranker_player_redis.password }}@{{ ranker_player_redis.ip }}:{{ ranker_player_redis.port }}/{{ ranker_player_redis.db }}           # 榜上玩家信息redis
    AIO_RANKER_PLAYER_URL:
      db: {{ ranker_player_redis.db }}
      ip: {{ ranker_player_redis.ip }}
      password: {{ ranker_player_redis.password }}
      port: {{ ranker_player_redis.port }}
    REDUCE_STRESS_REDIS_URL: redis://:{{ reduce_stress_redis.password }}@{{ reduce_stress_redis.ip }}:{{ reduce_stress_redis.port }}/{{ reduce_stress_redis.db }}     # 减轻压力的redis
    AIO_REDUCE_STRESS_REDIS_URL:
      db: {{ reduce_stress_redis.db }}
      ip: {{ reduce_stress_redis.ip }}
      password: {{ reduce_stress_redis.password }}
      port: {{ reduce_stress_redis.port }}
    AD_ANNOUNCEMENT_REDIS_URL: redis://:{{ ad_announcement_redis.password }}@{{ ad_announcement_redis.ip }}:{{ ad_announcement_redis.port }}/{{ ad_announcement_redis.db }}  # 缓存运营平台公告和广告的redis
    AIO_AD_ANNOUNCEMENT_REDIS_URL:
      db: {{ ad_announcement_redis.db }}
      ip: {{ ad_announcement_redis.ip }}
      password: {{ ad_announcement_redis.password }}
      port: {{ ad_announcement_redis.port }}

    FIRST_GAME_REDIS_URL: redis://:{{ first_game_redis.password }}@{{ first_game_redis.ip }}:{{ first_game_redis.port }}/{{ first_game_redis.db }}  # 缓存客户端版本号
    AIO_FIRST_GAME_REDIS_URL:
      db: {{ first_game_redis.db }}
      ip: {{ first_game_redis.ip }}
      password: {{ first_game_redis.password }}
      port: {{ first_game_redis.port }}

    LOCK_KEY_REDIS_URL: redis://:{{ lock_key_redis.password }}@{{ lock_key_redis.ip }}:{{ lock_key_redis.port }}/{{ lock_key_redis.db }}         # 所有的操作锁缓存redis
    PAY_NOTICE_REDIS_URL: redis://:{{ pay_notice_redis.password }}@{{ pay_notice_redis.ip }}:{{ pay_notice_redis.port }}/{{ pay_notice_redis.db }}       # 加钱运营平台开关缓存redis
    LOTTERY_DB_REDIS_URL: redis://:{{ lottery_redis.password }}@{{ lottery_redis.ip }}:{{ lottery_redis.port }}/{{ lottery_redis.db }}   # 抽奖业务缓存

    AIO_BAR_PLAYER_UPLOADTIME_REDIS_URL:
      db: {{ bar_player_uploadtime_redis.db }}
      ip: {{ bar_player_uploadtime_redis.ip }}
      password: {{ bar_player_uploadtime_redis.password }}
      port: {{ bar_player_uploadtime_redis.port }}
    GET_DEFAULT_DESKTOP: http://10.34.53.178:8089/index.php/lockscreenapi/get_user_lockscreen   # 查询默认桌面
    SET_DEFAULT_DESKTOP: http://10.34.53.178:8089/index.php/lockscreenapi/set_user_lockscreen   # 设置默认桌面
    CHECK_VIP_URL: http://10.34.53.178:8088/interface/vip_api.php  # 判断VIP
    PAY_SETTING:
      add_money_url: http://10.34.51.51/cxf/newpayagent/generalOrder/userGeneralOrderRequest?
      query_add_money_url: http://10.34.51.51/cxf/newpayagent/generalOrder/userGeneralOrderQueryRequest?
      callback_add_money_url: http://{{ ip_addrs.nginx_backend }}:8083/exchange_callback
      add_money_client_id: 'GamePlaza'
      add_money_md5key: '123456'
    PAY_SETTING_PUBWIN:
      add_money_url: 'http://10.34.45.12/cxf/newpayagent/generalOrder/userGeneralOrderRequest?'
      query_add_money_url: 'http://10.34.45.12/cxf/newpayagent/generalOrder/userGeneralOrderQueryRequest?'
      callback_add_money_url: http://{{ ip_addrs.nginx_backend }}:8083/exchange_callback
      add_money_client_id: 'GamePlaza'
      add_money_md5key: '123456'
    PAY_SETTING_OLD:
      add_money_url: 'http://10.34.51.51/cxf/payagent/addMoney/userAddSubmit?'
      query_add_money_url: 'http://10.34.51.51/cxf/payagent/query/userAddSubmit?'
      callback_add_money_url: 'http://{{ ip_addrs.nginx_backend }}/exchange_callback_old'
      add_money_client_id: 'GamePlaza'
      add_money_md5key: '123456'
    GAMEPLAZA_MONGODB_URL: mongodb://gameplaza:plaza123@127.0.0.1:{{ ip_addrs.mongos.port }}/gameplaza  # mongodb url
    VOICE_MONGODB_URL: mongodb://voicemaster:voice123@127.0.0.1:{{ ip_addrs.mongos.port }}/voice # mongodb url
    WXDESK_MONGODB_URL: mongodb://gamedesk:desk123@127.0.0.1:{{ ip_addrs.mongos.port }}/gamedesk # mongodb url
    MISC_MONGODB_URL: mongodb://misc:misc123@127.0.0.1:{{ ip_addrs.mongos.port}}/misc # mongodb url
    LWSM_MONGODB_URL: mongodb://lwsm:lwsm123@127.0.0.1:{{ ip_addrs.mongos.port }}/lwsm
    GAMEPLAZA_HOST: {{ ip_addrs.nginx_backend }}:8083  # plaza.wxdesk.com
    LOG_ADDRESS:
    - 10.34.52.174
    - 22002

    GOLOG_ADDRESS:
    - 10.34.56.110
    - 22177

    VOICE_CELERY_BROKER_URL: redis://:{{ voice_celery_redis.password }}@{{ voice_celery_redis.ip }}:{{ voice_celery_redis.port }}/0
    VOICE_CELERY_RESULT_URL: redis://:{{ voice_celery_redis.password }}@{{ voice_celery_redis.ip }}:{{ voice_celery_redis.port }}/0
    VOICE_REDIS_URL: redis://:{{ voice_redis.password }}@{{ voice_redis.ip }}:{{ voice_redis.port }}/{{ voice_redis.db }}
    AIO_VOICE_REDIS:
     db: {{ voice_redis.db }}
     ip: {{ voice_redis.ip }}
     password: {{ voice_redis.password }}
     port: {{ voice_redis.port }}
    VOICE_REDIS_EXPIRE: 86400
    VOICE_REALTIME_TIMEOUT: 120

    LCS_ONLINE_URL: http://monitor:monitor!123@10.34.60.17:8080/funny?cmd=show_detail_client
    LCS_PUBLISH_CONN:
        host: 10.34.60.19
        port: 5672
        username: sicent
        password: sicent
        vhost: sicent_vhost
        exchange_name: amq.direct
        routingkey: longsystem
    IMAGE_SERVER_URL: http://10.34.56.107:8085
    ZZOT_HOST: 0013.96ni.net

    DAILY_TASK_SNBID_REDIS: redis://:{{ daily_task_redis.password }}@{{ daily_task_redis.ip }}:{{ daily_task_redis.port }}/0
    DAILY_TASK_PLAYER_REDIS: redis://:{{ daily_task_redis.password }}@{{ daily_task_redis.ip }}:{{ daily_task_redis.port }}/1
    ONLINE_PLAYER_REDIS: redis://:{{ daily_task_redis.password }}@{{ daily_task_redis.ip }}:{{ daily_task_redis.port }}/2
    DAILY_SHARE_TASK_REDIS: redis://:{{ daily_task_redis.password }}@{{ daily_task_redis.ip }}:{{ daily_task_redis.port }}/3
    OSS_HOST: 172.30.236.102:8085
    LCS_MQ:
        host: 101.37.254.199
        port: 5672
        username: wxdesk
        password: desk123
        vhost: wxdesk_vhost
        exchange_name: amq.direct
        routingkey: desknotify
    VOICE_API_HOST: 172.30.236.102:8084
    AWARD_SHARE_REDIS: redis://:{{ award_share_redis.password }}@{{ award_share_redis.ip }}:{{ award_share_redis.port }}/{{ award_share_redis.db }}
    C_USER_CENTER_MONGODB_URL: mongodb://usercenter:user123@127.0.0.1:{{ ip_addrs.mongos.port}}/usercenter
    C_USER_CENTER_REDIS_URL: redis://:{{ user_info_center_redis.password }}@{{ user_info_center_redis.ip }}:{{ user_info_center_redis.port }}/{{ user_info_center_redis.db }}

    DETAIL_CLIENT_IP: {{ ip_addrs.DETAIL_CLIENT_IP }}
    LEAGUE_MAX_JOIN: 5
    LEAGUE_BEGIN_TIME: 1512964800
    LEAGUE_END_TIME: 1515081540
    BAIDU_AK: 3OWAwrH47n2wdiVHAmfa3ytk

    LEAGUE_DRAW_LOTTERY_REDIS: redis://:{{ league_draw_lottery_redis.password }}@{{ league_draw_lottery_redis.ip }}:{{ league_draw_lottery_redis.port }}/{{ league_draw_lottery_redis.db }}
