{% set ip_addrs = stack.get('ip_addrs', {}) %}

deskcomponent:
  settings:
    GET_DEFAULT_DESKTOP: http://10.34.53.178:8089/index.php/lockscreenapi/get_user_lockscreen   # 查询默认桌面
    SET_DEFAULT_DESKTOP: http://10.34.53.178:8089/index.php/lockscreenapi/set_user_lockscreen   # 设置默认桌面
    CHECK_VIP_URL: http://10.34.53.178:8088/interface/vip_api.php  # 判断VIP
    PAY_SETTING:
      add_money_url: http://10.34.51.51/cxf/newpayagent/generalOrder/userGeneralOrderRequest?
      query_add_money_url: http://10.34.51.51/cxf/newpayagent/generalOrder/userGeneralOrderQueryRequest?
      callback_add_money_url: http://{{ ip_addrs.nginx_backend }}:8083/exchange_callback
      add_money_client_id: 'TestPerf01'
      add_money_md5key: '123456'
    PAY_SETTING_PUBWIN:
      add_money_url: 'http://10.34.51.51/cxf/newpayagent/generalOrder/userGeneralOrderRequest?'
      query_add_money_url: 'http://10.34.51.51/cxf/newpayagent/generalOrder/userGeneralOrderQueryRequest?'
      callback_add_money_url: http://{{ ip_addrs.nginx_backend }}:8083/exchange_callback
      add_money_client_id: 'TestPerf01'
      add_money_md5key: '123456'
    PAY_SETTING_OLD:
      add_money_url: 'http://10.34.51.51/cxf/payagent/addMoney/userAddSubmit?'
      query_add_money_url: 'http://10.34.51.51/cxf/payagentnew/query/userAddSubmit?'
      callback_add_money_url: 'http://{{ ip_addrs.nginx_backend }}:8083/exchange_callback_old'
      add_money_client_id: 'TestPerf01'
      add_money_md5key: '123456'


    LCS_ONLINE_URL: http://monitor:monitor!123@10.34.60.17:8080/funny?cmd=show_detail_client
    LCS_PUBLISH_CONN:
        host: 10.34.60.19
        port: 5672
        username: sicent
        password: sicent
        vhost: sicent_vhost
        exchange_name: amq.direct
        routingkey: longsystem

    IMAGE_SERVER_URL: http://10.34.60.122:8085
    LWSM_MONGODB_URL: mongodb://lwsm:lwsm123@10.34.60.130:27017/lwsm  # light weight super market mongo url
    ZZOT_HOST: 10.34.57.48

    LCS_MQ:
        host: 10.34.60.19
        port: 5672
        username: sicent
        password: sicent
        vhost: sicent_vhost
        exchange_name: amq.direct
        routingkey: longsystem

    OSS_HOST: 10.34.60.122:8085

    USER_CENTER_CACHE_REDIS: redis://:redis123@{{ip_addrs.user_center_redis.ip}}:{{ip_addrs.user_center_redis.port}}/{{ip_addrs.user_center_redis.db}}  # 用户中心redis
    USER_CENTER_URL: http://{{ip_addrs.nginx_backend}}:8087 # 用户中心地址url
    PAYMENT_CODE_INFO_URL: http://10.34.60.119:8080 # 收款码支付信息请求地址url
    VOICE_API_HOST: 10.34.60.122:8084

    STATS_CENTER_URL: http://10.34.60.122:8081/statscenter
    DESKTOP_PLATFORM_HOST: 10.34.53.178:8088

    PUBG_ACTIVITY_REDIS: redis://:{{ip_addrs.pubg_activity_redis.password}}@{{ip_addrs.pubg_activity_redis.ip}}:{{ ip_addrs.pubg_activity_redis.port }}/{{ ip_addrs.pubg_activity_redis.db }}
    AIO_PUBG_ACTIVITY_REDIS_URL:
      ip: {{ ip_addrs.pubg_activity_redis.ip  }}
      port: {{ ip_addrs.pubg_activity_redis.port }}
      db: {{ ip_addrs.pubg_activity_redis.db }}
      password: {{ ip_addrs.pubg_activity_redis.password }}
    LEAGUE_BEGIN_TIME: 1512522000