{% set ip_addrs = stack.get('ip_addrs', {}) %}

deskcomponent:
  settings:
    GET_DEFAULT_DESKTOP: http://172.30.236.96:8089/index.php/lockscreenapi/get_user_lockscreen
    SET_DEFAULT_DESKTOP: http://172.30.236.96:8089/index.php/lockscreenapi/set_user_lockscreen
    CHECK_VIP_URL: http://172.30.236.96:8088/interface/vip_api.php  # 判断VIP
    PAY_SETTING:
      add_money_url: http://fop.sicent.com/cxf/newpayagent/generalOrder/userGeneralOrderRequest?
      query_add_money_url: http://fop.sicent.com/cxf/newpayagent/generalOrder/userGeneralOrderQueryRequest?
      callback_add_money_url: http://{{ ip_addrs.nginx_backend }}:8083/exchange_callback
      add_money_client_id: 'GamePlaza'
      add_money_md5key: 'DeskGamePlaza'
    PAY_SETTING_PUBWIN:
      add_money_url: http://fop.sicent.com/cxf/newpayagent/generalOrder/userGeneralOrderRequest?
      query_add_money_url: http://fop.sicent.com/cxf/newpayagent/generalOrder/userGeneralOrderQueryRequest?
      callback_add_money_url: 'http://{{ ip_addrs.nginx_backend }}:8083/exchange_callback'
      add_money_client_id: 'GamePlaza'
      add_money_md5key: 'DeskGamePlaza'
    PAY_SETTING_OLD:
      add_money_url: 'http://fop.sicent.com/cxf/payagent/addMoney/userAddSubmit?'
      query_add_money_url: 'http://fop.sicent.com/cxf/payagentnew/query/userAddSubmit?'
      callback_add_money_url: 'http://{{ ip_addrs.nginx_backend }}:8083/exchange_callback_old'
      add_money_client_id: 'GamePlaza'
      add_money_md5key: 'DeskGamePlaza'
    LOG_ADDRESS:
    - __: overwrite
    - 221.228.80.111
    - 22002

    GOLOG_ADDRESS:
    - __: overwrite
    - 172.30.236.178
    - 22002

    LCS_ONLINE_URL: http://monitor:monitor!123@lcs.baibwang.com:8080/funny?cmd=show_detail_client
    LCS_PUBLISH_CONN:
        host: 221.228.80.107
        port: 5672
        username: sicent
        password: sicent
        vhost: sicent_vhost
        exchange_name: amq.direct
        routingkey: longsystem
    IMAGE_SERVER_URL: http://uploadqdd.wxdesk.com

    OSS_HOST: 172.30.236.102:8085 # online env oss.wxdesk.com
    LCS_MQ:
        host: 101.37.254.199
        port: 5672
        username: wxdesk
        password: desk123
        vhost: wxdesk_vhost
        exchange_name: amq.direct
        routingkey: desknotify
    OSS_HOST: 172.30.236.102:8085

    PAYMENT_CODE_INFO_URL: http://qdd.sw0013.com # 收款码支付信息请求地址url
    USER_CENTER_CACHE_REDIS: redis://:redis123@{{ip_addrs.user_center_redis.ip}}:{{ip_addrs.user_center_redis.port}}/{{ip_addrs.user_center_redis.db}}  # 用户中心redis
    USER_CENTER_URL: http://{{ip_addrs.nginx_backend}}:8087 # 用户中心地址url
    VOICE_API_HOST: 172.30.236.102:8084
    AWARD_SHARE_REDIS: redis://:{{ ip_addrs.award_share_redis.password }}@{{ ip_addrs.award_share_redis.ip }}:{{ ip_addrs.award_share_redis.port }}/{{ ip_addrs.award_share_redis.db }}

    STATS_CENTER_URL: http://172.30.236.102:8081/statscenter
    DESKTOP_PLATFORM_HOST: 172.30.236.96:8088

    PUBG_ACTIVITY_REDIS: redis://:redis123@{{ip_addrs.pubg_activity_redis.ip}}:{{ ip_addrs.pubg_activity_redis.port }}/{{ ip_addrs.pubg_activity_redis.db }}
    AIO_PUBG_ACTIVITY_REDIS_URL: 
      ip: {{ip_addrs.pubg_activity_redis.ip}}
      port: {{ ip_addrs.pubg_activity_redis.port }}
      db: {{ ip_addrs.pubg_activity_redis.db }}
      password: redis123

    LEAGUE_MAX_JOIN: 5
    LEAGUE_BEGIN_TIME: 1513040400
    LEAGUE_END_TIME: 1515081540
