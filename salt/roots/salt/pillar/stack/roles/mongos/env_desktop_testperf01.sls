{% set ip_addrs = stack.get('ip_addrs', {}) %}
mongos:
  configsvr:
    user: admin
    passwd: admin123
    authdb: admin
    servers:
    - __: overwrite
    - host: {{ ip_addrs.mongo_configsvr01 }}
      port: 27019
  shardsvr:
    servers:
    - __: overwrite
    - host: shard01/{{ ip_addrs.mongo_shardsvr01_p }}:27018
      name: shard01