{% set ip_addrs = stack.get('ip_addrs', {}) %}
mongos:
  use_external_net: true
  port: 3306
  configsvr:
    passwd: admin2018
    replSetName: devtest_cfg_shard
    servers:
    - __: overwrite
    - host: {{ ip_addrs.get('mongo02_configsvr01', '0.0.0.0') }}
      port: 3307
    - host: {{ ip_addrs.get('mongo02_configsvr02', '0.0.0.0') }}
      port: 3307
    replSetName: devtest_cfg_shard
  shardsvr:
    servers:
    - __: overwrite
    - host: dev_shard01/{{ ip_addrs.mongodev_shardsvr01_p }}:3308
      name: dev_shard01
    - host: dev_shard02/{{ ip_addrs.mongodev_shardsvr02_p }}:3308
      name: dev_shard02
