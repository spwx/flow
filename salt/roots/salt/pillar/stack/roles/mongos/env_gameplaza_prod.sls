{% set ip_addrs = stack.get('ip_addrs', {}) %}
mongos:
  use_external_net: false
  port: 3306
  configsvr:
    passwd: admin2018
    servers:
    - __: overwrite
    - host: {{ ip_addrs.get('mongo02_configsvr01', '0.0.0.0') }}
      port: 3307
    - host: {{ ip_addrs.get('mongo02_configsvr02', '0.0.0.0') }}
      port: 3307
    replSetName: cfg_shard
  shardsvr:
    servers:
    - __: overwrite
    - host: shard01/{{ ip_addrs.mongo02_shardsvr01_p }}:3308
      name: shard01
    - host: shard02/{{ ip_addrs.mongo02_shardsvr02_p }}:3308
      name: shard02
