{% set ip_addrs = stack.get('ip_addrs', {}) %}
{% set mongos_config = stack.get('mongos', {}) %}
mongos:
  configsvr:
    user: clusterAdmin
    passwd: 123456
    authdb: admin
    servers:
    - __: overwrite
    - host: {{ ip_addrs.get('mongo_configsvr01', '0.0.0.0') }}
      port: 27019
    - host: {{ ip_addrs.get('mongo_configsvr02', '0.0.0.0') }}
      port: 27019
