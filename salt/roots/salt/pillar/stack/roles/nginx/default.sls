nginx:
  install_prefix: /data/SicentWebserver/nginx
  log_dir: /data/SicentWebserver/nginx/log
  with:
  - http_stub_status_module