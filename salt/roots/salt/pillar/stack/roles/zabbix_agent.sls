{% set hostname = pillar.get('hostname', 'localhost') %}
zabbix_agent:
  Hostname: {{ hostname }}
