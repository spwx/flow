{% set hostname, domainname = opts['id'].split('.', 1) %}
{% set domain_dirs = domainname.split('.') | reverse %}
base:
  '*':
    - stack/minions/{{ domain_dirs | join('/') }}/{{ hostname }}
