{% from 'nginx/vars.j2' import nginx with context %}
{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}

{{ nginx.conf_dir }}/conf.d/WxImageServer.backend.conf:
  file.managed:
    - source: salt://WxImageServer/nginx/files/conf.d/frontend.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - defaults:
        wis_frontend_nginx: {{ wis.nginx.backend }}
  service.running:
    - name: {{ nginx.svc_name }}
    - reload: True
    - watch:
      - file: {{ nginx.conf_dir }}/conf.d/WxImageServer.backend.conf
