{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}

nfs.packages:
  pkg.installed:
    - pkgs:
        - nfs-utils

{{ wis.client.store_path }}:
  file.directory:
    - makedirs: True

/etc/fstab:
  file.append:
    - text:
      - {{ wis.client.server.ip }}:{{ wis.client.server.store_path }} {{ wis.client.store_path }} nfs rsize=8192,wsize=8192,timeo=14,soft,intr
    - require:
      - file: {{ wis.client.store_path }}

mount_nfs:
  cmd.run:
    - names:
      - (umount {{ wis.client.store_path }} 2>1 &1>/dev/null 
        && mount -t nfs -o rw,soft {{ wis.client.server.ip }}:{{ wis.client.server.store_path }} {{ wis.client.store_path }})
