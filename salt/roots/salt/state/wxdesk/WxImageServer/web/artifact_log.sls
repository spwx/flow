{% from "log_artifact/vars.j2" import log_artifact with context %}
{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}

include:
  - log_artifact

# mkdir logrotate/logs
# mkdir scripts
{{ wis.web.install_path }}/scripts:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{{ wis.web.install_path }}/logrotate/logs/oss:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

# wis.web.conf to logrotate
# artifact_log.sh to script
{% if log_artifact.get('everydaylogs', false) %}
{{ wis.web.install_path }}/scripts/artifact_log.sh:
  file.managed:
    - source: salt://log_artifact/files/artifact_log.sh
    - template: jinja
    - defaults:
        everydaylogs: {{ log_artifact.get('everydaylogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root
    - require:
      - file: {{ wis.web.install_path }}/scripts

{{ wis.web.install_path }}/logrotate/oss.conf:
  file.managed:
    - source: salt://WxImageServer/web/files/logrotate.conf.j2
    - template: jinja
    - backup: True
    - user: root
    - group: root
    - require:
      - file: {{ wis.web.install_path }}/scripts/artifact_log.sh

oss_logrotate:
  cron.present:
    - name: /usr/sbin/logrotate -f {{ wis.web.install_path }}/logrotate/oss.conf
    - minute: 0
    - hour: '*/2'

{% endif %}
