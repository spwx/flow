{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}
{% from "WxImageServer/vars.j2" import WxImageServer_pkg as wis_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

{{ wis.web.log_dir }}:
  file.directory:
    - user: {{ wis.user }}
    - group: {{ wis.group }}
    - makedirs: True
    - recurse:
      - user
      - group

# create package directory
{{ wis.web.install_path }}/{{ wis_pkg.package_name }}:
  file.directory:
    - user: {{ wis.user }}
    - group: {{ wis.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wis_pkg.package_name }}-*

# get WxVoiceMaster_web pkg
get_WxImageServer_web:
  file.managed:
    - name: {{ src_dir }}/{{ wis_pkg.package_name }}-{{ wis_pkg.version }}.tar.gz
    - source: {{ wis_pkg.pkg_url }}
    {%- if wis_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wis_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wis_pkg.package_name }}-{{ wis_pkg.version }}.tar.gz
    - require:
      - file: get_WxImageServer_web


# install WxImageServer_web web
install_WxImageServer_web:
  cmd.run:
    - cwd: {{ wis.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wis_pkg.package_name }}-{{ wis_pkg.version }}/* {{ wis_pkg.package_name }}
        && source {{ wis.web.install_path }}/env/bin/activate
        && pip3 install -r {{ wis_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_WxImageServer_web

{% if wis.web.get('settings', False) %}
{{ wis.web.install_path }}/{{ wis_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wis.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{% endif %}


# reload WxImageServer_web service
start_web_service:
  cmd.run:
    - names:
      - initctl start oss || true || true
      - kill -HUP `ps -ef|grep /env/| grep -v grep | awk '{print $2 }'`;

    - watch:
      - file: /etc/init/oss.conf
      {% if wis.web.get('settings', False) %}
      - file: {{ wis.web.install_path }}/{{ wis_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/oss.conf
      - cmd: install_WxImageServer_web
