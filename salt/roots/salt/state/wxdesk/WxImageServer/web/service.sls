{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}
{% from "WxImageServer/vars.j2" import WxImageServer_pkg as wis_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/init/oss.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: huangqiyin
        description: oss daemon
        chdir: {{ wis.web.install_path }}/{{ wis_pkg.package_name }}
        command: {{ wis.web.install_path}}/env/bin/python3 main.py
    - user: root
    - group: root
    - mode: '644'