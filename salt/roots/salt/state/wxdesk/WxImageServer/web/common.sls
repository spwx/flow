{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}
{% from "python3/vars.j2" import python3 with context %}

include:
  - .common

{{ wis.web.install_path }}:
  file.directory:
    - user: {{ wis.user }}
    - group: {{ wis.group }}
    - makedirs: True
    - require:
      - user: {{ wis.user }}_user
      - group: {{ wis.group }}_group

create_env:
  cmd.run:
    - cwd: {{ wis.web.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ wis.web.install_path }}

