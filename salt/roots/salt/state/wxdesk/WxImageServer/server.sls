{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}

nfs.packages:
  pkg.installed:
  - pkgs:
    - nfs-utils
    - rpcbind

setting_os_start_enable:
  cmd.run:
    - names:
      - chkconfig --add nfs
      - chkconfig --add rpcbind
    - require:
      - pkg: nfs.packages

update_nfs_sysconfig:
  file.managed:
    - name: /etc/sysconfig/nfs
    - contents: |
        LOCKD_TCPPORT=32803
        LOCKD_UDPPORT=32769
        MOUNTD_PORT=892

{{ wis.server.store_path }}:
  file.directory:
    - makedirs: True
    - mode: 777

update_exports:
  file.managed:
    - name: /etc/exports
    - contents: {{ wis.server.exports }}
    - require:
      - file: {{ wis.server.store_path }}

{% for server in ['rpcbind', 'nfs'] %}
start_nft_server_{{ server  }}:
  service.running:
    - name: {{ server }}
    - enable: True
    - reload: True
    - watch:
      - file: update_nfs_sysconfig
      - file: update_exports
{% endfor %}
