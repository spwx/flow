{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}

{{ wis.group }}_group:
  group.present:
    - name: {{ wis.group }}

{{ wis.user }}_user:
  user.present:
    - name: {{ wis.user }}
    - shell: /bin/bash
    - groups:
      - {{ wis.group }}
    - require:
      - group: {{ wis.group }}_group

/var/run/WxImageServer:
  file.directory:
    - user: {{ wis.user }}
    - group: {{ wis.group }}
    - makedirs: True
    - require:
      - user: {{ wis.user }}_user
      - group: {{ wis.group }}_group