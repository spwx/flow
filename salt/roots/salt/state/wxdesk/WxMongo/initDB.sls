{% from "mongo/vars.j2" import mongo with context %}
{% from "mongos/vars.j2" import mongos as config with context %}
{% from "WxMongo/vars.j2" import WxMongo with context %}
{% set dbName = salt['pillar.get']('DBNAME', '___') %}

{% set mongos = config.configsvr %}

{% for user in WxMongo.users %}
    {% set name = user.name %}
    {% set passwd = user.passwd %}
    {% set database = user.database %}

{% if database == dbName %}

mongodb-create-{{ name }}-account:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo --port {{ config.port }} -u {{ mongos.user }} -p {{ mongos.passwd }} {{ mongos.authdb }} --quiet --eval
        "db.getSiblingDB('{{ database }}').createUser({user:'{{ name }}', pwd:'{{ passwd }}', roles: []})"
    - unless: {{ mongo.install_path }}/bin/mongo --port {{ config.port }} -u {{ mongos.user }} -p {{ mongos.passwd }} {{ mongos.authdb }} --quiet --eval "db.getSiblingDB('{{ database }}').getUser('{{ name }}')" |grep {{ name }}

{% for role in user.roles %}
command-mongodb-grant-{{ role }}-role-to-admin:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo --port {{ config.port }} -u {{ mongos.user }} -p {{ mongos.passwd }} {{ mongos.authdb }} --quiet --eval
        "db.getSiblingDB('{{ database }}').grantRolesToUser('{{ name }}', [{role:'{{ role }}', db:'{{ database }}'}])"
    - output_loglevel: quiet
    - unless: {{ mongo.install_path }}/bin/mongo --port {{ config.port }} -u {{ mongos.user }} -p {{ mongos.passwd }} {{ mongos.authdb }} --quiet --eval "db.getSiblingDB('{{ database }}').getUser('{{ name}}')" |grep {{ role }}
{% endfor %} # --//end user.roles

{% endif %}
{% endfor %} # --//end WxMongo.users

init-{{dbName}}:
  file.managed:
    - name: /tmp/init_{{ dbName }}_db.js
    - source: salt://WxMongo/files/init_{{dbName}}_db.j2
    - template: jinja
    - defaults:
        dbName: {{ dbName }}
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo --port {{ config.port }} -u {{ mongos.user }} -p {{ mongos.passwd }} {{ mongos.authdb }} --quiet
        /tmp/init_{{ dbName }}_db.js
    - require:
        - file: init-{{ dbName }}
