{% from "gologstat/vars.j2" import gologstat as wxp with context %}
{% from "gologstat/vars.j2" import gologstat_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/supervisord:
  file.directory:
    - user: root
    - mode: 755
    - makedirs: True


/etc/supervisord/gologstat.conf:
  file.managed:
    - source: salt://python3/files/supervisor.conf.j2
    - template: jinja
    - defaults:
        name: gologstat
        user: gologstat
        directory: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/python app.py
    - user: root
    - group: root
    - mode: '644'


/etc/supervisord/gologstat-celerybeat.conf:
  file.managed:
    - source: salt://python3/files/supervisor.conf.j2
    - template: jinja
    - defaults:
        name: gologstat-celerybeat
        user: gologstat
        directory: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/celery -A gologstat.celery.celery beat
    - user: root
    - group: root
    - mode: '644'


/etc/supervisord/gologstat-celeryworker.conf:
  file.managed:
    - source: salt://python3/files/supervisor.conf.j2
    - template: jinja
    - defaults:
        name: gologstat-celeryworker
        user: gologstat
        directory: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/celery worker -A gologstat.celery.celery -l info -f logs/celery_worker.log
    - user: root
    - group: root
    - mode: '644'


reload_supervisor:
  cmd.run:
    - name: supervisorctl update
    - user: root
    - group: root