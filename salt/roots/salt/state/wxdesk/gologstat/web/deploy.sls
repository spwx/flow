{% from "gologstat/vars.j2" import gologstat as wxp with context %}
{% from "gologstat/vars.j2" import gologstat_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# create package directory
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True
    - mode: 755

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wxp_pkg.package_name }}-*

# get gologstat_web pkg
get_gologstat_web:
  file.managed:
    - name: {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - source: {{ wxp_pkg.pkg_url }}
    {%- if wxp_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wxp_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - require:
      - file: get_gologstat_web



# install gologstat web
install_gologstat_web:
  cmd.run:
    - cwd: {{ wxp.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}/* {{ wxp_pkg.package_name }}
        && source {{ wxp.web.install_path }}/env/bin/activate
        && pip3 install -r {{ wxp_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_gologstat_web

    - user: {{ wxp.user }}
    - group: {{ wxp.group }}


# install web sertting.yaml
{% if wxp.get('settings', False) %}
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wxp.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: install_gologstat_web
{% endif %}


# reload gologstat_web service
start_api_and_web_service:
  cmd.run:
    - user: root
    - group: root
    - names:
      - supervisorctl start gologstat || true
      - kill -HUP `ps -ef|grep /env/| grep -v grep | awk '{print $2 }'`;

    - watch:
      - file: /etc/supervisord/gologstat.conf
    - require:
      - file: /etc/supervisord/gologstat.conf
      - cmd: install_gologstat_web

