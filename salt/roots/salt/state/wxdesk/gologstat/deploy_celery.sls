{% from "gologstat/vars.j2" import gologstat as wxp with context %}
{% from "gologstat/vars.j2" import gologstat_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .web.service


# reload gologstat_clerybeat service
start_celerybeat_service:
  cmd.run:
    - user: root
    - group: root
    - names:
      -  supervisorctl restart gologstat-celerybeat
    - watch:
      - file: /etc/supervisord/gologstat-celerybeat.conf

start_celeryworker_service:
  cmd.run:
    - user: root
    - group: root
    - names:
      - supervisorctl restart gologstat-celeryworker
    - watch:
      - file: /etc/supervisord/gologstat-celeryworker.conf
    - require:
      - file: /etc/supervisord/gologstat-celeryworker.conf
