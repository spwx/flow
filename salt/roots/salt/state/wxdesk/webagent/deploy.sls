{% from "webagent/vars.j2" import webagent with context %}

{% set src_dir = '/usr/local/src' %}
{% set package_name = 'webagent' %}

include:
  - .common
  - .service

get_webagent:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ package_name }}.tar.gz
  file.managed:
    - name: {{ src_dir }}/{{ package_name }}.tar.gz
    - source: salt://webagent/files/{{ package_name }}.tar.gz
    - require:
      - cmd: get_webagent

# stop svc_name
stop_{{ webagent.svc_name }}_service:
  service.dead:
    - name: {{ webagent.svc_name }}
    - require:
      - file: /etc/init.d/{{ webagent.svc_name }}
      - file: get_webagent

install_webagent:
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (rm -rf {{ webagent.install_path }}/*
        && tar -xzf {{ package_name }}.tar.gz -C {{ webagent.install_path }})
    - require:
      - file: get_webagent
      - service: stop_{{ webagent.svc_name }}_service

{{ webagent.install_path }}/etc/app.cfg:
  file.managed:
    - source: salt://webagent/files/app_{{ webagent.conf_env }}.cfg
    - template: jinja
    - defaults:
        listen_ip: {{ webagent.listen_ip }}
        listen_port: {{ webagent.listen_port }}
    - backup: True
    - require:
      - service: stop_{{ webagent.svc_name }}_service

{{ webagent.install_path }}/log:
  file.directory:
    - user: {{ webagent.user }}
    - group: {{ webagent.group }}
    - makedirs: True
    - require:
      - service: stop_{{ webagent.svc_name }}_service

# start svc_name
start_{{ webagent.svc_name }}_service:
  service.running:
    - name: {{ webagent.svc_name }}
    - reload: True
    - enable: True
    - watch:
      - file: {{ webagent.install_path }}/etc/app.cfg
    - require:
      - file: /etc/init.d/{{ webagent.svc_name }}
      - cmd: install_webagent

update_unix_socket_path_mod:
  cmd.run:
    - name: chmod 777 /tmp/008wgt
    - require:
      - service: start_{{ webagent.svc_name }}_service
