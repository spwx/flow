{% from "webagent/vars.j2" import webagent with context %}

/etc/init.d/{{ webagent.svc_name }}:
  file.managed:
    - source: salt://webagent/files/webagent.init.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0755'
  cmd.run:
    - name: chkconfig --add {{ webagent.svc_name }}
    - unless: chkconfig --list | grep {{ webagent.svc_name }}
    - require:
      - file: /etc/init.d/{{ webagent.svc_name }}