{% from "webagent/vars.j2" import webagent with context %}

{{ webagent.group }}_group:
  group.present:
    - name: {{ webagent.group }}

{{ webagent.user }}_user:
  user.present:
    - name: {{ webagent.user }}
    - shell: /bin/false
    - groups:
      - {{ webagent.group }}
    - require:
      - group: {{ webagent.group }}_group

{{ webagent.install_path }}:
  file.directory:
    - user: {{ webagent.user }}
    - group: {{ webagent.group }}
    - makedirs: True
    - require:
      - user: {{ webagent.user }}_user
      - group: {{ webagent.group }}_group
