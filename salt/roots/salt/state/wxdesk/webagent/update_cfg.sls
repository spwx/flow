{% from "webagent/vars.j2" import webagent with context %}

{% set src_dir = '/usr/local/src' %}
{% set package_name = 'webagent' %}

{{ webagent.install_path }}/etc/app.cfg:
  file.managed:
    - source: salt://webagent/files/app_{{ webagent.conf_env }}.cfg
    - template: jinja
    - defaults:
        listen_ip: {{ webagent.listen_ip }}
        listen_port: {{ webagent.listen_port }}
    - backup: True
