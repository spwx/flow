{% from "WxDesktopClient/vars.j2" import WxDesktopClient as wdc with context %}
{% from "WxDesktopClient/vars.j2" import WxDesktopClient_frontend_pkg as wdc_frontend_pkg with context %}

{% set src_dir = '/usr/local/src' %}

get_WxDesktopClient_frontend_pkg:
  cmd.run:
    - cwd: {{ src_dir }}
      name: rm -rf {{ wdc_frontend_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ wdc_frontend_pkg.package_name }}-{{ wdc_frontend_pkg.version }}.tar.gz
    - source: {{  wdc_frontend_pkg.pkg_url }}
    {% if wdc_frontend_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wdc_frontend_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_WxDesktopClient_frontend_pkg

install_WxDesktopClient_frontend_pkg:
  file.directory:
    - name: {{ wdc.frontend.install_path }}
    - makedirs: True
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (tar -xzf {{ wdc_frontend_pkg.package_name }}-{{ wdc_frontend_pkg.version }}.tar.gz -C {{ wdc.frontend.install_path }})
    - require:
      - file: install_WxDesktopClient_frontend_pkg
      - file: get_WxDesktopClient_frontend_pkg