{% from "LcServer/vars.j2" import LcServer as lcs with context %}

{%- set agent = lcs.agent -%}

/etc/init/{{ agent.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: {{ agent.svc_name }} daemon
        chdir: {{ lcs.install_path }}/agent
        command: {{ lcs.install_path }}/agent/server
    - user: root
    - group: root
    - mode: '644'

