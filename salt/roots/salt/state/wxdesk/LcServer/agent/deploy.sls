{% from "LcServer/vars.j2" import LcServer as lcs with context %}
{% from "LcServer/vars.j2" import LcAgentPkg as lcagent_pkg with context %}
{% set agent = lcs.agent %}
{% set src_dir = '/usr/local/src' %}
{% set install_path = "{0}/agent".format(lcs.install_path) %}

include:
  - .service

get_lcagent:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ lcagent_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ lcagent_pkg.package_name }}-{{ lcagent_pkg.version }}.tar.gz
    - source: {{ lcagent_pkg.pkg_url }}
    {% if lcagent_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ lcagent_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_lcagent

# stop svc_name
stop_{{ agent.svc_name }}_service:
  service.dead:
    - name: {{ agent.svc_name }}
    - require:
      - file: /etc/init/{{ agent.svc_name }}.conf
      - file: get_lcagent

install_lcagent:
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (rm -rf {{ install_path }}/*
        && tar -xzf {{ lcagent_pkg.package_name }}-{{ lcagent_pkg.version }}.tar.gz
        && \cp -r {{ lcagent_pkg.package_name }}-{{ lcagent_pkg.version }}/* {{ install_path }})
    - require:
      - file: get_lcagent
      - service: stop_{{ agent.svc_name }}_service

{{ install_path }}/etc/app.cfg:
  file.managed:
    - source: salt://LcServer/files/agent/etc/app.cfg
    - template: jinja
    - backup: True
    - require:
      - service: stop_{{ agent.svc_name }}_service

{{ install_path }}/log:
  file.directory:
    - user: {{ lcs.user }}
    - group: {{ lcs.group }}
    - makedirs: True
    - require:
      - service: stop_{{ agent.svc_name }}_service

# start svc_name
start_{{ agent.svc_name }}_service:
  service.running:
    - name: {{ agent.svc_name }}
    - enable: True
    - reload: True
    - watch:
      - file: /etc/init/{{ agent.svc_name }}.conf
      - file: {{ install_path }}/etc/app.cfg
    - require:
      - file: /etc/init/{{ agent.svc_name }}.conf
      - file: {{ install_path }}/etc/app.cfg
      - cmd: install_lcagent
