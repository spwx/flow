{%- from "LcServer/vars.j2" import rabbitmq as mq with context -%}

{% set src_dir = '/usr/local/src' %}

erlang.install:
  file.managed:
    - name: {{ src_dir }}/erlang.rpm
    - source: {{ mq.erlang_pkg_url }}
    {% if mq.get('erlang_pkg_checksum', false) %}
    - source_hash: {{ mq.erlang_pkg_checksum }}
    {% endif %}
  cmd.run:
    - name: yum install localinstall -y {{ src_dir }}/erlang.rpm
    - unless: rpm  -qa |grep erlang
    - require:
      - file: erlang.install

rabbitmq.install:
  file.managed:
    - name: {{ src_dir }}/rabbitmq.rpm
    - source: {{ mq.rabbitmq_pkg_url }}
    {% if mq.get('rabbitmq_pkg_checksum', false) %}
    - source_hash: {{ mq.rabbitmq_pkg_checksum }}
    {% endif %}
  cmd.run:
    - name: yum install localinstall -y {{ src_dir }}/rabbitmq.rpm
    - unless: rpm  -qa |grep rabbitmq
    - require:
      - file: rabbitmq.install

rabbitmq.config:
  file.managed:
    - name: /etc/rabbitmq/rabbitmq.config
    - source: salt://LcServer/rabbitmq/files/rabbitmq.config
    - user: root
    - group: root
    - mode: '0644'
  cmd.run:
    - name: rabbitmq-plugins enable --offline rabbitmq_management
    - unless: rabbitmq-plugins list | grep '\[E.*rabbitmq_management'

rabbitmq-server:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: rabbitmq.config
    - require:
      - file: rabbitmq.config
      - cmd: rabbitmq.config
    
