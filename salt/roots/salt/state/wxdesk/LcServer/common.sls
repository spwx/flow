{% from "LcServer/vars.j2" import LcServer as lcs with context %}

{{ lcs.group }}_group:
  group.present:
    - name: {{ lcs.group }}

{{ lcs.user }}_user:
  user.present:
    - name: {{ lcs.user }}
    - shell: /bin/bash
    - groups:
      - {{ lcs.group }}
    - require:
      - group: {{ lcs.group }}_group

/var/run/LcServer:
  file.directory:
    - user: {{ lcs.user }}
    - group: {{ lcs.group }}
    - makedirs: True
    - require:
      - user: {{ lcs.user }}_user
      - group: {{ lcs.group }}_group
