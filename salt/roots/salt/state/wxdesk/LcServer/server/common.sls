{%- from "LcServer/vars.j2" import LcServer as lcs with context -%}

include:
  - LcServer.common

{{ lcs.install_path }}/server:
  file.directory:
    - user: {{ lcs.user }}
    - group: {{ lcs.group }}
    - makedirs: True
    - require:
      - user: {{ lcs.user }}_user
      - group: {{ lcs.group }}_group
