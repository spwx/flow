{% from "LcServer/vars.j2" import LcServer as lcs with context %}

{%- set server = lcs.server -%}

/etc/init/{{ server.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: {{ server.svc_name }} daemon
        chdir: {{ lcs.install_path }}/server
        command: {{ lcs.install_path }}/server/server
    - user: root
    - group: root
    - mode: '644'
