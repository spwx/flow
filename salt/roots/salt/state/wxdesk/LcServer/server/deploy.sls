{% from "LcServer/vars.j2" import LcServer as lcs with context %}
{% from "LcServer/vars.j2" import LcServerPkg as lcserver_pkg with context %}
{% set server = lcs.server %}
{% set src_dir = '/usr/local/src' %}
{% set install_path = "{0}/server".format(lcs.install_path) %}

include:
  - .service

get_lcserver:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ lcserver_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ lcserver_pkg.package_name }}-{{ lcserver_pkg.version }}.tar.gz
    - source: {{ lcserver_pkg.pkg_url }}
    {% if lcserver_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ lcserver_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_lcserver

# stop svc_name
stop_{{ server.svc_name }}_service:
  service.dead:
    - name: {{ server.svc_name }}
    - require:
      - file: /etc/init/{{ server.svc_name }}.conf
      - file: get_lcserver

install_lcserver:
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (rm -rf {{ install_path }}/*
        && tar -xzf {{ lcserver_pkg.package_name }}-{{ lcserver_pkg.version }}.tar.gz
        && \cp -r {{ lcserver_pkg.package_name }}-{{ lcserver_pkg.version }}/* {{ install_path }})
    - require:
      - file: get_lcserver
      - service: stop_{{ server.svc_name }}_service

{{ install_path }}/etc/app.cfg:
  file.managed:
    - source: salt://LcServer/files/server/etc/app.cfg
    - template: jinja
    - backup: True
    - require:
      - service: stop_{{ server.svc_name }}_service

{{ install_path }}/log:
  file.directory:
    - user: {{ lcs.user }}
    - group: {{ lcs.group }}
    - makedirs: True
    - require:
      - service: stop_{{ server.svc_name }}_service

# start svc_name
start_{{ server.svc_name }}_service:
  service.running:
    - name: {{ server.svc_name }}
    - enable: True
    - reload: True
    - watch:
      - file: /etc/init/{{ server.svc_name }}.conf
      - file: {{ install_path }}/etc/app.cfg
    - require:
      - file: /etc/init/{{ server.svc_name }}.conf
      - file: {{ install_path }}/etc/app.cfg
      - cmd: install_lcserver
