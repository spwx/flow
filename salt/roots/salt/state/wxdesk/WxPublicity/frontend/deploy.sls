{% from "WxPublicity/vars.j2" import wxp, wxp_pkg with context %}

{% set src_dir = '/usr/local/src' %}

get_pkg:
  cmd.run:
    - cwd: {{ src_dir }}
      name: rm -rf {{ wxp_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - source: {{ wxp_pkg.pkg_url }}
    {% if wxp_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wxp_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_pkg

install_pkg:
  file.directory:
    - name: {{ wxp.frontend.install_path }}
    - makedirs: True
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (tar -xzf {{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz -C {{ wxp.frontend.install_path }})
    - require:
      - file: get_pkg
      - file: install_pkg