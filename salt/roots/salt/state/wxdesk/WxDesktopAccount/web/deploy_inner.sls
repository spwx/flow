{% from "WxDesktopAccount/vars.j2" import wxdesktop_account as wxp with context %}
{% from "WxDesktopAccount/vars.j2" import wxdesktop_account_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# create package directory
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wxp_pkg.package_name }}-*

# get WxDesktopAccount_web pkg
get_WxDesktopAccount_web:
  file.managed:
    - name: {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - source: {{ wxp_pkg.pkg_url }}
    {%- if wxp_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wxp_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - require:
      - file: get_WxDesktopAccount_web

# stop WxDesktopAccount_web service
stop_api_and_web_service:
  cmd.run:
    - names:
      - initctl stop WxDesktopAccount_inner | exit 0
    - require:
      - file: /etc/init/WxDesktopAccount.conf
      - file: get_WxDesktopAccount_web

# install WxDesktopAccount web
install_WxDesktopAccount_web:
  cmd.run:
    - cwd: {{ wxp.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}/* {{ wxp_pkg.package_name }}
        && source {{ wxp.web.install_path }}/env/bin/activate
        && pip3 install -r {{ wxp_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_WxDesktopAccount_web
      - cmd: stop_api_and_web_service

# install web sertting.yaml
{% if wxp.get('settings', False) %}
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wxp.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_api_and_web_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wxp.web.install_path, wxp_pkg.package_name))%}
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_api_and_web_service
{%- endif %}

# reload WxDesktopAccount_web service
start_api_and_web_service:
  cmd.run:
    - names:
      - initctl start WxDesktopAccount_inner
    - watch:
      - file: /etc/init/WxDesktopAccount_inner.conf
      {% if wxp.web.get('settings', False) %}
      - file: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/WxDesktopAccount_inner.conf
      - cmd: install_WxDesktopAccount_web