{% from "WxDesktopAccount/vars.j2" import wxdesktop_account as wxp with context %}
{% from "WxDesktopAccount/vars.j2" import wxdesktop_account_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/init/WxDesktopAccount.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: policy-api daemon
        chdir: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/gunicorn app:app  -c {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/gunicorn_config.py
    - user: root
    - group: root
    - mode: '644'

/etc/init/WxDesktopAccount_inner.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: policy-api daemon
        chdir: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/gunicorn app_inter:app  -c {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/gunicorn_config_inter.py
    - user: root
    - group: root
    - mode: '644'


