{% from "WxDesktopAccount/vars.j2" import wxdesktop_account as wbs with context %}
{% from "WxDesktopAccount/vars.j2" import wxdesktop_account_pkg as wbs_pkg with context %}

{% set src_dir = '/usr/local/src' %}

get_WxDesktopAccount_frontend_pkg:
  cmd.run:
    - cwd: {{ src_dir }}
      name: rm -rf {{ wbs_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ wbs_pkg.package_name }}-{{ wbs_pkg.version }}.tar.gz
    - source: {{ wbs_pkg.pkg_url }}
    {% if wbs_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wbs_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_WxDesktopAccount_frontend_pkg

install_WxDesktopAccount_frontend_pkg:
  file.directory:
    - name: {{ wbs.frontend.install_path }}
    - makedirs: True
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (tar -xzf {{ wbs_pkg.package_name }}-{{ wbs_pkg.version }}.tar.gz
        && \cp -Rf {{ wbs_pkg.package_name }}-{{ wbs_pkg.version }}/* {{ wbs.frontend.install_path }})
    - require:
      - file: install_WxDesktopAccount_frontend_pkg
      - file: get_WxDesktopAccount_frontend_pkg
