{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/accountcenter-celery-worker.conf:
  file.managed:
    - source: salt://WxAccountCenter/celeryd/files/logrotate.conf.j2
    - template: jinja
