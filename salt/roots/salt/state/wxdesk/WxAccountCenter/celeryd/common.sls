{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

include:
  - WxAccountCenter.common

{{ wac.celeryd.install_path }}:
  file.directory:
    - user: {{ wac.user }}
    - group: {{ wac.group }}
    - makedirs: True
    - require:
      - user: {{ wac.user }}_user
      - group: {{ wac.group }}_group

create_env:
  cmd.run:
    - cwd: {{ wac.celeryd.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ wac.celeryd.install_path }}
