{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter_pkg as wac_pkg with context %}

# sync worker conf.
/etc/init/accountcenter-sync-worker.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: accountcenter-sync-worker daemon
        chdir: {{ wac.celeryd.install_path }}
        command: {{ wac.celeryd.install_path}}/env/bin/python -m celery.__main__ -l info -A accountcenter.accounttimedtask worker -Q sync -c 4 --uid={{ wac.user }} --gid={{ wac.group }} --logfile {{ wac.celeryd.log_dir }}/celery-worker-sync.log -n sync-celery
    - user: root
    - group: root
    - mode: '644'