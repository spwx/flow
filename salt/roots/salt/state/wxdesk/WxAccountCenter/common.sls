{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}

{{ wac.group }}_group:
  group.present:
    - name: {{ wac.group }}

{{ wac.user }}_user:
  user.present:
    - name: {{ wac.user }}
    - shell: /bin/bash
    - groups:
      - {{ wac.group }}
    - require:
      - group: {{ wac.group }}_group

/var/run/WxAccountCenter:
  file.directory:
    - user: {{ wac.user }}
    - group: {{ wac.group }}
    - makedirs: True
    - require:
      - user: {{ wac.user }}_user
      - group: {{ wac.group }}_group
