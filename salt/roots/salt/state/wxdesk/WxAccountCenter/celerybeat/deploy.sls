{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter_pkg as wac_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}
{% set celerybeat_redis = 'celerybeat-redis-{0}.tar.gz'.format(python3.deplibs.celerybeat_redis) %}

include:
  - .service

{{ wac.celerybeat.log_dir }}:
  file.directory:
    - user: {{ wac.user }}
    - group: {{ wac.group }}
    - makedirs: True
    - recurse:
      - user
      - group

{{ wac.celerybeat.install_path }}/{{ wac_pkg.package_name }}:
  file.directory:
    - user: {{ wac.user }}
    - group: {{ wac.group }}
    - makedirs: True

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wac_pkg.package_name }}-*

get_wac.celerybeat:
  file.managed:
    - name: {{ src_dir }}/{{ wac_pkg.package_name }}-{{ wac_pkg.version }}.tar.gz
    - source: {{ wac_pkg.pkg_url }}
    {% if wac_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wac_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wac_pkg.package_name }}-{{ wac_pkg.version }}.tar.gz
    - require:
      - file: get_wac.celerybeat

# stop svc_name
stop_celerybeat_service:
  cmd.run:
    - names:
      - initctl stop accountcenter-celerybeat | exit 0
    - require:
      - file: /etc/init/accountcenter-celerybeat.conf
      - file: get_wac.celerybeat

install_celerybeat_redis:
  file.managed:
    - name: {{ src_dir }}/{{ celerybeat_redis }}
    - source: salt://python3/files/{{ celerybeat_redis }}
  cmd.run:
    - name: source {{ wac.celerybeat.install_path }}/env/bin/activate && pip3 install -I {{ src_dir }}/{{
    celerybeat_redis }}
    - require:
      - file: install_celerybeat_redis

install_wac.celerybeat:
  cmd.run:
    - cwd: {{ wac.celerybeat.install_path }}
    - names:
      - (rm -rf {{ wac_pkg.package_name }}/*
        && \cp -rf {{ src_dir }}/{{ wac_pkg.package_name }}-{{ wac_pkg.version }}/* {{ wac_pkg.package_name }}
        && source {{ wac.celerybeat.install_path }}/env/bin/activate
        && pip3 install -r {{ wac_pkg.package_name }}/requirements.txt)
    - require:
      - cmd: get_wac.celerybeat
      - cmd: install_celerybeat_redis
      - cmd: stop_celerybeat_service

{% if wac.get('settings', False) %}
{{ wac.celerybeat.install_path }}/{{ wac_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_celerybeat_service
{% endif %}

start_celerybeat_service:
  cmd.run:
    - names:
      - initctl start accountcenter-celerybeat
      - sleep 5
    {%- if wac.get('settings', False) %}
    - watch:
      - file: {{ wac.celerybeat.install_path }}/{{ wac_pkg.package_name }}/settings.yaml
    {%- endif %}
    - require:
      - file: /etc/init/accountcenter-celerybeat.conf
      - cmd: install_wac.celerybeat

check_service_status:
  cmd.run:
    - names:
      - initctl status accountcenter-celerybeat |grep start
    - require:
      - cmd: start_celerybeat_service
