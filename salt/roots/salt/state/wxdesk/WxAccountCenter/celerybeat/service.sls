{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter_pkg as wac_pkg with context %}

/etc/init/accountcenter-celerybeat.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: accountcenter-celerybeat daemon
        chdir: {{ wac.celerybeat.install_path }}
        command: {{ wac.celerybeat.install_path}}/env/bin/python -m celery.__main__ -l info -A accountcenter.accounttimedtask beat -s /var/run/WxAccountCenter/celerybeat.db --uid={{ wac.user }} --gid={{ wac.group }} --logfile {{ wac.celerybeat.log_dir }}/celerybeat.log --pidfile=/var/run/WxAccountCenter/celerybeat.pid
    - user: root
    - group: root
    - mode: '644'

