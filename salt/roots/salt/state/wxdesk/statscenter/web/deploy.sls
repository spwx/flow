{% from "statscenter/vars.j2" import statscenter as wxp with context %}
{% from "statscenter/vars.j2" import statscenter_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# create package directory
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wxp_pkg.package_name }}-*

# get statscenter_web pkg
get_statscenter_web:
  file.managed:
    - name: {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - source: {{ wxp_pkg.pkg_url }}
    {%- if wxp_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wxp_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - require:
      - file: get_statscenter_web

# install statscenter web
install_statscenter_web:
  cmd.run:
    - cwd: {{ wxp.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}/* {{ wxp_pkg.package_name }}
        && source {{ wxp.web.install_path }}/env/bin/activate
        && pip3 install -r {{ wxp_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_statscenter_web

# install web sertting.yaml
{% if wxp.get('settings', False) %}
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wxp.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{% endif %}


# reload statscenter_web service
start_api_and_web_service:
  cmd.run:
    - names:
      - initctl start statscenter
    - watch:
      - file: /etc/init/statscenter.conf
      {% if wxp.web.get('settings', False) %}
      - file: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/statscenter.conf
      - cmd: install_statscenter_web


start_celeryworker_service:
  cmd.run:
    - names:
      - initctl start statscenter-celeryworker || true
      - kill -HUP `ps -ef|grep /env/| grep -v grep | awk '{print $2 }'`;

    - watch:
      - file: /etc/init/statscenter-celeryworker.conf
      {% if wxp.web.get('settings', False) %}
      - file: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/statscenter-celeryworker.conf
      - cmd: install_statscenter_web

