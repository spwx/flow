{% from "statscenter/vars.j2" import statscenter as wxp with context %}
{% from "statscenter/vars.j2" import statscenter_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# stop statscenter_web service
stop_api_and_web_service:
  cmd.run:
    - names:
      - initctl stop statscenter-celerybeat | exit 0

# reload statscenter_clerybeat service
start_celerybeat_service:
  cmd.run:
    - names:
      -  initctl start statscenter-celerybeat
    - watch:
      - file: /etc/init/statscenter-celerybeat.conf
      {% if wxp.web.get('settings', False) %}
      - file: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml
      {% endif %}
