{% from "statscenter/vars.j2" import statscenter as wxp with context %}
{% from "statscenter/vars.j2" import statscenter_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/init/statscenter.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: policy-api daemon
        chdir: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/gunicorn app:app  -c {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/gunicorn_config.py
    - user: root
    - group: root
    - mode: '644'


/etc/init/statscenter-celerybeat.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: statscenter-celerybeat daemon
        chdir: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/celery -A servers.schedule.celery beat
    - user: root
    - group: root
    - mode: '644'

/etc/init/statscenter-celeryworker.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: statscenter-celeryworker daemon
        chdir: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/celery worker -A servers.schedule.celery -l info -f logs/worker.log
    - user: root
    - group: root
    - mode: '644'
