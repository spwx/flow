{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza_web_pkg as celeryd_pkg with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}
{% set celerybeat_redis = 'celerybeat-redis-{0}.tar.gz'.format(python3.deplibs.celerybeat_redis) %}

include:
  - .service

{{ wgp.celeryd.log_dir }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
    - recurse:
      - user
      - group

{{ wgp.celeryd.install_path }}/{{ celeryd_pkg.package_name }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True

# create backup directory
{{ wgp.celeryd.install_path }}/backup:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ celeryd_pkg.package_name }}-*

get_{{ celeryd_pkg.package_name }}:
  file.managed:
    - name: {{ src_dir }}/{{ celeryd_pkg.package_name }}-{{ celeryd_pkg.version }}.tar.gz
    - source: {{ celeryd_pkg.pkg_url }}
    {% if celeryd_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ celeryd_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ celeryd_pkg.package_name }}-{{ celeryd_pkg.version }}.tar.gz
    - require:
      - file: get_{{ celeryd_pkg.package_name }}

# stop svc_name
stop_{{ wgp.celeryd.svc_name }}_service:
  cmd.run:
    - name: initctl stop {{ wgp.celeryd.svc_name }} | exit 0
    - require:
      - file: /etc/init/{{ wgp.celeryd.svc_name }}.conf
      - file: get_{{ celeryd_pkg.package_name }}

# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ wgp.celeryd.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ celeryd_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ celeryd_pkg.package_name }}/logs
    - require:
      - cmd: stop_{{ wgp.celeryd.svc_name }}_service

install_celerybeat_redis:
  file.managed:
    - name: {{ src_dir }}/{{ celerybeat_redis }}
    - source: salt://python3/files/{{ celerybeat_redis }}
  cmd.run:
    - name: source {{ wgp.celeryd.install_path }}/env/bin/activate && pip3 install -I {{ src_dir }}/{{ celerybeat_redis }}
    - require:
      - file: install_celerybeat_redis

install_celeryd:
  cmd.run:
    - cwd: {{ wgp.celeryd.install_path }}
    - names:
      - (rm -rf {{ celeryd_pkg.package_name }}/*
        && \cp -rf {{ src_dir }}/{{ celeryd_pkg.package_name }}-{{ celeryd_pkg.version }}/* {{ celeryd_pkg.package_name }}
        && source {{ wgp.celeryd.install_path }}/env/bin/activate
        && pip3 install -r {{ celeryd_pkg.package_name }}/{{ deskcommon.path }}/requirements.txt
        && pip3 install -r {{ celeryd_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_{{ celeryd_pkg.package_name }}
      - cmd: install_celerybeat_redis
      - cmd: stop_{{ wgp.celeryd.svc_name }}_service

{% if wgp.web.get('settings', False) %}
{{ wgp.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/settings.yaml:
  file.serialize:
    - name:
    - dataset: {{ wgp.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ wgp.celeryd.svc_name }}_service
{% endif %}

# set deskcomponent settings.yaml
{% if deskcommon.get('settings', False) %}
{{ wgp.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ wgp.celeryd.svc_name }}_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wgp.celeryd.install_path, celeryd_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ wgp.celeryd.install_path }}
    - name: {{ wgp.celeryd.install_path }}/env/bin/pip3 install -r {{ celeryd_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_celeryd
{{ wgp.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ wgp.celeryd.svc_name }}_service
{%- endif %}

disable_uwsgi:
  file.replace:
    - name: {{ wgp.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/settings.yaml
    - pattern: |
        ENABLE_UWSGI: .*
    - repl: |
        ENABLE_UWSGI: False
    - append_if_not_found: True
    - backup: True
    - require:
      - cmd: stop_{{ wgp.celeryd.svc_name }}_service

restore_log_dir:
  cmd.run:
    - cwd: {{ wgp.celeryd.install_path }}
    - names:
      - (rm -rf {{ celeryd_pkg.package_name }}/logs
        &&mv -f backup/logs {{ celeryd_pkg.package_name }} )
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ wgp.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/logs:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir
      - cmd: stop_{{ wgp.celeryd.svc_name }}_service

# start svc_name
start_{{ wgp.celeryd.svc_name }}_service:
  cmd.run:
    - name: initctl start {{ wgp.celeryd.svc_name }}
    - require:
      - file: /etc/init/{{ wgp.celeryd.svc_name }}.conf
      - cmd: install_celeryd
    {% if wgp.web.get('settings', False) %}
      - file: {{ wgp.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/settings.yaml
    {% endif %}
