{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/{{ wgp.celeryd.svc_name }}.conf:
  file.managed:
    - source: salt://WxGamePlaza/celeryd/files/logrotate.conf.j2
    - template: jinja
