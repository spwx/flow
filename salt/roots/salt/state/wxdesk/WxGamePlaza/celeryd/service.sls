{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza_web_pkg as wgp_web_pkg with context %}

{% set celery = wgp.celeryd %}

/etc/init/{{ wgp.celeryd.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: celery worker daemon
        chdir: {{ celery.install_path }}/{{ wgp_web_pkg.package_name }}
        command: {{ celery.install_path }}/env/bin/python -m celery.__main__ -l info -A gameplaza worker --time-limit=300 -c {{ celery.concurrency }} -Q {{ celery.queue }} --uid={{ wgp.user }} --gid={{ wgp.group }} --logfile {{ celery.log_dir }}/{{ celery.svc_name }}.log
    - user: root
    - group: root
    - mode: '644'
