{% from "python3/vars.j2" import python3 with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza_handler_pkg as handler_pkg with context %}

{% set handler = wgp.handler %}

/etc/init/{{ handler.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: huangqiyin
        description: handler daemon
        chdir: {{ handler.install_path }}/{{ handler_pkg.package_name }}
        command: {{ handler.install_path }}/env/bin/gunicorn main:app -c {{ handler.install_path }}/{{ handler_pkg.package_name }}/gunicorn_config.py
    - user: root
    - group: root
    - mode: '644'
