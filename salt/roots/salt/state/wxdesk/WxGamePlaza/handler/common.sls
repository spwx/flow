{% from "python3/vars.j2" import python3 with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% set handler = wgp.handler %}

include:
  - WxGamePlaza.common

{{ handler.install_path }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
    - require:
      - user: {{ wgp.user }}_user
      - group: {{ wgp.group }}_group

create_env:
  cmd.run:
    - cwd: {{ handler.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ handler.install_path }}

