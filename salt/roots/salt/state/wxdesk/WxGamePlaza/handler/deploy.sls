{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza_handler_pkg as handler_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% set handler = wgp.handler %}

{% set src_dir = '/usr/local/src' %}

include:
  - .service

{{ handler.log_dir }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
    - recurse:
      - user
      - group


{{ handler.install_path }}/{{ handler_pkg.package_name }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
# create backup directory
{{ handler.install_path }}/backup:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ handler_pkg.package_name }}-*

get_gameplaza_handler:
  file.managed:
    - name: {{ src_dir }}/{{ handler_pkg.package_name }}-{{ handler_pkg.version }}.tar.gz
    - source: {{ handler_pkg.pkg_url }}
    {% if handler_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ handler_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ handler_pkg.package_name }}-{{ handler_pkg.version }}.tar.gz
    - require:
      - file: get_gameplaza_handler

# stop svc_name
stop_{{ handler.svc_name }}_service:
  cmd.run:
    - name: initctl stop {{ handler.svc_name }} | exit 0
    - require:
      - file: /etc/init/{{ handler.svc_name }}.conf
      - file: get_gameplaza_handler

# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ handler.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ handler_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ handler_pkg.package_name }}/logs
    - require:
      - cmd: stop_{{ handler.svc_name }}_service

install_gameplaza_handler:
  cmd.run:
    - cwd: {{ handler.install_path }}
    - names:
      - (rm -rf {{ handler_pkg.package_name }}/*
        && \cp -rf {{ src_dir }}/{{ handler_pkg.package_name }}-{{ handler_pkg.version }}/* {{ handler_pkg.package_name }}
        && source {{ handler.install_path }}/env/bin/activate
        && pip3 install -r {{ handler_pkg.package_name }}/{{ deskcommon.path }}/requirements.txt
        && pip3 install -r {{ handler_pkg.package_name }}/requirements.txt)
    - require:
      - cmd: get_gameplaza_handler
      - cmd: stop_{{ handler.svc_name }}_service

{% if handler.get('settings', False) %}
{{ handler.install_path }}/{{ handler_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ handler.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ handler.svc_name }}_service
{% endif %}

# set deskcomponent settings.yaml
{% if deskcommon.get('settings', False) %}
{{ handler.install_path }}/{{ handler_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ handler.svc_name }}_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(handler.install_path, handler_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ handler.install_path }}
    - name: {{ handler.install_path }}/env/bin/pip3 install -r {{ handler_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_gameplaza_handler
{{ handler.install_path }}/{{ handler_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ handler.svc_name }}_service
{%- endif %}

restore_log_dir:
  cmd.run:
    - cwd: {{ handler.install_path }}
    - names:
      - (rm -rf {{ handler_pkg.package_name }}/logs
        && mv -f backup/logs {{ handler_pkg.package_name }} )
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ handler.install_path }}/{{ handler_pkg.package_name }}/logs:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir
      - cmd: stop_{{ handler.svc_name }}_service

# start svc_name
start_{{ handler.svc_name }}_service:
  cmd.run:
    - name: initctl start {{ handler.svc_name }}
{% if handler.get('settings', False) %}
    - watch:
      - file: {{ handler.install_path }}/{{ handler_pkg.package_name }}/settings.yaml
{% endif %}
    - require:
      - file: /etc/init/{{ handler.svc_name }}.conf
      - cmd: install_gameplaza_handler
