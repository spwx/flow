{% from "log_artifact/vars.j2" import log_artifact with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}

include:
  - log_artifact

# mkdir logrotate/logs
# mkdir scripts
{{ wgp.handler.install_path }}/scripts:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{{ wgp.handler.install_path }}/logrotate/logs:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

# wgp.handler.conf to logrotate
# artifact_log.sh to script
{% if log_artifact.get('everydaylogs', false) %}
{{ wgp.handler.install_path }}/scripts/artifact_log.sh:
  file.managed:
    - source: salt://log_artifact/files/artifact_log.sh
    - template: jinja
    - defaults:
        everydaylogs: {{ log_artifact.get('everydaylogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root
    - require:
      - file: {{ wgp.handler.install_path }}/scripts

{{ wgp.handler.install_path }}/logrotate/{{ wgp.handler.svc_name }}.conf:
  file.managed:
    - source: salt://WxGamePlaza/handler/files/logrotate.conf.j2
    - template: jinja
    - backup: True
    - user: root
    - group: root
    - require:
      - file: {{ wgp.handler.install_path }}/logrotate/logs
      - file: {{ wgp.handler.install_path }}/scripts/artifact_log.sh

{{ wgp.handler.svc_name }}_logrotate:
  cron.present:
    - name: /usr/sbin/logrotate -f {{ wgp.handler.install_path }}/logrotate/{{ wgp.handler.svc_name }}.conf
    - minute: 0
    - hour: '*/2'

{% endif %}
