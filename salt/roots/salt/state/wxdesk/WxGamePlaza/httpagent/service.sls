{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% set handler = wgp.handler %}

/etc/init/{{ httpagent.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: {{ handler.svc_name }} daemon
        chdir: {{ handler.install_path }}
        command: {{ handler.install_path }}/HTTPAgent
    - user: root
    - group: root
    - mode: '644'
