{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% set handler = wgp.handler %}

{% set host = '127.0.0.1' %}
{% set port = handler.app_conf.httpport %}
{% set username = handler.user_infos[0].username %}
{% set password = handler.user_infos[0].password %}

/tmp/wait_queue_size.py:
  file.managed:
    - source: salt://WxGamePlaza/handler/files/wait_queue_size.py
    - defaults:
        host: {{ host }}
        port: {{ port }}
        username: {{ username }}
        password: {{ password }}
    - user: root
    - group: root
    - mode: '0755'
  cmd.run:
    - cwd: /tmp
    - name: /usr/bin/python wait_queue_size.py --host {{ host }} --port {{ port }} --username {{ username }} --password {{ password }}
