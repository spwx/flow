{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza_httpagent_pkg as httpagent_pkg with context %}
{% set handler = wgp.handler %}

{% set src_dir = '/usr/local/src' %}

include:
  - WxGamePlaza.handler.service

get_httpagent:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ httpagent_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ httpagent_pkg.package_name }}-{{ httpagent_pkg.version }}.tar.gz
    - source: {{ httpagent_pkg.pkg_url }}
    {% if httpagent_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ httpagent_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_httpagent

# stop svc_name
stop_{{ httpagent.svc_name }}_service:
  cmd.run:
    - name: initctl stop {{ handler.svc_name }} | exit 0
    - require:
      - file: /etc/init/{{ handler.svc_name }}.conf
      - file: get_httpagent

install_httpagent:
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (rm -rf {{ handler.install_path }}/*
        && tar -xzf {{ httpagent_pkg.package_name }}-{{ httpagent_pkg.version }}.tar.gz -C {{ handler.install_path }})
    - require:
      - file: get_httpagent
      - cmd: stop_{{ handler.svc_name }}_service

{{ handler.install_path }}/conf/app.conf:
  file.managed:
    - source: salt://WxGamePlaza/handler/files/conf/app.conf.j2
    - template: jinja
    - backup: True
    - require:
      - cmd: stop_{{ handler.svc_name }}_service

{{ handler.install_path }}/conf/handler.conf:
  file.managed:
    - source: salt://WxGamePlaza/handler/files/conf/handler.conf.j2
    - template: jinja
    - backup: True
    - require:
      - cmd: stop_{{ handler.svc_name }}_service

{% for user_info in handler.user_infos %}
passwd_{{ user_info.username }}:
  cmd.run:
    - name: htpasswd -cbm {{ handler.install_path }}/conf/pwd.file {{ user_info.username }} {{ user_info.password }}
    - require:
      - cmd: stop_{{ handler.svc_name }}_service
{% endfor %}

{{ handler.install_path }}/logs:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
    - require:
      - cmd: stop_{{ handler.svc_name }}_service

# start svc_name
start_{{ httpagent.svc_name }}_service:
  cmd.run:
    - name: initctl start {{ handler.svc_name }}
    - require:
      - file: /etc/init/{{ handler.svc_name }}.conf
      - cmd: install_httpagent
      - file: {{ handler.install_path }}/conf/app.conf
      - file: {{ handler.install_path }}/conf/handler.conf
