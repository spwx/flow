#!/usr/bin/env python
# coding: utf-8
# author: seanly@aliyun.com

from base64 import b64encode
import httplib
import json


class AgentException(Exception):
    pass


def get_queue_size(host, port, username, password):

    try:
        conn = httplib.HTTPConnection(host=host, port=port)

        user_info = b64encode(b"%s:%s" % (username, password)).decode("ascii")
        headers = {'Authorization': 'Basic %s' % user_info}
        conn.request('GET', '/funny?cmd=get_queue_size', headers=headers)
        r1 = conn.getresponse()
        status = r1.status
        if status == 401:
            raise AgentException('Authentication denied')

        if status == 500:
            raise AgentException('Server error.')
    except Exception:
        raise AgentException('Unkown error.')

    return r1.read()


def main():

    try:
        host = '{{ host }}'
        port = int('{{ port }}')
        username = '{{ username }}'
        password = '{{ password }}'
        result = get_queue_size(host, port, username, password)
        ret_json = json.loads(result)

        if 'queue_size' in ret_json:
            queue_size = int(ret_json['queue_size'])
            while queue_size > 0:
                result = get_queue_size(host, port, username, password)
                ret_json = json.loads(result)
                queue_size = int(ret_json['queue_size'])

    except Exception as e:
        print(e)
        exit(0)


if __name__ == '__main__':
    main()
