{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% set handler = wgp.handler %}

include:
  - WxGamePlaza.common

{{ handler.install_path }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
    - require:
      - user: {{ wgp.user }}_user
      - group: {{ wgp.group }}_group

httpd-tools:
  pkg.installed