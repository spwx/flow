{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "python3/vars.j2" import python3 with context %}

include:
  - WxGamePlaza.common

{{ wgp.web.install_path }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
    - require:
      - user: {{ wgp.user }}_user
      - group: {{ wgp.group }}_group

create_env:
  cmd.run:
    - cwd: {{ wgp.web.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ wgp.web.install_path }}

