{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/wxgameplaza-uwsgi.conf:
  file.managed:
    - source: salt://WxGamePlaza/web/files/uwsgi.logrotate.j2
    - template: jinja
