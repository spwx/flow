{% from "log_artifact/vars.j2" import log_artifact with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}

include:
  - log_artifact

# mkdir logrotate/logs
# mkdir scripts
{{ wgp.web.install_path }}/scripts:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{{ wgp.web.install_path }}/logrotate/logs:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

# wgp.web.conf to logrotate
# artifact_log.sh to script
{% if log_artifact.get('everydaylogs', false) %}
{{ wgp.web.install_path }}/scripts/artifact_log.sh:
  file.managed:
    - source: salt://log_artifact/files/artifact_log.sh
    - template: jinja
    - defaults:
        everydaylogs: {{ log_artifact.get('everydaylogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root
    - require:
      - file: {{ wgp.web.install_path }}/scripts

{{ wgp.web.install_path }}/logrotate/{{ wgp.web.svc_name }}.conf:
  file.managed:
    - source: salt://WxGamePlaza/web/files/logrotate.conf.j2
    - template: jinja
    - backup: True
    - user: root
    - group: root
    - require:
      - file: {{ wgp.web.install_path }}/logrotate/logs
      - file: {{ wgp.web.install_path }}/scripts/artifact_log.sh

{{ wgp.web.svc_name }}_logrotate:
  cron.present:
    - name: /usr/sbin/logrotate -f {{ wgp.web.install_path }}/logrotate/{{ wgp.web.svc_name }}.conf
    - minute: 0
    - hour: '*/2'

{% endif %}
