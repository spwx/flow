{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza_web_pkg as wgp_web_pkg with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}
{% set celerybeat_redis = 'celerybeat-redis-{0}.tar.gz'.format(python3.deplibs.celerybeat_redis) %}

# load config file
include:
  - .service

{{ wgp.web.log_dir }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
    - recurse:
      - user
      - group

# create package directory
{{ wgp.web.install_path }}/{{ wgp_web_pkg.package_name }}:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True

# create backup directory
{{ wgp.web.install_path }}/backup:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wgp_web_pkg.package_name }}-*

# get WxGamePlaza_web pkg
get_WxGamePlaza_web:
  file.managed:
    - name: {{ src_dir }}/{{ wgp_web_pkg.package_name }}-{{ wgp_web_pkg.version }}.tar.gz
    - source: {{ wgp_web_pkg.pkg_url }}
    {% if wgp_web_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wgp_web_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wgp_web_pkg.package_name }}-{{ wgp_web_pkg.version }}.tar.gz
    - require:
      - file: get_WxGamePlaza_web

# stop WxGamePlaza_web service
stop_{{ wgp.web.svc_name }}_service:
  cmd.run:
    - name: initctl stop {{ wgp.web.svc_name }} | exit 0
    - require:
      - file: /etc/init/{{ wgp.web.svc_name }}.conf
      - file: get_WxGamePlaza_web

# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ wgp.web.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ wgp_web_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ wgp_web_pkg.package_name }}/logs
    - require:
      - cmd: stop_{{ wgp.web.svc_name }}_service

install_celerybeat_redis:
  file.managed:
    - name: {{ src_dir }}/{{ celerybeat_redis }}
    - source: salt://python3/files/{{ celerybeat_redis }}
  cmd.run:
    - name: source {{ wgp.web.install_path }}/env/bin/activate && pip3 install -I {{ src_dir }}/{{ celerybeat_redis }}
    - require:
      - file: install_celerybeat_redis

# install WxGamePlaza web
install_WxGamePlaza_web:
  cmd.run:
    - cwd: {{ wgp.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wgp_web_pkg.package_name }}-{{ wgp_web_pkg.version }}/* {{ wgp_web_pkg.package_name }}
        && source {{ wgp.web.install_path }}/env/bin/activate
        && pip3 install -r {{ wgp_web_pkg.package_name }}/{{ deskcommon.path }}/requirements.txt
        && pip3 install -r {{ wgp_web_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_WxGamePlaza_web
      - cmd: install_celerybeat_redis
      - cmd: stop_{{ wgp.web.svc_name }}_service

{% if wgp.web.get('settings', False) %}
{{ wgp.web.install_path }}/{{ wgp_web_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wgp.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ wgp.web.svc_name }}_service
{% endif %}

# set deskcomponent settings.yaml
{% if deskcommon.get('settings', False) %}
{{ wgp.web.install_path }}/{{ wgp_web_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ wgp.web.svc_name }}_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wgp.web.install_path, wgp_web_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ wgp.web.install_path }}
    - name: {{ wgp.web.install_path }}/env/bin/pip3 install -r {{ wgp_web_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_WxGamePlaza_web
{{ wgp.web.install_path }}/{{ wgp_web_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ wgp.web.svc_name }}_service
{%- endif %}

restore_log_dir:
  cmd.run:
    - cwd: {{ wgp.web.install_path }}
    - names:
      - (rm -rf {{ wgp_web_pkg.package_name }}/logs
        && mv -f backup/logs {{ wgp_web_pkg.package_name }})
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ wgp.web.install_path }}/{{ wgp_web_pkg.package_name }}/logs:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir
      - cmd: stop_{{ wgp.web.svc_name }}_service

# reload WxGamePlaza_web service
start_{{ wgp.web.svc_name }}_service:
  cmd.run:
    - name: initctl start {{ wgp.web.svc_name }}
    - require:
      - file: /etc/init/{{ wgp.web.svc_name }}.conf
      - cmd: install_WxGamePlaza_web
      {% if wgp.web.get('settings', False) %}
      - file: {{ wgp.web.install_path }}/{{ wgp_web_pkg.package_name }}/settings.yaml
      {% endif %}
      {% if deskcommon.get('settings', False) %}
      - file: {{ wgp.web.install_path }}/{{ wgp_web_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml
      {% endif %}
