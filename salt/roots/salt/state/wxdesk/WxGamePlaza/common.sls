{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}

{{ wgp.group }}_group:
  group.present:
    - name: {{ wgp.group }}

{{ wgp.user }}_user:
  user.present:
    - name: {{ wgp.user }}
    - shell: /bin/bash
    - groups:
      - {{ wgp.group }}
    - require:
      - group: {{ wgp.group }}_group

/var/run/WxGamePlaza:
  file.directory:
    - user: {{ wgp.user }}
    - group: {{ wgp.group }}
    - makedirs: True
    - require:
      - user: {{ wgp.user }}_user
      - group: {{ wgp.group }}_group
