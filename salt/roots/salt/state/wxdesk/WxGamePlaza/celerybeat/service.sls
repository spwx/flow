{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza_web_pkg as wgp_web_pkg with context %}

{% set celerybeat = wgp.celerybeat %}

/etc/init/{{ celerybeat.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: celerybeat daemon
        chdir: {{ celerybeat.install_path }}/{{ wgp_web_pkg.package_name }}
        command: {{ celerybeat.install_path }}/env/bin/python -m celery.__main__ -l info -A gameplaza beat -s /var/run/WxGamePlaza/{{ celerybeat.svc_name }}.db -S celerybeatredis.schedulers.RedisScheduler --uid={{ wgp.user }} --gid={{ wgp.group }} --logfile {{ celerybeat.log_dir }}/{{ celerybeat.svc_name }}.log
    - user: root
    - group: root
    - mode: '644'
