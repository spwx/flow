{% from "WxGamePlaza/vars.j2" import wxgameplaza as wgp with context %}
{% from "WxGamePlaza/vars.j2" import wxgameplaza_frontend_pkg as wgp_frontend_pkg with context %}

{% set src_dir = '/usr/local/src' %}

get_wxgameplaza_frontend_pkg:
  cmd.run:
    - cwd: {{ src_dir }}
      name: rm -rf {{ wgp_frontend_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ wgp_frontend_pkg.package_name }}-{{ wgp_frontend_pkg.version }}.tar.gz
    - source: {{  wgp_frontend_pkg.pkg_url }}
    {% if wgp_frontend_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wgp_frontend_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_wxgameplaza_frontend_pkg

install_wxgameplaza_frontend_pkg:
  file.directory:
    - name: {{ wgp.frontend.install_path }}
    - makedirs: True
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (tar -xzf {{ wgp_frontend_pkg.package_name }}-{{ wgp_frontend_pkg.version }}.tar.gz -C {{ wgp.frontend.install_path }})
    - require:
      - file: install_wxgameplaza_frontend_pkg
      - file: get_wxgameplaza_frontend_pkg