{% from "usercenter/vars.j2" import usercenter as wxp with context %}

{{ wxp.group }}_group:
  group.present:
    - name: {{ wxp.group }}

{{ wxp.user }}_user:
  user.present:
    - name: {{ wxp.user }}
    - shell: /bin/bash
    - groups:
      - {{ wxp.group }}
    - require:
      - group: {{ wxp.group }}_group

/var/run/goodscenter:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: Ture
    - require:
      - user: {{ wxp.user }}_user
      - group: {{ wxp.group }}_group