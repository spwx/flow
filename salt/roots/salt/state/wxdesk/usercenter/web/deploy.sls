{% from "usercenter/vars.j2" import usercenter as wxp with context %}
{% from "usercenter/vars.j2" import usercenter_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# create package directory
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wxp_pkg.package_name }}-*

# get usercenter_web pkg
get_usercenter_web:
  file.managed:
    - name: {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - source: {{ wxp_pkg.pkg_url }}
    {%- if wxp_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wxp_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - require:
      - file: get_usercenter_web

# install usercenter web
install_usercenter_web:
  cmd.run:
    - cwd: {{ wxp.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}/* {{ wxp_pkg.package_name }}
        && source {{ wxp.web.install_path }}/env/bin/activate
        && pip3 install -r {{ wxp_pkg.package_name }}/{{ deskcommon.path }}/requirements.txt
        && pip3 install -r {{ wxp_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_usercenter_web

# install web sertting.yaml
{% if wxp.get('settings', False) %}
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wxp.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{% endif %}



# set deskcomponent settings.yaml

{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/deskcomponent/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True


{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wxp.web.install_path, wxp_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ wxp.web.install_path }}
    - name: {{ wxp.web.install_path }}/env/bin/pip3 install -r {{ wxp_pkg.package_name }}/accountcenter/requirements.txt
{{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{%- endif %}


# reload usercenter_web service
start_api_and_web_service:
  cmd.run:
    - names:
      - initctl start usercenter || true
      - kill -HUP `ps -ef|grep /env/| grep -v grep | awk '{print $2 }'`;
    - watch:
      - file: /etc/init/usercenter.conf
      {% if wxp.web.get('settings', False) %}
      - file: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/usercenter.conf
      - cmd: install_usercenter_web

