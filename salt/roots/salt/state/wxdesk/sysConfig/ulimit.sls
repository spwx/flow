/etc/security/limits.d/91-wxdesk.conf:
  file.managed:
    - source: salt://sysConfig/files/91-wxdesk.conf.j2
    - template: jinja
    - backup: True

/etc/security/limits.d/90-nproc.conf:
  file.managed:
    - source: salt://sysConfig/files/90-nproc.conf.j2
    - template: jinja
    - backup: True
