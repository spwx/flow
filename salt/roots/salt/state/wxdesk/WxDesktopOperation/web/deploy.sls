{% from "WxDesktopOperation/vars.j2" import WxDesktopOperation as wbs with context %}
{% from "WxDesktopOperation/vars.j2" import WxDesktopOperation_pkg as wbs_pkg with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

{{ wbs.web.log_dir }}:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True
    - recurse:
      - user
      - group

# create package directory
{{ wbs.web.install_path }}/{{ wbs_pkg.package_name }}:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True

# create backup directory
{{ wbs.web.install_path }}/backup:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wbs_pkg.package_name }}-*

# get WxDesktopOperation_web pkg
get_WxDesktopOperation_web:
  file.managed:
    - name: {{ src_dir }}/{{ wbs_pkg.package_name }}-{{ wbs_pkg.version }}.tar.gz
    - source: {{ wbs_pkg.pkg_url }}
    {% if wbs_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wbs_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - ( tar -xzf {{ wbs_pkg.package_name }}-{{ wbs_pkg.version }}.tar.gz -C ./deskplatform & cd ./deskplatform && python config_util.py )
    - require:
      - file: get_WxDesktopOperation_web


# install WxDesktopOperation web
install_WxDesktopOperation_web:
  cmd.run:
    - cwd: {{ wbs.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/deskplatform/*  . && python config_util.py -e {{ wbs_pkg.test }})

    - require:
      - file: get_WxDesktopOperation_web

