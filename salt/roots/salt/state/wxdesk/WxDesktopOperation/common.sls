{% from "WxDesktopOperation/vars.j2" import WxDesktopOperation as wbs with context %}

{{ wbs.group }}_group:
  group.present:
    - name: {{ wbs.group }}

{{ wbs.user }}_user:
  user.present:
    - name: {{ wbs.user }}
    - shell: /bin/bash
    - groups:
      - {{ wbs.group }}
    - require:
      - group: {{ wbs.group }}_group

/var/run/WxDesktopOperation:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True
    - require:
      - user: {{ wbs.user }}_user
      - group: {{ wbs.group }}_group
