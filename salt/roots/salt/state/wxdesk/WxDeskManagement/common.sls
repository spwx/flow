{% from "WxDeskManagement/vars.j2" import WxDeskManagement as wdm with context %}
{% from "python3/vars.j2" import python3 with context %}

{{ wdm.group }}_group:
  group.present:
    - name: {{ wdm.group }}

{{ wdm.user }}_user:
  user.present:
    - name: {{ wdm.user }}
    - shell: /bin/bash
    - groups:
      - {{ wdm.group }}
    - require:
      - group: {{ wdm.group }}_group

/var/run/WxDeskManagement:
  file.directory:
    - user: {{ wdm.user }}
    - group: {{ wdm.group }}
    - makedirs: True
    - require:
      - user: {{ wdm.user }}_user
      - group: {{ wdm.group }}_group
