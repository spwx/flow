{% from "WxDeskManagement/vars.j2" import WxDeskManagement as wdm with context %}
{% from "python3/vars.j2" import python3 with context %}

include:
  - WxDeskManagement.common

{{ wdm.web.install_path }}:
  file.directory:
    - user: {{ wdm.user }}
    - group: {{ wdm.group }}
    - makedirs: True
    - require:
      - user: {{ wdm.user }}_user
      - group: {{ wdm.group }}_group

create_env:
  cmd.run:
    - cwd: {{ wdm.web.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ wdm.web.install_path }}
