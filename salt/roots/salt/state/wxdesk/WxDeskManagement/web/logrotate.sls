{% from "WxDeskManagement/vars.j2" import WxDeskManagement as wdm with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/deskmanagement-uwsgi.conf:
  file.managed:
    - source: salt://WxDeskManagement/web/files/uwsgi.logrotate.j2
    - template: jinja
