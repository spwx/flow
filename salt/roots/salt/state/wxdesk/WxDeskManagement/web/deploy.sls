{% from "WxDeskManagement/vars.j2" import WxDeskManagement as wdm with context %}
{% from "WxDeskManagement/vars.j2" import WxDeskManagement_pkg as wdm_pkg with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

{{ wdm.web.log_dir }}:
  file.directory:
    - user: {{ wdm.user }}
    - group: {{ wdm.group }}
    - makedirs: True
    - recurse:
      - user
      - group

# create package directory
{{ wdm.web.install_path }}/{{ wdm_pkg.package_name }}:
  file.directory:
    - user: {{ wdm.user }}
    - group: {{ wdm.group }}
    - makedirs: True

# create backup directory
{{ wdm.web.install_path }}/backup:
  file.directory:
    - user: {{ wdm.user }}
    - group: {{ wdm.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wdm_pkg.package_name }}-*

# get WxDeskManagement_web pkg
get_WxDeskManagement_web:
  file.managed:
    - name: {{ src_dir }}/{{ wdm_pkg.package_name }}-{{ wdm_pkg.version }}.tar.gz
    - source: {{ wdm_pkg.pkg_url }}
    {% if wdm_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wdm_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wdm_pkg.package_name }}-{{ wdm_pkg.version }}.tar.gz
    - require:
      - file: get_WxDeskManagement_web


# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ wdm.web.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ wdm_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ wdm_pkg.package_name }}/logs

# install WxDeskManagement web
install_WxDeskManagement_web:
  cmd.run:
    - cwd: {{ wdm.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wdm_pkg.package_name }}-{{ wdm_pkg.version }}/* {{ wdm_pkg.package_name }}
        && source {{ wdm.web.install_path }}/env/bin/activate
        && pip3 install -r {{   wdm_pkg.package_name }}/{{ deskcommon.path }}/requirements.txt
        && pip3 install -r {{ wdm_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_WxDeskManagement_web


{% if wdm.web.get('settings', False) %}
{{ wdm.web.install_path }}/{{ wdm_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wdm.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True

{% endif %}

# set deskcomponent settings.yaml
{% if deskcommon.get('settings', False) %}
{{ wdm.web.install_path }}/{{ wdm_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True

{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wdm.web.install_path, wdm_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ wdm.web.install_path }}
    - name: {{ wdm.web.install_path }}/env/bin/pip3 install -r {{ wdm_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_WxDeskManagement_web
{{ wdm.web.install_path }}/{{ wdm_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True

{%- endif %}

restore_log_dir:
  cmd.run:
    - cwd: {{ wdm.web.install_path }}
    - names:
      - (rm -rf {{ wdm_pkg.package_name }}/logs
        && mv -f backup/logs {{ wdm_pkg.package_name }})
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ wdm.web.install_path }}/{{ wdm_pkg.package_name }}/logs:
  file.directory:
    - user: {{ wdm.user }}
    - group: {{ wdm.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir

# reload WxDeskManagement_Web service
start_{{ wdm.web.svc_name }}_service:
  cmd.run:
    - names: 
      - initctl start {{ wdm.web.svc_name }} || true
      - kill -HUP `ps -ef|grep /env/| grep -v grep | awk '{print $2 }'`;
    - require:
      - file: /etc/init/{{ wdm.web.svc_name }}.conf
      - cmd: install_WxDeskManagement_web
      {% if wdm.web.get('settings', False) %}
      - file: {{ wdm.web.install_path }}/{{ wdm_pkg.package_name }}/settings.yaml
      {% endif %}
