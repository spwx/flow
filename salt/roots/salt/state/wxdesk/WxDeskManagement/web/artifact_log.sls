{% from "log_artifact/vars.j2" import log_artifact with context %}
{% from "WxDeskManagement/vars.j2" import WxDeskManagement as wdm with context %}

include:
  - log_artifact

# mkdir logrotate/logs
# mkdir scripts
{{ wdm.web.install_path }}/scripts:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{{ wdm.web.install_path }}/logrotate/logs:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

# wdm.web.conf to logrotate
# artifact_log.sh to script
{% if log_artifact.get('everydaylogs', false) %}
{{ wdm.web.install_path }}/scripts/artifact_log.sh:
  file.managed:
    - source: salt://log_artifact/files/artifact_log.sh
    - template: jinja
    - defaults:
        everydaylogs: {{ log_artifact.get('everydaylogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root
    - require:
      - file: {{ wdm.web.install_path }}/scripts

{{ wdm.web.install_path }}/logrotate/{{ wdm.web.svc_name }}.conf:
  file.managed:
    - source: salt://WxDeskManagement/web/files/logrotate.conf.j2
    - template: jinja
    - backup: True
    - user: root
    - group: root
    - require:
      - file: {{ wdm.web.install_path }}/logrotate/logs
      - file: {{ wdm.web.install_path }}/scripts/artifact_log.sh

{{ wdm.web.svc_name }}_logrotate:
  cron.present:
    - name: /usr/sbin/logrotate -f {{ wdm.web.install_path }}/logrotate/{{ wdm.web.svc_name }}.conf
    - minute: 0
    - hour: '*/2'

{% endif %}
