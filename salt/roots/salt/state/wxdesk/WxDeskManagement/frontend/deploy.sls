{% from "WxDeskManagement/vars.j2" import WxDeskManagement as wdm with context %}
{% from "WxDeskManagement/vars.j2" import WxDeskManagement_pkg as wdm_frontend_pkg with context %}

{% set src_dir = '/usr/local/src' %}

get_WxDeskManagement_frontend_pkg:
  cmd.run:
    - cwd: {{ src_dir }}
      name: rm -rf {{ wdm_frontend_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ wdm_frontend_pkg.package_name }}-{{ wdm_frontend_pkg.version }}.tar.gz
    - source: {{ wdm_frontend_pkg.pkg_url }}
    {% if wdm_frontend_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wdm_frontend_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_WxDeskManagement_frontend_pkg

install_WxDeskManagement_frontend_pkg:
  file.directory:
    - name: {{ wdm.frontend.install_path }}
    - makedirs: True
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (tar -xzf {{ wdm_frontend_pkg.package_name }}-{{ wdm_frontend_pkg.version }}.tar.gz
        && \cp -Rf {{ wdm_frontend_pkg.package_name }}-{{ wdm_frontend_pkg.version }}/* {{ wdm.frontend.install_path }})
    - require:
      - file: install_WxDeskManagement_frontend_pkg
      - file: get_WxDeskManagement_frontend_pkg
