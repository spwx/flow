{% from "WxGameMenu/vars.j2" import WxGameMenu as wgm with context %}
{% from "WxGameMenu/vars.j2" import WxGameMenu_pkg as wgm_pkg with context %}

/etc/init/wxgamemenu-celery-worker.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: wxgamemenu-celery-worker daemon
        chdir: {{ wgm.celeryd.install_path }}/{{ wgm_pkg.package_name }}
        command: {{ wgm.celeryd.install_path}}/env/bin/python -m celery.__main__  -A syncgamemenu worker -l info -c 4 --uid={{ wgm.user }} --gid={{ wgm.group }} --logfile {{ wgm.celeryd.log_dir }}/celery-worker.log
    - user: root
    - group: root
    - mode: '644'
