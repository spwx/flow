{% from "WxGameMenu/vars.j2" import WxGameMenu as wgm with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

include:
  - WxGameMenu.common

{{ wgm.celeryd.install_path }}:
  file.directory:
    - user: {{ wgm.user }}
    - group: {{ wgm.group }}
    - makedirs: True
    - require:
      - user: {{ wgm.user }}_user
      - group: {{ wgm.group }}_group

create_env:
  cmd.run:
    - cwd: {{ wgm.celeryd.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ wgm.celeryd.install_path }}
