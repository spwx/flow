{% from "WxGameMenu/vars.j2" import WxGameMenu as wgm with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/wxgamemenu-celery-worker.conf:
  file.managed:
    - source: salt://WxGameMenu/celeryd/files/logrotate.conf.j2
    - template: jinja
