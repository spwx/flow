{% from "WxGameMenu/vars.j2" import WxGameMenu as wgm with context %}
{% from "WxGameMenu/vars.j2" import WxGameMenu_pkg as wgm_pkg with context %}

/etc/init/wxgamemenu-celerybeat.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: wxgamemenu-celerybeat daemon
        chdir: {{ wgm.celerybeat.install_path }}/{{ wgm_pkg.package_name }}
        command: {{ wgm.celerybeat.install_path}}/env/bin/python -m celery.__main__ -A syncgamemenu beat  -l info -s /var/run/WxGameMenu/celerybeat.db --uid={{ wgm.user }} --gid={{ wgm.group }} --logfile {{ wgm.celerybeat.log_dir }}/celerybeat.log --pidfile=/var/run/WxGameMenu/celerybeat.pid
    - user: root
    - group: root
    - mode: '644'

