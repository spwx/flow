{% from "WxGameMenu/vars.j2" import WxGameMenu as wgm with context %}
{% from "WxGameMenu/vars.j2" import WxGameMenu_pkg as wgm_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}
{% set celerybeat_redis = 'celerybeat-redis-{0}.tar.gz'.format(python3.deplibs.celerybeat_redis) %}

include:
  - .service

{{ wgm.celerybeat.log_dir }}:
  file.directory:
    - user: {{ wgm.user }}
    - group: {{ wgm.group }}
    - makedirs: True
    - recurse:
      - user
      - group

{{ wgm.celerybeat.install_path }}/{{ wgm_pkg.package_name }}:
  file.directory:
    - user: {{ wgm.user }}
    - group: {{ wgm.group }}
    - makedirs: True

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wgm_pkg.package_name }}-*

get_wgm.celerybeat:
  file.managed:
    - name: {{ src_dir }}/{{ wgm_pkg.package_name }}-{{ wgm_pkg.version }}.tar.gz
    - source: {{ wgm_pkg.pkg_url }}
    {% if wgm_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wgm_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wgm_pkg.package_name }}-{{ wgm_pkg.version }}.tar.gz
    - require:
      - file: get_wgm.celerybeat

# stop svc_name
stop_celerybeat_service:
  cmd.run:
    - names:
      - initctl stop wxgamemenu-celerybeat | exit 0
    - require:
      - file: /etc/init/wxgamemenu-celerybeat.conf
      - file: get_wgm.celerybeat

install_celerybeat_redis:
  file.managed:
    - name: {{ src_dir }}/{{ celerybeat_redis }}
    - source: salt://python3/files/{{ celerybeat_redis }}
  cmd.run:
    - name: source {{ wgm.celerybeat.install_path }}/env/bin/activate && pip3 install -I {{ src_dir }}/{{
    celerybeat_redis }}
    - require:
      - file: install_celerybeat_redis

install_wgm.celerybeat:
  cmd.run:
    - cwd: {{ wgm.celerybeat.install_path }}
    - names:
      - (rm -rf {{ wgm_pkg.package_name }}/*
        && \cp -rf {{ src_dir }}/{{ wgm_pkg.package_name }}-{{ wgm_pkg.version }}/* {{ wgm_pkg.package_name }}
        && source {{ wgm.celerybeat.install_path }}/env/bin/activate
        && pip3 install -r {{ wgm_pkg.package_name }}/requirements.txt)
    - require:
      - cmd: get_wgm.celerybeat
      - cmd: install_celerybeat_redis
      - cmd: stop_celerybeat_service

{% if wgm.get('settings', False) %}
{{ wgm.celerybeat.install_path }}/{{ wgm_pkg.package_name }}/syncgamemenu/settings.yaml:
  file.serialize:
    - dataset: {{ wgm.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_celerybeat_service
{% endif %}

start_celerybeat_service:
  cmd.run:
    - names:
      - initctl start wxgamemenu-celerybeat
      - sleep 5
    {%- if wgm.get('settings', False) %}
    - watch:
      - file: {{ wgm.celerybeat.install_path }}/{{ wgm_pkg.package_name }}/syncgamemenu/settings.yaml
    {%- endif %}
    - require:
      - file: /etc/init/wxgamemenu-celerybeat.conf
      - cmd: install_wgm.celerybeat

check_service_status:
  cmd.run:
    - names:
      - initctl status wxgamemenu-celerybeat |grep start
    - require:
      - cmd: start_celerybeat_service
