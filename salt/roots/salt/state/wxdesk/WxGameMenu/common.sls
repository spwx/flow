{% from "WxGameMenu/vars.j2" import WxGameMenu as wgm with context %}

{{ wgm.group }}_group:
  group.present:
    - name: {{ wgm.group }}

{{ wgm.user}}_user:
  user.present:
    - name: {{ wgm.user }}
    - shell: /bin/bash
    - groups:
      - {{ wgm.group }}
    - require:
      - group: {{ wgm.group }}_group

/var/run/WxGameMenu:
  file.directory:
    - user: {{ wgm.user }}
    - group: {{ wgm.group }}
    - makedirs: True
    - require:
      - user: {{ wgm.user }}_user
      - group: {{ wgm.group }}_group