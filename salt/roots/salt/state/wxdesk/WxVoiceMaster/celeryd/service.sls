{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster_pkg as wvm_pkg with context %}

/etc/init/voice-celery-worker.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: voice-celery-worker daemon
        chdir: {{ wvm.celeryd.install_path }}/{{ wvm_pkg.package_name }}
        command: {{ wvm.celeryd.install_path}}/env/bin/python -m celery.__main__ -l info -A deskcomponent.voice worker -c 4 --uid={{ wvm.user }} --gid={{ wvm.group }} --logfile {{ wvm.celeryd.log_dir }}/celery-worker.log -I interface,web
    - user: root
    - group: root
    - mode: '644'
