{% from "log_artifact/vars.j2" import log_artifact with context %}
{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}

include:
  - log_artifact

# mkdir logrotate/logs
# mkdir scripts
{{ wvm.celeryd.install_path }}/scripts:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{{ wvm.celeryd.install_path }}/logrotate/logs:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

# wvm.web.conf to logrotate
# artifact_log.sh to script
{% if log_artifact.get('everydaylogs', false) %}
{{ wvm.celeryd.install_path }}/scripts/artifact_log.sh:
  file.managed:
    - source: salt://log_artifact/files/artifact_log.sh
    - template: jinja
    - defaults:
        everydaylogs: {{ log_artifact.get('everydaylogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root
    - require:
      - file: {{ wvm.celeryd.install_path }}/scripts

{{ wvm.celeryd.install_path }}/logrotate/voice-celery-worker.conf:
  file.managed:
    - source: salt://WxVoiceMaster/celeryd/files/logrotate.conf.j2
    - template: jinja
    - backup: True
    - user: root
    - group: root
    - require:
      - file: {{ wvm.celeryd.install_path }}/scripts/artifact_log.sh

voice_logrotate:
  cron.present:
    - name: /usr/sbin/logrotate -f {{ wvm.celeryd.install_path }}/logrotate/voice-celery-worker.conf
    - minute: 0
    - hour: '*/2'

{% endif %}
