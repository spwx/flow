{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster_pkg as wvm_pkg with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}
{% set celerybeat_redis = 'celerybeat-redis-{0}.tar.gz'.format(python3.deplibs.celerybeat_redis) %}

include:
  - .service

{{ wvm.celeryd.log_dir }}:
  file.directory:
    - user: {{ wvm.user }}
    - group: {{ wvm.group }}
    - makedirs: True
    - recurse:
      - user
      - group

{{ wvm.celeryd.install_path }}/{{ wvm_pkg.package_name }}:
  file.directory:
    - user: {{ wvm.user }}
    - group: {{ wvm.group }}
    - makedirs: True

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wvm_pkg.package_name }}-*

get_celeryd:
  file.managed:
    - name: {{ src_dir }}/{{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}.tar.gz
    - source: {{ wvm_pkg.pkg_url }}
    {% if wvm_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wvm_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}.tar.gz
    - require:
      - file: get_celeryd

# stop svc_name
stop_celery_worker_service:
  cmd.run:
    - name: initctl stop voice-celery-worker | exit 0
    - require:
      - file: /etc/init/voice-celery-worker.conf
      - file: get_celeryd

install_celerybeat_redis:
  file.managed:
    - name: {{ src_dir }}/{{ celerybeat_redis }}
    - source: salt://python3/files/{{ celerybeat_redis }}
  cmd.run:
    - name: source {{ wvm.celeryd.install_path }}/env/bin/activate && pip3 install -I {{ src_dir }}/{{ celerybeat_redis }}
    - require:
      - file: install_celerybeat_redis

install_celeryd:
  cmd.run:
    - cwd: {{ wvm.celeryd.install_path }}
    - names:
      - (rm -rf {{ wvm_pkg.package_name }}/*
        && \cp -rf {{ src_dir }}/{{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}/* {{ wvm_pkg.package_name }}
        && source {{ wvm.celeryd.install_path }}/env/bin/activate
        && pip3 install -r {{ wvm_pkg.package_name }}/{{ deskcommon.path }}/requirements.txt
        && pip3 install -r {{ wvm_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_celeryd
      - cmd: install_celerybeat_redis
      - cmd: stop_celery_worker_service

{% if wvm.web.get('settings', False) %}
{{ wvm.celeryd.install_path }}/{{ wvm_pkg.package_name }}/settings.yaml:
  file.serialize:
    - name:
    - dataset: {{ wvm.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_celery_worker_service
{% endif %}

# set deskcomponent settings.yaml
{% if deskcommon.get('settings', False) %}
{{ wvm.celeryd.install_path }}/{{ wvm_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_celery_worker_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wvm.celeryd.install_path, wvm_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ wvm.celeryd.install_path }}
    - name: {{ wvm.celeryd.install_path }}/env/bin/pip3 install -r {{ wvm_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_celeryd
{{ wvm.celeryd.install_path }}/{{ wvm_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_celery_worker_service
{%- endif %}

start_celery_worker_service:
  cmd.run:
    - names:
      - initctl start voice-celery-worker
    {% if wvm.web.get('settings', False) %}
    - watch:
      - file: {{ wvm.celeryd.install_path }}/{{ wvm_pkg.package_name }}/settings.yaml
    {% endif %}
    - require:
      - file: /etc/init/voice-celery-worker.conf
      - cmd: install_celeryd

check_service_status:
  cmd.run:
    - names:
      - initctl status voice-celery-worker |grep start
    - require:
      - cmd: start_celery_worker_service
