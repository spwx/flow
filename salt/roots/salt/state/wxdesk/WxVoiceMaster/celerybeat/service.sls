{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster_pkg as wvm_pkg with context %}

/etc/init/voice-celerybeat-sys.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: voice-celerybeat-sys daemon
        chdir: {{ wvm.celerybeat.install_path }}/{{ wvm_pkg.package_name }}
        command: {{ wvm.celerybeat.install_path}}/env/bin/python -m celery.__main__ -l info -A deskcomponent.voice beat -s /var/run/WxVoiceMaster/celearybeat.sys.db --uid={{ wvm.user }} --gid={{ wvm.group }} --logfile {{ wvm.celerybeat.log_dir }}/celerybeat.sys.log --pidfile=/var/run/WxVoiceMaster/celerybeat.sys.pid
    - user: root
    - group: root
    - mode: '644'

/etc/init/voice-celerybeat.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: voice-celerybeat-sys daemon
        chdir: {{ wvm.celerybeat.install_path }}/{{ wvm_pkg.package_name }}
        command: {{ wvm.celerybeat.install_path}}/env/bin/python -m celery.__main__ -l info -A deskcomponent.voice beat -s /var/run/WxVoiceMaster/celearybeat.db -S deskcomponent.voice.beat.VoiceBeatScheduler --uid={{ wvm.user }} --gid={{ wvm.group }} --logfile {{ wvm.celerybeat.log_dir }}/celerybeat.log --pidfile=/var/run/WxVoiceMaster/celerybeat.pid
    - user: root
    - group: root
    - mode: '644'

