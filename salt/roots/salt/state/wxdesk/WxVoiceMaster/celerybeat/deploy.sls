{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster_pkg as wvm_pkg with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}
{% set celerybeat_redis = 'celerybeat-redis-{0}.tar.gz'.format(python3.deplibs.celerybeat_redis) %}

include:
  - .service

{{ wvm.celerybeat.log_dir }}:
  file.directory:
    - user: {{ wvm.user }}
    - group: {{ wvm.group }}
    - makedirs: True
    - recurse:
      - user
      - group

{{ wvm.celerybeat.install_path }}/{{ wvm_pkg.package_name }}:
  file.directory:
    - user: {{ wvm.user }}
    - group: {{ wvm.group }}
    - makedirs: True

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wvm_pkg.package_name }}-*

get_wvm.celerybeat:
  file.managed:
    - name: {{ src_dir }}/{{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}.tar.gz
    - source: {{ wvm_pkg.pkg_url }}
    {% if wvm_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wvm_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}.tar.gz
    - require:
      - file: get_wvm.celerybeat

# stop svc_name
stop_celerybeat_service:
  cmd.run:
    - names:
      - initctl stop voice-celerybeat-sys | exit 0
      - initctl stop voice-celerybeat | exit 0
    - require:
      - file: /etc/init/voice-celerybeat-sys.conf
      - file: /etc/init/voice-celerybeat.conf
      - file: get_wvm.celerybeat

install_celerybeat_redis:
  file.managed:
    - name: {{ src_dir }}/{{ celerybeat_redis }}
    - source: salt://python3/files/{{ celerybeat_redis }}
  cmd.run:
    - name: source {{ wvm.celerybeat.install_path }}/env/bin/activate && pip3 install -I {{ src_dir }}/{{
    celerybeat_redis }}
    - require:
      - file: install_celerybeat_redis

install_wvm.celerybeat:
  cmd.run:
    - cwd: {{ wvm.celerybeat.install_path }}
    - names:
      - (rm -rf {{ wvm_pkg.package_name }}/*
        && \cp -rf {{ src_dir }}/{{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}/* {{ wvm_pkg.package_name }}
        && source {{ wvm.celerybeat.install_path }}/env/bin/activate
        && pip3 install -r {{ wvm_pkg.package_name }}/{{ deskcommon.path }}/requirements.txt
        && pip3 install -r {{ wvm_pkg.package_name }}/requirements.txt)
    - require:
      - cmd: get_wvm.celerybeat
      - cmd: install_celerybeat_redis
      - cmd: stop_celerybeat_service

{% if wvm.web.get('settings', False) %}
{{ wvm.celerybeat.install_path }}/{{ wvm_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wvm.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_celerybeat_service
{% endif %}

# set deskcomponent settings.yaml
{% if deskcommon.get('settings', False) %}
{{ wvm.celerybeat.install_path}}/{{ wvm_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_celerybeat_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wvm.celerybeat.install_path, wvm_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ wvm.celerybeat.install_path }}
    - name: {{ wvm.celerybeat.install_path }}/env/bin/pip3 install -r {{ wvm_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_wvm.celerybeat
{{ wvm.celerybeat.install_path }}/{{ wvm_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_celerybeat_service
{%- endif %}

start_celerybeat_service:
  cmd.run:
    - names:
      - initctl start voice-celerybeat-sys
      - initctl start voice-celerybeat
      - sleep 5
    {%- if wvm.web.get('settings', False) %}
    - watch:
      - file: {{ wvm.celerybeat.install_path }}/{{ wvm_pkg.package_name }}/settings.yaml
    {%- endif %}
    - require:
      - file: /etc/init/voice-celerybeat-sys.conf
      - file: /etc/init/voice-celerybeat.conf
      - cmd: install_wvm.celerybeat

check_service_status:
  cmd.run:
    - names:
      - initctl status voice-celerybeat-sys |grep start
      - initctl status voice-celerybeat |grep start
    - require:
      - cmd: start_celerybeat_service
