{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/voice-celerybeat.conf:
  file.managed:
    - source: salt://WxVoiceMaster/celerybeat/files/logrotate.conf.j2
    - template: jinja
