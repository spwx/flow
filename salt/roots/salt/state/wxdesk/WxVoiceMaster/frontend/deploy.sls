{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster_pkg as wvm_pkg with context %}

{% set src_dir = '/usr/local/src' %}

get_wxvoicemaster_frontend_pkg:
  cmd.run:
    - cwd: {{ src_dir }}
      name: rm -rf {{ wvm_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}.tar.gz
    - source: {{  wvm_pkg.pkg_url }}
    {% if wvm_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wvm_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_wxvoicemaster_frontend_pkg

install_wxvoicemaster_frontend_pkg:
  file.directory:
    - name: {{ wvm.frontend.install_path }}
    - makedirs: True
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (tar -xzf {{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}.tar.gz
       && \cp -Rf {{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}/* {{ wvm.frontend.install_path }} )
    - require:
      - file: install_wxvoicemaster_frontend_pkg
      - file: get_wxvoicemaster_frontend_pkg
