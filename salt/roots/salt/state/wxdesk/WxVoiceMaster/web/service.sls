{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster_pkg as wvm_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/init/voice-api.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: voice-api daemon
        chdir: {{ wvm.web.install_path }}/{{ wvm_pkg.package_name }}
        command: {{ wvm.web.install_path}}/env/bin/python3 -m interface.websvc
    - user: root
    - group: root
    - mode: '644'

/etc/init/voice-web.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: voice-web daemon
        chdir: {{ wvm.web.install_path }}/{{ wvm_pkg.package_name }}
        command: {{ wvm.web.install_path}}/env/bin/python3 -m web.websvc
    - user: root
    - group: root
    - mode: '644'
