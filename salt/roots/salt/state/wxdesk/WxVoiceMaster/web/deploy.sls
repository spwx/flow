{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster_pkg as wvm_pkg with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}
{% set celerybeat_redis = 'celerybeat-redis-{0}.tar.gz'.format(python3.deplibs.celerybeat_redis) %}

# load config file
include:
  - .service

{{ wvm.web.log_dir }}:
  file.directory:
    - user: {{ wvm.user }}
    - group: {{ wvm.group }}
    - makedirs: True
    - recurse:
      - user
      - group

# create package directory
{{ wvm.web.install_path }}/{{ wvm_pkg.package_name }}:
  file.directory:
    - user: {{ wvm.user }}
    - group: {{ wvm.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wvm_pkg.package_name }}-*

# get WxVoiceMaster_web pkg
get_WxVoiceMaster_web:
  file.managed:
    - name: {{ src_dir }}/{{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}.tar.gz
    - source: {{ wvm_pkg.pkg_url }}
    {%- if wvm_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wvm_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}.tar.gz
    - require:
      - file: get_WxVoiceMaster_web

install_celerybeat_redis:
  file.managed:
    - name: {{ src_dir }}/{{ celerybeat_redis }}
    - source: salt://python3/files/{{ celerybeat_redis }}
  cmd.run:
    - name: source {{ wvm.web.install_path }}/env/bin/activate && pip3 install -I {{ src_dir }}/{{ celerybeat_redis }}
    - require:
      - file: install_celerybeat_redis

# install WxVoiceMaster web
install_WxVoiceMaster_web:
  cmd.run:
    - cwd: {{ wvm.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wvm_pkg.package_name }}-{{ wvm_pkg.version }}/* {{ wvm_pkg.package_name }}
        && source {{ wvm.web.install_path }}/env/bin/activate
        && pip3 install -r {{ wvm_pkg.package_name }}/{{ deskcommon.path }}/requirements.txt
        && pip3 install -r {{ wvm_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_WxVoiceMaster_web
      - cmd: install_celerybeat_redis

{% if wvm.web.get('settings', False) %}
{{ wvm.web.install_path }}/{{ wvm_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wvm.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    
{% endif %}

# set deskcomponent settings.yaml
{% if deskcommon.get('settings', False) %}
{{ wvm.web.install_path }}/{{ wvm_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wvm.web.install_path, wvm_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ wvm.web.install_path }}
    - name: {{ wvm.web.install_path }}/env/bin/pip3 install -r {{ wvm_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_WxVoiceMaster_web
{{ wvm.web.install_path }}/{{ wvm_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{%- endif %}

# reload WxVoiceMaster_web service
start_api_and_web_service:
  cmd.run:
    - names:
      - initctl start voice-api || true
      - initctl start voice-web || true
      - kill -HUP `ps -ef|grep /env/| grep -v grep | awk '{print $2 }'`;
    - watch:
      - file: /etc/init/voice-api.conf
      - file: /etc/init/voice-web.conf
      {% if wvm.web.get('settings', False) %}
      - file: {{ wvm.web.install_path }}/{{ wvm_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/voice-api.conf
      - file: /etc/init/voice-web.conf
      - cmd: install_WxVoiceMaster_web
