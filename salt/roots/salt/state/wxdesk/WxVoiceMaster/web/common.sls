{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}
{% from "python3/vars.j2" import python3 with context %}

include:
  - .common

{{ wvm.web.install_path }}:
  file.directory:
    - user: {{ wvm.user }}
    - group: {{ wvm.group }}
    - makedirs: True
    - require:
      - user: {{ wvm.user }}_user
      - group: {{ wvm.group }}_group

create_env:
  cmd.run:
    - cwd: {{ wvm.web.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ wvm.web.install_path }}

