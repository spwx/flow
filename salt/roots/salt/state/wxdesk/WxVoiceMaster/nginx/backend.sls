{% from 'nginx/vars.j2' import nginx with context %}

{{ nginx.conf_dir }}/conf.d/WxVoiceMaster_web.backend.conf:
  file.managed:
    - source: salt://WxVoiceMaster/nginx/files/conf.d/web.backend.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
  service.running:
    - name: {{ nginx.svc_name }}
    - reload: True
    - watch:
      - file: {{ nginx.conf_dir }}/conf.d/WxVoiceMaster_web.backend.conf

{{ nginx.conf_dir }}/conf.d/WxVoiceMaster_api.backend.conf:
  file.managed:
    - source: salt://WxVoiceMaster/nginx/files/conf.d/api.backend.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
  service.running:
    - name: {{ nginx.svc_name }}
    - reload: True
    - watch:
      - file: {{ nginx.conf_dir }}/conf.d/WxVoiceMaster_api.backend.conf
