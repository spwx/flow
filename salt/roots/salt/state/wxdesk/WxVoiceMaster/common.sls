{% from "WxVoiceMaster/vars.j2" import WxVoiceMaster as wvm with context %}

{{ wvm.group }}_group:
  group.present:
    - name: {{ wvm.group }}

{{ wvm.user }}_user:
  user.present:
    - name: {{ wvm.user }}
    - shell: /bin/bash
    - groups:
      - {{ wvm.group }}
    - require:
      - group: {{ wvm.group }}_group

/var/run/WxVoiceMaster:
  file.directory:
    - user: {{ wvm.user }}
    - group: {{ wvm.group }}
    - makedirs: True
    - require:
      - user: {{ wvm.user }}_user
      - group: {{ wvm.group }}_group