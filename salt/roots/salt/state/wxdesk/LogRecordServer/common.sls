{% from "LogRecordServer/vars.j2" import LogRecordServer with context %}

{{ LogRecordServer.group }}_group:
  group.present:
    - name: {{ LogRecordServer.group }}

{{ LogRecordServer.user }}_user:
  user.present:
    - name: {{ LogRecordServer.user }}
    - shell: /bin/false
    - groups:
      - {{ LogRecordServer.group }}
    - require:
      - group: {{ LogRecordServer.group }}_group

{{ LogRecordServer.install_path }}:
  file.directory:
    - user: {{ LogRecordServer.user }}
    - group: {{ LogRecordServer.group }}
    - makedirs: True
    - require:
      - user: {{ LogRecordServer.user }}_user
      - group: {{ LogRecordServer.group }}_group