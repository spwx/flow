{% from "LogRecordServer/vars.j2" import LogRecordServer,LogRecordServer_pkg with context %}

{% set src_dir = '/usr/local/src' %}

include:
  - LogRecordServer.service

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ LogRecordServer_pkg.package_name }}-*

get_LogRecordServer:
  file.managed:
    - name: {{ src_dir }}/{{ LogRecordServer_pkg.package_name }}-{{ LogRecordServer_pkg.version }}.tar.gz
    - source: {{ LogRecordServer_pkg.pkg_url }}
    {% if LogRecordServer_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ LogRecordServer_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ LogRecordServer_pkg.package_name }}-{{ LogRecordServer_pkg.version }}.tar.gz
    - require:
      - file: get_LogRecordServer

# stop svc_name
stop_{{ LogRecordServer.svc_name }}_service:
  service.dead:
    - name: {{ LogRecordServer.svc_name }}
    - require:
      - file: /etc/init.d/{{ LogRecordServer.svc_name }}
      - file: get_LogRecordServer

install_LogRecordServer:
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (rm -rf {{ LogRecordServer.install_path }}/*
        && \cp -rf {{ LogRecordServer_pkg.package_name }}-{{ LogRecordServer_pkg.version }}/* {{ LogRecordServer.install_path }})
    - require:
      - file: get_LogRecordServer
      - service: stop_{{ LogRecordServer.svc_name }}_service

{{ LogRecordServer.install_path }}/etc/LogRecordServer.cfg:
  file.managed:
    - source: salt://LogRecordServer/files/etc/LogRecordServer.cfg.j2
    - template: jinja
    - backup: True
    - require:
      - service: stop_{{ LogRecordServer.svc_name }}_service

{{ LogRecordServer.install_path }}/logs:
  file.directory:
    - user: {{ LogRecordServer.user }}
    - group: {{ LogRecordServer.group }}
    - makedirs: True
    - require:
      - service: stop_{{ LogRecordServer.svc_name }}_service

# start svc_name
start_{{ LogRecordServer.svc_name }}_service:
  service.running:
    - name: {{ LogRecordServer.svc_name }}
    - reload: True
    - enable: True
    - watch:
      - file: {{ LogRecordServer.install_path }}/etc/LogRecordServer.cfg
    - require:
      - file: /etc/init.d/{{ LogRecordServer.svc_name }}
      - cmd: install_LogRecordServer
