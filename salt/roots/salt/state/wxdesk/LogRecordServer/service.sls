{% from "LogRecordServer/vars.j2" import LogRecordServer with context %}

/etc/init.d/{{ LogRecordServer.svc_name }}:
  file.managed:
    - source: salt://LogRecordServer/files/LogRecordServer.init.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0755'
  cmd.run:
    - name: chkconfig --add {{ LogRecordServer.svc_name }}
    - unless: chkconfig --list | grep {{ LogRecordServer.svc_name }}
    - require:
      - file: /etc/init.d/{{ LogRecordServer.svc_name }}