{% from "egs/vars.j2" import egs as wxp with context %}
{% from "egs/vars.j2" import egs_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/init/egs.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: policy-api daemon
        chdir: {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.web.install_path}}/env/bin/gunicorn main:app  -c {{ wxp.web.install_path }}/{{ wxp_pkg.package_name }}/gunicorn_config.py
    - user: root
    - group: root
    - mode: '644'

