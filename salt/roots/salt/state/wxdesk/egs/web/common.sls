{% from "egs/vars.j2" import egs as wxp with context %}
{% from "python3/vars.j2" import python3 with context %}

include:
  - .common

{{ wxp.web.install_path }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True
    - require:
      - user: {{ wxp.user }}_user
      - group: {{ wxp.group }}_group

creat_env:
  cmd.run:
    - cwd: {{ wxp.web.install_path }}
    - name: {{ python3.install_prefix }}/bin/virtualenv env --always-copy  --system-site-packages
    - unless: test -d env
    - requires:
      - file: {{ wxp.web.install_path}}