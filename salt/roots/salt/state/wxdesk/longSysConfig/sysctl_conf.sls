/etc/sysctl.conf:
  file.managed:
    - source: salt://sysConfig/files/sysctl.conf.j2
    - template: jinja
    - backup: True
  cmd.run:
    - name: /sbin/sysctl -p
    - require:
      - file: /etc/sysctl.conf
