turn_off_selinux:
  file.replace:
    - name: /etc/selinux/config
    - pattern: ^SELINUX=.*$
    - repl: SELINUX=permissive
    - backup: True
  service.dead:
    - name: iptables
    - enable: False
  cmd.run:
    - name: setenforce 0

# command: setenforce 0
# command: getenforce
