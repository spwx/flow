/etc/security/limits.d/91-wxdesk.conf:
  file.managed:
    - source: salt://sysConfig/files/91-wxdesk.conf.j2
    - template: jinja
    - backup: True
