{% from "WxPolicyServer/vars.j2" import wxpolicy as wxp with context %}
{% from "WxPolicyServer/vars.j2" import wxpolicy_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# create package directory
{{ wxp.celeryd.install_path }}/{{ wxp_pkg.package_name }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wxp_pkg.package_name }}-*

# get WxPolicyServer_celeryd pkg
get_WxPolicyServer_celeryd:
  file.managed:
    - name: {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - source: {{ wxp_pkg.pkg_url }}
    {%- if wxp_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wxp_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - require:
      - file: get_WxPolicyServer_celeryd

# stop WxPolicyServer_celeryd service
stop_api_and_celeryd_service:
  cmd.run:
    - names:
      - initctl stop policy-celeryd | exit 0
    - require:
      - file: /etc/init/policy-celeryd.conf
      - file: get_WxPolicyServer_celeryd

# install WxPolicyServer celeryd
install_WxPolicyServer_celeryd:
  cmd.run:
    - cwd: {{ wxp.celeryd.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}/* {{ wxp_pkg.package_name }}
        && source {{ wxp.celeryd.install_path }}/env/bin/activate
        && pip3 install -r {{ wxp_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_WxPolicyServer_celeryd
      - cmd: stop_api_and_celeryd_service

# install celeryd sertting.yaml
{% if wxp.get('settings', False) %}
{{ wxp.celeryd.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wxp.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_api_and_celeryd_service
{% endif %}

# reload WxPolicyServer_celeryd service
start_api_and_celeryd_service:
  cmd.run:
    - names:
      - initctl start policy-celeryd
    - watch:
      - file: /etc/init/policy-celeryd.conf
      {% if wxp.celeryd.get('settings', False) %}
      - file: {{ wxp.celeryd.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/policy-celeryd.conf
      - cmd: install_WxPolicyServer_celeryd