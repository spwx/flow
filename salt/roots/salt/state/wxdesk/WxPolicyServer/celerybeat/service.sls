{% from "WxPolicyServer/vars.j2" import wxpolicy as wxp with context %}
{% from "WxPolicyServer/vars.j2" import wxpolicy_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/init/policy-celerybeat.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: policy-celerybeat daemon
        chdir: {{ wxp.celerybeat.install_path }}/{{ wxp_pkg.package_name }}
        command: {{ wxp.celerybeat.install_path}}/env/bin/python3 -m celery -A policymgr beat
    - user: root
    - group: root
    - mode: '644'
