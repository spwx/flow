{% from "WxPolicyServer/vars.j2" import wxpolicy as wxp with context %}
{% from "WxPolicyServer/vars.j2" import accountsync_pkg as async_pkg with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# create package directory
{{ wxp.accountsync.install_path }}/{{ async_pkg.package_name }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ async_pkg.package_name }}-*

# get WxPolicyServer_accountsync pkg
get_WxPolicyServer_accountsync:
  file.managed:
    - name: {{ src_dir }}/{{ async_pkg.package_name }}-{{ async_pkg.version }}.tar.gz
    - source: {{ async_pkg.pkg_url }}
    {%- if async_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ async_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ async_pkg.package_name }}-{{ async_pkg.version }}.tar.gz
    - require:
      - file: get_WxPolicyServer_accountsync

# stop WxPolicyServer_accountsync service
stop_api_and_accountsync_service:
  cmd.run:
    - names:
      - initctl stop policy-accountsync | exit 0
    - require:
      - file: /etc/init/policy-accountsync.conf
      - file: get_WxPolicyServer_accountsync

# install WxPolicyServer accountsync
install_WxPolicyServer_accountsync:
  cmd.run:
    - cwd: {{ wxp.accountsync.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ async_pkg.package_name }}-{{ async_pkg.version }}/* {{ async_pkg.package_name }}
        && source {{ wxp.accountsync.install_path }}/env/bin/activate
        && pip3 install -r {{ async_pkg.package_name }}/accountcenter/requirements.txt
        && pip3 install -r {{ async_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_WxPolicyServer_accountsync
      - cmd: stop_api_and_accountsync_service

# install accountsync sertting.yaml
{% if wxp.accountsync.get('settings', False) %}
{{ wxp.accountsync.install_path }}/{{ async_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wxp.accountsync.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_api_and_accountsync_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wxp.accountsync.install_path, async_pkg.package_name))%}
{{ wxp.accountsync.install_path }}/{{ async_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_api_and_accountsync_service
{%- endif %}

# reload WxPolicyServer_accountsync service
start_api_and_accountsync_service:
  cmd.run:
    - names:
      - initctl start policy-accountsync
    - watch:
      - file: /etc/init/policy-accountsync.conf
      {% if wxp.accountsync.get('settings', False) %}
      - file: {{ wxp.accountsync.install_path }}/{{ async_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/policy-accountsync.conf
      - cmd: install_WxPolicyServer_accountsync