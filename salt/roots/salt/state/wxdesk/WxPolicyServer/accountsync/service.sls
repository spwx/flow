{% from "WxPolicyServer/vars.j2" import wxpolicy as wxp with context %}
{% from "WxPolicyServer/vars.j2" import accountsync_pkg as async_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/init/policy-accountsync.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: wanglele
        description: policy-accountsync daemon
        chdir: {{ wxp.accountsync.install_path }}/{{ async_pkg.package_name }}
        command: {{ wxp.accountsync.install_path}}/env/bin/python3 -m sync
    - user: root
    - group: root
    - mode: '644'
