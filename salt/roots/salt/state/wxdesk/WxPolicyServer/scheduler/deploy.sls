{% from "WxPolicyServer/vars.j2" import wxpolicy as wxp with context %}
{% from "WxPolicyServer/vars.j2" import wxpolicy_pkg as wxp_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# create package directory
{{ wxp.scheduler.install_path }}/{{ wxp_pkg.package_name }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wxp_pkg.package_name }}-*

# get WxPolicyServer_scheduler pkg
get_WxPolicyServer_scheduler:
  file.managed:
    - name: {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - source: {{ wxp_pkg.pkg_url }}
    {%- if wxp_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wxp_pkg.pkg_checksum }}
    {%- endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}.tar.gz
    - require:
      - file: get_WxPolicyServer_scheduler

# stop WxPolicyServer_scheduler service
stop_api_and_scheduler_service:
  cmd.run:
    - names:
      - initctl stop policy-scheduler | exit 0
    - require:
      - file: /etc/init/policy-scheduler.conf
      - file: get_WxPolicyServer_scheduler

# install WxPolicyServer scheduler
install_WxPolicyServer_scheduler:
  cmd.run:
    - cwd: {{ wxp.scheduler.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wxp_pkg.package_name }}-{{ wxp_pkg.version }}/* {{ wxp_pkg.package_name }}
        && source {{ wxp.scheduler.install_path }}/env/bin/activate
        && pip3 install -r {{ wxp_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_WxPolicyServer_scheduler
      - cmd: stop_api_and_scheduler_service

# install scheduler sertting.yaml
{% if wxp.get('settings', False) %}
{{ wxp.scheduler.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wxp.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_api_and_scheduler_service
{% endif %}

# reload WxPolicyServer_scheduler service
start_api_and_scheduler_service:
  cmd.run:
    - names:
      - initctl start policy-scheduler
    - watch:
      - file: /etc/init/policy-scheduler.conf
      {% if wxp.scheduler.get('settings', False) %}
      - file: {{ wxp.scheduler.install_path }}/{{ wxp_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/policy-scheduler.conf
      - cmd: install_WxPolicyServer_scheduler