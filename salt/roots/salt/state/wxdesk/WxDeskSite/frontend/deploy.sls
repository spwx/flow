{% from "WxDeskSite/vars.j2" import desk_site as wds with context %}
{% from "WxDeskSite/vars.j2" import desk_site_pkg as pkg with context %}

{% set src_dir = '/usr/local/src' %}

get_desk_site:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf desk_site-*
  file.managed:
    - name: {{ src_dir }}/{{ pkg.package_name }}-{{ pkg.version }}.tar.gz
    - source: {{ pkg.pkg_url }}
    {% if pkg.get('pkg_checksum', false) %}
    - source_hash: {{ pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_desk_site

install_desk_site:
  file.directory:
    - name: {{ wds.frontend.install_path }}
    - makedirs: True
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (tar -xzf {{ pkg.package_name }}-{{ pkg.version }}.tar.gz -C {{ wds.frontend.install_path }})
    - require:
      - file: install_desk_site
      - file: get_desk_site