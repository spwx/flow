{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}

{{ pds.group }}_group:
  group.present:
    - name: {{ pds.group }}

{{ pds.user }}_user:
  user.present:
    - name: {{ pds.user }}
    - shell: /bin/bash
    - groups:
      - {{ pds.group }}
    - require:
      - group: {{ pds.group }}_group

/var/run/PolicyDataServer:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - makedirs: True
    - require:
      - user: {{ pds.user }}_user
      - group: {{ pds.group }}_group