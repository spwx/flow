{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}
{% from "PolicyDataServer/vars.j2" import PolicyDataServer_pkg as pds_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}

{% set src_dir = '/usr/local/src' %}

include:
  - .service

{{ pds.celeryd.install_path }}/{{ pds_pkg.package_name }}:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - makedirs: True

# create backup directory
{{ pds.celeryd.install_path }}/backup:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - makedirs: True

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ pds_pkg.package_name }}-*

get_celeryd:
  file.managed:
    - name: {{ src_dir }}/{{ pds_pkg.package_name }}-{{ pds_pkg.version }}.tar.gz
    - source: {{ pds_pkg.pkg_url }}
    {% if pds_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ pds_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ pds_pkg.package_name }}-{{ pds_pkg.version }}.tar.gz
    - require:
      - file: get_celeryd

# stop svc_name
stop_{{ pds.celeryd.svc_name }}_service:
  cmd.run:
    - name: initctl stop {{ pds.celeryd.svc_name }} | exit 0
    - require:
      - file: /etc/init/{{ pds.celeryd.svc_name }}.conf
      - file: get_celeryd

# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ pds.celeryd.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ pds_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ pds_pkg.package_name }}/logs
    - require:
      - cmd: stop_{{ pds.celeryd.svc_name }}_service

install_celeryd:
  cmd.run:
    - cwd: {{ pds.celeryd.install_path }}
    - names:
      - (rm -rf {{ pds_pkg.package_name }}/*
        && \cp -rf {{ src_dir }}/{{ pds_pkg.package_name }}-{{ pds_pkg.version }}/* {{ pds_pkg.package_name }}
        && source {{ pds.celeryd.install_path }}/env/bin/activate
        && pip3 install -r {{ pds_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_celeryd
      - cmd: stop_{{ pds.celeryd.svc_name }}_service

{% if pds.web.get('settings', False) %}
{{ pds.celeryd.install_path }}/{{ pds_pkg.package_name }}/settings.yaml:
  file.serialize:
    - name:
    - dataset: {{ pds.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ pds.celeryd.svc_name }}_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(pds.celeryd.install_path, pds_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ pds.celeryd.install_path }}
    - name: {{ pds.celeryd.install_path }}/env/bin/pip3 install -r {{ pds_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_celeryd
{{ pds.celeryd.install_path }}/{{ pds_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ pds.celeryd.svc_name }}_service
{%- endif %}

disable_uwsgi:
  file.replace:
    - name: {{ pds.celeryd.install_path }}/{{ pds_pkg.package_name }}/settings.yaml
    - pattern: |
        ENABLE_UWSGI: .*
    - repl: |
        ENABLE_UWSGI: False
    - append_if_not_found: True
    - backup: True
    - require:
      - cmd: stop_{{ pds.celeryd.svc_name }}_service

restore_log_dir:
  cmd.run:
    - cwd: {{ pds.celeryd.install_path }}
    - names:
      - (rm -rf {{ pds_pkg.package_name }}/logs
        &&mv -f backup/logs {{ pds_pkg.package_name }} )
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ pds.celeryd.install_path }}/{{ pds_pkg.package_name }}/logs:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir
      - cmd: stop_{{ pds.celeryd.svc_name }}_service

# start svc_name
start_{{ pds.celeryd.svc_name }}_service:
  cmd.run:
    - name: initctl start {{ pds.celeryd.svc_name }}
    {% if pds.web.get('settings', False) %}
    - watch:
      - file: {{ pds.celeryd.install_path }}/{{ pds_pkg.package_name }}/settings.yaml
    {% endif %}
    - require:
      - file: /etc/init/{{ pds.celeryd.svc_name }}.conf
      - cmd: install_celeryd
