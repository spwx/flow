{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}
{% from "PolicyDataServer/vars.j2" import PolicyDataServer_pkg as pds_pkg with context %}

{% set celery = pds.celeryd %}

/etc/init/{{ celery.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: celery worker daemon
        chdir: {{ celery.install_path }}/{{ pds_pkg.package_name }}
        command: {{ celery.install_path }}/env/bin/python -m celery.__main__ -l info -A policydataserver worker -P gevent --time-limit=3600 -c {{ celery.concurrency }} --uid={{ pds.user }} --gid={{ pds.group }} --logfile {{ celery.log_dir }}/{{ celery.svc_name }}.log
    - user: root
    - group: root
    - mode: '644'
