{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}
{% from "PolicyDataServer/vars.j2" import PolicyDataServer_pkg as pds_pkg with context %}

{% set celerybeat = pds.celerybeat %}

/etc/init/{{ celerybeat.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '644'
    - defaults:
        author: liuyongsheng
        description: celerybeat daemon
        chdir: {{ celerybeat.install_path }}/{{ pds_pkg.package_name }}
        command: {{ celerybeat.install_path }}/env/bin/python -m celery.__main__ -l info -A policydataserver beat -s /var/run/PolicyDataServer/{{ celerybeat.svc_name }} --uid={{ pds.user }} --gid={{ pds.group }} --logfile {{ celerybeat.log_dir }}/{{ celerybeat.svc_name }}.log
