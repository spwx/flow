{% from "log_artifact/vars.j2" import log_artifact with context %}
{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}

include:
  - log_artifact

{% if log_artifact.get('templogs', false) %}
/data/SicentTools/deploy/push_{{ pds.celerybeat.svc_name }}_log.sh:
  file.managed:
    - source: salt://PolicyDataServer/celerybeat/files/push_log.sh.j2
    - template: jinja
    - defaults:
        templogs: {{ log_artifact.get('templogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root

{% endif %}


{% if log_artifact.get('everydaylogs', false) %}
/data/SicentScripts/everyday_push_{{ pds.celerybeat.svc_name }}_log.sh:
  file.managed:
    - source: salt://PolicyDataServer/celerybeat/files/everyday_push_log.sh.j2
    - template: jinja
    - defaults:
        everydaylogs: {{ log_artifact.get('everydaylogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root

{% endif %}
