{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/{{ pds.celerybeat.svc_name }}.conf:
  file.managed:
    - source: salt://PolicyDataServer/celerybeat/files/logrotate.conf.j2
    - template: jinja