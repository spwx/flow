{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}
{% set web = pds.web %}

{{ web.install_path }}/{{ web.svc_name }}.ini:
  file.managed:
    - source: salt://PolicyDataServer/web/files/web.ini.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644

/etc/init/{{ pds.web.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '644'
    - defaults:
        author: liuyongsheng
        description: {{ web.svc_name }} daemon
        expect: daemon
        chdir: {{ web.install_path }}
        command: {{ web.install_path }}/env/bin/uwsgi --ini {{ web.install_path }}/{{ web.svc_name }}.ini
    - require:
      - file: {{ web.install_path }}/{{ web.svc_name }}.ini
