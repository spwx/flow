{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}
{% from "PolicyDataServer/vars.j2" import PolicyDataServer_pkg as pds_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

# create package directory
{{ pds.web.install_path }}/{{ pds_pkg.package_name }}:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - makedirs: True

# create backup directory
{{ pds.web.install_path }}/backup:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ pds_pkg.package_name }}-*

# get PolicyDataServer_web pkg
get_PolicyDataServer_web:
  file.managed:
    - name: {{ src_dir }}/{{ pds_pkg.package_name }}-{{ pds_pkg.version }}.tar.gz
    - source: {{ pds_pkg.pkg_url }}
    {% if pds_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ pds_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ pds_pkg.package_name }}-{{ pds_pkg.version }}.tar.gz
    - require:
      - file: get_PolicyDataServer_web

# stop PolicyDataServer_web service
stop_{{ pds.web.svc_name }}_service:
  cmd.run:
    - name: initctl stop {{ pds.web.svc_name }} | exit 0
    - require:
      - file: /etc/init/{{ pds.web.svc_name }}.conf
      - file: get_PolicyDataServer_web

# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ pds.web.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ pds_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ pds_pkg.package_name }}/logs
    - require:
      - cmd: stop_{{ pds.web.svc_name }}_service

# install PolicyDataServer web
install_PolicyDataServer_web:
  cmd.run:
    - cwd: {{ pds.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ pds_pkg.package_name }}-{{ pds_pkg.version }}/* {{ pds_pkg.package_name }}
        && source {{ pds.web.install_path }}/env/bin/activate
        && pip3 install -r {{ pds_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_PolicyDataServer_web
      - cmd: stop_{{ pds.web.svc_name }}_service

{% if pds.web.get('settings', False) %}
{{ pds.web.install_path }}/{{ pds_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ pds.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ pds.web.svc_name }}_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(pds.web.install_path, pds_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ pds.web.install_path }}
    - name: {{ pds.web.install_path }}/env/bin/pip3 install -r {{ pds_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_PolicyDataServer_web
{{ pds.web.install_path }}/{{ pds_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ pds.web.svc_name }}_service
{%- endif %}

{{ pds.web.install_path }}/htpasswd:
  file.managed:
    - source: salt://PolicyDataServer/web/files/htpasswd

restore_log_dir:
  cmd.run:
    - cwd: {{ pds.web.install_path }}
    - names:
      - (rm -rf {{ pds_pkg.package_name }}/logs
        && mv -f backup/logs {{ pds_pkg.package_name }})
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ pds.web.install_path }}/{{ pds_pkg.package_name }}/logs:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir
      - cmd: stop_{{ pds.web.svc_name }}_service

# reload PolicyDataServer_web service
start_{{ pds.web.svc_name }}_service:
  cmd.run:
    - name: initctl start {{ pds.web.svc_name }}
    - watch:
      - file: {{ pds.web.install_path }}/{{ pds.web.svc_name }}.ini
      {% if pds.web.get('settings', False) %}
      - file: {{ pds.web.install_path }}/{{ pds_pkg.package_name }}/settings.yaml
      {% endif %}
    - require:
      - file: /etc/init/{{ pds.web.svc_name }}.conf
      - cmd: install_PolicyDataServer_web