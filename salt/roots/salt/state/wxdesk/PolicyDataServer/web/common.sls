{% from "PolicyDataServer/vars.j2" import PolicyDataServer as pds with context %}
{% from "python3/vars.j2" import python3 with context %}

include:
  - PolicyDataServer.common

{{ pds.web.install_path }}:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - makedirs: True
    - require:
      - user: {{ pds.user }}_user
      - group: {{ pds.group }}_group

create_env:
  cmd.run:
    - cwd: {{ pds.web.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ pds.web.install_path }}

{{ pds.web.log_dir }}:
  file.directory:
    - user: {{ pds.user }}
    - group: {{ pds.group }}
    - makedirs: True
    - require:
      - user: {{ pds.user }}_user
      - group: {{ pds.group }}_group
