{% from "WxBarshop/vars.j2" import WxBarshop as wbs with context %}

{% set web = wbs.webc %}

{{ web.install_path }}/{{ web.svc_name }}.ini:
  file.managed:
    - source: salt://WxBarshop/webc/files/webc.ini.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644

/etc/init/{{ web.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: liuyongsheng
        description: {{ web.svc_name }}
        expect: daemon
        chdir: {{ web.install_path }}
        command: {{ web.install_path }}/env/bin/uwsgi --ini {{ web.install_path }}/{{ web.svc_name }}.ini --uid={{ wbs.user }} --gid={{ wbs.group }}
    - user: root
    - group: root
    - mode: '644'
    - require:
      - file: {{ web.install_path }}/{{ web.svc_name }}.ini
