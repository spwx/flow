{% from "WxBarshop/vars.j2" import WxBarshop as wbs with context %}
{% from "python3/vars.j2" import python3 with context %}

include:
  - WxBarshop.common

{{ wbs.webb.install_path }}:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True
    - require:
      - user: {{ wbs.user }}_user
      - group: {{ wbs.group }}_group

create_env:
  cmd.run:
    - cwd: {{ wbs.webb.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ wbs.webb.install_path }}
