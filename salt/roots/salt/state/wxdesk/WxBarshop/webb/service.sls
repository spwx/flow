{% from "WxBarshop/vars.j2" import WxBarshop as wbs with context %}

{% set webb = wbs.webb %}

{{ webb.install_path }}/{{ webb.svc_name }}.ini:
  file.managed:
    - source: salt://WxBarshop/webb/files/webb.ini.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644

/etc/init/{{ webb.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: yanglinshan
        description: {{ webb.svc_name }}
        expect: daemon
        chdir: {{ webb.install_path }}
        command: {{ webb.install_path }}/env/bin/uwsgi --ini {{ webb.install_path }}/{{ webb.svc_name }}.ini --uid={{ wbs.user }} --gid={{ wbs.group }}
    - user: root
    - group: root
    - mode: '644'
    - require:
      - file: {{ webb.install_path }}/{{ webb.svc_name }}.ini
