{% from "WxBarshop/vars.j2" import WxBarshop as wbs with context %}
{% from "WxBarshop/vars.j2" import WxBarshop_pkg as wbs_pkg with context %}
{% from "WxAccountCenter/vars.j2" import WxAccountCenter as wac with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

{{ wbs.webb.log_dir }}:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True
    - recurse:
      - user
      - group

# create package directory
{{ wbs.webb.install_path }}/{{ wbs_pkg.package_name }}:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True

# create backup directory
{{ wbs.webb.install_path }}/backup:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ wbs_pkg.package_name }}-*

# get WxBarshop_web pkg
get_WxBarshop_web:
  file.managed:
    - name: {{ src_dir }}/{{ wbs_pkg.package_name }}-{{ wbs_pkg.version }}.tar.gz
    - source: {{ wbs_pkg.pkg_url }}
    {% if wbs_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ wbs_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ wbs_pkg.package_name }}-{{ wbs_pkg.version }}.tar.gz
    - require:
      - file: get_WxBarshop_web

# stop WxBarshop_web service
stop_{{ wbs.webb.svc_name }}_service:
  cmd.run:
    - name: initctl stop {{ wbs.webb.svc_name }} | exit 0
    - require:
      - file: /etc/init/{{ wbs.webb.svc_name }}.conf
      - file: get_WxBarshop_web

# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ wbs.webb.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ wbs_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ wbs_pkg.package_name }}/logs
    - require:
      - cmd: stop_{{ wbs.webb.svc_name }}_service

# install WxBarshop web
install_WxBarshop_web:
  cmd.run:
    - cwd: {{ wbs.webb.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ wbs_pkg.package_name }}-{{ wbs_pkg.version }}/* {{ wbs_pkg.package_name }}
        && source {{ wbs.webb.install_path }}/env/bin/activate
        && pip3 install -r {{ wbs.webb.install_path  }}/{{ wbs_pkg.package_name  }}/requirements.txt)
    - require:
      - file: get_WxBarshop_web
      - cmd: stop_{{ wbs.webb.svc_name }}_service

{% if wbs.web.get('settings', False) %}
{{ wbs.webb.install_path }}/{{ wbs_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ wbs.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ wbs.webb.svc_name }}_service
{% endif %}

{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(wbs.webb.install_path, wbs_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ wbs.webb.install_path }}
    - name: {{ wbs.webb.install_path }}/env/bin/pip3 install -r {{ wbs_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_WxBarshop_web
{{ wbs.webb.install_path }}/{{ wbs_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ wbs.webb.svc_name }}_service
{%- endif %}

restore_log_dir:
  cmd.run:
    - cwd: {{ wbs.webb.install_path }}
    - names:
      - (rm -rf {{ wbs_pkg.package_name }}/logs
        && mv -f backup/logs {{ wbs_pkg.package_name }})
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ wbs.webb.install_path }}/{{ wbs_pkg.package_name }}/logs:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir
      - cmd: stop_{{ wbs.webb.svc_name }}_service

# reload WxBarshop_Web service
start_{{ wbs.webb.svc_name }}_service:
  cmd.run:
    - name: initctl start {{ wbs.webb.svc_name }}
    - require:
      - file: /etc/init/{{ wbs.webb.svc_name }}.conf
      - cmd: install_WxBarshop_web
      {% if wbs.web.get('settings', False) %}
      - file: {{ wbs.webb.install_path }}/{{ wbs_pkg.package_name }}/settings.yaml
      {% endif %}
