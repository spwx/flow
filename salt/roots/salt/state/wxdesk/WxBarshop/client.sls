{% from "WxImageServer/vars.j2" import WxImageServer as wis with context %}

nfs.packages:
  pkg.installed:
    - pkgs:
        - nfs-utils

{{ wis.client.store_path }}:
  file.directory:
    - makedirs: True
