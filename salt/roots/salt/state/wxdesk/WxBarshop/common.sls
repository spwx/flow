{% from "WxBarshop/vars.j2" import WxBarshop as wbs with context %}
{% from "python3/vars.j2" import python3 with context %}

{{ wbs.group }}_group:
  group.present:
    - name: {{ wbs.group }}

{{ wbs.user }}_user:
  user.present:
    - name: {{ wbs.user }}
    - shell: /bin/bash
    - groups:
      - {{ wbs.group }}
    - require:
      - group: {{ wbs.group }}_group

/var/run/WxBarshop:
  file.directory:
    - user: {{ wbs.user }}
    - group: {{ wbs.group }}
    - makedirs: True
    - require:
      - user: {{ wbs.user }}_user
      - group: {{ wbs.group }}_group
