{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}
{% from "LwMarket/vars.j2" import LwMarket_web_pkg as lmt_web_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

# load config file
include:
  - .service

{{ lmt.web.log_dir }}:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - makedirs: True
    - recurse:
      - user
      - group

# create package directory
{{ lmt.web.install_path }}/{{ lmt_web_pkg.package_name }}:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - makedirs: True

# create backup directory
{{ lmt.web.install_path }}/backup:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - makedirs: True

# clean package directory
clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ lmt_web_pkg.package_name }}-*

# get LwMarket_web pkg
get_LwMarket_web:
  file.managed:
    - name: {{ src_dir }}/{{ lmt_web_pkg.package_name }}-{{ lmt_web_pkg.version }}.tar.gz
    - source: {{ lmt_web_pkg.pkg_url }}
    {% if lmt_web_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ lmt_web_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ lmt_web_pkg.package_name }}-{{ lmt_web_pkg.version }}.tar.gz
    - require:
      - file: get_LwMarket_web

# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ lmt.web.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ lmt_web_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ lmt_web_pkg.package_name }}/logs

# install LwMarket web
install_LwMarket_web:
  cmd.run:
    - cwd: {{ lmt.web.install_path }}
    - names:
      - ( \cp -rf {{ src_dir }}/{{ lmt_web_pkg.package_name }}-{{ lmt_web_pkg.version }}/* {{ lmt_web_pkg.package_name }}
        && source {{ lmt.web.install_path }}/env/bin/activate
        && pip3 install -r {{ lmt_web_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_LwMarket_web

{% if lmt.web.get('settings', False) %}
{{ lmt.web.install_path }}/{{ lmt_web_pkg.package_name }}/settings.yaml:
  file.serialize:
    - dataset: {{ lmt.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{% endif %}

# set deskcomponent settings.yaml
{%- if deskcommon.get('settings', False) %}
install_deskcomponent_dep:
  cmd.run:
    - cwd: {{ lmt.web.install_path }}
    - name: {{ lmt.web.install_path }}/env/bin/pip3 install -r {{ lmt_web_pkg.package_name }}/deskcomponent/requirements.txt
    - require:
      - cmd: install_LwMarket_web
{{ lmt.web.install_path }}/{{ lmt_web_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{%- endif %}


{%- if salt['file.directory_exists']('{0}/{1}/accountcenter'.format(lmt.web.install_path, lmt_web_pkg.package_name)) %}
install_accountcenter_dep:
  cmd.run:
    - cwd: {{ lmt.web.install_path }}
    - name: {{ lmt.web.install_path }}/env/bin/pip3 install -r {{ lmt_web_pkg.package_name }}/accountcenter/requirements.txt
    - require:
      - cmd: install_LwMarket_web
{{ lmt.web.install_path }}/{{ lmt_web_pkg.package_name }}/accountcenter/settings.yaml:
  file.serialize:
    - dataset: {{ wac.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{%- endif %}

restore_log_dir:
  cmd.run:
    - cwd: {{ lmt.web.install_path }}
    - names:
      - (rm -rf {{ lmt_web_pkg.package_name }}/logs
        && mv -f backup/logs {{ lmt_web_pkg.package_name }})
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ lmt.web.install_path }}/{{ lmt_web_pkg.package_name }}/logs:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir

# reload LwMarket_web service
start_{{ lmt.web.svc_name }}_service:
  cmd.run:
    - names: 
      - initctl start {{ lmt.web.svc_name }} || true
      - kill -HUP `ps -ef|grep /env/| grep -v grep | awk '{print $2 }'`;

    - require:
      - file: /etc/init/{{ lmt.web.svc_name }}.conf
      - cmd: install_LwMarket_web
      {% if lmt.web.get('settings', False) %}
      - file: {{ lmt.web.install_path }}/{{ lmt_web_pkg.package_name }}/settings.yaml
      {% endif %}
