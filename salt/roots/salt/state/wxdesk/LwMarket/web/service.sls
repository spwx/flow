{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}
{% set web = lmt.web %}

{{ web.install_path }}/{{ web.svc_name }}.ini:
  file.managed:
    - source: salt://LwMarket/web/files/web.ini.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644

/etc/init/{{ web.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: yanglinshan
        description: {{ web.svc_name }} daemon
        expect: daemon
        chdir: {{ web.install_path }}
        command: {{ web.install_path }}/env/bin/uwsgi --ini {{ web.install_path }}/{{ web.svc_name }}.ini --uid={{ lmt.user }} --gid={{ lmt.group }}
    - user: root
    - group: root
    - mode: '644'
    - require:
      - file: {{ web.install_path }}/{{ web.svc_name }}.ini
