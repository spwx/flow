{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/lightsupermarket-uwsgi.conf:
  file.managed:
    - source: salt://LwMarket/web/files/uwsgi.logrotate.j2
    - template: jinja
