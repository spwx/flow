{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}
{% from "LwMarket/vars.j2" import LwMarket_web_pkg as lmt_frontend_pkg with context %}

{% set src_dir = '/usr/local/src' %}

get_LwMarket_frontend_pkg:
  cmd.run:
    - cwd: {{ src_dir }}
      name: rm -rf {{ lmt_frontend_pkg.package_name }}-*
  file.managed:
    - name: {{ src_dir }}/{{ lmt_frontend_pkg.package_name }}-{{ lmt_frontend_pkg.version }}.tar.gz
    - source: {{  lmt_frontend_pkg.pkg_url }}
    {% if lmt_frontend_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ lmt_frontend_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: get_LwMarket_frontend_pkg

install_LwMarket_frontend_pkg:
  file.directory:
    - name: {{ lmt.frontend.install_path }}
    - makedirs: True
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - (tar -xzf {{ lmt_frontend_pkg.package_name }}-{{ lmt_frontend_pkg.version }}.tar.gz 
        && \cp -Rf {{ lmt_frontend_pkg.package_name }}-{{ lmt_frontend_pkg.version }}/* {{ lmt.frontend.install_path }})
    - require:
      - file: install_LwMarket_frontend_pkg
      - file: get_LwMarket_frontend_pkg
