{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}

{{ lmt.group }}_group:
  group.present:
    - name: {{ lmt.group }}

{{ lmt.user }}_user:
  user.present:
    - name: {{ lmt.user }}
    - shell: /bin/bash
    - groups:
      - {{ lmt.group }}
    - require:
      - group: {{ lmt.group }}_group

/var/run/LwMarket:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - makedirs: True
    - require:
      - user: {{ lmt.user }}_user
      - group: {{ lmt.group }}_group
