{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}

nfs.packages:
  pkg.installed:
    - pkgs:
        - nfs-utils

{{ lmt.client.store_path }}:
  file.directory:
    - makedirs: True

/etc/fstab:
  file.append:
    - text:
      - {{ lmt.client.server.ip }}:{{ lmt.client.server.store_path }} {{ lmt.client.store_path }} nfs rsize=8192,wsize=8192,timeo=14,soft,intr
    - require:
      - file: {{ lmt.client.store_path }}

mount_nfs:
  cmd.run:
    - names:
      - (umount {{ lmt.client.store_path }} 2>1 &1>/dev/null
        && mount -t nfs -o rw,soft {{ lmt.client.server.ip }}:{{ lmt.client.server.store_path }} {{ lmt.client.store_path }})