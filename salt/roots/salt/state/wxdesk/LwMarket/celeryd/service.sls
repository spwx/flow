{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}
{% from "LwMarket/vars.j2" import LwMarket_web_pkg as lmt_web_pkg with context %}

{% set celery = lmt.celeryd %}

/etc/init/{{ lmt.celeryd.svc_name }}.conf:
  file.managed:
    - source: salt://python3/files/upstart.conf.j2
    - template: jinja
    - defaults:
        author: huangqiyin
        description: celery worker daemon
        chdir: {{ celery.install_path }}/{{ lmt_web_pkg.package_name }}
        command: {{ celery.install_path }}/env/bin/python -m celery.__main__ -l info -A supermarket worker --time-limit=300 -c {{ celery.concurrency }} --uid={{ lmt.user }} --gid={{ lmt.group }} --logfile {{ celery.log_dir }}/{{ celery.svc_name }}.log
    - user: root
    - group: root
    - mode: '644'
