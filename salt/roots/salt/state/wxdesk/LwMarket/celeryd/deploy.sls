{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}
{% from "LwMarket/vars.j2" import LwMarket_web_pkg as celeryd_pkg with context %}
{% from "python3/vars.j2" import python3 with context %}
{% from "deskcomponent/vars.j2" import deskcomponent as deskcommon with context %}

{% set src_dir = '/usr/local/src' %}


include:
  - .service

{{ lmt.celeryd.log_dir }}:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - makedirs: True
    - recurse:
      - user
      - group

{{ lmt.celeryd.install_path }}/{{ celeryd_pkg.package_name }}:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - makedirs: True

# create backup directory
{{ lmt.celeryd.install_path }}/backup:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - makedirs: True

clean_temp_package:
  cmd.run:
    - cwd: {{ src_dir }}
    - name: rm -rf {{ celeryd_pkg.package_name }}-*

get_{{ celeryd_pkg.package_name }}:
  file.managed:
    - name: {{ src_dir }}/{{ celeryd_pkg.package_name }}-{{ celeryd_pkg.version }}.tar.gz
    - source: {{ celeryd_pkg.pkg_url }}
    {% if celeryd_pkg.get('pkg_checksum', false) %}
    - source_hash: {{ celeryd_pkg.pkg_checksum }}
    {% endif %}
    - require:
      - cmd: clean_temp_package
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ celeryd_pkg.package_name }}-{{ celeryd_pkg.version }}.tar.gz
    - require:
      - file: get_{{ celeryd_pkg.package_name }}

# stop svc_name
stop_{{ lmt.celeryd.svc_name }}_service:
  cmd.run:
    - name: initctl stop {{ lmt.celeryd.svc_name }} | exit 0
    - require:
      - file: /etc/init/{{ lmt.celeryd.svc_name }}.conf
      - file: get_{{ celeryd_pkg.package_name }}

# backup logs directory
backup_logs_dir:
  cmd.run:
    - cwd: {{ lmt.celeryd.install_path }}
    - names:
      - (rm -rf backup/logs && mv {{ celeryd_pkg.package_name }}/logs backup/)
    - onlyif: test -d {{ celeryd_pkg.package_name }}/logs
    - require:
      - cmd: stop_{{ lmt.celeryd.svc_name }}_service

install_celeryd:
  cmd.run:
    - cwd: {{ lmt.celeryd.install_path }}
    - names:
      - (rm -rf {{ celeryd_pkg.package_name }}/*
        && \cp -rf {{ src_dir }}/{{ celeryd_pkg.package_name }}-{{ celeryd_pkg.version }}/* {{ celeryd_pkg.package_name }}
        && source {{ lmt.celeryd.install_path }}/env/bin/activate
        && pip3 install -r {{ celeryd_pkg.package_name }}/requirements.txt)
    - require:
      - file: get_{{ celeryd_pkg.package_name }}
      - cmd: stop_{{ lmt.celeryd.svc_name }}_service

{% if lmt.web.settings|default(false) %}
{{ lmt.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/settings.yaml:
  file.serialize:
    - name:
    - dataset: {{ lmt.web.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ lmt.celeryd.svc_name }}_service
{% endif %}

# set deskcomponent settings.yaml
{%- if deskcommon.get('settings', False) %}
install_deskcomponent_dep:
  cmd.run:
    - cwd: {{ lmt.celeryd.install_path }}
    - name: {{ lmt.celeryd.install_path }}/env/bin/pip3 install -r {{ celeryd_pkg.package_name }}/deskcomponent/requirements.txt
    - require:
      - cmd: install_celeryd
{{ lmt.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/{{ deskcommon.path }}/settings.yaml:
  file.serialize:
    - dataset: {{ deskcommon.settings }}
    - formatter: yaml
    - backup: True
    - show_diff: True
    - require:
      - cmd: stop_{{ lmt.celeryd.svc_name }}_service
{%- endif %}

disable_uwsgi:
  file.replace:
    - name: {{ lmt.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/settings.yaml
    - pattern: |
        ENABLE_UWSGI: .*
    - repl: |
        ENABLE_UWSGI: False
    - append_if_not_found: True
    - backup: True
    - require:
      - cmd: stop_{{ lmt.celeryd.svc_name }}_service

restore_log_dir:
  cmd.run:
    - cwd: {{ lmt.celeryd.install_path }}
    - names:
      - (rm -rf {{ celeryd_pkg.package_name }}/logs
        &&mv -f backup/logs {{ celeryd_pkg.package_name }} )
    - onlyif: test -d backup/logs
    - require:
      - cmd: backup_logs_dir

{{ lmt.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/logs:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - cmd: restore_log_dir
      - cmd: stop_{{ lmt.celeryd.svc_name }}_service

# start svc_name
start_{{ lmt.celeryd.svc_name }}_service:
  cmd.run:
    - name: initctl start {{ lmt.celeryd.svc_name }}
    - require:
      - file: /etc/init/{{ lmt.celeryd.svc_name }}.conf
      - cmd: install_celeryd
    {% if lmt.web.get('settings', False) %}
      - file: {{ lmt.celeryd.install_path }}/{{ celeryd_pkg.package_name }}/settings.yaml
    {% endif %}
