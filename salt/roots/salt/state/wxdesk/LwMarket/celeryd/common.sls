{% from "LwMarket/vars.j2" import LwMarket as lmt with context %}
{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}

include:
  - LwMarket.common

{{ lmt.celeryd.install_path }}:
  file.directory:
    - user: {{ lmt.user }}
    - group: {{ lmt.group }}
    - makedirs: True
    - require:
      - user: {{ lmt.user }}_user
      - group: {{ lmt.group }}_group

create_env:
  cmd.run:
    - cwd: {{ lmt.celeryd.install_path }}
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ lmt.celeryd.install_path }}
