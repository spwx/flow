{% from "zabbix_agent/vars.j2" import zabbix_agent with context %}

# https://www.mjmwired.net/kernel/Documentation/block/stat.txt

{{ zabbix_agent.ScriptsDir }}/diskPerf:
  file.directory:
    - makedirs: True

{{ zabbix_agent.ScriptsDir }}/diskPerf/lld-disks.py:
  file.managed:
    - source: salt://diskPerf/files/lld-disks.py
    - user: root
    - group: root
    - mode: 755
    - require:
      - file: {{ zabbix_agent.ScriptsDir }}/diskPerf

{{ zabbix_agent.ScriptsDir }}/diskPerf/lld-disks.json:
  file.managed:
    - source: salt://diskPerf/files/lld-disks.json.j2
    - template: jinja
    - require:
      - file: {{ zabbix_agent.ScriptsDir }}/diskPerf

{{ zabbix_agent.IncludeDir }}/diskPerf.conf:
  file.managed:
    - source: salt://diskPerf/files/diskstats.conf.j2
    - template: jinja
    - require:
      - file: {{ zabbix_agent.ScriptsDir }}/diskPerf/lld-disks.py

restart_zabbix_agent:
  cmd.run:
    - name: service {{ zabbix_agent.svc_name }} restart
    - require:
      - file: {{ zabbix_agent.IncludeDir }}/diskPerf.conf
