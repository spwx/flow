{% from "zbxMongo/vars.j2" import zbxMongo with context %}
{% from "zabbix_agent/vars.j2" import zabbix_agent as zbxAgent with context %}
{% from "python3/vars.j2" import python3 with context %}

{{ zbxAgent.ScriptsDir }}/zbxMongo:
  file.directory:
    - makedirs: True

create_env:
  cmd.run:
    - cwd: {{ zbxAgent.ScriptsDir }}/zbxMongo
    - name: {{ python3.install_prefix }}/bin/pyvenv env
    - unless: test -d env
    - require:
      - file: {{ zbxAgent.ScriptsDir }}/zbxMongo
  
{{ zbxAgent.ScriptsDir }}/zbxMongo/zbx_mongo.py:
  file.managed:
    - source: salt://zbxMongo/files/zbx_mongo.py
    - user: root
    - group: root
    - mode: 755
    - require:
      - file: {{ zbxAgent.ScriptsDir }}/zbxMongo
  cmd.run:
    - cwd: {{ zbxAgent.ScriptsDir }}/zbxMongo
    - names: 
      - (source {{ zbxAgent.ScriptsDir }}/zbxMongo/env/bin/activate
        && pip3 install pymongo && pip3 install bson)
    - require:
      - cmd: create_env
      - file: {{ zbxAgent.ScriptsDir }}/zbxMongo/zbx_mongo.py

{{ zbxAgent.IncludeDir }}/zbxMongo.conf:
  file.managed:
    - source: salt://zbxMongo/files/zbx_mongo.conf.j2
    - template: jinja
    - require:
      - file: {{ zbxAgent.ScriptsDir }}/zbxMongo/zbx_mongo.py

restart_zabbix_agent:
  cmd.run:
    - name: service {{ zbxAgent.svc_name }} restart
    - require:
      - file: {{ zbxAgent.IncludeDir }}/zbxMongo.conf
