{% from "zbxRedis/vars.j2" import zbxRedis with context %}
{% from "zabbix_agent/vars.j2" import zabbix_agent with context %}

{{ zabbix_agent.ScriptsDir }}/zbxRedis:
  file.directory:
    - makedirs: True

{{ zabbix_agent.ScriptsDir }}/zbxRedis/lld-redis.json:
  file.managed:
    - source: salt://zbxRedis/files/lld-redis.json.j2
    - template: jinja
    - require:
      - file: {{ zabbix_agent.ScriptsDir }}/zbxRedis
  
{{ zabbix_agent.ScriptsDir }}/zbxRedis/zbx_redis.sh:
  file.managed:
    - source: salt://zbxRedis/files/zbx_redis.sh
    - user: root
    - group: root
    - mode: 755
    - require:
      - file: {{ zabbix_agent.ScriptsDir }}/zbxRedis

{{ zabbix_agent.IncludeDir }}/zbxRedis.conf:
  file.managed:
    - source: salt://zbxRedis/files/zbx_redis.conf.j2
    - template: jinja
    - require:
      - file: {{ zabbix_agent.ScriptsDir }}/zbxRedis/zbx_redis.sh

restart_zabbix_agent:
  cmd.run:
    - name: service {{ zabbix_agent.svc_name }} restart
    - require:
      - file: {{ zabbix_agent.IncludeDir }}/zbxRedis.conf
