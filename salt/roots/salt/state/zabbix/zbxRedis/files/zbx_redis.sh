#!/bin/bash

REDISHOST=127.0.0.1
REDISPORT=6379
GETTYPE=cpu

while getopts h:p:t: OPTION
do
    case "${OPTION}"
    in
        h)
            REDISHOST=${OPTARG};;
        p)
            REDISPORT=${OPTARG};;
        t)
            GETTYPE=${OPTARG};;
        \?)
            echo "USAGE: args -t cpu/mem/pid -h 127.0.0.1 -p 6379"
            exit 1;;
    esac
done

if [ $GETTYPE = 'cpu' ]
then
    ps axo %cpu,command | grep ${REDISHOST}:${REDISPORT} | grep -v grep | awk -F " " '{print $1}'
    exit $?
fi

if [ $GETTYPE = 'mem' ]
then
    ps axo rss,command | grep ${REDISHOST}:${REDISPORT} | grep -v grep | awk -F " " '{print $1}' | xargs expr 1024 \*
    exit $?
fi

if [ $GETTYPE = 'pid' ]
then
    ps axo pid,command | grep ${REDISHOST}:${REDISPORT} | grep -v grep | awk -F " " '{print $1}'
    exit $?
fi

echo "usage: args -t cpu/mem/pid -h 127.0.0.1 -p 6379"
exit 1
