#!/bin/bash
#function +++++++++++++zabbix_proxy for centos6 auto install scripts+++++++++++++++++++
#author sucre
#version 1.0 
#date 2013-10-24 

#version 1.1 
#date 2014-05-05
#增加iptables设置

#version 1.2
#date 2016-06-21
#更新为zabbix3.0

#mail sucre800@qq.com 
#转载请注明http://mjjwu.blog.51cto.com，谢谢合作

green='\e[0;32m'
red='\e[0;31m'
blue='\e[0;36m'
blue1='\e[5;31m'
NC='\e[0m'
path_soft=$(pwd)


function base(){
yum -y install  make gcc gcc-c++ autoconf  net-snmp-devel curl-devel OpenIPMI-devel vim

ls /etc/zabbix/process.conf
  if [ $? -eq 0 ]
  then
        mv /etc/zabbix/process.conf /etc/zabbix/zabbix_agentd.conf.d/old.process.conf
  fi

/etc/init.d/zabbix_agentd stop

tar zcvf /tmp/zabbix.old.tar.gz /etc/zabbix/*
mv /etc/zabbix /etc/old.zabbix
mv /usr/local/zabbix /usr/local/old.zabbix


}

function install_zabbix_agent(){

wget http://downloads.js-ops.com/zabbix/zabbix-3.0.3.tar.gz

groupadd zabbix -g 201
useradd -g zabbix -u 201 -m zabbix
tar xvf zabbix-3.0.3.tar.gz
cd zabbix-3.0.3

make clean

./configure --prefix=/usr/local/zabbix  --enable-agent
make&&make install

cat >> /etc/services << EOF
zabbix-agent 10050/tcp #Zabbix Agent
zabbix-agent 10050/udp #Zabbix Agent
zabbix-trapper 10051/tcp #Zabbix Trapper
zabbix-trapper 10051/udp #Zabbix Trappe
EOF


mkdir /var/log/zabbix
chown zabbix.zabbix /var/log/zabbix
ln -s /usr/local/zabbix/etc /etc/zabbix
ln -s /usr/local/zabbix/bin/* /usr/bin/
ln -s /usr/local/zabbix/sbin/* /usr/sbin/

cp misc/init.d/fedora/core/zabbix_* /etc/init.d/
chmod 755 /etc/init.d/zabbix_*
sed -i "s#BASEDIR=/usr/local#BASEDIR=/usr/local/zabbix#g" /etc/init.d/zabbix_agentd


sed -i "s/Server\=127.0.0.1/Server\=122.224.184.216,172.30.236.82/g" /etc/zabbix/zabbix_agentd.conf
#sed -i "s/ServerActive\=127.0.0.1/ServerActive\=172.30.1.170:10051/g" /etc/zabbix/zabbix_agentd.conf
sed -i "s#tmp/zabbix_agentd.log#var/log/zabbix/zabbix_agentd.log#g" /etc/zabbix/zabbix_agentd.conf
sed -i "/UnsafeUserParameters=0/aUnsafeUserParameters=1\n" /etc/zabbix/zabbix_agentd.conf
sed -i '/Include=\/usr\/local\/etc\/zabbix_agentd.conf.d\/\*/aInclude=\/etc\/zabbix\/scripts\/\*.conf' /etc/zabbix/zabbix_agentd.conf


mkdir -p /etc/zabbix/alertscripts
mkdir -p /etc/zabbix/scripts
\cp -rf /etc/old.zabbix/alertscripts/* /etc/zabbix/alertscripts/
\cp -rf /etc/old.zabbix/scripts/* /etc/zabbix/scripts/
\cp -rf /etc/old.zabbix/zabbix_agentd.conf.d/* /etc/zabbix/scripts/

start zabbix serivce
chkconfig zabbix_agentd on
service zabbix_agentd restart


}

function iptables (){
localip=`ifconfig  |awk  -F'addr:|  Bcast'   '/Bcast/{print $2}'`
/sbin/iptables -I INPUT -s 122.224.184.0/23 -d $localip  -p tcp --dport 10050 -j ACCEPT 
/sbin/iptables-save | cat >/etc/sysconfig/iptables
}

function main (){
base
install_zabbix_agent
iptables
}

main
