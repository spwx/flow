{% from "zabbix_agent/vars.j2" import zabbix_agent with context %}

{{ zabbix_agent.IncludeDir }}:
  file.directory:
    - makedirs: True
/etc/zabbix/zabbix_agentd.conf:
  file.managed:
    - source: salt://zabbix_agent/files/zabbix_agentd.conf.j2
    - template: jinja
    - require:
      - file: {{ zabbix_agent.IncludeDir }}
zabbix_agent_service:
  cmd.run:
    - name: chkconfig --add {{zabbix_agent.svc_name }}
    - unless: chkconfig --list | grep {{ zabbix_agent.svc_name }}
  service.running:
    - name: {{ zabbix_agent.svc_name }}
    - enable: True
    - require:
      - file: /etc/zabbix/zabbix_agentd.conf
    - watch:
      - file: /etc/zabbix/zabbix_agentd.conf
