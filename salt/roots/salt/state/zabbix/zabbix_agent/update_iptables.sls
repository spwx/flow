{% set device_info = grains['ip_interfaces'].get('eth0', grains['ip_interfaces']['eth1']) %}
{% set local_ip = device_info[0] %}

{% if local_ip.find('221.228.80') != -1 %}
{% set zbx_ip = '122.224.184.216' %}
{% else %}
{% set zbx_ip = '172.30.236.82' %}
{% endif %}

add-zbx-rule:
  cmd.run:
    - name: iptables -A INPUT -s {{ zbx_ip }} -d {{ local_ip }} -p tcp -m tcp --dport 10050 -j ACCEPT
    #- name: iptables -A INPUT -s 172.30.236.82 -d 127.0.0.1 -p tcp -m tcp --dport 10050 -j ACCEPT

save-rules:
  cmd.run: 
    - name: /etc/init.d/iptables save
    - require:
      - cmd: add-zbx-rule

