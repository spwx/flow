{% from "zabbix_agent/vars.j2" import zabbix_agent as zbxAgent with context %}

update-zbxConfig:
  file.replace:
    - name: /etc/zabbix/zabbix_agentd.conf
    - pattern: |
        ^Server=.*
    - repl:
        Server=122.224.184.216,172.30.236.82

restart_zabbix_agent:
  cmd.run:
    - name: service {{ zbxAgent.svc_name }} restart
    - require:
      - file: update-zbxConfig
