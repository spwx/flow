{% set zabbix_agent_pkg = "zabbix-agent-3.0.5-1.el6.x86_64.rpm" %}
{% set zabbix_get_pkg = "zabbix-get-3.0.5-1.el6.x86_64.rpm" %}

/usr/local/src/{{ zabbix_agent_pkg }}:
  file.managed:
    - source: salt://zabbix_agent/files/{{ zabbix_agent_pkg }}
  cmd.run:
    - name: yum install -y /usr/local/src/{{ zabbix_agent_pkg }}
    - unless: rpm -qa | grep zabbix-agent
    - require:
      - file: /usr/local/src/{{ zabbix_agent_pkg }}

/usr/local/src/{{ zabbix_get_pkg }}:
  file.managed:
    - source: salt://zabbix_agent/files/{{ zabbix_get_pkg }}
  cmd.run:
    - name: yum install -y /usr/local/src/{{ zabbix_get_pkg }}
    - unless: rpm -qa | grep zabbix-get
    - require:
      - file: /usr/local/src/{{ zabbix_get_pkg }}
