#!/bin/bash

NGINX_FULL_STATUS_JSON_FILE=/tmp/full_status.json

while getopts p:k: OPTION
do
  case "${OPTION}"
  in
    p)
      JSON_URL=${OPTARG};;
    k)
      MONITOR_KEY=${OPTARG};;
    ?)
      #echo "USAGE: args -p {#FULL_STATUS_URL} -k conn_active/conn_reading"
      exit -1;;
  esac
done

if [ x${JSON_URL} = 'x']
then
  #echo "-p argument not conf"
  exit -1
fi

if [ x${MONITOR_KEY}  = 'x' ]
then
  #echo "-k argument not conf"
  exit -1
fi

wget ${JSON_URL} -O ${NGINX_FULL_STATUS_JSON_FILE}

if [ ! -f ${NGINX_FULL_STATUS_JSON_FILE} ]
then
  #echo "get full status json data error"
  exit -1
fi

case ${MONITOR_KEY} in
  conn_active)
    jq ".connections.active" ${NGINX_FULL_STATUS_JSON_FILE};;
  conn_reading)
    jq ".connections.reading" ${NGINX_FULL_STATUS_JSON_FILE};;
  conn_writing)
    jq ".connections.waiting" ${NGINX_FULL_STATUS_JSON_FILE};;
  req_accepted)
    jq ".connections.accepted" ${NGINX_FULL_STATUS_JSON_FILE};;
  req_handled)
    jq ".connections.handled" ${NGINX_FULL_STATUS_JSON_FILE};;
  req_total)
    jq ".connections.requests" ${NGINX_FULL_STATUS_JSON_FILE};;
  *)
    echo -1;;  
esac
