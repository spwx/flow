#!/bin/bash

PROCNAME=nginx
GETTYPE=cpu

while getopts t: OPTION
do
    case "${OPTION}"
    in
        t)
            GETTYPE=${OPTARG};;
        \?)
            echo "USAGE: args -t cpu/mem/pid "
            exit 1;;
    esac
done

if [ $GETTYPE = 'cpu' ]
then
    ps axo %cpu,command | grep ${PROCNAME}} | grep -v grep | awk -F " " '{print $1}' | awk '{sum +=$1};END {print sum}'
    exit $?
fi

if [ $GETTYPE = 'mem' ]
then
    ps axo rss,command | grep ${PROCNAME} | grep -v grep | awk -F " " '{print $1}' | awk '{sum +=$1};END {print sum}' | xargs expr 1024 \*
    exit $?
fi

if [ $GETTYPE = 'pid' ]
then
    ps axo pid,command | grep ${PROCNAME} | grep -v grep | awk -F " " '{print $1}' | head -1
    exit $?
fi

echo "usage: args -t cpu/mem/pid"
exit 1
