{% from "zbxNginx/vars.j2" import zbxNginx with context %}
{% from "zabbix_agent/vars.j2" import zabbix_agent as zbxAgent with context %}

install-tools:
  pkg.installed:
    - names:
      - jq

{{ zbxAgent.ScriptsDir }}/zbxNginx:
  file.directory:
    - makedirs: True

{{ zbxAgent.ScriptsDir }}/zbxNginx/lld-url.json:
  file.managed:
    - source: salt://zbxNginx/files/lld-url.json.j2
    - template: jinja
    - require:
      - file: {{ zbxAgent.ScriptsDir }}/zbxNginx

{{ zbxAgent.ScriptsDir }}/zbxNginx/zbx_nginx.sh:
  file.managed:
    - source: salt://zbxNginx/files/zbx_nginx.sh
    - mode: '0755'
    - require:
      - file: {{ zbxAgent.ScriptsDir }}/zbxNginx

{{ zbxAgent.ScriptsDir }}/zbxNginx/zbx_nginx_full_status.sh:
  file.managed:
    - source: salt://zbxNginx/files/zbx_nginx_full_status.sh
    - mode: '0755'
    - require:
      - file: {{ zbxAgent.ScriptsDir }}/zbxNginx

{{ zbxAgent.IncludeDir }}/zbxNginx.conf:
  file.managed:
    - source: salt://zbxNginx/files/zbx_nginx.conf.j2
    - template: jinja
    - require:
      - file: {{ zbxAgent.ScriptsDir }}/zbxNginx/zbx_nginx.sh
      - file: {{ zbxAgent.ScriptsDir }}/zbxNginx/zbx_nginx_full_status.sh

restart_zabbix_agent:
  cmd.run:
    - name: service {{ zbxAgent.svc_name }} restart
    - require:
      - file: {{ zbxAgent.IncludeDir }}/zbxRedis.conf
