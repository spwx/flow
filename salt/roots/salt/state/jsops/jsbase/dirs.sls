{% set jsops_dirs = ['SicentTools', 'SicentApp', 'SicentWebserver', 'SicentWebsite', 'SicentScripts'] %}

{% for data_dir in jsops_dirs %}
/data/{{ data_dir }}:
  file.directory:
    - user: root
    - group: root
    - makedirs: True
{% endfor %}