iptables:
  service.dead:
  - enable: False

/etc/sysconfig/selinux:
  file.replace:
    - pattern: ^SELINUX=enforcing
    - repl: SELINUX=disable
    - backup: True

{% if pillar.get('hostname', false) %}
/etc/sysconfig/network:
  file.replace:
    - pattern: ^HOSTNAME=.*
    - repl: HOSTNAME={{ pillar.get('hostname') }}
    - backup: True
  cmd.run:
    - name: hostname {{ pillar.get('hostname') }}
    - require:
      - file: /etc/sysconfig/network
{% endif %}