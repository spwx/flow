{% if not salt['file.file_exists']('/etc/yum.repos.d/CentOS-Base.repo') %}
/etc/yum.repos.d/CentOS-Base.repo:
  file.managed:
    - source: salt://jsops/files/CentOS-Base.repo
    - user: root
    - group: root
{% endif %}

install_depend_tools:
  pkg.installed:
    - pkgs:
      - make
      - wget
      - perl
      - gcc
      - gcc-c++
      #- gcc-g77
      - ncurses
      - ncurses-base
      - ncurses-devel
      - ncurses-libs
      - ncurses-static
      - ncurses-term
      - telnet
      - ftp
      - setuptool
      - ntsysv
      - system-config-firewall-tui
      - system-config-network-tui
      - sysstat
      - mlocate
      - ntp
      - ntpdate
