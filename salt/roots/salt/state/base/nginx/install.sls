{% from 'nginx/vars.j2' import nginx with context %}

{% set src_dir = '/usr/local/src' %}
{% set nginx_pkg = "{0}/nginx-{1}.tar.gz".format(src_dir, nginx.version) %}
{% set nginx_src = "{0}/nginx-{1}".format(src_dir, nginx.version) %}
{% set nginx_modules_dir = '{0}/nginx-modules'.format(src_dir) %}
{% set base_temp_dir = '/tmp' %}

{% set ng_mod_vts = 'nginx-module-vts-0.1.11' %}

include:
  - nginx.common

{{ nginx_modules_dir }}:
  file.directory:
    - makedirs: True

get_nginx:
  pkg.installed:
    - names:
      - pcre-devel
      - zlib-devel
      - openssl-devel
      - gd-devel
  file.managed:
    - name: {{ nginx_pkg }}
    - source: {{ nginx.pkg_url }}
    {% if nginx.pkg_checksum != '' %}
    - source_hash: {{ nginx.pkg_checksum }}
    {% endif %}
    - require:
      - file: {{ nginx_modules_dir }}
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -zvxf {{ nginx_pkg }}
    - require:
      - pkg: get_nginx
      - file: get_nginx

get_nginx-module-vts:
  file.managed:
    - name: {{ nginx_modules_dir }}/{{ ng_mod_vts }}.tar.gz
    - source: salt://nginx/files/{{ ng_mod_vts }}.tar.gz
  cmd.run:
    - cwd: {{ nginx_modules_dir }}
    - name: tar zvxf {{ ng_mod_vts }}.tar.gz
    - require:
      - file: get_nginx-module-vts

nginx_install:
  cmd.run:
    - cwd: {{ nginx_src }}
    - names:
      - (
        ./configure --conf-path={{ nginx.conf_dir }}/{{ nginx.svc_name }}.conf
        --sbin-path={{ nginx.install_prefix }}/sbin/nginx
        --user={{ nginx.user }}
        --group={{ nginx.group }}
        --prefix={{ nginx.install_prefix }}
        --http-log-path={{ nginx.log_dir }}/access.log
        --error-log-path={{ nginx.log_dir }}/error.log
        --pid-path=/var/run/nginx/{{ nginx.svc_name }}.pid
        --lock-path=/var/lock/subsys/{{ nginx.svc_name }}
        --http-client-body-temp-path={{ base_temp_dir }}/body
        --http-proxy-temp-path={{ base_temp_dir }}/proxy
        --http-fastcgi-temp-path={{ base_temp_dir }}/fastcgi
        --http-uwsgi-temp-path={{ base_temp_dir }}/temp_uwsgi
        --with-stream
        --http-scgi-temp-path={{ base_temp_dir }}/temp_scgi
        --add-module={{ nginx_modules_dir }}/{{ ng_mod_vts }}
        {%- for name in nginx.with %}
        --with-{{ name }}
        {%- endfor %}
        {%- for name in nginx.without %}
        --without-{{ name }}
        {%- endfor %}
        && make {{ nginx.make_flags }}
        && make install
        )
        {#- If they want to silence the compiler output, then save it to file so we can reference it later if needed #}
        {%- if nginx.get('silence_compiler', true) %}
        > {{ nginx_src }}/build.out 2> {{ nginx_src }}/build.err;
        {#- If the build process failed, write stderr to stderr and exit with the error code #}
        r=$?;
        if [ x$r != x0 ]; then
          cat {{ nginx_src }}/build.err 1>&2;  {#- copy err output to stderr #}
          exit $r;
        fi;
        {% endif %}
    - require:
      - cmd: get_nginx
      - cmd: get_nginx-module-vts
  file.append:
    - name: /etc/profile
    - text:
      - export PATH={{ nginx.install_prefix }}/sbin:$PATH
    - require:
      - cmd: nginx_install
