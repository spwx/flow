{% from "log_artifact/vars.j2" import log_artifact with context %}

include:
  - log_artifact

{% if log_artifact.get('templogs', false) %}
/data/SicentTools/deploy/push_nginx_log.sh:
  file.managed:
    - source: salt://nginx/files/push_log.sh.j2
    - template: jinja
    - defaults:
        templogs: {{ log_artifact.get('templogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root

{% endif %}

{% if log_artifact.get('everydaylogs', false) %}
/data/SicentScripts/everyday_push_nginx_log.sh:
  file.managed:
    - source: salt://nginx/files/everyday_push_log.sh.j2
    - template: jinja
    - defaults:
        everydaylogs: {{ log_artifact.get('everydaylogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root
{% endif %}
