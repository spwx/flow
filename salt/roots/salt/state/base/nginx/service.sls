{% from 'nginx/vars.j2' import nginx with context %}

{{ nginx.conf_dir }}/{{ nginx.svc_name }}.conf:
  file.managed:
    - source: salt://nginx/files/nginx.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644

{{ nginx.conf_dir }}/conf.d/gzip.conf:
  file.managed:
    - source: salt://nginx/files/conf.d/gzip.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: {{ nginx.conf_dir }}/conf.d
    - context:
        nginx: {{ nginx }}

/etc/init.d/{{ nginx.svc_name }}:
  file.managed:
    - name: /etc/init.d/{{ nginx.svc_name }}
    - template: jinja
    - source: salt://nginx/files/nginx.init.j2
    - user: root
    - group: root
    - mode: '0755'
  cmd.run:
    - name: chkconfig --add {{ nginx.svc_name }}
    - unless: chkconfig --list | grep {{ nginx.svc_name }}
    - require:
      - file: /etc/init.d/{{ nginx.svc_name }}
  service.running:
    - name: {{ nginx.svc_name }}
    - enable: True
    - reload: True
    - watch:
      - file: {{ nginx.conf_dir }}/{{ nginx.svc_name }}.conf
    - require:
      - file: /etc/init.d/{{ nginx.svc_name }}
      - file: {{ nginx.conf_dir }}/conf.d/gzip.conf