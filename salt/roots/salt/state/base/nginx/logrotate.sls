{% from "nginx/vars.j2" import nginx with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/{{ nginx.svc_name }}.conf:
  file.managed:
    - source: salt://nginx/files/logrotate.conf.j2
    - template: jinja