{% from "nginx/vars.j2" import nginx with context %}

{{ nginx.group }}_group:
  group.present:
    - name: {{ nginx.group }}

{{ nginx.user }}_user:
  file.directory:
    - name: {{ nginx.home }}
    - user: {{ nginx.user }}
    - group: {{ nginx.group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - user: {{ nginx.user }}_user
      - group: {{ nginx.group }}_group
  user.present:
    - name: {{ nginx.user }}
    - home: {{ nginx.home }}
    - shell: /bin/false
    - groups:
      - {{ nginx.group }}
    - require:
      - group: {{ nginx.group }}_group

{{ nginx.conf_dir }}:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{{ nginx.conf_dir }}/conf.d:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

/var/run/nginx:
  file.directory:
    - user: {{ nginx.user }}
    - group: {{ nginx.group }}
    - makedirs: True
    - require:
      - user: {{ nginx.user }}_user
      - group: {{ nginx.group }}_group

{{ nginx.log_dir }}:
  file.directory:
    - user: {{ nginx.user }}
    - group: {{ nginx.group }}
    - makedirs: True
    - require:
      - user: {{ nginx.user }}_user
      - group: {{ nginx.group }}_group