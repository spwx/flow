
/var/lib/pgsql/9.4/data/postgresql.conf:
  file.managed:
    - source: salt://postgresql/files/postgresql.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644

/var/lib/pgsql/9.4/data/pg_hba.conf:
  file.managed:
    - source: salt://postgresql/files/pg_hba.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
