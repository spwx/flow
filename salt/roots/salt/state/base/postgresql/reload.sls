
{% set src_dir = '/usr/local/src' %}

include:
  - postgresql.common


postgresql_restart:
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - service postgresql-9.4 restart;
