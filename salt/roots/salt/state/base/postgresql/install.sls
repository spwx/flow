{% from 'postgresql/vars.j2' import postgresql with context %}

{% set src_dir = '/usr/local/src' %}
{% set base_temp_dir = '/tmp' %}


include:
  - postgresql.common


postgresql_install:
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - yum install https://download.postgresql.org/pub/repos/yum/9.4/redhat/rhel-6-x86_64/pgdg-centos94-9.4-2.noarch.rpm -y;
      # - yum clean all
      # - yum list postgresql*
      - yum install postgresql94 -y;
      - yum install postgresql94-server -y;
      - yum install  postgresql-plpython -y;
      - yum install postgresql94-devel postgresql94-libs postgresql94-contrib  -y;
      - service postgresql-9.4 initdb;
      - chkconfig postgresql-9.4 on;
      - service postgresql-9.4 start;
      

