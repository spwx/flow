{% from "postgresql/vars.j2" import postgresql with context %}

{{ postgresql.group }}_group:
  group.present:
    - name: {{ postgresql.group }}

{{ postgresql.user }}_user:
  file.directory:
    - name: {{ postgresql.home }}
    - user: {{ postgresql.user }}
    - group: {{ postgresql.group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - user: {{ postgresql.user }}_user
      - group: {{ postgresql.group }}_group
  user.present:
    - name: {{ postgresql.user }}
    - home: {{ postgresql.home }}
    - shell: /bin/false
    - groups:
      - {{ postgresql.group }}
    - require:
      - group: {{ postgresql.group }}_group

{{ postgresql.conf_dir }}:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{{ postgresql.conf_dir }}/conf.d:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

/var/run/postgresql:
  file.directory:
    - user: {{ postgresql.user }}
    - group: {{ postgresql.group }}
    - makedirs: True
    - require:
      - user: {{ postgresql.user }}_user
      - group: {{ postgresql.group }}_group

{{ postgresql.log_dir }}:
  file.directory:
    - user: {{ postgresql.user }}
    - group: {{ postgresql.group }}
    - makedirs: True
    - require:
      - user: {{ postgresql.user }}_user
      - group: {{ postgresql.group }}_group