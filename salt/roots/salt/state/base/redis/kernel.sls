include:
  - sysConfig.selinux
  - sysConfig.sysctl_conf

{%- if not salt['file.file_exists']('/sys/kernel/mm/transparent_hugepage') %}
update_kernel_config:
  file.append:
    - name: /etc/rc.local
    - text:
      - echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled
      - echo never > /sys/kernel/mm/redhat_transparent_hugepage/defrag
  cmd.run:
    - names:
      - echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled
      - echo never > /sys/kernel/mm/redhat_transparent_hugepage/defrag
{%- else %}
update_kernel_config:
  file.append:
    - name: /etc/rc.local
    - text:
      - echo never > /sys/kernel/mm/transparent_hugepage/enabled
      - echo never > /sys/kernel/mm/transparent_hugepage/defrag
  cmd.run:
    - names:
      - echo never > /sys/kernel/mm/transparent_hugepage/enabled
      - echo never > /sys/kernel/mm/transparent_hugepage/defrag
{%- endif %}
