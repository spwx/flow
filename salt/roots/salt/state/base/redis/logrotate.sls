{% from "redis/vars.j2" import redis with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/{{ redis.svc_name }}.conf:
  file.managed:
    - source: salt://redis/files/logrotate.conf.j2
    - template: jinja
