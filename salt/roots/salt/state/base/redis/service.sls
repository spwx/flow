{% from "redis/vars.j2" import redis with context %}

{{ redis.install_prefix }}/conf/{{ redis.svc_name }}.conf:
  file.managed:
    - source: salt://redis/files/redis.conf.j2
    - template: jinja
    - mode: '0644'
    - user: root
    - group: root

/etc/init.d/{{ redis.svc_name }}:
  file.managed:
    - template: jinja
    - source: salt://redis/files/redis.init.j2
    - mode: '0750'
    - user: root
    - group: root
  cmd.run:
    - name: chkconfig --add {{ redis.svc_name }}
    - unless: chkconfig --list | grep {{ redis.svc_name }}
    - require:
      - file: /etc/init.d/{{ redis.svc_name }}
  service.running:
    - name: {{ redis.svc_name }}
    - enable: True
    - require:
      - file: /etc/init.d/{{ redis.svc_name }}
    - watch:
      - file: {{ redis.install_prefix }}/conf/{{ redis.svc_name }}.conf
      - file: /etc/init.d/{{ redis.svc_name }}