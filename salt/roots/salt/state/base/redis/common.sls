{% from "redis/vars.j2" import redis with context %}

{% set src_dir = redis.src_dir %}

redis_dependencies:
  pkg.installed:
    - names:
      - python-devel
      - make
      - libxml2-devel
      - tcl

get_redis:
  file.managed:
    - name: {{ src_dir }}/redis-{{ redis.version }}.tar.gz
    - source: {{ redis.pkg_url }}
    {% if redis.get('pkg_checksum', false) %}
    - source_hash: {{ redis.pkg_checksum }}
    {% endif %}
    - require:
      - pkg: redis_dependencies
  cmd.wait:
    - cwd: {{ src_dir }}
    - names:
      - tar -xvzf {{ src_dir }}/redis-{{ redis.version }}.tar.gz -C {{ src_dir }}
    - watch:
      - file: get_redis

redis_group:
  group.present:
    - name: {{ redis.group }}

redis_user:
  user.present:
    - name: {{ redis.user }}
    - gid_from_name: True
    - shell: /bin/false
    - groups: 
      - {{ redis.group }}
    - require:
      - group: redis_group

{{ redis.install_prefix }}:
  file.directory:
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - makedirs: True
    - require:
      - user: redis_user
      - group: redis_group

{{ redis.install_prefix }}/conf:
  file.directory:
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - makedirs: True
    - require:
      - user: redis_user
      - group: redis_group

{{ redis.log_dir }}:
  file.directory:
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - makedirs: True
    - require:
      - user: redis_user
      - group: redis_group

{{ redis.data_dir }}:
  file.directory:
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - makedirs: True
    - require:
      - user: redis_user
      - group: redis_group

/var/run/redis:
  file.directory:
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - makedirs: True
    - require:
      - user: redis_user
      - group: redis_group