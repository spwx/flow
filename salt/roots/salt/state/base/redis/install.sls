{% from "redis/vars.j2" import redis with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{% set src_dir = redis.src_dir %}
include:
  - redis.common

install_redis:
  cmd.run:
    - cwd: {{ src_dir }}/redis-{{ redis.version }}
    - names:
      - make distclean
      - make
      - make PREFIX={{ redis.install_prefix }} install
    - require:
      - cmd: get_redis
  file.append:
    - name: /etc/profile
    - text:
      - export PATH={{ redis.install_prefix }}/bin:$PATH
    - require:
      - cmd: install_redis