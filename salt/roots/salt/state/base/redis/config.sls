{% from "redis/vars.j2" import redis with context %}

/tmp/redis_config_update.txt:
  file.managed:
    - contents:
      - CONFIG SET SAVE "900 1000 300 100000 60 1000000"
      - CONFIG REWRITE
  cmd.run:
    - name: cat /tmp/redis_config_update.txt | {{ redis.install_prefix }}/bin/redis-cli -h {{ redis.bind }} -p {{ redis.port }} -a {{ redis.password }} 
    - require:
      - file: /tmp/redis_config_update.txt
