{% from "python3/vars.j2" import python3 with context %}

{% set src_dir = '/usr/local/src' %}
{% set version = '3.5.2' %}
{% set pkg_url = 'salt://python3/files/Python-{0}.tgz'.format(version) %}
{% set python3_pkg_name = 'Python-{0}.tgz'.format(version) %}
{% set python3_src_dir = '{0}/Python-{1}'.format(src_dir, version) %}


python3-deps-install:
  pkg.installed:
    - pkgs:
      - openssl-devel
      - binutils
      - automake
      - autoconf
      - ncurses-devel
      - gdbm
      - gdbm-devel

get_python3:
  file.managed:
    - name: {{ src_dir }}/{{ python3_pkg_name }}
    - source: {{ pkg_url }}
    {% if python3.get('pkg_checksum', false) %}
    - source_hash: {{ python3.pkg_checksum }}
    {% endif %}
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ python3_pkg_name }}
    - unless: test -f /usr/local/bin/python3
    - require:
      - file: get_python3

install_python3:
  cmd.run:
    - cwd: {{ python3_src_dir }}
    - names:
      - ( ./configure --prefix={{ python3.install_prefix }} --with-dbmliborder=gdbm
        && make
        && make install) > {{ python3_src_dir }}/build.out 2> {{ python3_src_dir }}/build.err;
        r=$?;
        if [ x$r != x0 ]; then
          cat {{ python3_src_dir }}/build.err 1>&2;
          exit $r;
        fi;
    - unless: which python3
    - require:
      - cmd: get_python3
      - pkg: python3-deps-install
  file.append:
    - name: /etc/profile
    - text:
      - export PATH={{ python3.install_prefix }}/bin:$PATH
    - require:
      - cmd: install_python3

/root/.pip:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

/root/.pip/pip.conf:
  file.managed:
    - source: salt://python3/files/pip.conf.j2
    - template: jinja
    - context:
      mirror_url: {{ python3.mirror_url }}
      mirror_host: {{ python3.mirror_host }}

install_virtualenv:
  cmd.run:
    - name: {{ python3.install_prefix }}/bin/pip3 install virtualenv
    - unless: {{ python3.install_prefix }}/bin/pip3 list | grep virtualenv

get_uwsgi:
  file.managed:
    - name: {{ src_dir }}/uwsgi-2.0.13.1.tar.gz
    - source: salt://python3/files/uwsgi-2.0.13.1.tar.gz
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - ({{ python3.install_prefix }}/bin/pip3 install -I {{ src_dir }}/uwsgi-2.0.13.1.tar.gz
        && {{ python3.install_prefix }}/bin/pip3 install gevent)
    - require:
      - file: get_uwsgi

get_m2crypto:
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - rm m2crypto -rf && git clone -b python3 https://gitlab.com/m2crypto/m2crypto.git && cd m2crypto && {{ python3.install_prefix }}/bin/python3 setup.py build && {{ python3.install_prefix }}/bin/python3 setup.py install 
