{% from 'logrotate/vars.j2' import logrotate with context %}

logrotate:
  pkg.installed:
    - name: {{ logrotate.pkg | json }}
  service.running:
    - name: {{ logrotate.service }}
    - enable: True
    - reload: True

{{ logrotate.include_dir }}:
  file.directory:
    - user: {{ logrotate.user }}
    - group: {{ logrotate.group }}
    - mode: 755
    - makedirs: True
    - require:
      - pkg: logrotate

{{ logrotate.conf_file }}:
  file.managed:
    - source: salt://logrotate/files/logrotate.conf.j2
    - template: jinja
    - user: {{ logrotate.user }}
    - group: {{ logrotate.group }}
    - require:
      - file: {{ logrotate.include_dir }}

logrotate_cron:
  file.managed:
    - name: /etc/cron.daily/logrotate
    - source: salt://logrotate/files/logrotate.sh.j2
    - template: jinja
    - mode: 700
  cron.present:
    - name: /etc/cron.daily/logrotate
    - user: root
    - minute: 0
    - hour: 4