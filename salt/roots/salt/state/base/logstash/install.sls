{% from "logstash/vars.j2" import logstash with context %}
{% set pkg_dldir = "/usr/local/src" %}

get_pkg:
  file.managed:
    - name: {{ pkg_dldir }}/logstash-{{ logstash.version }}.rpm
    - source: {{ logstash.pkg_url }}
    - source_hash: md5=6e0463ef907708cf328dbfff35353956
  cmd.run:
    - cwd: {{ pkg_dldir }}
    - name: source /etc/profile; ln -s /opt/jdk/jdk1.8.0_144/bin/java /usr/bin/java ; yum install -y logstash-{{ logstash.version }}.rpm
    - unless: rpm -q logstash
    - require:
      - file: get_pkg

