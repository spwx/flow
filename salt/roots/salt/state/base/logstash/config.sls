{% from "logstash/vars.j2" import logstash with context %}
{% from "WxLogstash/vars.j2" import WxLogstash with context %}

{%- if logstash.config|default(false) %}
/etc/logstash/logstash.yml:
  file.serialize:
    - dataset: {{ logstash.config }}
    - formatter: yaml
    - backup: True
    - show_diff: True

{% set pathConfigDir = logstash.config.get('path.config', '/etc/logstash/conf.d') %}
{{ pathConfigDir }}:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{%- for module in WxLogstash.modules %}
{{ pathConfigDir }}/{{ module }}.conf:
  file.managed:
    - source: salt://WxLogstash/files/{{ module }}.conf.j2
    - template: jinja

{%- endfor %}
{%- endif %}

start-logstash-service:
  service.running:
    - name: logstash
    - enable: True
    - init_delay: 3
{%- if logstash.config|default(false) %}
    - require:
      - file: /etc/logstash/logstash.yml
{%- for module in WxLogstash.modules %}
      - file: {{ pathConfigDir }}/{{ module }}.conf
{%- endfor %}
    - watch:
{%- for module in WxLogstash.modules %}
      - file: {{ pathConfigDir }}/{{ module }}.conf
{%- endfor %}
      - file: /etc/logstash/logstash.yml
{%- endif %}
