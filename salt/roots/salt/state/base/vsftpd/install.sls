{% from 'vsftpd/vars.j2' import vsftpd with context %}

install_vsftpd:
  pkg.installed:
    - names:
      - db4
      - db4-utils
      - vsftpd
      - lftp

create_directory_for_user_configuration:
  file.directory:
    - name: {{ vsftpd.user_config_dir }}
    - mode: 755
    - makedirs: True

set_permissions_for_local_root:
  file.directory:
    - name: {{ vsftpd.local_root }}
    - user: ftp
    - group: ftp
    - mode: 755
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

copy_file_for_PAM_service:
  file.managed:
    - name: /etc/pam.d/vsftpd
    - source: salt://vsftpd/files/vsftpd.pam.j2
    - template: jinja
    - mode: 644
    - backup: True
