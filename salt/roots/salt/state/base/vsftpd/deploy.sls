{% from 'vsftpd/vars.j2' import vsftpd with context %}

create_vusers_db:
  file.managed:
    - name: /tmp/vuser.txt
    - source: salt://vsftpd/files/vuser.j2
    - template: jinja
    - defaults:
        users: {{ vsftpd.users }}
  cmd.run:
    - name: /usr/bin/db_load -T -t hash -f /tmp/vuser.txt {{ vsftpd.pam_vuser_db }}.db
    - require:
      - file: create_vusers_db

{%- for user, user_data in vsftpd.users.items() -%}
  {%- if user_data.get('conf', false) %}
create_vuser_{{ user }}_conf:
  file.managed:
    - name: {{ vsftpd.user_config_dir }}/{{ user }}
    - source: salt://vsftpd/files/vuser.conf.j2
    - template: jinja
    - defaults:
        user_conf: {{ user_data.conf }}

    {%- if user_data.conf.get('local_root', false) %}
create_vuser_{{ user }}_datadir:
  file.directory:
    - name: {{ user_data.conf.local_root }}
    - user: ftp
    - group: ftp
    - mode: 755
    - makedirs: True
    - recurse:
      - user
      - group
      - mode
    {%- else %}
create_vuser_{{ user }}_datadir:
  file.directory:
    - name: {{ vsftpd.local_root }}/{{ user }}
    - user: ftp
    - group: ftp
    - mode: 755
    - makedirs: True
    - recurse:
      - user
      - group
      - mode
    {%- endif %}

  {%- else %}
create_vuser_{{ user }}_datadir:
  file.directory:
    - name: {{ vsftpd.local_root }}/{{ user }}
    - user: ftp
    - group: ftp
    - mode: 755
    - makedirs: True
    - recurse:
      - user
      - group
      - mode
  {%- endif %}
{%- endfor %}

/etc/vsftpd/vsftpd.conf:
  file.managed:
    - source: salt://vsftpd/files/vsftpd.conf.j2
    - template: jinja
    - mode: 644
    - backup: True
    - require:
      - file: create_vusers_db
  cmd.run:
    - name: chkconfig --add vsftpd
    - unless: chkconfig --list | grep vsftpd
    - require:
      - file: /etc/vsftpd/vsftpd.conf
  service.running:
    - name: vsftpd
    - enable: True
    - reload: True
    - watch:
      - file: /etc/vsftpd/vsftpd.conf
    - require:
      - file: /etc/vsftpd/vsftpd.conf