{% if grains['os_family'] == 'RedHat' and grains['osmajorrelease'] == '6' %}
{% set daemon_pkg = 'start-stop-daemon-1.18.2-1.el6.x86_64.rpm' %}
daemon-tool-install:
  file.managed:
    - name: /tmp/{{ daemon_pkg }}
    - source: salt://base/files/{{ daemon_pkg }}
    - user: root
    - group: root
  cmd.run:
    - name: rpm -ivh /tmp/{{ daemon_pkg }}
    - unless: which start-stop-daemon
    - require:
      - file: daemon-tool-install

{% endif %}
