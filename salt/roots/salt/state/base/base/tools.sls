install_base_tools:
  pkg.installed:
    - pkgs:
      - openssl-devel
      - curl
      - wget
      - ntp
      - gcc
      - cpp
      - binutils
      - automake
      - autoconf
      - logrotate
      - vim-enhanced
