{% if grains['os_family'] == 'RedHat' %}
epel_install:
  file.managed:
    - name: /tmp/epel-release-latest-{{ grains['osmajorrelease'][0] }}.noarch.rpm
    - source: salt://base/files/epel-release-latest-{{ grains['osmajorrelease'][0] }}.noarch.rpm
    - user: root
    - group: root
  cmd.run:
    - name: rpm -ivh /tmp/epel-release-latest-{{ grains['osmajorrelease'][0] }}.noarch.rpm
    - unless: test -f /etc/yum.repos.d/epel.repo
    - require:
      - file: epel_install

# fix: https exception.
epel_update:
  cmd.run:
    - name:  sed -i 's/https/http/g' /etc/yum.repos.d/epel.repo
    - require:
      - cmd: epel_install
{% endif %}
