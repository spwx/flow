{% set pillar_ntp = salt['pillar.get']('ntp') %}
{% set ntp_server_list = ' '.join(pillar_ntp.get('servers', [])) %}

ntp_update_cron:
  cron.present:
    - identifier: ntpdate_{{ ntp_server_list }}
    - name: /usr/sbin/ntpdate {{ ntp_server_list }}
    - user: root
    - minute: 0
    - hour: '*/1'
