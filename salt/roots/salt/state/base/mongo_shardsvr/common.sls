{% from "mongo/vars.j2" import mongo with context %}
{% from "mongo_shardsvr/vars.j2" import mongo_shardsvr with context %}

{{ mongo_shardsvr.dataPath }}:
  file.directory:
    - user: {{ mongo.user }}
    - group: {{ mongo.group }}
    - makedirs: True
