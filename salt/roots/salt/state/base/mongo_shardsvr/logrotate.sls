{% from "mongo_shardsvr/vars.j2" import mongo_shardsvr with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/{{ mongo_shardsvr.svc_name }}.conf:
  file.managed:
    - source: salt://mongo/files/logrotate.conf.j2
    - template: jinja
    - defaults:
           svc_name: {{ mongo_shardsvr.svc_name }}