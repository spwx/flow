{% from "mongo/vars.j2" import mongo with context %}
{% from "mongo_shardsvr/vars.j2" import mongo_shardsvr as config with context %}
{% from "mongo_shardsvr/_macro.j2" import service with context %}

{% set defined_role = 'executeEval' %}

{%- if config.is_master|default(False) %}
# need run this state in replset master node.

# todo:
# 1. create localhost exception
{{ service(sharding=False) }}
# 2. create users
{% for user in config.users %}
  {% set name = user.name %}
  {% set passwd = user.passwd %}
  {% set _user = user.user %}
  {% set password = user.password %}
  {% set database = user.database %}
  {% set authdb = user.authdb %}

mongodb-create-{{ name }}-account:
  cmd.run:
    - name: >-
    {%- if database != 'admin' %}
        {{ mongo.install_path }}/bin/mongo {{ database }} --port {{ config.port }} -u {{ _user }} -p {{ password }} --authenticationDatabase {{ authdb }} --quiet --eval
    {%- else %}
        {{ mongo.install_path }}/bin/mongo {{ database }} --port {{ config.port }} --quiet --eval
    {%- endif %}
        "db.createUser({user:'{{ name }}', pwd: '{{ passwd }}', roles: []})"
    - require:
      - service: create_sharding_False_service
    - unless: {{ mongo.install_path }}/bin/mongo --port {{ config.port }} --quiet --eval "db.getUser('{{ name }}')" {{ database }} |grep {{ name }}

{% if database == 'admin' %}
command-mongodb-create-{{ defined_role }}-role:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo {{ database }} --port {{ config.port}} --quiet --eval
        "db.createRole({role:'{{ defined_role }}',privileges: [{resource:{anyResource: true}, actions:['anyAction']}],roles:[]})"
    - output_loglevel: quiet
    - require:
      - cmd: mongodb-create-admin-account
    - unless: {{ mongo.install_path }}/bin/mongo --port {{ config.port }} --quiet --eval "db.getRoles()" {{ database }} |grep {{ defined_role }}

# grant role to user
command-mongodb-grant-{{ defined_role }}-role-to-admin:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo {{ database }} --port {{ config.port }} --quiet --eval
        "db.grantRolesToUser('{{ name }}',[{role:'{{ defined_role }}', db:'{{ database }}'}])"
    - output_loglevel: quiet
    - require:
      - cmd: command-mongodb-create-{{ defined_role }}-role
    - unless: {{ mongo.install_path }}/bin/mongo --port {{ config.port }} --quiet --eval "db.getUser('{{ name }}')" {{ database }} |grep {{ defined_role }}
{% endif %}

{% endfor %} # //--end users

# 1. create shard server
{{ service(sharding=True) }}
# 2. init rs

{% for node in config.sources %}

{% for user in config.users %}
{% if user.name == 'admin' %}
  {% set name = user.name %}
  {% set passwd = user.passwd %}
  {% set _user = user.user %}
  {% set password = user.password %}
  {% set database = user.database %}
  {% set authdb = user.authdb %}

{% if node.master|default(False) %}

mongodb-initiate-{{ node.name }}-replset:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo --port {{ config.port }} -u {{ name }} -p {{ passwd }} {{ database }} --quiet --eval
        "rs.initiate()"
    - shell: /bin/bash
    - output_loglevel: quiet
    - require:
      - service: create_sharding_True_service
    - unless: {{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval "rs.status()" |grep {{ node.ip}}:{{ node.port }}

mongodb-status-{{ node.name }}-replset:
  cmd.run:
    - name: |
        until [ `{{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval \
        'rs.status().members[0].stateStr' | grep -i PRIMARY | wc -l` -eq 1 ]
        do
          sleep 1
        done
        sleep 5
    - shell: /bin/bash
    - output_loglevel: quiet
    - require:
      - cmd: mongodb-create-admin-account
      - cmd: mongodb-initiate-{{node.name}}-replset
    - unless: {{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval "rs.status()" |grep {{ node.ip }}:{{node.port}}

mongodb-reconfig-{{ node.name }}-replset:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval
        "cfg = rs.conf(); cfg.members[0].priority=2; cfg.members[0].host='{{ node.ip }}:{{ node.port }}'; rs.reconfig(cfg, {force: true});"
    - shell: /bin/bash
    - output_loglevel: quiet
    - require:
      - cmd: mongodb-create-admin-account
    - unless: {{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval "rs.status().members[0].stateStr" |grep -i PRIMARY

{% elif node.arbiter == 'true' %}

mongodb-add-{{ node.name }}-replset:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval
        "rs.add('{{ node.ip }}:{{node.port}}', true)"
    - shell: /bin/bash
    - output_loglevel: quiet
    - require:
      - cmd: mongodb-create-admin-account
    - unless: {{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval "rs.status()" |grep {{ node.ip }}:{{node.port}} 

{% else %}

mongodb-add-{{ node.name }}-replset:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval
        "rs.add('{{ node.ip }}:{{ node.port }}')"
    - shell: /bin/bash
    - output_loglevel: quiet
    - require:
      - cmd: mongodb-create-admin-account
    - unless: {{ mongo.install_path }}/bin/mongo {{ database }} -u {{ name }} -p {{ passwd }} --port {{ config.port }} --quiet --eval "rs.status()" |grep {{ node.ip }}:{{node.port}} 

{% endif %} # --end node.master

{% endif %} # --end user.name is admin
{% endfor %} 


{% endfor %} # --end config.sources

{%- endif %} # --end config.is_master
