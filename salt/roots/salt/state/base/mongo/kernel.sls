{% from "mongo/vars.j2" import mongo with context %}

{%- if not salt['file.directory_exists']('/sys/kernel/mm/transparent_hugepage') %}
update_kernel_config:
  file.append:
    - name: /etc/rc.local
    - text:
      - echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled
      - echo never > /sys/kernel/mm/redhat_transparent_hugepage/defrag
      - sysctl -w net.core.somaxconn=65535
  cmd.run:
    - names:
      - echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled
      - echo never > /sys/kernel/mm/redhat_transparent_hugepage/defrag
      - sysctl -w net.core.somaxconn=65535
{%- else %}
update_kernel_config:
  file.append:
    - name: /etc/rc.local
    - text:
      - echo never > /sys/kernel/mm/transparent_hugepage/enabled
      - echo never > /sys/kernel/mm/transparent_hugepage/defrag
      - sysctl -w net.core.somaxconn=65535
  cmd.run:
    - names:
      - echo never > /sys/kernel/mm/transparent_hugepage/enabled
      - echo never > /sys/kernel/mm/transparent_hugepage/defrag
      - sysctl -w net.core.somaxconn=65535
{%- endif %}

/etc/security/limits.d/99-mongodb.conf:
  file.managed:
    - source: salt://mongo/files/99-mongodb.conf.j2
    - template: jinja
    - defaults:
        user: {{ mongo.user }}
        ulimit: {{ mongo.ulimit }}

off_selinux:
  cmd.run:
    - name: setenforce permissive
    - unless: getenforce | grep Disabled
  file.replace:
    - name: /etc/selinux/config
    - pattern: ^SELINUX=.*$
    - repl: SELINUX=permissive
    - backup: True
  service.dead:
    - name: iptables
    - enable: False
