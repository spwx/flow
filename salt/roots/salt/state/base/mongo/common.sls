{% from "mongo/vars.j2" import mongo with context %}

mongo_group:
  group.present:
    - name: {{ mongo.group }}

mongo_user:
  user.present:
    - name: {{ mongo.user }}
    - gid_from_name: True
    - shell: /bin/bash
    - groups:
      - {{ mongo.group }}
    - require:
      - group: mongo_group

{{ mongo.data_path }}:
  file.directory:
    - user: {{ mongo.user }}
    - group: {{ mongo.group }}
    - makedirs: True
    - require:
      - user: mongo_user
      - group: mongo_group

{{ mongo.data_path }}/secret:
  file.managed:
    - contents_pillar: mongo:secret
    - user: {{ mongo.user }}
    - group: {{ mongo.group }}
    - mode: 600
    - require:
      - file: {{ mongo.data_path }}

{{ mongo.log_path }}:
  file.directory:
    - user: {{ mongo.user }}
    - group: {{ mongo.group }}
    - makedirs: True
    - require:
      - user: mongo_user
      - group: mongo_group

/var/run/mongo:
  file.directory:
    - user: {{ mongo.user }}
    - group: {{ mongo.group }}
    - makedirs: True
    - require:
      - user: mongo_user
      - group: mongo_group
