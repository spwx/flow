{% from "mongo/vars.j2" import mongo with context %}

{% set mongo_dir_name = 'mongodb-linux-{0}-{1}'.format(grains['cpuarch'], mongo.version) %}
{% set mongo_pkg_name = mongo_dir_name + '.tgz' %}

{% set src_dir = '/usr/local/src' %}

get_mongo:
  file.managed:
    - name: {{ src_dir }}/{{ mongo_pkg_name }}
    - source: {{ mongo.pkg_url }}
    {% if mongo.get('pkg_checksum', false) %}
    - source_hash: {{ mongo.pkg_checksum }}
    {% endif %}
  cmd.run:
    - cwd: {{ src_dir }}
    - names:
      - tar -xvzf {{ src_dir }}/{{ mongo_pkg_name }} -C {{ src_dir }}
    - require:
      - file: get_mongo

install_mongo:
  file.directory:
    - name: {{ mongo.install_path }}
    - user: {{ mongo.user }}
    - group: {{ mongo.group }}
    - makedirs: True
    - require:
      - user: mongo_user
      - group: mongo_group
  cmd.run:
    - name: \cp -Rf {{ src_dir }}/{{ mongo_dir_name }}/* {{ mongo.install_path }}
    - require:
      - cmd: get_mongo
      - file: install_mongo

install-pymongo:
  pkg.installed:
    - pkgs:
      - python-pymongo
    - require:
      - cmd: install_mongo
  
add_mongo_command_to_path_envvar:
  file.append:
    - name: /etc/profile
    - text:
      - export PATH={{ mongo.install_path }}/bin:$PATH
    - require:
      - cmd: install_mongo

{% if mongo.get('is_master', false) %}
# create a single mongo

{{ mongo.data_path }}/{{ mongo.svc_name }}:
  file.directory:
    - user: {{ mongo.user }}
    - group: {{ mongo.group }}
    - makedirs: True

{{ mongo.install_path }}/{{ mongo.svc_name }}.conf:
  file.managed:
    - source: salt://mongo/files/mongod.conf.j2
    - template: jinja
    - defaults:
        svc_name: {{ mongo.svc_name }}
        dataPath: {{ mongo.dataPath }}
        port: {{ mongo.port }}
        bindIp: {{ mongo.bindIp }}
    - require:
      - file: {{ mongo.data_path }}/{{ mongo.svc_name }}

/etc/init.d/{{ mongo.svc_name }}:
  file.managed:
    - source: salt://mongo/files/mongod.j2
    - mode: '0750'
    - template: jinja
    - defaults:
        svc_name: {{ mongo.svc_name }}
  cmd.run:
    - name: chkconfig --add {{ mongo.svc_name }}
    - unless: chkconfig --list | grep {{ mongo.svc_name }}
    - require:
      - file: /etc/init.d/{{ mongo.svc_name }}
  service.running:
    - name: {{ mongo.svc_name }}
    - enable: True
    - init_delay: 3
    - require:
      - file: /etc/init.d/{{ mongo.svc_name }}
{% if mongo.restart_service_after_state_change == 'true' %}
    - watch:
      - file: {{ mongo.install_path }}/{{ mongo.svc_name }}.conf
{% endif %}

{% endif %}
