{% set src_dir = '/usr/local/src' %}
{% set python2_setuptools_pkg_name = 'setuptools-36.2.7.tar.gz' %}
{% set python2_setuptools_pkg_name_dir = '{0}/setuptools-36.2.7'.format(src_dir) %}
{% set supervisor_pkg_name = 'supvervisor-3.3.3.tar.gz' %}
{% set supervisor_pkg_name_dir = '{0}/supervisor-3.3.3'.format(src_dir) %}

/run:
  file.directory:
    - user: root
    - mode: 755
    - makedirs: True

get_setuptools:
  file.managed:
    - name: {{src_dir }}/{{ python2_setuptools_pkg_name }}
    - source: 'salt://supervisor/files/setuptools-36.2.7.tar.gz'
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ python2_setuptools_pkg_name }}
    - require:
      - file: get_setuptools

install_setuptools:
  cmd.run:
    - cwd: {{ python2_setuptools_pkg_name_dir }}
    - name: python setup.py install
    - require:
      - cmd: get_setuptools

get_supervisor:
  file.managed:
    - name: {{ src_dir}}/{{supervisor_pkg_name}}
    - source: 'salt://supervisor/files/supervisor-3.3.3.tar.gz'
  cmd.run:
    - cwd: {{ src_dir}}
    - name: tar -xzf {{ supervisor_pkg_name }}
    - require:
      - file: get_supervisor

install_supervisor:
  cmd.run:
    - cwd: {{ supervisor_pkg_name_dir }}
    - name: python setup.py install
    - unless: which supervisord
    - require:
      - cmd: get_supervisor


/tmp/get-pip.py:
  file.managed:
    - source: salt://base/files/get-pip.py
    - user: root
    - group: root
    - mode: '644'

install_meld:
  cmd.run:
    - cwd: {{ supervisor_pkg_name_dir }}
    - name: python /tmp/get-pip.py ;  pip install meld3
    - require:
      - cmd: get_supervisor


/etc/supervisord.conf:
  file.managed:
    - source: salt://base/files/supervisord.conf
    - user: root
    - group: root
    - mode: '644'

run_supervisor:
  cmd.run:
    - name: supervisord -c /etc/supervisord.conf
    - unless: ps -ef | grep supervisord | grep -v grep
    - require:
      - file: /etc/supervisord.conf
