#!/usr/bin/env bash
# author: seanly@aliyun.com

# --// constant variable section
artifact_ftp='{{ everydaylogs.artifact_ftp }}'
artifact_http='{{ everydaylogs.artifact_http }}'
ftp_user='{{ everydaylogs.ftp_user }}'
ftp_pass='{{ everydaylogs.ftp_pass }}'

# --// argument variable
log_glob=*.log.1
while getopts i:d:g: option
do
    case "${option}"
    in
        i)
            artifact_id=${OPTARG};;
        d)
            log_dir=${OPTARG};;
        g)
            log_glob=${OPTARG};;
        \?)
            echo "USAGE: args -i artifact_id -d log_dir -g log_glob"
            exit 1;;
    esac
done

if [ "$log_dir" == '' ]; then
    echo "usage: args -i artifact_id -d log_dir -g log_glob"
    exit 1
fi

# --// inner variables
host_ip=`/sbin/ifconfig |grep "inet addr"|head -1|awk -F ":" '{print $2}'|awk -F " " '{print $1}'`
date_time=$(date +%Y%m%d)
hour_time=$(date +%H%M%S)

# artifact directory format.
artifact_dir=/${artifact_id}/${date_time}/${host_ip}
artifact_filename=${hour_time}.tar.gz
artifact_http_url=${artifact_http}/${artifact_dir}

log_files=${log_dir}/${log_glob}

cd ${log_dir}
logfile_size=`du -cb ${log_files} |tail -n 1 |awk '{print $1}'`
logfile_mbsize=`awk 'BEGIN{printf "%.2f\n", '${logfile_size}/1048576'}'`

# source log file. 1g
if [ ${logfile_size} -gt 1073741824 ]
then
    echo "--//filename :$log_files  size:$logfile_mbsize mb]"
    echo "--// file too big, don't upload"
    exit 1
else
    echo "--//filename :$log_files size:$logfile_mbsize mb]"
    echo "--// start tar..."
    cd ${log_dir}
    tar cvzf ${artifact_filename} ${log_glob}
    retval=$?
    echo
    if [ ${retval} = 0 ]
    then
        echo "--// tar successfully"
    else
        echo '--// tar error'
        exit ${retval}
    fi
    tarfile_size=`du -cb ${artifact_filename} |tail -n 1 |awk '{print $1}'`
    tarfile_mbsize=`awk 'BEGIN{printf "%.2f\n", '${tarfile_size}/1000000'}'`
    # --// tar file > 250m
    if [ ${tarfile_size} -gt 262144000 ]
    then
        echo "--// filename :$artifact_filename  size: $tarfile_mbsize mb]"
        echo "--// zip file too big, than 250m"
        rm -rf ${log_dir}/${artifact_filename}
        exit 1
    else
        echo "--// filename:$artifact_filename size:$tarfile_mbsize mb]"
        echo "--// start uploading ..."
        /usr/bin/lftp -u ${ftp_user},${ftp_pass} ${artifact_ftp} -e "mkdir -p $artifact_dir; exit"
        /usr/bin/lftp -u ${ftp_user},${ftp_pass} ${artifact_ftp}/${artifact_dir} -e "put ${log_dir}/${artifact_filename}; exit" || exit -1

        retval=$?
        echo
        if [ ${retval} = 0 ]
        then
            rm -rf ${log_dir}/${artifact_filename}
            cd ${log_dir} && rm -rf ${log_glob}
            echo "--// upload successfully"
            exit 0
        fi
        echo "--// upload error"
        exit ${retval}
    fi
fi
echo -e "<a href="${artifact_http_url}">${artifact_http_url}</a>"
