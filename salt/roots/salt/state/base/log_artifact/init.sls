lftp_install:
  pkg.installed:
    - pkgs:
      - lftp
/data/SicentTools/deploy:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

/data/SicentScripts:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

include:
  - .install
