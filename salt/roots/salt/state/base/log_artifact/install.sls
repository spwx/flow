{% from "log_artifact/vars.j2" import log_artifact with context %}
{% from "log_artifact/vars.j2" import log_path  with context %}
{% from "log_artifact/vars.j2" import svc_name  with context %}


include:
  - log_artifact

# mkdir logrotate/logs
# mkdir scripts
{{ log_path }}/scripts:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

{{ log_path }}/logrotate/logs:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

# wgp.web.conf to logrotate
# artifact_log.sh to script
{% if log_artifact.get('everydaylogs', false) %}
{{ log_path }}/scripts/artifact_log.sh:
  file.managed:
    - source: salt://log_artifact/files/artifact_log.sh
    - template: jinja
    - defaults:
        everydaylogs: {{ log_artifact.get('everydaylogs', {}) }}
    - backup: True
    - mode: '0755'
    - user: root
    - group: root
    - require:
      - file: {{ log_path }}/scripts

{{ log_path }}/logrotate/{{ svc_name }}.conf:
  file.managed:
    - source: salt://log_artifact/files/logrotate.conf.j2
    - template: jinja
    - backup: True
    - user: root
    - group: root
    - require:
      - file: {{ log_path }}/logrotate/logs
      - file: {{ log_path }}/scripts/artifact_log.sh

{{ svc_name }}_logrotate:
  cron.present:
    - name: /usr/sbin/logrotate -f {{ log_path }}/logrotate/{{ svc_name }}.conf
    - minute: 0
    - hour: '*/2'

{% endif %}
