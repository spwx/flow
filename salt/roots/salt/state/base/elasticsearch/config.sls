{% from "elasticsearch/vars.j2" import elasticsearch with context %}

{%- if elasticsearch.config|default(false) %}
{% set config = elasticsearch.config %}

{%- if config.get('path.data', false) %}
{{ config.get('path.data', '/data/SicentApp/elasticsearch/db') }}:
  file.directory:
    - user: elasticsearch
    - group: elasticsearch
    - makedirs: True
{%- endif %}

{%- if config.get('path.logs', false) %}
{{ config.get('path.logs', '/data/SicentApp/elasticsearch/logs') }}:
  file.directory:
    - user: elasticsearch
    - group: elasticsearch
    - makedirs: True
{%- endif %}

/etc/elasticsearch/elasticsearch.yml:
  file.serialize:
    - dataset: {{ elasticsearch.config }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{%- endif %}

/etc/elasticsearch/jvm.options:
  file.managed:
    - source: salt://elasticsearch/files/jvm.options
    - template: jinja

/etc/sysconfig/elasticsearch:
  file.managed:
    - source: salt://elasticsearch/files/elasticsearch.j2
    - template: jinja

start-elasticsearch-service:
  service.running:
    - name: elasticsearch
    - enable: True
    - init_delay: 3
    - require:
      - file: /etc/elasticsearch/jvm.options
      - file: /etc/sysconfig/elasticsearch
{%- if elasticsearch.config|default(false) %}
      - file: /etc/elasticsearch/elasticsearch.yml
{%- endif %}
    - watch:
      - file: /etc/elasticsearch/jvm.options
{%- if elasticsearch.config|default(false) %}
      - file: /etc/elasticsearch/elasticsearch.yml
{%- endif %}
