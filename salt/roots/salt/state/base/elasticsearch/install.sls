{% from "elasticsearch/vars.j2" import elasticsearch with context %}
{% set pkg_dldir = "/usr/local/src" %}
get_pkg:
  file.managed:
    - name: {{ pkg_dldir }}/elasticsearch-{{ elasticsearch.version }}.rpm
    - source: {{ elasticsearch.pkg_url }}
    - source_hash: md5=8fcd56eaf849273a00ccb60456948add
  cmd.run:
    - cwd: {{ pkg_dldir }}
    - name: yum install -y elasticsearch-{{ elasticsearch.version }}.rpm
    - unless: rpm -q elasticsearch
    - require:
      - file: get_pkg

install-x-pack:
  cmd.run:
    - cwd: /usr/share/elasticsearch
    - name: bin/elasticsearch-plugin install --batch x-pack 
    - unless: bin/elasticsearch-plugin list | grep x-pack
    - require:
      - cmd: get_pkg

