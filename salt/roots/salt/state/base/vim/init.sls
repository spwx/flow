ctags:
  pkg.installed

dotfiles.git:
  file.directory:
    - name: /root/dotfiles
  git.latest:
    - name: https://github.com/seanly/dotfiles.git
    - target: /root/dotfiles

install vim-plug:
  cmd.run:
    - name: curl -fLo /root/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    - unless: test -f /root/.config/nvim/autoload/plug.vim

create vim directory and link to nvim config directory:
  cmd.run:
    - name: mkdir -p /root/.vim/autoload
    - unless: test -d /root/.vim/autoload
  file.symlink:
    - name: /root/.vim/autoload/plug.vim
    - target: /root/.config/nvim/autoload/plug.vim

create plugged directory:
  cmd.run:
    - name: mkdir -p /root/.config/nvim/plugged
    - unless: test -d /root/.config/nvim/plugged

vim:
  pkg.installed:
    - name: vim-enhanced
  file.managed:
    - name: /root/.vimrc
    - source: /root/dotfiles/init.vim
  cmd.script:
    - name: install_vim_plug.sh
    - source: salt://vim/files/install.sh
    - require: 
      - pkg: vim
      - file: vim

