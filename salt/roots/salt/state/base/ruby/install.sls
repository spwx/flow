{% from "ruby/vars.j2" import ruby with context %}

{% set src_dir = '/usr/local/src' %}
{% set ruby_pkg_name = "ruby-{0}.tar.gz".format(ruby.version) %}
{% set ruby_src_dir = "{0}/ruby-{1}".format(src_dir, ruby.version) %}

ruby-deps-install:
  pkg.installed:
    - pkgs:
      - openssl-devel
      - libxml2-devel
      - libxslt-devel
      - mysql-devel
      - binutils
      - automake
      - autoconf

{{ ruby.install_path }}:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

get_ruby:
  file.managed:
    - name: {{ src_dir }}/{{ ruby_pkg_name }}
    - source: {{ ruby.pkg_url }}
    {% if ruby.get('pkg_checksum', false) %}
    - source_hash: {{ ruby.pkg_checksum }}
    {% endif %}
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ ruby_pkg_name }}
    - unless: which ruby
    - require:
      - file: get_ruby

install_ruby:
  cmd.run:
    - cwd: {{ ruby_src_dir }}
    - names:
      - (./configure --prefix={{ ruby.install_path }}
        && make
        && make install) > {{ ruby_src_dir }}/build.out 2> {{ ruby_src_dir }}/build.err;
        r=$?;
        if [ x$r != x0 ]; then
          cat {{ ruby_src_dir }}/build.err 1>&2;
          exit $r;
        fi;
    - unless: which ruby
    - require:
      - cmd: get_ruby
      - pkg: ruby-deps-install
  file.append:
    - name: /etc/profile
    - text:
      - export PATH={{ ruby.install_path }}/bin:$PATH
    - require:
      - cmd: install_ruby

/root/.gemrc:
  file.managed:
    - source: salt://ruby/files/gemrc.j2
    - template: jinja

install_bundle:
  cmd.run: 
    - name: gem install bundle
    - unless: which bundle
    - require:
        - file: /root/.gemrc
  
