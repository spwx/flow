{% from "kibana/vars.j2" import kibana with context %}
{% set pkg_dldir = "/usr/local/src" %}
get_pkg:
  file.managed:
    - name: {{ pkg_dldir }}/kibana-{{ kibana.version }}.rpm
    - source: {{ kibana.pkg_url }}
    - source_hash: md5=80c6d88d209ea7744f42eb048ffe8a1a
  cmd.run:
    - cwd: {{ pkg_dldir }}
    - name: yum install -y kibana-{{ kibana.version }}.rpm
    - unless: rpm -q kibana
    - require:
      - file: get_pkg

install-x-pack:
  cmd.run:
    - cwd: /usr/share/kibana
    - name: bin/kibana-plugin install x-pack
    - unless: bin/kibana-plugin list | grep x-pack
    - require:
      - cmd: get_pkg

