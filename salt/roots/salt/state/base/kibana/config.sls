{% from "kibana/vars.j2" import kibana with context %}

{%- if kibana.config|default(false) %}
/etc/kibana/kibana.yml:
  file.serialize:
    - dataset: {{ kibana.config }}
    - formatter: yaml
    - backup: True
    - show_diff: True
{%- endif %}

start-kibana-service:
  service.running:
    - name: kibana
    - enable: True
    - init_delay: 3
{%- if kibana.config|default(false) %}
    - require:
      - file: /etc/kibana/kibana.yml
    - watch:
      - file: /etc/kibana/kibana.yml
{%- endif %}
