{% from "filebeat/vars.j2" import filebeat with context %}


/etc/filebeat/filebeat.yml:
  file.serialize:
    - dataset: {{ filebeat.config }}
    - formatter: yaml
    - backup: True
    - show_diff: True


start-filebeat-service:
  service.running:
    - name: filebeat
    - enable: True
    - init_delay: 3
    - require:
      - file: /etc/filebeat/filebeat.yml
    - watch:
      - file: /etc/filebeat/filebeat.yml
