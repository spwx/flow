{% from "filebeat/vars.j2" import filebeat with context %}
{% set pkg_dldir = "/usr/local/src" %}
get_pkg:
  file.managed:
    - name: {{ pkg_dldir }}/filebeat-{{ filebeat.version }}.rpm
    - source: {{ filebeat.pkg_url }}
    - source_hash: md5=56ad9c91aecfa88949f9c933136efd67
  cmd.run:
    - cwd: {{ pkg_dldir }}
    - name: yum install -y filebeat-{{ filebeat.version }}.rpm
    - unless: rpm -q filebeat
    - require:
      - file: get_pkg

