{% from 'vcs/vars.j2' import git with context %}

{% set pkg_name = 'git' %}
{% set pkg_version = git.version %}
{% set pkg_url = git.pkg_url %}
{% set pkg_checksum = git.pkg_checksum %}

{% set src_dir = '/usr/local/src' %}
{% set git_src_dir = '/usr/local/src/git-{0}'.format(pkg_version) %}

dep_install:
  pkg.installed:
    - pkgs:
      - perl-devel
      - tcl
      - gettext
      - libcurl-devel
      - expat-devel
      - openssl-devel
      - zlib-devel
      - ctags

get_git_pkg:
  file.managed:
    - name: {{ src_dir }}/{{pkg_name}}-{{pkg_version}}.tar.gz
    - source: {{ pkg_url }}
    - source_hash: {{ pkg_checksum }}
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ pkg_name }}-{{ pkg_version }}.tar.gz
    - unless: test -d {{ pkg_name }}-{{ pkg_version }}
    - require:
      - file: get_git_pkg

install_git:
  cmd.run:
    - cwd: {{ git_src_dir }}
    - names:
      - (./configure --prefix=/opt/{{ pkg_name }}
        && make
        && make install) > {{ git_src_dir }}/build.out 2> {{ git_src_dir }}/build.err;
        r=$?;
        if [ x$r != x0 ]; then
          cat {{ git_src_dir }}/build.err 1>&2;
          exit $r;
        fi;
    - unless: which git |grep opt
    - require:
      - cmd: get_git_pkg
      - pkg: dep_install
  file.append:
    - name: /etc/profile
    - text:
      - export PATH=/opt/{{ pkg_name }}/bin:/opt/{{pkg_name}}/libexec/git-core:$PATH
    - require:
      - cmd: install_git
