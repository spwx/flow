{% if grains['os_family'] == 'RedHat' and grains['osmajorrelease'][0] == '6' %}

{% set subversion_pkg = "subversion-1.7.4-0.1.el6.rfx.x86_64.rpm" %}
{% set subversion_version = "1.7.4" %}

svn.depends:
  pkg.installed:
    - names:
      - apr
      - apr-util
      - neon
      - neon-devel

svn.uninstall.old:
  cmd.run:
    - name: yum erase subversion -y
    - unless: rpm -qa | grep subversion-{{ subversion_version }}

svn.install:
  file.managed:
    - name: /tmp/{{ subversion_pkg }}
    - source: salt://vcs/files/{{ subversion_pkg }}
  cmd.run:
    - name: rpm -ivh /tmp/{{ subversion_pkg }}
    - unless: rpm -qa | grep subversion-{{ subversion_version }}
    - require:
      - cmd: svn.uninstall.old
      - pkg: svn.depends
      - file: svn.install

{% endif %}
