{% from "redmine/vars.j2" import redmine with context %}

{{ redmine.group }}_group:
  group.present:
    - name: {{ redmine.group }}

{{ redmine.user }}_user:
  user.present:
    - name: {{ redmine.user }}
    - shell: /bin/bash
    - groups:
      - {{ redmine.group }}
    - require:
      - group: {{ redmine.group }}_group

/var/run/redmine:
  file.directory:
    - user: {{ redmine.user }}
    - group: {{ redmine.group }}
    - makedirs: True
    - require:
      - user: {{ redmine.user }}_user
      - group: {{ redmine.group }}_group
