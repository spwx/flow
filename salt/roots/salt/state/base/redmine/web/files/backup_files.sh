#!/usr/bin/env bash
# author: seanly@aliyun.com

# --// constant variable section
. /etc/default/redmine_backup.conf

# --// inner variables
DATE_YEAR=$(date +%Y)
DATE_TIME=$(date +%Y%m%d)

mkdir -p /data/SicentBackup
cd /data/SicentBackup

tar -zcvf - ${REDMINE_FILES_PATH}/${DATE_YEAR} |openssl des3 -salt -k ${PKG_PASS} | dd of=${DATE_TIME}.files.des3

# artifact directory format.
ARTIFACT_DIR=/redmineBackup/${DATE_TIME}

echo "--// filename:$ARTIFACT_FILENAME size:$TARFILE_MBSIZE MB]"
echo "--// start uploading ..."
/usr/bin/lftp -u ${FTP_USER},${FTP_PASS} ${FTP_SERVER} -e "mkdir -p $ARTIFACT_DIR; exit"
/usr/bin/lftp -u ${FTP_USER},${FTP_PASS} ${FTP_SERVER}/${ARTIFACT_DIR} -e "put ${DATE_TIME}.files.des3; exit" || exit -1

RETVAL=$?
echo
if [ ${RETVAL} = 0 ]
then
    rm -rf ${DATE_TIME}.files.des3
    echo "--// upload successfully"
    exit 0
fi
exit ${RETVAL}
