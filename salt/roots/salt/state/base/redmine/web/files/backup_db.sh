#!/usr/bin/env bash
# author: seanly@aliyun.com

# --// constant variable section
. /etc/default/redmine_backup.conf

# --// inner variables
DATE_TIME=$(date +%Y%m%d)
mkdir -p /data/SicentBackup
cd /data/SicentBackup

mysqldump -u${DB_USER} -p${DB_PASS} ${DB_NAME} > ${DATE_TIME}.sql
tar -zcvf - ${DATE_TIME}.sql |openssl des3 -salt -k ${PKG_PASS} | dd of=${DATE_TIME}.sql.des3

# artifact directory format.
ARTIFACT_DIR=/redmineBackup/${DATE_TIME}

echo "--// filename:$ARTIFACT_FILENAME size:$TARFILE_MBSIZE MB]"
echo "--// start uploading ..."
/usr/bin/lftp -u ${FTP_USER},${FTP_PASS} ${FTP_SERVER} -e "mkdir -p $ARTIFACT_DIR; exit"
/usr/bin/lftp -u ${FTP_USER},${FTP_PASS} ${FTP_SERVER}/${ARTIFACT_DIR} -e "put ${DATE_TIME}.sql.des3; exit" || exit -1

RETVAL=$?
echo
if [ ${RETVAL} = 0 ]
then
    rm -rf ${DATE_TIME}.sql*
    echo "--// upload successfully"
    exit 0
fi
echo "--// upload error"
exit ${RETVAL}
