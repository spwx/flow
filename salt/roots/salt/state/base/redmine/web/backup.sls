{% from "redmine/vars.j2" import redmine with context %}

/etc/default/redmine_backup.conf:
  file.managed:
    - source: salt://redmine/web/files/backup.conf
    - template: jinja

/data/SicentScripts/redmine_backup_db.sh:
  file.managed:
    - source: salt://redmine/web/files/backup_db.sh
    - mode: '0755'

/data/SicentScripts/redmine_backup_files.sh:
  file.managed:
    - source: salt://redmine/web/files/backup_files.sh
    - mode: '0755'

backup_redmine_db:
  cron.present:
    - identifier: backup_redmine_db
    - name: /data/SicentScripts/redmine_backup_db.sh 2>&1 > /tmp/redmine_backup_db.log
    - hour: 2
backup_redmine_files:
  cron.present:
    - identifier: backup_redmine_files
    - name: /data/SicentScripts/redmine_backup_files.sh 2>&1 > /tmp/redmine_backup_files.log
    - hour: 2
