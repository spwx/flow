{% from "redmine/vars.j2" import redmine with context %}

include:
  - redmine.common

# git clone https://github.com/ChatWorks/redmine.git
# git checkout sicentFlow
# git submodule init
# git submodule update

{{ redmine.web.install_path }}:
  file.directory:
    - user: {{ redmine.user }}
    - group: {{ redmine.group }}
    - makedirs: True
    - require:
      - user: {{ redmine.user }}_user
      - group: {{ redmine.group }}_group

{{ redmine.web.log_dir }}:
  file.directory:
    - user: {{ redmine.user }}
    - group: {{ redmine.group }}
    - makedirs: True
    - require:
      - user: {{ redmine.user }}_user
      - group: {{ redmine.group }}_group

{{ redmine.web.install_path }}/config/database.yml:
  file.serialize:
    - dataset: {{ redmine.web.config.database }}
    - formatter: yaml
    - backupt: True
    - show_diff: True
