{% from "redmine/vars.j2" import redmine with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/{{ redmine.web.svc_name }}.conf:
  file.managed:
    - source: salt://redmine/web/files/logrotate.conf.j2
    - template: jinja