{% from "redmine/vars.j2" import redmine with context %}

{{ redmine.web.install_path }}/config/puma_{{ redmine.web.svc_name }}.rb:
  file.managed:
    - source: salt://redmine/web/files/puma.rb.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644