{% from "sentry/vars.j2" import sentry with context %}
{% from "python27/vars.j2" import python27 with context %}

deps_install:
  pkg.installed:
    - pkgs:
      - postgresql-devel
      - postgresql-libs 
      - postgresql-contrib 


install_sentry_web:
  cmd.run:
    - user: root
    - group: root

    - cwd: {{ sentry.web.install_path }}
    - names:
      - {{ sentry.web.install_path }}/env/bin/pip install -U sentry
    - require:
      - pkg: deps_install
    

# reload sentry_web service
start_service:
  cmd.run:
    - user: root
    - group: root
    - names:
      - supervisorctl update
      - supervisorctl restart sentry
    - watch:
      - file: /etc/supervisord/sentry.conf
    - require:
      - file: /etc/supervisord/sentry.conf
      - cmd: install_sentry_web


# reload worker service
start_worker:
  cmd.run:
    - user: root
    - group: root
    - names:
      - supervisorctl restart sentry_worker
    - watch:
      - file: /etc/supervisord/sentry_worker.conf
    - require:
      - file: /etc/supervisord/sentry_worker.conf
      - cmd: install_sentry_web

# reload cron service
start_cron:
  cmd.run:
    - user: root
    - group: root
    - names:
      - supervisorctl restart sentry_cron
    - watch:
      - file: /etc/supervisord/sentry_cron.conf
    - require:
      - file: /etc/supervisord/sentry_cron.conf
      - cmd: install_sentry_web

