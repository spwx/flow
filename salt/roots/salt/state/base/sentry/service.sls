
{% from "sentry/vars.j2" import sentry with context %}
{% from "python3/vars.j2" import python3 with context %}

/etc/supervisord:
  file.directory:
    - user: root
    - mode: 755
    - makedirs: True


/etc/supervisord/sentry.conf:
  file.managed:
    - source: salt://python3/files/supervisor.conf.j2
    - template: jinja
    - defaults:
        name: sentry
        user: root
        directory: {{ sentry.web.install_path }}
        command: {{ sentry.web.install_path}}/env/bin/sentry start

    - user: root
    - group: root
    - mode: '644'


/etc/supervisord/sentry_worker.conf:
  file.managed:
    - source: salt://python3/files/supervisor.conf.j2
    - template: jinja
    - defaults:
        name: sentry_worker
        user: root
        directory: {{ sentry.web.install_path }}
        command: {{ sentry.web.install_path}}/env/bin/sentry run worker    
        env: "C_FORCE_ROOT=True"
    - user: root
    - group: root
    - mode: '644'

/etc/supervisord/sentry_cron.conf:
  file.managed:
    - source: salt://python3/files/supervisor.conf.j2
    - template: jinja
    - defaults:
        name: sentry_cron
        user: root
        directory: {{ sentry.web.install_path }}
        command: {{ sentry.web.install_path}}/env/bin/sentry run cron    
    - user: root
    - group: root
    - mode: '644'


~/.sentry/config.yml:
  file.managed:
    - source: salt://sentry/files/config.yml.j2
    - template: jinja
    - defaults:
        redis_host: {{ sentry.sentry_redis.ip }}
        redis_port: {{ sentry.sentry_redis.port }}
    - user: root
    - group: root
    - mode: '644'


~/.sentry/sentry.conf.py:
  file.managed:
    - source: salt://sentry/files/sentry.conf.py.j2
    - template: jinja
    - defaults:
        pg_host: {{ sentry.pg_host }}
        redis_host: {{ sentry.sentry_redis.ip }}
        redis_port: {{ sentry.sentry_redis.port }}

    - user: root
    - group: root
    - mode: '644'
