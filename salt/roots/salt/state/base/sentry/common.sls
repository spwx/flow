{% from "sentry/vars.j2" import sentry as wxp with context %}
{% from "python27/vars.j2" import python27 with context %}

include:
  - .common

{{ wxp.group }}_group:
  group.present:
    - name: {{ wxp.group }}

{{ wxp.user }}_user:
  user.present:
    - name: {{ wxp.user }}
    - shell: /bin/bash
    - groups:
      - {{ wxp.group }}
    - require:
      - group: {{ wxp.group }}_group


{{ wxp.web.install_path }}:
  file.directory:
    - user: {{ wxp.user }}
    - group: {{ wxp.group }}
    - makedirs: True
    - require:
      - user: {{ wxp.user }}_user
      - group: {{ wxp.group }}_group

creat_env:
  cmd.run:
    - cwd: {{ wxp.web.install_path }}
    - name: {{ python27.install_prefix }}/bin/virtualenv env
    - unless: test -d env
    - requires:
      - file: {{ wxp.web.install_path}}