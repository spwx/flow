{%- from "k8s/vars.j2" import k8s with context -%}

/etc/systemd/system/kubelet.service.d/10-kubeadm.conf:
  file.replace:
    - pattern: |
        (KUBELET_KUBECONFIG_ARG.*true)"
    - repl:
        \1 --cgroup-driver=systemd"\n

/etc/sysctl.d/k8s.conf:
  file.managed:
    - contents: |
        net.bridge.bridge-nf-call-ip6tables = 1
        net.bridge.bridge-nf-call-iptables = 1
    - mode: '0777'
  cmd.run:
    - name: sysctl -p /etc/sysctl.d/k8s.conf
    - require:
      - file: /etc/sysctl.d/k8s.conf


install-k8s:
  cmd.run:
    - name: kubeadm init --kubernetes-version v1.6.1 --pod-network-cidr 10.244.0.0/16 --apiserver-advertise-address {{ k8s.apiserver_ipaddr }}
    - unless: test -d /etc/kubernetes

config-kubectl-env:
  cmd.run:
    - name: cp /etc/kubernetes/admin.conf $HOME/ && chown root:root $HOME/admin.conf
  file.append:
    - name: /root/.bash_profile
    - text: 
      - export KUBECONFIG=$HOME/admin.conf
    - require:
      - cmd: config-kubectl-env
