install-k8s-repo:
  file.managed:
    - name: /etc/yum.repos.d/magina-k8s.repo
    - source: salt://k8s/files/magina-k8s.repo
  cmd.run:
    - name: yum makecache
    - require:
      - file: install-k8s-repo

install-k8s-pkg:
  pkg.installed:
    - names:
      - kubelet
      - kubectl
      - kubeadm
    - require:
      - cmd: install-k8s-repo

dl-docker-images:
  file.managed:
    - name: /opt/k8s-docker-images.sh
    - source: salt://k8s/files/k8s-docker-images.sh
    - user: root
    - group: root
    - mode: '0755'
  cmd.run:
    - name: /opt/k8s-docker-images.sh
    - require:
      - file: dl-docker-images

