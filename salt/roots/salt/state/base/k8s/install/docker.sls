{%- from "k8s/vars.j2" import docker with context %}
docker.install:
  pkg.installed:
    - names:
      - docker

docker-storage-setup:
  file.managed:
    - name: /etc/sysconfig/docker-storage-setup
    - contents: |
        DEVS={{ docker.storage.DEV }} 
        VG={{ docker.storage.VG }} 
  cmd.run:
    - name: docker-storage-setup
    - require:
      - file: docker-storage-setup

/etc/docker/daemon.json:
  file.managed:
    - contents: |
        { "registry-mirrors": ["{{ docker.registryMirrors }}"] }
    - require:
      - pkg: docker.install

docker.service:
  service.running:
    - name: docker
    - enable: True
    - reload: True
    - watch:
      - file: /etc/docker/daemon.json
    - require:
      - file: /etc/docker/daemon.json
      - cmd: docker-storage-setup

