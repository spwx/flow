#############################################
# 添加需要的镜像
images=(
    pause-amd64:3.0
    kube-proxy-amd64:v1.6.1
    kube-scheduler-amd64:v1.6.1
    kube-controller-manager-amd64:v1.6.1
    kube-apiserver-amd64:v1.6.1
    etcd-amd64:3.0.17

    k8s-dns-sidecar-amd64:1.14.1
    k8s-dns-kube-dns-amd64:1.14.1
    k8s-dns-dnsmasq-nanny-amd64:1.14.1  
)

for imageName in ${images[@]} ; do
    docker pull registry.cn-hangzhou.aliyuncs.com/magina-k8s/$imageName
    docker tag registry.cn-hangzhou.aliyuncs.com/magina-k8s/$imageName gcr.io/google_containers/$imageName
    docker rmi registry.cn-hangzhou.aliyuncs.com/magina-k8s/$imageName
done

docker pull registry.cn-hangzhou.aliyuncs.com/magina-k8s/flannel:v0.7.0-amd64
docker tag registry.cn-hangzhou.aliyuncs.com/magina-k8s/flannel quay.io/coreos/flannel:v0.7.0-amd64
docker rmi registry.cn-hangzhou.aliyuncs.com/magina-k8s/flannel:v0.7.0-amd64
#############################################
