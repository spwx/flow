{% from "python27/vars.j2" import python27 with context %}

{% set src_dir = '/usr/local/src' %}
{% set version = '2.7.13' %}
{% set python27_pkg_name = 'Python-{0}.tgz'.format(version) %}
{% set python27_src_dir = '{0}/Python-{1}'.format(src_dir, version) %}

python27-deps-install:
  pkg.installed:
    - pkgs:
      - openssl-devel
      - binutils
      - automake
      - autoconf
      - ncurses-devel
      - gdbm
      - gdbm-devel


get_python27:
  file.managed:
    - name: {{ src_dir }}/{{ python27_pkg_name }}
    - source: {{ python27.pkg_url }}
    {% if python27.get('pkg_checksum', false) %}
    - source_hash: {{ python27.pkg_checksum }}
    {% endif %}
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xzf {{ python27_pkg_name }}
    - unless: test -f /usr/local/bin/python27
    - require:
      - file: get_python27

install_python27:
  cmd.run:
    - cwd: {{ python27_src_dir }}
    - names:
      - ( ./configure --prefix={{ python27.install_prefix }} --with-dbmliborder=gdbm
        && make
        && make install) > {{ python27_src_dir }}/build.out 2> {{ python27_src_dir }}/build.err;
        r=$?;
        if [ x$r != x0 ]; then
          cat {{ python27_src_dir }}/build.err 1>&2;
          exit $r;
        fi;
    - unless: which python27
    - require:
      - cmd: get_python27
      - pkg: python27-deps-install
  file.append:
    - name: /etc/profile
    - text:
      - export PATH={{ python27.install_prefix }}/bin:$PATH
    - require:
      - cmd: install_python27

/root/.pip:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

/root/.pip/pip.conf:
  file.managed:
    - source: salt://python27/files/pip.conf.j2
    - template: jinja
    - context:
      mirror_url: {{ python27.mirror_url }}
      mirror_host: {{ python27.mirror_host }}

install_pip:
  file.managed:
    - name: {{ python27.install_prefix }}/bin/get-pip.py
    - source: salt://base/files/get-pip.py
  cmd.run:
    - user: root
    - group: root
    - cwd: {{ python27.install_prefix }}/bin
    - names:
      - ({{ python27.install_prefix }}/bin/python get-pip.py
    - require:
      - file: install_pip

install_virtualenv:
  cmd.run:
    - name: {{ python27.install_prefix }}/bin/pip install virtualenv
    - unless: {{ python27.install_prefix }}/bin/pip list | grep virtualenv