include:
  - .dep

gcc_compiler_install:
  pkg.installed:
    - pkgs:
      - gcc
      - cpp
