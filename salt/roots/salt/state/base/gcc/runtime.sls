{% if grains['os_family'] == 'RedHat' and grains['osmajorrelease'][0] == '6' %}
devtools-2.install:
  file.managed:
    - name: /etc/yum.repos.d/devtools-2.repo
    - source: salt://gcc/files/devtools-2.repo
    - user: root
    - group: root
  pkg.installed:
    - name: devtoolset-2-runtime

devtoolset-env:
  file.append:
    - name: /etc/profile
    - text:
      - source /opt/rh/devtoolset-2/enable
{% endif %}
