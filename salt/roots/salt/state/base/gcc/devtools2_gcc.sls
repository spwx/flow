include:
  - .runtime
  - .dep

gcc.install:
  pkg.installed:
    - names:
{% if grains['os_family'] == 'RedHat' %}
  {% if grains['osmajorrelease'][0] == '6' %}
      - devtoolset-2-gcc
      - devtoolset-2-gcc-c++
      - devtoolset-2-binutils
  {% elif grains['osmajorrelease'][0] == '7' %}
      - gcc
      - gcc-c++
      - binutils
  {% endif %}
{% endif %}
    - require:
{% if grains['os_family'] == 'RedHat' and grains['osmajorrelease'][0] == '6' %}
      - file: devtools-2.install
{% endif %}
      - pkg: gcc.depends
