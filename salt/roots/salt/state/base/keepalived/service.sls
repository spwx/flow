{% from "keepalived/vars.j2" import keepalived as ka with context %}

{{ ka.install_path }}/etc/sysconfig/keepalived:
  file.managed:
    - source: salt://keepalived/files/etc/sysconfig/keepalived
    - template: jinja

{{ ka.install_path }}/etc/keepalived/keepalived.conf:
  file.managed:
    - source: salt://keepalived/files/etc/keepalived/keepalived.j2
    - template: jinja

/etc/init/keepalived.conf:
  file.managed:
    - source: salt://keepalived/files/etc/init/keepalived.conf
    - template: jinja
    - user: root
    - group: root
    - mode: '644'

keepalived.service:
  service.running:
    - name: keepalived
    - enable: True
    - reload: True
    - require:
      - file: {{ ka.install_path }}/etc/sysconfig/keepalived
      - file: {{ ka.install_path }}/etc/keepalived/keepalived.conf
      - file: /etc/init/keepalived.conf
    - watch:
      - file: {{ ka.install_path }}/etc/sysconfig/keepalived
      - file: {{ ka.install_path }}/etc/keepalived/keepalived.conf
      - file: /etc/init/keepalived.conf
