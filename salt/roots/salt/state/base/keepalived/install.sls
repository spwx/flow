{% from "keepalived/vars.j2" import keepalived as ka with context %}

{% set src_dir = '/usr/local/src' %}
include:
  - gcc

{% if ka.is_use_ipvs|default(false) %}
ipvsadm.install:
  pkg.installed:
    - name: ipvsadm
{% endif %}

get_keepalived_pkg:
  file.managed:
    - name: {{ src_dir }}/keepalived-{{ ka.version }}.tar.gz
    - source: {{ ka.pkg_url }}
    {% if ka.pkg_checksum|default(false) %}
    - source_hash: {{ ka.pkg_checksum }}
    {% endif %}
  cmd.run:
    - cwd: {{ src_dir }}
    - name: tar -xvzf keepalived-{{ ka.version }}.tar.gz
    - unless: test -d {{ ka.install_path }}
    - require:
      - file: get_keepalived_pkg

install.keepalived:
  cmd.run:
    - cwd: {{ src_dir }}/keepalived-{{ ka.version }}
    - names:
      - (./configure --prefix={{ ka.install_path }} --disable-fwmark && make && make install)
        {#- If they want to silence the compiler output, then save it to file so we can reference it later if needed #}
        {%- if ka.silence_compiler|default(true) %}
        > build.out 2> build.err;
        {#- If the build process failed, write stderr to stderr and exit with the error code #}
        r=$?;
        if [ x$r != x0 ]; then
          cat build.err 1>&2;  {#- copy err output to stderr #}
          exit $r;
        fi;
        {% endif %}
    - require:
      - cmd: get_keepalived_pkg
  file.append:
    - name: /etc/profile
    - text:
      - export PATH={{ ka.install_path }}/sbin:${{ ka.install_path }}/bin:$PATH
    - require:
      - cmd: install.keepalived
