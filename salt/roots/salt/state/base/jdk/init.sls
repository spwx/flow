{% from 'jdk/vars.j2' import jdk with context %}

{% set pkg_dldir = "/usr/local/src" %}

get_pkg:
  file.managed:
    - name: {{ pkg_dldir }}/{{ jdk.version }}.tar.gz
    - source: {{ jdk.pkg_url }}
    - source_hash: md5=2d59a3add1f213cd249a67684d4aeb83

{{ jdk.install_path }}:
  file.directory:
    - user: root
    - group: root
    - makedirs: True

install-jdk:
  cmd.run:
    - cwd: {{ pkg_dldir }}
    - names: 
      - (tar -xvzf {{ jdk.version }}.tar.gz -C {{ jdk.install_path }})
      {%- if jdk.silence_install|default(True) %}
        > /tmp/jdk.out 2> /tmp/jdk.err;
        r=$?;
        if [ x$r != x0 ]; then
          cat /tmp/jdk.err 1>&2;
          exit $r;
        fi;
      {%- endif %}
    - unless: which java
    - require:
      - file: get_pkg
      - file: {{ jdk.install_path }}
  file.append:
    - name: /etc/profile
    - text:
        - export JAVA_HOME={{ jdk.install_path }}/{{ jdk.version }}
        - export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$CLASSPATH
        - export PATH=$JAVA_HOME/bin:$PATH
    - require:
      - cmd: install-jdk
  environ.setenv:
    - name: JAVA_HOME
    - value: {{ jdk.install_path }}/{{ jdk.version }}
    - update_minion: True
