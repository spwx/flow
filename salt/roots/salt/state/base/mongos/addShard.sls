{% from "mongo/vars.j2" import mongo with context %}
{% from "mongos/vars.j2" import mongos with context %}

{% set user = mongos.configsvr.user %}
{% set passwd = mongos.configsvr.passwd %}
{% set authdb = mongos.configsvr.authdb %}

{%- for server in mongos.shardsvr.servers %}

mongodb-add-shard-{{ server.name }}-server:
  cmd.run:
    - name: >-
        {{ mongo.install_path }}/bin/mongo --port {{ mongos.port }} -u {{ user }} -p {{ passwd }} {{ authdb }} --quiet --eval
        "db.runCommand({ addShard: '{{ server.host }}', name: '{{ server.name }}'})"
    - shell: /bin/bash
    - output_loglevel: quiet
    - unless: >-
        {{ mongo.install_path }}/bin/mongo {{ authdb }} -u {{ user }} -p {{ passwd }} --port {{ mongos.port }} --quiet --eval 
        "db.runCommand({listshards: 1})" |grep {{ server.name }}

{%- endfor %}
