{% from "mongo/vars.j2" import mongo with context %}
{% from "mongos/vars.j2" import mongos with context %}

{{ mongo.install_path }}/{{ mongos.svc_name }}.conf:
  file.managed:
    - source: salt://mongo/files/mongod.conf.j2
    - template: jinja
    - defaults:
        svc_name: {{ mongos.svc_name }}
        port: {{ mongos.port }}
      {%- if mongos.use_external_net == true %}
        bindIp: {{ mongos.external_net_bindip }},{{ mongos.local_net_bindip }}
      {%- else %}
        bindIp: {{ mongos.local_net_bindip }}  
      {%- endif %}
      sharding:
      {%- set hosts = '{0}/'.format(mongos.configsvr.replSetName) %}
      {%- for server in mongos.configsvr.servers %}
          {%- if loop.last %}
              {%- set hosts = hosts + server.host + ':' ~ server.port  %}
        configDB: {{ hosts }}
          {%- else %}
              {%- set hosts = hosts + server.host + ':' ~ server.port + ','  %}
          {%- endif %}
      {%- endfor %}
      security_auth: true
      is_mongos: true

/etc/init.d/{{ mongos.svc_name }}:
  file.managed:
    - source: salt://mongo/files/mongod.j2
    - mode: '0655'
    - template: jinja
    - defaults:
        svc_name: {{ mongos.svc_name }}
        is_mongos: {{ mongos.is_mongos }}
  cmd.run:
    - name: chkconfig --add {{ mongos.svc_name }}
    - unless: chkconfig --list | grep {{ mongos.svc_name }}
    - require:
      - file: /etc/init.d/{{ mongos.svc_name }}
  service.running:
    - name: {{ mongos.svc_name }}
    - enable: True
    - init_delay: 3
    - require:
      - file: /etc/init.d/{{ mongos.svc_name }}
    - watch:
      - file: {{ mongo.install_path }}/{{ mongos.svc_name }}.conf
