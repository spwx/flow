{% from "mongos/vars.j2" import mongos with context %}
{% from 'logrotate/vars.j2' import logrotate with context %}

{{ logrotate.include_dir }}/{{ mongos.svc_name }}.conf:
  file.managed:
    - source: salt://mongo/files/logrotate.conf.j2
    - template: jinja
    - defaults:
        svc_name: {{ mongos.svc_name }}