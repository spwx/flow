#!/usr/bin/env python
# coding=utf-8

import os
import re


class OSType:

    WIN, LINUX, MACOSX, UNKNOWN = range(4)

    def __init__(self):
        pass

    @staticmethod
    def get_type():
        import platform
        system_name = platform.system()
        if system_name.lower() == 'windows':
            return OSType.WIN
        elif system_name.lower() == 'linux':
            return OSType.LINUX
        elif system_name.lower() == 'darwin':
            return OSType.MACOSX
        else:
            return OSType.UNKNOWN


WIN_SCRIPT_DIR = "d:\\data\\SicentTools\\deploy"
LINUX_SCRIPT_DIR = "/data/SicentTools/deploy"


def push_log():

    grains = {}
    script_dir = LINUX_SCRIPT_DIR
    if OSType.get_type() is OSType.WIN:
        script_dir = WIN_SCRIPT_DIR

    if not os.path.exists(script_dir):
        print('--//{0} directory is not exists.'.format(script_dir))
        return grains

    script_files = []
    for filename in os.listdir(script_dir):
        if not os.path.isfile(os.path.join(script_dir, filename)):
            continue
        if re.search("push_.+.(sh|bat)$", filename):
            script_files.append(filename)

    if len(script_files) > 0:
        grains['push_scripts'] = script_files

    return grains
