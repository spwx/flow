# 网吧管家 游戏广场web 线上服务器

## 1 游戏广场web详细部署步骤
由于游戏广场web访问量，线上并发数最高值能够到2500上，所以必须采用蓝绿部署，防止业务被中断的问题。
所有脚本 test默认为True 防止粘贴后立即部署带来的失误问题。如果需要真正的执行部署，需要吧test 改为False

详细部署步骤如下：

1 部署游戏广场定时任务服务
```js
salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_celery pillar='{"test":"True"}'
```
2 部署游戏广场web后端服务。

```js
2.0 拉取gameplaza_web 包

cd /root/dev/salt/deploy/;
python deploy.py  prod -c config.wxdesk.yaml -p wxgameplaza_web

2.1 切换nginx到blue节点

salt -E 'nginx_backend.prod.gameplaza.yewu.js' state.sls WxGamePlaza.nginx.web.backend pillar='{"wxgameplaza_nginx_web_backend_active_upstream":"blue"}' -l debug test=True

2.2 部署green节点

salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_web pillar='{"node_pcre":"gameplaza_web1(1|2|3|4|5|6|7|8|9)","env_id":"green","test":"True"}'

2.3 查看green节点部署是否成功

salt -E 'gameplaza_web1(1|2|3|4|5|6|7|8|9)' cmd.run ' initctl list |grep wxgameplaza_web'
```

```js
2.4 切换nginx到green节点。

salt -E 'nginx_backend.prod.gameplaza.yewu.js' state.sls WxGamePlaza.nginx.web.backend pillar='{"wxgameplaza_nginx_web_backend_active_upstream":"green"}' -l debug test=True

2.5 部署blue节点

salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_web pillar='{"node_pcre":"gameplaza_web0(1|2|3|4|5|6|7|8|9)","env_id":"blue","test":"True"}'


2.6 查看blue节点是否部署成功

salt -E 'gameplaza_web0(1|2|3|4|5|6|7|8|9)' cmd.run 'initctl list |grep wxgameplaza_web'

2.7 最后都成功了 需要切换nginx 到所有节点

salt -E 'nginx_backend.prod.gameplaza.yewu.js' state.sls WxGamePlaza.nginx.web.backend -l debug test=True

```
