# 网吧管家 游戏广场 handler 线上服务器


## 4 游戏广场handler详细部署步骤

执行顺序, 务必按照下面的顺序执行，否则会出问题。默认 都是 test=True 需要在生成环境中真实生效，需要修改 test 为 false 才会生效。切记。

```js
4.0 拉取gameplaza_handler包 

cd /root/dev/salt/deploy/;
./deploy.py  prod -c config.wxdesk.yaml -p wxgameplaza_handler;

4.1 nginx切换到blue节点，部署handler green节点 命令如下

salt -E "wx.pro.*" state.sls WxGamePlaza.nginx.handler.handler_backend pillar='{"blue_green":"blue"}' test=True

salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_handler pillar='{"node_pcre":"gameplaza_handler0(8|1|2)","test":"true"}'

这条命令是观察刚刚部署的handler节点的 进程是否正常启动了，

salt -E "gameplaza_handler0(8|1|2).*" cmd.run "initctl list | grep swgameplaza_handler"

如果进程id没有变化表示是OK的。

4.2 切换nginx切换到green节点，部署handler_blue节点

salt -E "wx.pro.*" state.sls WxGamePlaza.nginx.handler.handler_backend pillar='{"blue_green": "green"}' test=True

salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_handler pillar='{"node_pcre":"gameplaza_handler(06|09)","test":"true"}'

这条命令是观察刚刚部署的handler节点的 进程是否正常启动了，

salt -E "gameplaza_(handler(06|09)).*" cmd.run "initctl list | grep swgameplaza_handler"

多运行几次观察进程是否有变化

4.3 完成后，切换nginx到all节点

salt -E "wx.pro.*" state.sls WxGamePlaza.nginx.handler.handler_backend pillar='{"blue_green": "all"}' test=True
```