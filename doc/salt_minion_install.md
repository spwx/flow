# salt-ssh install 
yum install -y openssh-clients openssh-server
/bin/sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers

yum -y  install https://repo.saltstack.com/yum/redhat/salt-repo-2016.3-1.el6.noarch.rpm

# install salt-master
yum install salt*

config master state and pillar path and ext_pillar

edit /etc/salt/roster

salt-ssh \* test.ping

need update states/salt/salt-minion/install.sls

salt-ssh \* state.sls salt.salt-minion.install -l debug test=True

salt \* saltutil.refresh_pillar
