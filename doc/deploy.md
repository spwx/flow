# 网吧管家线上服务器自动化部署详细说明

## 1 背景
    由于网吧管家采用了类似微服务的线上架构设计，线上服务器是星状拓扑图。所以线上服务器众多，手动部署是非常麻烦的事情，
    所以采用了salt进行自动化部署，目前只有python相关的服务做了自动化服务，线上用go写的相关服务并没有做自动化部署，
    这个力争在转为之前完成自动化部署。

## 2 部署指南
使用salt进行自动化部署，原则只有两点如下：
1. 通过ftp拉取包到salt-master节点指定目录下。
2. 用salt-run 执行已经编排的服务脚本即可。

网吧管家IDC线上salt-master 是 172.30.236.102服务器。
salt-master脚本代码在 ```/root/dev/salt/flow/```采用git管理。
git用户名是： ```wxdesk```, 密码是 ```sicent110```

### 2.1 以部署管理后台为例讲解详细的部署步骤。

```js
cd /root/dev/salt/deploy
./deploy.py  prod -c config.wxdesk.yaml -p deskmanagement
上面的脚本执行拉取的是桌面管理后台的包

-p制定的参数来自
/root/dev/salt/deploy/config.wxdesk.yaml 文件 拉取制定的包
会在 /data/SicentTools/artifacts/files 生成相应的文件夹 下有一个 VERSION文件。

强烈建议使用下面的方式：
tmux a -t seanly
ctrl+b + 1 切换到1窗口
```

![窗口1样图](mdimages/1.jpg)

再次切换到 ```ctrl+b+2 ```切换到2窗口，运行部署脚本即可。tmux相关的内容切莫关闭。
比如 桌面管理后台 web服务，查看salt相关脚本得知。
执行部署脚本为 

```salt-run state.orch js.yewu.wxdesk.prod.deploy.WxDeskManagement_web pillar='{"test":"False"}'```

## 3 游戏广场web详细部署步骤
由于游戏广场web访问量，线上并发数最高值能够到2500上，所以必须采用蓝绿部署，防止业务被中断的问题。
所有脚本 test默认为True 防止粘贴后立即部署带来的失误问题。如果需要真正的执行部署，需要吧test 改为False

详细部署步骤如下：

1 部署游戏广场定时任务服务
```js
salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_celery pillar='{"test":"True"}'
```
2 部署游戏广场web后端服务。

```js
2.0 拉取gameplaza_web 包
cd /root/dev/salt/deploy/
./deploy.py  prod -c config.wxdesk.yaml -p wxgameplaza_web

2.1 切换nginx到blue节点
salt -E 'nginx_backend.prod.gameplaza.yewu.js' state.sls WxGamePlaza.nginx.web.backend pillar='{"wxgameplaza_nginx_web_backend_active_upstream":"blue"}' -l debug test=True

2.2 部署green节点
salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_web pillar='{"node_pcre":"gameplaza_web1(1|2|3|4|5|6|7)","env_id":"green","test":"True"}'

2.3 查看green节点部署是否成功
salt -E 'gameplaza_web1(1|2|3|4|5|6|7)' cmd.run ' initctl list |grep wxgameplaza_web'
```
如果服务显示如下图，表明执行成功了。
![部署逻辑](mdimages/2.jpg)

```js
2.4 切换nginx到green节点。
salt -E 'nginx_backend.prod.gameplaza.yewu.js' state.sls WxGamePlaza.nginx.web.backend pillar='{"wxgameplaza_nginx_web_backend_active_upstream":"green"}' -l debug test=True

2.5 部署blue节点
salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_web pillar='{"node_pcre":"gameplaza_web0(1|2|3|4|5|6|7)","env_id":"blue","test":"True"}'

2.6 查看blue节点是否部署成功
salt -E 'gameplaza_web0(1|2|3|4|5|6|7)' cmd.run 'initctl list |grep wxgameplaza_web'

2.7 最后都成功了 需要切换nginx 到所有节点

salt -E 'nginx_backend.prod.gameplaza.yewu.js' state.sls WxGamePlaza.nginx.web.backend -l debug test=True

```

3 部署游戏广场前端资源

```js
拉取游戏广场前端资源包
cd /root/dev/salt/deploy/
./deploy.py  prod -c config.wxdesk.yaml -p wxgameplaza_frontend

执行部署命令
salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_frontend pillar='{"test":"True"}'
```

## 4 游戏广场handler详细部署步骤

执行顺序, 务必按照下面的顺序执行，否则会出问题。默认 都是 test=True 需要在生成环境中真实生效，需要修改 test 为 false 才会生效。切记。
```js
4.0 拉取gameplaza_handler包 
cd /root/dev/salt/deploy/
./deploy.py  prod -c config.wxdesk.yaml -p wxgameplaza_handler

4.1 nginx切换到blue节点，部署handler green节点 命令如下
salt -E "wx.pro.*" state.sls WxGamePlaza.nginx.handler.handler_backend pillar='{"blue_green":"blue"}' test=True

salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_handler pillar='{"node_pcre":"gameplaza_handler0(8|1|2)","test":"true"}'

这条命令是观察刚刚部署的handler节点的 进程是否正常启动了，
salt -E "gameplaza_handler0(8|1|2).*" cmd.run "initctl list | grep swgameplaza_handler"
如果进程id没有变化表示是OK的。

4.2 切换nginx切换到green节点，部署handler_blue节点

salt -E "wx.pro.*" state.sls WxGamePlaza.nginx.handler.handler_backend pillar='{"blue_green": "green"}' test=True

salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_handler pillar='{"node_pcre":"gameplaza_handler(06|09)","test":"true"}'

这条命令是观察刚刚部署的handler节点的 进程是否正常启动了，

salt -E "gameplaza_(handler(06|09)).*" cmd.run "initctl list | grep swgameplaza_handler"
多运行几次观察进程是否有变化

4.3 完成后，切换nginx到all节点

salt -E "wx.pro.*" state.sls WxGamePlaza.nginx.handler.handler_backend pillar='{"blue_green": "all"}' test=True
```