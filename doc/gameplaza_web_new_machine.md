# 新机器上部署 gameplaza web 


* 1. 为 172.30.236.156, 172.30.236.157, 172.30.236.158, 172.30.236.159, 扩展磁盘, 数据盘挂载 /data下 安装salt-mion, 分别命名 gameplaza_web08.prod.gameplaza.yewu.js, gameplaza_web09.prod.gameplaza.yewu.js, gameplaza_web18.prod.gameplaza.yewu.js, gameplaza_web19.prod.gameplaza.yewu.js

    (master为236.102)

* 2. 分别为这4台机器进行系统初始化 (在236.102)


```
salt -E "gameplaza_web08.*" state.sls base test=False
salt -E "gameplaza_web09.*" state.sls base test=False
salt -E "gameplaza_web18.*" state.sls base test=False
salt -E "gameplaza_web19.*" state.sls base test=False

salt -E "gameplaza_web08.*" state.sls sysConfig test=False
salt -E "gameplaza_web09.*" state.sls sysConfig test=False
salt -E "gameplaza_web18.*" state.sls sysConfig test=False
salt -E "gameplaza_web19.*" state.sls sysConfig test=False
```

* 3. 安装python mongo mongos

```
salt -E "gameplaza_web08.*" state.sls mongo test=False
salt -E "gameplaza_web09.*" state.sls mongo test=False
salt -E "gameplaza_web18.*" state.sls mongo test=False
salt -E "gameplaza_web19.*" state.sls mongo test=False

salt -E "gameplaza_web08.*" state.sls mongos test=False
salt -E "gameplaza_web09.*" state.sls mongos test=False
salt -E "gameplaza_web18.*" state.sls mongos test=False
salt -E "gameplaza_web19.*" state.sls mongos test=False

salt -E "gameplaza_web08.*" state.sls python3 test=False
salt -E "gameplaza_web09.*" state.sls python3 test=False
salt -E "gameplaza_web18.*" state.sls python3 test=False
salt -E "gameplaza_web19.*" state.sls python3 test=False

```

* 4. 项目初始化

```
 salt-run state.orch js.yewu.wxdesk.prod.init.WxGamePlaza_web pillar='{"node_pcre":"gameplaza_web(18|19)","env_id":"green","test":"False"}'


 salt-run state.orch js.yewu.wxdesk.prod.init.WxGamePlaza_web pillar='{"node_pcre":"gameplaza_web(08|09)","env_id":"blue","test":"False"}'

```

* 5. 安装web服务

```
 salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_web pillar='{"node_pcre":"gameplaza_web(18|19)","env_id":"green","test":"False"}'

 salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_web pillar='{"node_pcre":"gameplaza_web(08|09)","env_id":"blue","test":"False"}'


验证各个机器上web进程是否正常

salt -E "gameplaza_web08.*" cmd.run "ps -ef|grep uwsgi"
salt -E "gameplaza_web09.*" cmd.run "ps -ef|grep uwsgi"
salt -E "gameplaza_web18.*" cmd.run "ps -ef|grep uwsgi"
salt -E "gameplaza_web19.*" cmd.run "ps -ef|grep uwsgi"

```


* 6. reloadnginx

```

salt -E 'nginx_backend.prod.gameplaza.yewu.js' state.sls WxGamePlaza.nginx.web.backend -l debug test=True

```
