# 网吧管家 游戏广场 frontend 线上服务器


# 1. 部署游戏广场前端资源

```js
拉取游戏广场前端资源包

cd /root/dev/salt/deploy/;
python deploy.py  prod -c config.wxdesk.yaml -p wxgameplaza_frontend

执行部署命令

salt-run state.orch js.yewu.wxdesk.prod.deploy.WxGamePlaza_frontend pillar='{"test":"True"}'

```